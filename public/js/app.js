var gm_authFailure;

(function () {
  'use strict';
  var appRun = [
    '$ionicPlatform',
    '$rootScope',
    '$ionicLoading',
    '$http',
    '$timeout',
    'Authentication',
    // 'miLogger',
    '$ionicPopup',
    '$ionicPopover',
    '$localStorage',
    '$state',
    '$q',
    'Job',
    function ($ionicPlatform, $rootScope, $ionicLoading, $http, $timeout, Authentication, /*miLogger,*/ $ionicPopup, $ionicPopover, $localStorage, $state, $q, Job) {
      var stateTimeout = {},
        googleApiKey = "AIzaSyD26vJwvZ2dL8MLPKs416Cf0HlcYiCtnxY",
        myStringify = function (x) {
          if (!x) {
            return x;
          }
          return JSON.stringify(x).replace(/"/g, "");
        },
        $mainContainer;

      $ionicPlatform.ready(function () {
        if (ionic.Platform.isIOS()) {//iOS doesn't handle :focus selectors correctly--they work when the app is first started but not after multiple pages with inputs have been navigated to
          $rootScope.iOSFocusElement = function (event) {//however, ng-focus and ng-blur work fine on iOS--so it's necessary to manage things in a clunkier way.
            if (event && event.target && event.target.className) {
              if (event.target.className.indexOf("has-focus") < 0) {
                event.target.className += ' has-focus';
              }
            }
          };
          $rootScope.iOSUnfocusElement = function (event) {
            if (event && event.target && event.target.className) {
              if (event.target.className.indexOf("has-focus") >= 0) {
                event.target.className = event.target.className.replace("has-focus", "");
              }
            }
          };
        } else {
          $rootScope.iOSFocusElement = function () {
          };

          $rootScope.iOSUnfocusElement = function () {
          };
        }

        try {
          Capacitor.Plugins.Keyboard.setAccessoryBarVisible({isVisible: true});
        } catch (ignore) {
          //console.log("Keyboard accessory bar not functioning properly.");
        }

        $rootScope.useBackUpAddresses = false;
        gm_authFailure = function () {
          console.log("Google autocomplete failed");
          $rootScope.useBackUpAddresses = true;
        };

        if (!$rootScope.userName) {
          $rootScope.userName = $localStorage.userName;
        }
      });
      /*jslint unparam: true*/
      $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
        console.log("stateChangeError", error);
        $ionicLoading.hide();
        $rootScope.cancelTimeout();
      });
      /*jslint unparam:false */
      $rootScope.lastStateChangeTime = 0;
      $rootScope.$on('$stateChangeStart', function (e, toState) {
        $rootScope.lastStateChangeTime = (new Date()).getTime();
        if ($rootScope.closePopup  && $rootScope.closePopup.close) {
          $rootScope.closePopup.close();
        }
        stateTimeout = $timeout(function () { //If transition takes longer than 5 seconds, timeout.
          $ionicLoading.hide();
          // $ionicPopup.alert({'title': 'Timed Out', 'template': 'Communication with the server timed out. Please check your connection and try again.'});
          angular.forEach($http.pendingRequests, function (req) {
            if (req.abort) {
              req.abort.resolve();
            }
          });
        }, 5000);
      });
      // $interval(function () {
      //   if ((new Date()).getTime() - $rootScope.lastStateChangeTime > 1800000) {//If no user activity has occurred in the last 30 minutes, turn on a screensaver.
      //     $rootScope.showScreensaver = true;
      //   }
      // }, 600000);
      // $interval(function () {
      //   if ($rootScope.showScreensaver) {
      //     $rootScope.screensaverImageLocation = {left: Math.round(Math.random() * 70) + "%", top: Math.round(Math.random() * 80) + "%"};
      //   }
      // }, 10000);
      $rootScope.hideScreensaver = function () {
        $rootScope.showScreensaver = false;
        $rootScope.lastStateChangeTime = (new Date()).getTime();
      };
      $rootScope.$on('$stateChangeSuccess', function (e, toState) {
        $rootScope.cancelTimeout();
      });
      $rootScope.hideKeyboard = function () {
        if (window.cordova && window.cordova.plugins.Keyboard) {
          window.cordova.plugins.Keyboard.close();
        }
      };
      $rootScope.cancelTimeout = function () {
        $timeout.cancel(stateTimeout);
      };
      $rootScope.closeLoading = function () {
        $ionicLoading.hide();
        angular.forEach($http.pendingRequests, function (req) {
          if (req.abort) {
            req.abort.resolve();
          }
        });
      };
      $rootScope.preLoginErrorMessage = function (messageTitle, err) {
        $timeout(function () {
          $ionicLoading.hide();
        }, 100);
        if (err && err.title && err.template) {
          $ionicPopup.alert(err);
        } else if (!err || !err.status || err.status < 1) {
          $ionicPopup.alert({title: 'Could not connect to server', template: 'Check your internet connection.'});
        } else {
          $ionicPopup.alert({
            title: messageTitle,
            template: myStringify(err.message || (err.data && err.data.message) || err.data || err)
          });
        }
      };
      $rootScope.internetIsOkay = function (err) {
        if (!$localStorage.token) {
          if (!$rootScope.onlyLogoutPopup) {
            $rootScope.onlyLogoutPopup = $ionicPopup.confirm({
              title: "Could not connect to server",
              template: "You need to sign in to your account.",
              okText: "LOG IN",
              okType: "right-button",
              cancelText: "CANCEL",
              cancelType: "left-button"
            });
            $rootScope.onlyLogoutPopup.then(function (res) {
              Authentication.logOut(res);
              delete $rootScope.onlyLogoutPopup;
            });
          }
          return 0;
        }
        if (!err || !err.status || err.status < 1) {
          $ionicPopup.alert({title: 'Could not connect to server', template: 'Check your internet connection.'});
          return 0;
        }
        return 1;
      };
      $rootScope.handleInternetErrorMessage = function (messageTitle, err) {
        var message,
          parsedJSON;
        $timeout(function () {
          $ionicLoading.hide();
        }, 100);
        if ($rootScope.internetIsOkay(err)) {
          message = myStringify(err.message || (err.data && err.data.message));
          if (!message && err.data) {
            try {
              parsedJSON = JSON.parse(err.data);
              message = myStringify(parsedJSON.message || parsedJSON);
            } catch (ignore) {
            }
          }
          if (!message) {
            message = myStringify(err.data || err);
          }
          if (((message.indexOf("ccount not found or ") > -1 || message.indexOf("ccount entry not found") > -1) && message.indexOf("disabled") > -1) ||
              (message.indexOf("Invalid account") > -1 && message.indexOf("not associated with company") > -1)) {
            message = 'Problem unknown. Contact Therma-Stor support if the following message doesn\'t make sense: <br><br>"' + message + '"';
          }
          $ionicPopup.alert({
            title: messageTitle,
            template: message
          });
        }
      };
      delete $rootScope.onlyLogoutPopup;
      $rootScope.checkThenSignOut = function () {
        if ($state.current.url !== '/welcome' && $state.current.url !== '/enterEmail' &&
            $state.current.url !== '/signUp' && $state.current.url !== '/signupWait' && $state.current.url !== '/login' && $state.current.url !== '/forgotPassword' && $state.current.url !== '/troubleshoot' &&
            $state.current.url !== '/dehuExplanation' && $state.current.url !== '/initialAddUsers') {
          if (!$rootScope.onlyLogoutPopup) {
            $rootScope.onlyLogoutPopup = $ionicPopup.confirm({
              title: "You are logged out",
              template: "Internet access and a user account are required to access this functionality. Do you want to log in?",
              okText: "LOG IN",
              okType: "right-button",
              cancelText: "CANCEL",
              cancelType: "left-button"
            });
            $rootScope.onlyLogoutPopup.then(function (res) {
              Authentication.logOut(res);
              delete $rootScope.onlyLogoutPopup;
            });
          }
        }
      };
      $rootScope.signOutPopover = function ($event) {
        var popoverPromise;
        popoverPromise = $ionicPopover.fromTemplateUrl('views/Main/accountPopover.html');
        popoverPromise.then(function (popover) {
          $rootScope.popover = popover;
          $rootScope.popover.show($event);
        });
      };
      $rootScope.goToStateFromPopover = function (destination) {
        if ($rootScope.popover) {
          $rootScope.popover.hide();
          $rootScope.popover = undefined;
        }
        $state.go(destination);
      };
      $rootScope.signOut = function () {
        if ($rootScope.popover) {
          $rootScope.popover.hide();
          $rootScope.popover = undefined;
        }
        Authentication.logOut(true);
        window.onerror = function () {};
      };
      $rootScope.reloadAutoComplete = function () {
        var newAutoComplete;

        try {
          if (google.maps.places.Autocomplete) {
            return;
          }
        } catch (e) {
          console.log("reloading autocomplete API");
        }

        newAutoComplete = document.createElement('script');
        newAutoComplete.type = "text/javascript";
        newAutoComplete.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyAwC3yLYo3LqAkk-EA6ZE6-k0F1xEbaBd4&libraries=places";
        document.head.appendChild(newAutoComplete);
      };
      $rootScope.openInNewWindow = function (url, silent) {
        if (silent || (silent === 0 && $localStorage.mapsInfoRead && $localStorage.userEmail && $localStorage.mapsInfoRead.indexOf($localStorage.userEmail) !== -1)) {
          window.open(url, '_system', 'location=yes');
        } else if (silent === 0) {
          $rootScope.pester = {silence: false};
          $ionicPopup.confirm({
            title: "External link",
            scope: $rootScope,
            template: '<p>Map will be opened in another window.</p>' +
              '<ion-checkbox checked="false" ng-model="$root.pester.silence">Don\'t show again</ion-checkbox>',
            okText: "OK",
            okType: "right-button",
            cancelText: "CANCEL",
            cancelType: "left-button"
          }).then(function (res) {
            if (res) {
              if (!$localStorage.mapsInfoRead) {
                $localStorage.mapsInfoRead = [];
              }
              if ($rootScope.pester.silence) {
                $localStorage.mapsInfoRead.push($localStorage.userEmail);
              }
              window.open(url, '_system', 'location=yes');
            }
          });
        } else {
          $ionicPopup.alert({'title': 'External link', 'template': 'Opening a separate browser window to go to ' + url}).then(function () {
            window.open(url, '_system', 'location=yes');
          });
        }
      };
      $rootScope.openExternalMap = function (latitude, longitude) {
        if (latitude || longitude) {
          if (ionic.Platform.isAndroid()) {
            $rootScope.openInNewWindow("geo:0,0?q=" + latitude + "," + longitude, 0);
          } else if (ionic.Platform.isIOS()) {
            $rootScope.openInNewWindow("maps://?q=" + latitude + "," + longitude, 0);
          } else {
            $rootScope.openInNewWindow("https://maps.google.com/?q=" + latitude + "," + longitude, 0);
          }
        }
      };
      $rootScope.addressLocations = {};
      $rootScope.openExternalAddressedMap = function (address) {
        if (ionic.Platform.isAndroid()) {
          $rootScope.openInNewWindow("geo:0,0?q=" + encodeURIComponent(address), 0);
        } else if (ionic.Platform.isIOS()) {
          $rootScope.openInNewWindow("maps://?q=" + encodeURIComponent(address), 0);
        } else {
          $rootScope.openInNewWindow("https://maps.google.com/?q=" + encodeURIComponent(address), 0);
        }
      };
      $rootScope.mapsAreCancelled = 0;
      $rootScope.cancelMapRendering = function () {//Cancel rendering new maps for at least the next 4 seconds.
        $rootScope.mapsAreCancelled = (new Date()).getTime();
        setTimeout(function () {
          if ((new Date()).getTime() - $rootScope.mapsAreCancelled > 3000) {
            $rootScope.mapsAreCancelled = 0;
          }
        }, 4000);
      };
      $rootScope.uncancelMapRendering = function () {
        $rootScope.mapsAreCancelled = 0;
      };
      $rootScope.renderMap = function (mapId, address) {
        var myElement = mapId && document.getElementById(mapId),
          deferredPromise = $q.defer();

        if (myElement) {
          if (myElement.className && myElement.className.indexOf("rendered") > -1) {//Do not redraw maps that are already there, to minimize Google API calls.
            if ($rootScope.mapsAreCancelled) {
              deferredPromise.reject();
            } else {
              deferredPromise.resolve();
            }
          } else {
            myElement.className = myElement.className + " rendered";
            if (address) {
              myElement.src = "https://www.google.com/maps/embed/v1/place?zoom=13&key=" + googleApiKey + "&q=" + encodeURIComponent(address);
            } else {
              myElement.src = "https://www.google.com/maps/embed/v1/place?zoom=13&key=" + googleApiKey + "&q=" + $localStorage.location.latitude + "," + localStorage.location.longitude;
            }

            setTimeout(function () {
              if ($rootScope.mapsAreCancelled) {
                deferredPromise.reject();
              } else {
                deferredPromise.resolve();
              }
            }, 200);
          }
        } else {
          if ($rootScope.mapsAreCancelled) {
            deferredPromise.reject();
          } else {
            deferredPromise.resolve();
          }
        }
        return deferredPromise.promise;
      };
    }],
    appResolve = {
      // authUser: ['Authentication', '$state', '$q',
      //   function (Authentication, $state, $q) {
      //     return Authentication.login()
      //       .then($q.when, function (result) {
      //         console.log(result);
      //         return $state.go('login');
      //       });
      //   }]
    },
    appConfig = ['$stateProvider', '$urlRouterProvider', '$httpProvider', '$ionicLoadingConfig', '$ionicConfigProvider', 'ionicDatePickerProvider', '$compileProvider',
      function ($stateProvider, $urlRouterProvider, $httpProvider, $ionicLoadingConfig, $ionicConfigProvider, ionicDatePickerProvider, $compileProvider) {
        $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|capacitor):/);
        $ionicConfigProvider.tabs.position('bottom');
        $ionicConfigProvider.tabs.style('standard');
        $ionicConfigProvider.views.swipeBackEnabled(false);//Prevents white screen on iOS when swiping from the left side of the screen.
        $stateProvider
          .state('app', {
            url: "/app",
            abstract: true,
            templateUrl: "templates/menu.html",
            controller: "appController as appVM",
            resolve: appResolve
          });
        //If none of the above states are matched, use this as the fallback.
        $urlRouterProvider.otherwise('/login');
        /// date picker options & config
        var datePickerObj = {
          inputDate: new Date(),
          titleLabel: 'Select a Date',
          setLabel: 'SET',
          closeLabel: 'CLOSE',
          mondayFirst: false,
          weeksList: ["S", "M", "T", "W", "T", "F", "S"],
          monthsList: ["Jan", "Feb", "Mar", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"],
          templateType: 'popup',
          from: new Date(2017, 7, 13),
          to: new Date(),
          showTodayButton: false,
          dateFormat: 'dd MMMM yyyy',
          closeOnSelect: false,
          disableWeekdays: []
        };
        ionicDatePickerProvider.configDatePicker(datePickerObj);
        //$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|content|sms|tel|file):/);
        $httpProvider.interceptors.push(['$q', '$injector', function ($q, $injector) {
          return {
            request: function (config) {
              //Add an http aborter.
              var Authentication,
                $rootScope = $injector.get('$rootScope'),
                token,
                // oldTokenLoc,
                // endTokenLoc,
                // afterToken,
                abort = $q.defer();
              config.abort = abort;
              config.timeout = abort.promise;

              if (config.url.indexOf('.html') === -1 && config.url.indexOf('apikey') === -1 && config.headers["Content-Type"] !== "application/x-www-form-urlencoded") {
                //Add token onto end of request.
                Authentication = $injector.get('Authentication');
                token = Authentication.getAccessToken();
                if (!token) {//Don't send a request to server if it has no token.
                  $rootScope.checkThenSignOut();
                  return $q.reject("Did not send a request to " + config.url + " because no token was found.");
                }
                config.headers['X-Auth-Token'] = token;
                // oldTokenLoc = config.url.indexOf('accesstoken=');
                // if (oldTokenLoc !== -1) {
                //   //If there's already a token in the url (because the packet is being re-tried),
                //   //replace it with the new one rather than tacking another on at the end.
                //   endTokenLoc = config.url.indexOf('&', oldTokenLoc);
                //   if (endTokenLoc === -1) {
                //     afterToken = "";
                //   } else {
                //     afterToken = config.url.slice(endTokenLoc);
                //   }
                //   config.url = config.url.slice(0, oldTokenLoc + 12) + token + afterToken;
                //   return config;
                // }
                // if (config.url.indexOf('?') === -1) {
                //   config.url += "?";
                // } else {
                //   config.url += "&";
                // }
                // config.url += "accesstoken=" + encodeURIComponent(token);
              }
              return config;
            },

            responseError: function (response) {
              var Authentication = $injector.get('Authentication'),
                $rootScope = $injector.get('$rootScope');
              if (response.status === 401) {
                console.log("401!");
                if (response.config.url.indexOf('oauth2/token') !== -1) {
                  console.log("invalid credentials " + response.config.url + " ... " + response.config.data.grant_type);
                  //If refresh token isn't being accepted, then token should be invalidated.
                  $rootScope.checkThenSignOut();
                  return $q.reject(response);
                }
                Authentication.invalidateToken();//Invalidate access token;
                return Authentication.login(response.data).then(function () {
                  var $http = $injector.get('$http');
                  console.log("retrying", response.config);
                  return $http(response.config);//Send the last http request again after updating token.
                }, $q.reject);
              }
              if (response.status === -1) {
                console.log("-1 error: " + JSON.stringify(response));
              } else {
                console.log("other error: " + response.status);
              }
              return $q.reject(response);
            }
          };
        }]);
        //Customize ionicLoading, add cancel button.
        $ionicLoadingConfig.template = '<ion-spinner class="spinner-assertive huge"></ion-spinner><h3>Loading...</h3><button class="button button-block button-transparent icon icon-right ion-close-circled" ng-click="closeLoading()">Cancel</button>';
      }],
    // appCtrl = [
    //   'Authentication',
    //   'miLogger',
    //   function (Authentication, miLogger) {
    //     miLogger.setDefaultPayload({'accessToken': Authentication.getAccessToken()});
    //   }];
    appCtrl = [function () {}];
  angular.module('User', []);
  angular.module('Dehumidifier', []);
  angular.module('ThermaStor', [
    'ionic',
    'ngStorage',
    // 'miAnalytics',
    'ionic-datepicker',
    'User',
    'Dehumidifier'
  ])
    .config(appConfig)
    .controller('appController', appCtrl)
    .run(appRun)
    .value('Integration_Base', 'https://integrationservicep.azurewebsites.net/v1/')
    .value('API_Base', 'https://restoreservice.thermastor.com/v1/')//'https://restoreservicep.azurewebsites.net/v1/' or 'https://rservice.azurewebsites.net/v1/' or 'https://tsiot.thermastor.com:8443/' or 'https://restorestaging.azurewebsites.net/v1/')
    .directive('ngEnter', function () {//From https://gist.github.com/EpokK/5884263
      return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
          if (event.which === 13) {
            scope.$apply(function () {
              scope.$eval(attrs.ngEnter);
            });

            event.preventDefault();
          }
        });
      };
    })
    .directive('expanding', ['$timeout', function ($timeout) {//https://stackoverflow.com/questions/17772260/textarea-auto-height
      return {
        restrict: 'A',
        link: function ($scope, element) {
          var resize = function () {
            if (!element[0].style.height || (element[0].scrollHeight > $scope.lastTextAreaHeight)) {
              element[0].style.height = (element[0].scrollHeight + 6) + "px";
            }
            $scope.lastTextAreaHeight = element[0].scrollHeight;
          };
          $scope.lastTextAreaHeight = element[0].style.height;
          element.on("input change", resize);
          $timeout(resize, 0);
        }
      };
    }]);
}());