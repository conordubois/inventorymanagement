(function () {
  'use strict';
  var jobFactory = ['$http', '$q', 'API_Base', 'Integration_Base', '$rootScope', '$ionicLoading', '$timeout', '$localStorage', 'Snackbar', 'Dehumidifier', function ($http, $q, API_Base, Integration_Base, $rootScope, $ionicLoading, $timeout, $localStorage, Snackbar, Dehumidifier) {
    var Job = {},
      locationFields = ['name', 'address', 'city', 'state', 'zip'],
      requiredFields = ['jobName', 'startDateSecs', 'address', 'zip'],
      validFields = requiredFields.concat(['city', 'state', 'insuranceName', 'claimNumber', 'description', 'endDateSecs']),
      validSizeFields = ['ceilingSqFt', 'wallSqFt', 'floorSqFt', 'ceilingWetPercent', 'wallWetPercent', 'floorWetPercent', 'cubicFt'],
      july2017Cutoff = 1500000000,
      getTimeString = function (timeSecs) {
        if (!timeSecs || timeSecs < july2017Cutoff) {
          return '---';
        }

        var aDate = new Date(timeSecs * 1000),
          time = aDate.toLocaleTimeString();

        time = time.replace(/\u200E/g, '');//from https://stackoverflow.com/questions/17913681
        time = time.replace(/^([^\d]*\d{1,2}:\d{1,2}):\d{1,2}([^\d]*)$/, '$1$2');
        return time;
      },
      formatLocation = function (location) {
        var i,
          payload = {},
          urlEncoded = [];

        payload = angular.copy(location);

        for (i in payload) {
          if (payload.hasOwnProperty(i) && locationFields.indexOf(i) === -1) {
            delete payload[i];
          }
        }
        // for (i = 0; i < locationFields.length; i += 1) {
        //   if (!payload.hasOwnProperty(locationFields[i])) {
        //     return $q.reject({title: "Error updating job", template: locationFields[i] + " is a required field"});
        //   }
        // }
        for (i in payload) {
          if (payload.hasOwnProperty(i)) {
            urlEncoded.push(encodeURIComponent(i) + "=" + encodeURIComponent(payload[i]));
          }
        }

        if (urlEncoded.length) {
          return $q.when("?" + urlEncoded.join("&"));
        }
        return $q.when("");
      },
      formatJobBasics = function (jobBasics) {
        var i, payload;

        payload = angular.copy(jobBasics);

        for (i in payload) {
          if (payload.hasOwnProperty(i) && validFields.indexOf(i) === -1) {
            delete payload[i];
          }
        }
        for (i = 0; i < requiredFields.length; i += 1) {
          if (!payload.hasOwnProperty(requiredFields[i])) {
            return $q.reject({title: "Error updating job", template: requiredFields[i] + " is a required field"});
          }
        }
        return $q.when(payload);
      },
      formatChamberSizes = function (chamber) {
        var i,
          payload = {},
          urlEncoded = [];

        for (i in chamber) {
          if (chamber.hasOwnProperty(i) && validSizeFields.indexOf(i) !== -1 && !isNaN(chamber[i])) {
            payload[i] = chamber[i];
          }
        }
        for (i in payload) {
          if (payload.hasOwnProperty(i)) {
            urlEncoded.push(encodeURIComponent(i) + "=" + encodeURIComponent(payload[i]));
          }
        }

        if (urlEncoded.length) {
          return $q.when("?" + urlEncoded.join("&"));
        }
        return $q.when();
      },
      formatChamber = function (chamber, useUrlEncoding, includeSizes) {
        var i,
          payload = {},
          urlEncoded = [];

        if (chamber && chamber.chamberName) {
          payload.chamberName = chamber.chamberName;
        } else {
          payload.chamberName = "New Chamber";
        }
        if (chamber && chamber.category) {
          payload.category = chamber.category;
        } else {
          payload.category = "1";
        }
        if (chamber && chamber.classification) {
          if (useUrlEncoding) {
            payload.class = chamber.classification;//Diane calls this variable "class" sometimes and other times "classification".
          } else {
            payload.classification = chamber.classification;
          }
        } else {
          if (useUrlEncoding) {
            payload.class = "1";
          } else {
            payload.classification = "1";
          }
        }

        if (!useUrlEncoding) {
          return $q.when(payload);
        }

        for (i in payload) {
          if (payload.hasOwnProperty(i)) {
            urlEncoded.push(encodeURIComponent(i) + "=" + encodeURIComponent(payload[i]));
          }
        }

        if (includeSizes) {
          return formatChamberSizes(chamber).then(function (formattedSizes) {
            if (formattedSizes && formattedSizes.length) {
              return $q.when(formattedSizes + '&' + urlEncoded.join("&"));
            }
            return $q.when("?" + urlEncoded.join("&"));
          });
        }
        return $q.when("?" + urlEncoded.join("&"));
      },
      formatContact = function (contact, useUrlEncoding) {
        var i,
          payload = {},
          urlEncoded = [];

        if (contact && contact.contactName) {
          payload.contactName = contact.contactName;
        } else {
          payload.contactName = "";
        }
        if (useUrlEncoding) {
          payload.name = payload.contactName;//Diane calls this variable "name" sometimes and other times "contactName".
          delete payload.contactName;
        }
        if (contact && contact.phone) {
          payload.phone = contact.phone;
        } else {
          payload.phone = "";
        }

        if (!useUrlEncoding) {
          return $q.when(payload);
        }

        for (i in payload) {
          if (payload.hasOwnProperty(i)) {
            urlEncoded.push(encodeURIComponent(i) + "=" + encodeURIComponent(payload[i]));
          }
        }

        return $q.when("?" + urlEncoded.join("&"));
      },
      formatCompanyEquipment = function (equipment, useUrlEncoding) {
        var i,
          payload = {},
          urlEncoded = [];

        equipment = equipment || {};
        if (equipment.fullName) {
          payload.typeName = equipment.fullName;
        }
        if (equipment.equipmentName) {
          payload.equipName = equipment.equipmentName;
        }
        if (equipment.manufacturer) {
          payload.manufacturer = equipment.manufacturer;
        }
        if (equipment.model) {
          payload.model = equipment.model;
        }
        if (equipment.serialNumber) {
          payload.serialNumber = equipment.serialNumber;
        }

        if (!useUrlEncoding) {
          return $q.when(payload);
        }

        for (i in payload) {
          if (payload.hasOwnProperty(i)) {
            urlEncoded.push(encodeURIComponent(i) + "=" + encodeURIComponent(payload[i]));
          }
        }

        return $q.when("?" + urlEncoded.join("&"));
      },
      formatEquipment2 = function (equipment) {
        var i,
          payload = {},
          urlEncoded = [];

        if (equipment.databaseId > -1) {
          payload.dehuId = equipment.databaseId;
        }
        if (equipment.msensorId > -1) {
          payload.msensorId = equipment.msensorId;
          if (equipment.location) {
            payload.location = equipment.location;
          }
          if (equipment.material) {
            payload.material = equipment.material;
          }
          if (equipment.target) {
            payload.target = equipment.target;
          }
        }
        if (equipment.cequipId > -1) {
          payload.cequipId = equipment.cequipId;
        }

        for (i in payload) {
          if (payload.hasOwnProperty(i)) {
            urlEncoded.push(encodeURIComponent(i) + "=" + encodeURIComponent(payload[i]));
          }
        }

        if (urlEncoded.length) {
          return $q.when("?" + urlEncoded.join("&"));
        }
        return $q.when("");
      },
      formatEquipment = function (equipment, suppressRetro) {
        var i,
          payload = {},
          urlEncoded = [];

        //Diane has different names for the put and post fields of equipment versus the get fields, for whatever unfathomable reason.
        if (equipment.equipmentType) {
          payload.equipType = equipment.equipmentType;
        } else if (!suppressRetro) {
          payload.equipType = 'retrodehu';
        }
        if (equipment.equipmentName) {
          payload.equipName = equipment.equipmentName;
        }
        if (equipment.exSize) {
          payload.equipSize = equipment.exSize;
        }
        if (equipment.location) {
          payload.location = equipment.location;
        } else {
          payload.location = "";
        }
        if (!isNaN(equipment.dehuId)) {
          payload.dehuId = equipment.dehuId;
        }

        for (i in payload) {
          if (payload.hasOwnProperty(i)) {
            urlEncoded.push(encodeURIComponent(i) + "=" + encodeURIComponent(payload[i]));
          }
        }

        if (urlEncoded.length) {
          return $q.when("?" + urlEncoded.join("&"));
        }
        return $q.when("");
      },
      formatArea = function (area) {
        var i,
          payload = {},
          urlEncoded = [];

        payload.areaType = area.measurementType;
        payload.areaName = area.editName;
        payload.jobId = area.jobId;

        for (i in payload) {
          if (payload.hasOwnProperty(i)) {
            urlEncoded.push(encodeURIComponent(i) + "=" + encodeURIComponent(payload[i]));
          }
        }

        if (urlEncoded.length) {
          return $q.when("?" + urlEncoded.join("&"));
        }
        return $q.when("");
      },
      formatMeasurement = function (measurement) {
        var i,
          payload = {},
          urlEncoded = [];

        if (measurement.measurementType) {
          payload.measuretype = measurement.measurementType;
        }
        payload.rh = measurement.editRh;
        payload.temperature = measurement.editTemp;
        payload.sampleSecs = measurement.sampleSecs;
        payload.visitId = measurement.visitId;

        for (i in payload) {
          if (payload.hasOwnProperty(i)) {
            urlEncoded.push(encodeURIComponent(i) + "=" + encodeURIComponent(payload[i]));
          }
        }

        if (urlEncoded.length) {
          return $q.when("?" + urlEncoded.join("&"));
        }
        return $q.when("");
      },
      formatMoisture = function (measurement) {
        var i,
          payload = {},
          urlEncoded = [];

        payload.moisture = measurement.editMoisture;
        payload.sampleSecs = measurement.sampleSecs;
        payload.visitId = measurement.visitId;

        for (i in payload) {
          if (payload.hasOwnProperty(i)) {
            urlEncoded.push(encodeURIComponent(i) + "=" + encodeURIComponent(payload[i]));
          }
        }

        if (urlEncoded.length) {
          return $q.when("?" + urlEncoded.join("&"));
        }
        return $q.when("");
      },
      formatMoisturePoint = function (measurement) {
        var i,
          payload = {},
          urlEncoded = [];

        payload.location = measurement.editLocation;
        payload.material = measurement.editMaterial;
        payload.target = measurement.editTarget;

        for (i in payload) {
          if (payload.hasOwnProperty(i)) {
            urlEncoded.push(encodeURIComponent(i) + "=" + encodeURIComponent(payload[i]));
          }
        }

        if (urlEncoded.length) {
          return $q.when("?" + urlEncoded.join("&"));
        }
        return $q.when("");
      },
      createOrUpdateReading = function (measurement) {
        // if (measurement && measurement.jobMeasureId) {
        //   return formatMeasurement(measurement).then(function (formattedMeasure) {
        //     return $http.put(API_Base + 'measure/' + measurement.jobMeasureId + formattedMeasure).then(function (response) {
        //       return response.data;
        //     }, $q.reject);
        //   });
        // }
        if (measurement && measurement.measurementType) {
          return Job.useOldVisitOrStartNew(measurement.jobId, measurement.sampleSecs, measurement.visitId).then(function (visitId) {
            measurement.visitId = visitId;

            if (measurement.measurementType === 'HVAC' && measurement.chamberId > -1) {
              delete measurement.measurementType;
              return formatMeasurement(measurement).then(function (formattedMeasure) {
                return $http.post(API_Base + 'chamber/' + measurement.chamberId + '/HVAC' + formattedMeasure).then(function (response) {
                  Snackbar.show({template: "Changes saved", hide: 1500});
                  response.data = response.data || {};
                  response.data.visitId = visitId;
                  return response.data;
                }, $q.reject);
              });
            }
            if (measurement.measurementType === 'outside' || measurement.measurementType === 'unaffected' || measurement.measurementType === 'HVAC') {
              if (isNaN(measurement.areaId)) {
                return $q.reject({message: "Could not create measurement for unknown area.", status: 400});
              }
              return formatMeasurement(measurement).then(function (formattedMeasure) {
                return $http.post(API_Base.replace(/v1/g, 'v2') + 'area/' + measurement.areaId + '/measure' + formattedMeasure).then(function (response) {
                  Snackbar.show({template: "Changes saved", hide: 1500});
                  response.data = response.data || {};
                  response.data.visitId = visitId;
                  return response.data;
                }, $q.reject);
              });
            }
            if (measurement.measurementType === 'affected') {
              if (isNaN(measurement.chamberId)) {
                return $q.reject({message: "Could not create measurement for unknown equipment.", status: 400});
              }
              return formatMeasurement(measurement).then(function (formattedMeasure) {
                return $http.post(API_Base.replace(/v1/g, 'v2') + 'chamber/' + measurement.chamberId + '/affected' + formattedMeasure).then(function (response) {
                  Snackbar.show({template: "Changes saved", hide: 1500});
                  response.data = response.data || {};
                  response.data.visitId = visitId;
                  return response.data;
                }, $q.reject);
              });
            }
            if (measurement.measurementType === 'dehu_out' || measurement.measurementType === 'other_air') {
              if (isNaN(measurement.equipmentId)) {
                return $q.reject({message: "Could not create measurement for unknown equipment.", status: 400});
              }
              return formatMeasurement(measurement).then(function (formattedMeasure) {
                return $http.post(API_Base.replace(/v1/g, 'v2') + 'equipment/' + measurement.equipmentId + '/measure' + formattedMeasure).then(function (response) {
                  Snackbar.show({template: "Changes saved", hide: 1500});
                  response.data = response.data || {};
                  response.data.visitId = visitId;
                  return response.data;
                }, $q.reject);
              });
            }
            return $q.reject({message: "Could not create measurement with unknown type.", status: 400});
          }, $q.reject);
        }
        return $q.reject({message: "Could not create measurement with empty entry.", status: 400});
      },
      cleanJobOverview = function (thisJob) {
        var unseenAlertCount = 0,
          i;

        thisJob.mapId = "map-" + thisJob.jobId;
        if (thisJob.endDateSecs && thisJob.endDateSecs >= thisJob.startDateSecs) {
          thisJob.status = "COMPLETE";
          thisJob.endDate = (new Date(thisJob.endDateSecs * 1000)).toLocaleDateString();
        } else {
          thisJob.status = "ACTIVE";
          thisJob.endDate = "Current";
        }
        thisJob.startDate = (new Date(thisJob.startDateSecs * 1000)).toLocaleDateString();
        if (thisJob.city) {
          thisJob.address2 = thisJob.city;
        }
        if (thisJob.state) {
          if (thisJob.address2) {
            thisJob.address2 += ", ";
          } else {
            thisJob.address2 = "";
          }
          thisJob.address2 += thisJob.state;
        }
        if (thisJob.zip) {
          if (thisJob.address2) {
            thisJob.address2 += " " + thisJob.zip;
          } else {
            thisJob.address2 = thisJob.zip;
          }
        }
        if (!$localStorage.seenJobAlertIds || !$localStorage.seenJobAlertIds.length) {
          $localStorage.seenJobAlertIds = [];
        }
        if (thisJob.activeAlerts && thisJob.activeAlerts.length) {
          for (i = 0; i < thisJob.activeAlerts.length; i += 1) {
            if (thisJob.activeAlerts[i] && $localStorage.seenJobAlertIds.indexOf(thisJob.activeAlerts[i]) < 0) {
              unseenAlertCount += 1;
            }
          }
        }
        if (unseenAlertCount > 99) {
          unseenAlertCount = 99;
        } else if (unseenAlertCount < 0) {
          unseenAlertCount = 0;
        }
        thisJob.unseenAlertCount = unseenAlertCount;
      },
      determineEquipmentType = function (thisEquipment) {
        var aDevice;
        if (!thisEquipment) {
          return {};
        }

        if (thisEquipment.dehuId > -1) {
          aDevice = $rootScope.searchBluetoothListsForMatch('databaseId', thisEquipment.dehuId);
          if (aDevice && aDevice.modelType) {
            thisEquipment.modelType = aDevice.modelType;
          } else {
            thisEquipment.modelType = thisEquipment.model;
          }
        } else if (thisEquipment.model && thisEquipment.model.indexOf('Dry Max') > -1) {
          thisEquipment.modelType = 'Unknown';
          thisEquipment.typeName = 'Unknown';
        } else {
          thisEquipment.modelType = thisEquipment.model;
        }
        return Dehumidifier.determineIconCategory(thisEquipment);
      },
      markExtraneousEquipment = function (equipmentList) {
        var i, j;
        if (equipmentList && equipmentList.length) {//Multiple equipment id's can be generated for a single dehumidifier if the user removes and re-adds them to a job.
          for (i = 0; i < equipmentList.length; i += 1) {//Mark the older ones as extraneous so that they can be hidden on the job equipment page.
            if (equipmentList[i]) {
              equipmentList[i].isExtraneous = false;
              if (equipmentList[i].msensorId && equipmentList[i].msensorId > -1) {
                for (j = 0; j < equipmentList.length; j += 1) {//Merge together equipment lists from the atmospheric-based and moisture-based lists that the server provides.
                  if (i !== j && equipmentList[j] && equipmentList[j].msensorId === equipmentList[i].msensorId) {
                    if (!(equipmentList[j].equipmentId > 0) && equipmentList[j].equipmentId > 0 && equipmentList[j].equipmentName) {
                      equipmentList[i].equipmentName = equipmentList[j].equipmentName;
                    }
                    if (!equipmentList[j].isRemoved && equipmentList[j].mequipId > 0 && !(equipmentList[i].mequipId > 0)) {
                      equipmentList[i].mequipId = equipmentList[j].mequipId;
                    }
                    if ((equipmentList[i].isRemoved && (!equipmentList[j].isRemoved || (equipmentList[j].equipmentId > 0 && !(equipmentList[i].equipmentId > 0) && !equipmentList[j].isExtraneous) ||
                         equipmentList[j].jobDayEnd > equipmentList[i].jobDayEnd || (equipmentList[j].jobDayEnd === equipmentList[i].jobDayEnd && !equipmentList[j].isExtraneous))) ||
                        (!equipmentList[i].isRemoved && !equipmentList[j].isRemoved && equipmentList[j].equipmentId > 0 && (!(equipmentList[i].equipmentId > 0) || !equipmentList[j].isExtraneous))) {
                      equipmentList[i].isExtraneous = true;
                      break;
                    }
                  }
                }
              } else if (equipmentList[i].isRemoved && (equipmentList[i].dehuId || (equipmentList[i].equipmentData && equipmentList[i].equipmentData.dehuId))) {
                for (j = 0; j < equipmentList.length; j += 1) {
                  if (i !== j && equipmentList[j] && ((equipmentList[j].equipmentData && equipmentList[i].equipmentData.dehuId === equipmentList[j].equipmentData.dehuId &&
                      (equipmentList[i].equipmentData.dehuId >= 0  || (equipmentList[i].equipmentData.equipmentType === equipmentList[j].equipmentData.equipmentType &&
                        equipmentList[i].equipmentData.equipmentName === equipmentList[j].equipmentData.equipmentName)) &&
                      (!equipmentList[j].isRemoved || equipmentList[j].equipmentData.jobDayEnd > equipmentList[i].equipmentData.jobDayEnd ||
                       (equipmentList[j].equipmentData.jobDayEnd === equipmentList[i].equipmentData.jobDayEnd && !equipmentList[j].isExtraneous))) ||
                      (equipmentList[j].dehuId && equipmentList[j].dehuId === equipmentList[i].dehuId && (equipmentList[i].dehuId >= 0 ||
                       (equipmentList[i].cequipId >= 0 && equipmentList[i].cequipId === equipmentList[j].cequipId) || ((!equipmentList[i].cequipId || equipmentList[i].cequipId < 0) &&
                          (equipmentList[i].modelType === equipmentList[j].modelType && equipmentList[i].equipmentName === equipmentList[j].equipmentName))) &&
                      (!equipmentList[j].isRemoved || equipmentList[j].jobDayEnd > equipmentList[i].jobDayEnd ||
                          (equipmentList[j].jobDayEnd === equipmentList[i].jobDayEnd && !equipmentList[j].isExtraneous))))) {
                    equipmentList[i].isExtraneous = true;
                    break;
                  }
                }
              }
            }
          }
        }
      };

    Job.convertArea = function (area, metricToImperial) {
      if (!area || isNaN(area)) {
        return area;
      }
      if (area < 0) {
        return 0;
      }
      if (!$localStorage.useMeters) {
        return Math.round(area);
      }
      if (!metricToImperial) {
        return Math.round(area * 0.092903);
      }
      return Math.round(area / 0.092903);
    };

    Job.convertVolume = function (volume, metricToImperial) {
      if (!volume || isNaN(volume)) {
        return volume;
      }
      if (volume < 0) {
        return 0;
      }
      if (!$localStorage.useMeters) {
        return Math.round(volume);
      }
      if (!metricToImperial) {
        return Math.round(volume * 0.0283168);
      }
      return Math.round(volume / 0.0283168);
    };

    Job.updateProgressBar = function (anEntry) {//Update the progress bar associated with moisture readings on the drying log page.
      if (!anEntry) {
        return {};
      }

      if (anEntry.initialMoisture > anEntry.target) {
        anEntry.percentDone = 100 * (anEntry.initialMoisture - anEntry.moistureValue) / (anEntry.initialMoisture - anEntry.target);
      } else if (anEntry.moistureValue > anEntry.target) {
        anEntry.percentDone = 5;
      } else {
        anEntry.percentDone = 100;
      }
      if (anEntry.percentDone < 5) {
        anEntry.percentDone = 5;
      } else if (anEntry.percentDone > 100) {
        anEntry.percentDone = 100;
      }
      anEntry.isGreen = false;
      anEntry.isYellow = false;
      anEntry.isOrange = false;
      anEntry.isRed = false;
      if (anEntry.percentDone >= 100) {
        anEntry.isGreen = true;
      } else if (anEntry.percentDone >= 66) {
        anEntry.isYellow = true;
      } else if (anEntry.percentDone >= 33) {
        anEntry.isOrange = true;
      } else {
        anEntry.isRed = true;
      }
      return anEntry;
    };

    Job.updateOtherTargets = function (newTarget, chamberId, material, location) {
      //All moisture points with the same material (and location?) in a given chamber will have the same moisture target.
      //If one get updated, then they all need to be updated.
      var i;

      if (isNaN(newTarget) || isNaN(chamberId) || !material || !location) {
        return;
      }
      if ($rootScope.latestMoisturePoints && $rootScope.latestMoisturePoints.length) {
        for (i = 0; i < $rootScope.latestMoisturePoints.length; i += 1) {
          if ($rootScope.latestMoisturePoints[i] && $rootScope.latestMoisturePoints[i].chamberId === chamberId &&
              $rootScope.latestMoisturePoints[i].material === material) {// && $rootScope.latestMoisturePoints[i].location === location) {
            $rootScope.latestMoisturePoints[i].target = newTarget;
          }
        }
      }
      if ($rootScope.latestLogEntry && $rootScope.latestLogEntry.moistureMeasures && $rootScope.latestLogEntry.moistureMeasures.length) {
        for (i = 0; i < $rootScope.latestLogEntry.moistureMeasures.length; i += 1) {
          if ($rootScope.latestLogEntry.moistureMeasures[i] && $rootScope.latestLogEntry.moistureMeasures[i].chamberId === chamberId &&
              $rootScope.latestLogEntry.moistureMeasures[i].material === material) {// && $rootScope.latestLogEntry.moistureMeasures[i].location === location) {
            $rootScope.latestLogEntry.moistureMeasures[i].target = newTarget;
          }
        }
      }
    };

    Job.minimumDateSecs = july2017Cutoff;//Only dates after July 2017 will be allowed for jobs and drying log entries.

    Job.getJob2 = function (jobId) {
      if (!jobId) {
        return $q.reject({message: "No job ID found.", status: 400});
      }

      // Job.getJob(jobId).then(function (response) {
      //   console.log("here's the original", response);
      // });
      return $http.get(API_Base.replace(/v1/g, 'v2') + 'job/' + jobId).then(function (response) {
        var i, j, graphable,
          thisEquipment,
          allData,
          anEntry,
          entryTime,
          chamberLookup = {},
          dehuLookup = {},
          equipmentLookup = {},
          areaLookup = {},
          unaffectedList = [],
          moistureLookup = {},
          aChamber,
          abbreviatedDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

        allData = (response && response.data) || {};
        if (allData.jobBasics) {
          cleanJobOverview(allData.jobBasics);
        }
        if (allData.chambers) {
          for (i = 0; i < allData.chambers.length; i += 1) {
            if (allData.chambers[i]) {
              allData.chambers[i].equipment = [];
              if (allData.chambers[i].chamberId) {
                chamberLookup[allData.chambers[i].chamberId] = i;
              }
            }
          }
        }
        if (allData.areas) {
          for (i = 0; i < allData.areas.length; i += 1) {
            if (allData.areas[i] && allData.areas[i].areaId) {
              allData.areas[i].equipment = [];
              areaLookup[allData.areas[i].areaId] = i;
              if (allData.areas[i].areaType === "unaffected") {
                unaffectedList.push(allData.areas[i]);
              }
            }
          }
        }
        if (allData.mequipment) {
          if (!allData.equipment) {
            allData.equipment = [];
          }
          for (i = 0; i < allData.mequipment.length; i += 1) {
            if (allData.mequipment[i] && allData.mequipment[i].mequipId) {
              moistureLookup[allData.mequipment[i].mequipId] = i;
            }
            if (allData.mequipment[i] && allData.mequipment[i].msensorId && allData.mequipment[i].msensorId > -1) {
              allData.mequipment[i].model = "DrySENSE";
              allData.mequipment[i].typeName = "Penetrating Meter";
              if (!allData.mequipment[i].equipmentName) {
                allData.mequipment[i].equipmentName = allData.mequipment[i].sensorName || allData.mequipment[i].label;
              }
              allData.equipment.push(allData.mequipment[i]);
            } else if (allData.mequipment[i]) {
              allData.mequipment[i].isRemoved = (allData.mequipment[i].jobDayEnd > 0);
              allData.mequipment[i].sensorName = undefined;
            }
          }
        }

        allData.equipmentList = allData.equipment;
        delete allData.equipment;
        if (allData.equipmentList) {
          for (i = 0; i < allData.equipmentList.length; i += 1) {
            thisEquipment = allData.equipmentList[i] || {};
            if (thisEquipment.equipmentId) {
              equipmentLookup[thisEquipment.equipmentId] = i;
            }
            if (thisEquipment.jobDayEnd > 0) {
              thisEquipment.dayCompleted = (new Date(allData.jobBasics.startDateSecs * 1000 +
                (thisEquipment.jobDayEnd - 1) * 24 * 60 * 60 * 1000)).toLocaleDateString();
              thisEquipment.isRemoved = true;
            } else {
              thisEquipment.isRemoved = false;
            }
            thisEquipment.hasBeacon = thisEquipment.beaconId > -1;
            determineEquipmentType(thisEquipment);

            if (thisEquipment.chamberId === -1 && (thisEquipment.modelType === 'DrySENSE' ||
                (thisEquipment.iconCategory === 'Hygrometer' && thisEquipment.hasBeaconRH))) {
              for (j = 0; j < unaffectedList.length; j += 1) {
                if (unaffectedList[j]) {
                  thisEquipment.chamberName = unaffectedList[j].areaName;
                  thisEquipment.areaId = unaffectedList[j].areaId;
                  unaffectedList[j].equipment.push(thisEquipment);
                }
              }
            } else if (thisEquipment.chamberId && chamberLookup.hasOwnProperty(thisEquipment.chamberId)) {
              aChamber = allData.chambers[chamberLookup[thisEquipment.chamberId]] || {};
              thisEquipment.chamberClass = 'Category ' + aChamber.category +  ' Class ' + aChamber.classification;
              thisEquipment.chamberName = aChamber.chamberName;
              aChamber.equipment.push(thisEquipment);
            }
          }
        }
        markExtraneousEquipment(allData.equipmentList);
        if (allData.equipmentList) {
          for (i = 0; i < allData.equipmentList.length; i += 1) {
            if (allData.equipmentList[i] && allData.equipmentList[i].dehuId && !allData.equipmentList[i].isExtraneous) {
              dehuLookup[allData.equipmentList[i].dehuId] = i;
            }
          }
        }
        if (allData.chambers) {
          for (i = 0; i < allData.chambers.length; i += 1) {
            if (allData.chambers[i]) {
              graphable = false;
              if (allData.chambers[i].equipment && allData.chambers[i].equipment.length) {
                for (j = 0; j < allData.chambers[i].equipment.length; j += 1) {
                  if (allData.chambers[i].equipment[j] && !isNaN(allData.chambers[i].equipment[j].dehuId) && allData.chambers[i].equipment[j].dehuId > -1 &&
                      !isNaN(allData.chambers[i].equipment[j].equipmentId) && allData.chambers[i].equipment[j].equipmentId > -1) {
                    graphable = true;
                    break;
                  }
                }
              }
              allData.chambers[i].graphable = graphable;
            }
          }
        }

        if (allData.visits) {
          for (i = 0; i < allData.visits.length; i += 1) {
            if (allData.visits[i]) {
              entryTime = new Date(allData.visits[i].startSecs * 1000);
              allData.visits[i].startTime = getTimeString(allData.visits[i].startSecs);
              allData.visits[i].endTime = getTimeString(allData.visits[i].endSecs);
              allData.visits[i].date = abbreviatedDays[entryTime.getDay()] + ' ' + entryTime.toLocaleDateString();

              if (allData.visits[i].airMeasures && allData.visits[i].airMeasures.length) {
                for (j = 0; j < allData.visits[i].airMeasures.length; j += 1) {
                  anEntry = allData.visits[i].airMeasures[j] || {};
                  if (anEntry.dehuId && anEntry.dehuId > -1 && dehuLookup.hasOwnProperty(anEntry.dehuId)) {
                    thisEquipment = allData.equipmentList[dehuLookup[anEntry.dehuId]] || {};
                    anEntry.name = thisEquipment.equipmentName;
                    anEntry.location = thisEquipment.location;
                  } else if (anEntry.equipmentId && anEntry.equipmentId > -1 && equipmentLookup.hasOwnProperty(anEntry.equipmentId)) {
                    thisEquipment = allData.equipmentList[equipmentLookup[anEntry.equipmentId]] || {};
                    anEntry.name = thisEquipment.equipmentName;
                    anEntry.location = thisEquipment.location;
                    anEntry.hasBeaconRH = thisEquipment.hasBeaconRH;
                    anEntry.msensorId = thisEquipment.msensorId;
                  } else if (anEntry.measurementType === 'outside' && anEntry.areaId && areaLookup.hasOwnProperty(anEntry.areaId)) {
                    thisEquipment = allData.areas[areaLookup[anEntry.areaId]] || {};
                    anEntry.name = thisEquipment.areaName;
                  }

                  if (!anEntry.sampleSecs || anEntry.sampleSecs < Job.minimumDateSecs) {
                    allData.visits[i].airMeasures.splice(j, 1);
                    j -= 1;
                  } else {
                    entryTime = new Date(anEntry.sampleSecs * 1000);
                    anEntry.time = entryTime.toLocaleTimeString();
                    anEntry.graphable = (anEntry.measurementType === 'affected') || (anEntry.measurementType === 'dehu_out' && anEntry.dehuId && anEntry.dehuId > -1);
                    if (anEntry.measurementType === "dehu_in") {
                      anEntry.measurementType = "affected";
                    } else if (anEntry.measurementType === 'heater' || anEntry.measurementType === 'airconditioner') {
                      anEntry.measurementType = 'other_air';
                    } else if (anEntry.measurementType === "dehu" || anEntry.measurementType === "retrodehu") {
                      anEntry.measurementType = "dehu_out";
                    }
                  }
                }
              }
              if (allData.visits[i].moistureMeasures && allData.visits[i].moistureMeasures.length) {
                for (j = 0; j <  allData.visits[i].moistureMeasures.length; j += 1) {
                  anEntry = allData.visits[i].moistureMeasures[j] || {};
                  if (anEntry.mequipId && moistureLookup.hasOwnProperty(anEntry.mequipId)) {
                    thisEquipment = allData.mequipment[moistureLookup[anEntry.mequipId]] || {};
                    anEntry.location = thisEquipment.location;
                    anEntry.material = thisEquipment.material;
                    anEntry.msensorId = thisEquipment.msensorId;
                    anEntry.sensorName = thisEquipment.sensorName;

                    if (thisEquipment.initialMoisture) {
                      anEntry.initialMoisture = thisEquipment.initialMoisture;
                    } else {
                      anEntry.initialMoisture = anEntry.moistureValue;
                      thisEquipment.initialMoisture = anEntry.moistureValue;
                    }
                    Job.updateProgressBar(anEntry);
                  }
                }
              }
            }
          }
        }

        return allData;
      }, $q.reject);
    };

    Job.getJob = function (jobId) {
      if (!jobId) {
        return $q.reject({message: "No job ID found.", status: 400});
      }
      return $http.get(API_Base + 'job/' + jobId).then(function (response) {
        var i, j, k, isRemoved, graphable, aDevice,
          defaultDate = Job.minimumDateSecs,
          abbreviatedDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
          addEntry = function (entryList, entry) {
            var entryTime;

            if (!entryList || !entry || isNaN(entry.jobDay) || !entry.sampleSecs || entry.sampleSecs < Job.minimumDateSecs) {
              return;
            }
            entryTime = new Date(entry.sampleSecs * 1000);
            entry.time = entryTime.toLocaleTimeString();
            entry.graphable = (entry.measurementType === 'affected') || (entry.measurementType === 'dehu_out' && entry.dehuId && entry.dehuId > -1);
            if (!entryList[entry.jobDay]) {
              entryList[entry.jobDay] = {day: "Day " + entry.jobDay, date: entryTime.toLocaleDateString(), sampleSecs: entry.sampleSecs,
                time: abbreviatedDays[entryTime.getDay()] + ' ' + entryTime.toLocaleTimeString(), jobId: jobId, measures: [], notes: []};
            }
            entryList[entry.jobDay].measures.push(entry);
          },
          addNote = function (entryList, note) {
            var entryTime;

            if (!entryList || !note) {
              return;
            }
            if (note.sampleSecs && note.sampleSecs > defaultDate) {
              note.jobDay = Math.round((note.sampleSecs - defaultDate) / (24 * 60 * 60)) + 1;
            } else {
              note.sampleSecs = defaultDate + 24 * 60 * 60 * (note.jobDay - 1);
            }
            if (!(note.jobDay > 0)) {
              note.jobDay = 1;
            }
            entryTime = new Date(note.sampleSecs * 1000);
            note.time = entryTime.toLocaleTimeString();
            if (!entryList[note.jobDay]) {
              entryList[note.jobDay] = {day: "Day " + note.jobDay, date: entryTime.toLocaleDateString(), sampleSecs: note.sampleSecs,
                time: abbreviatedDays[entryTime.getDay()] + ' ' + entryTime.toLocaleTimeString(), jobId: jobId, measures: [], notes: []};
            }
            entryList[note.jobDay].notes.push(note);
          };

        if (response && response.data && response.data.jobBasics) {
          cleanJobOverview(response.data.jobBasics);
          if (response.data.jobBasics.startDateSecs) {
            defaultDate = response.data.jobBasics.startDateSecs;
          }
        }
        response.data.equipmentList = [];
        response.data.entryList = {};
        if (response && response.data && response.data.areas && response.data.areas.length) {//Drying log entries and job equipment come from the server in fragmented parts.
          for (i = 0; i < response.data.areas.length; i += 1) {//They need to be found, sorted, and merged into usable form after each Get /job/jobId request.
            if (response.data.areas[i].measures && response.data.areas[i].measures.length) {
              for (j = 0; j < response.data.areas[i].measures.length; j += 1) {
                response.data.areas[i].measures[j].areaName = response.data.areas[i].areaName;
                addEntry(response.data.entryList, response.data.areas[i].measures[j]);
              }
            }
          }
        }
        if (response && response.data && response.data.chambers && response.data.chambers.length) {
          for (i = 0; i < response.data.chambers.length; i += 1) {
            graphable = false;
            if (response.data.chambers[i].equipment && response.data.chambers[i].equipment.length) {
              for (j = 0; j < response.data.chambers[i].equipment.length; j += 1) {
                if (response.data.chambers[i].equipment[j] && !isNaN(response.data.chambers[i].equipment[j].dehuId) && response.data.chambers[i].equipment[j].dehuId > -1 &&
                    !isNaN(response.data.chambers[i].equipment[j].equipmentId) && response.data.chambers[i].equipment[j].equipmentId > -1) {
                  graphable = true;
                }

                isRemoved = false;
                if (response.data.chambers[i].equipment[j]  && response.data.chambers[i].equipment[j].jobDayEnd > 0) {
                  response.data.chambers[i].equipment[j].dayCompleted = (new Date(response.data.jobBasics.startDateSecs * 1000 +
                    (response.data.chambers[i].equipment[j].jobDayEnd - 1) * 24 * 60 * 60 * 1000)).toLocaleDateString();
                  isRemoved = true;
                }
                response.data.chambers[i].equipment[j].isRemoved = isRemoved;
                if (response.data.chambers[i].equipment[j].dehuId > -1) {
                  aDevice = $rootScope.searchBluetoothListsForMatch('databaseId', response.data.chambers[i].equipment[j].dehuId);
                  if (aDevice && aDevice.modelType) {
                    response.data.chambers[i].equipment[j].modelType = aDevice.modelType;
                  } else {
                    if (response.data.chambers[i].equipment[j].exSize === 'XL') {
                      response.data.chambers[i].equipment[j].modelType = "Dry Max XL";
                    } else if (response.data.chambers[i].equipment[j].exSize === 'Large') {
                      response.data.chambers[i].equipment[j].modelType = "Dry Max";
                    }
                  }
                }
                response.data.equipmentList.push({isRemoved: isRemoved, chamberId: response.data.chambers[i].chamberId,
                  chamberName: response.data.chambers[i].chamberName, equipmentData: response.data.chambers[i].equipment[j],
                  chamberClass: 'Category ' + response.data.chambers[i].category +  ' Class ' + response.data.chambers[i].classification});

                if (response.data.chambers[i].equipment[j].measures && response.data.chambers[i].equipment[j].measures.length) {
                  for (k = 0; k < response.data.chambers[i].equipment[j].measures.length; k += 1) {
                    response.data.chambers[i].equipment[j].measures[k].chamberName = response.data.chambers[i].chamberName;
                    response.data.chambers[i].equipment[j].measures[k].chamberId = response.data.chambers[i].chamberId;
                    response.data.chambers[i].equipment[j].measures[k].location = response.data.chambers[i].equipment[j].location;
                    response.data.chambers[i].equipment[j].measures[k].equipmentName = response.data.chambers[i].equipment[j].equipmentName;
                    response.data.chambers[i].equipment[j].measures[k].areaName = response.data.chambers[i].equipment[j].equipmentName;
                    response.data.chambers[i].equipment[j].measures[k].dehuId = response.data.chambers[i].equipment[j].dehuId;
                    if (response.data.chambers[i].equipment[j].measures[k].measurementType === "dehu_in") {
                      response.data.chambers[i].equipment[j].measures[k].measurementType = "affected";
                    }
                    addEntry(response.data.entryList, response.data.chambers[i].equipment[j].measures[k]);
                  }
                }
              }
            }
            response.data.chambers[i].graphable = graphable;

            if (response.data.chambers[i].affectedArea && response.data.chambers[i].affectedArea.measures && response.data.chambers[i].affectedArea.measures.length) {
              for (k = 0; k < response.data.chambers[i].affectedArea.measures.length; k += 1) {
                response.data.chambers[i].affectedArea.measures[k].chamberName = response.data.chambers[i].chamberName;
                response.data.chambers[i].affectedArea.measures[k].chamberId = response.data.chambers[i].chamberId;
                response.data.chambers[i].affectedArea.measures[k].areaName = response.data.chambers[i].affectedArea.areaName;
                if (response.data.chambers[i].affectedArea.measures[k].equipmentId > -1 && response.data.chambers[i].equipment && response.data.chambers[i].equipment.length) {
                  for (j = 0; j < response.data.chambers[i].equipment.length; j += 1) {
                    if (response.data.chambers[i].equipment[j] && response.data.chambers[i].equipment[j].equipmentId === response.data.chambers[i].affectedArea.measures[k].equipmentId) {
                      response.data.chambers[i].affectedArea.measures[k].areaName = response.data.chambers[i].equipment[j].equipmentName;//If an inlet measurement comes from a Bluetooth-enabled dehumidifer,
                      response.data.chambers[i].affectedArea.measures[k].location = response.data.chambers[i].equipment[j].location;//Change the name of the affected area measurement to match its source.
                    }
                  }
                }
                addEntry(response.data.entryList, response.data.chambers[i].affectedArea.measures[k]);
              }
            }
          }
        }

        if (response && response.data && response.data.notes && response.data.notes.length) {
          for (i = 0; i < response.data.notes.length; i += 1) {
            addNote(response.data.entryList, response.data.notes[i]);
          }
        }

        markExtraneousEquipment(response.data.equipmentList);

        return response.data;
      }, $q.reject);
    };

    Job.create = function (jobBasics, chambers, contacts, notes) {
      var i, j, l,
        moreAPICalls = [],
        newJob = {},
        existingChamberIndex = 0,
        onlyVisitId,
        shouldAddWeather = false,
        currentTime,
        waitThenCreateChamber = function (k, m) {
          return moreAPICalls[m].promise.then(function () {
            if (!newJob.chambers || !newJob.chambers.length || newJob.chambers.length <= existingChamberIndex) {
              return Job.createChamber(newJob.jobBasics && newJob.jobBasics.jobId, chambers[k]).then(function (newChamber) {
                if (newChamber && newChamber.chamberId) {
                  chambers[k].chamberId = newChamber.chamberId;
                }
                return $q.resolve();
              }, $q.reject);
            }
            return Job.updateChamber(newJob.chambers[k].chamberId, chambers[k]).then(function () {
              chambers[k].chamberId = newJob.chambers[k].chamberId;
              existingChamberIndex += 1;
              return $q.resolve();
            }, $q.reject);
          }, $q.reject);
        },
        waitThenCreateContact = function (k, m) {
          return moreAPICalls[m].promise.then(function () {
            if (contacts[k] && (contacts[k].phone || contacts[k].contactName) && newJob.jobBasics && newJob.jobBasics.jobId) {
              return Job.createContact(newJob.jobBasics.jobId, contacts[k]);
            }
            return $q.resolve();
          }, $q.reject);
        },
        waitThenAddWeather = function (m) {
          return moreAPICalls[m].promise.then(function () {
            if (onlyVisitId && onlyVisitId > -1 && newJob && newJob.jobBasics && newJob.jobBasics.jobId && newJob.areas && newJob.areas.length) {
              for (i = 0; i < newJob.areas.length; i += 1) {
                if (newJob.areas[i] && newJob.areas[i].areaType === "outside" && newJob.areas[i].areaId && newJob.areas[i].areaId > -1) {
                  return Job.addCurrentWeatherReading({jobId: newJob.jobBasics.jobId, visitId: onlyVisitId, sampleSecs: currentTime,
                    areaId: newJob.areas[i].areaId}).then($q.resolve, $q.resolve);
                }
              }
            }
            return $q.resolve();
          }, $q.reject);
        },
        waitThenCreateChamberNote = function (k, m, n) {
          return moreAPICalls[m].promise.then(function () {
            if (chambers[n].notes[k] && chambers[n].notes[k].jobNote && chambers[n].chamberId && newJob && newJob.jobBasics && newJob.jobBasics.jobId) {
              chambers[n].notes[k].jobId = newJob.jobBasics.jobId;
              chambers[n].notes[k].sampleSecs = newJob.jobBasics.startDateSecs;
              chambers[n].notes[k].chamberId = chambers[n].chamberId;
              chambers[n].notes[k].visitId = onlyVisitId;
              return Job.createNote(chambers[n].notes[k], true).then(function (newNote) {
                if (newNote && newNote.visitId) {
                  onlyVisitId = newNote.visitId;
                }
                return $q.resolve();
              }, $q.reject);
            }
            return $q.resolve();
          }, $q.reject);
        },
        waitThenCreateNote = function (k, m) {
          return moreAPICalls[m].promise.then(function () {
            if (notes[k] && notes[k].jobNote && newJob && newJob.jobBasics && newJob.jobBasics.jobId) {
              notes[k].jobId = newJob.jobBasics.jobId;
              notes[k].sampleSecs = newJob.jobBasics.startDateSecs;
              notes[k].visitId = onlyVisitId;
              return Job.createNote(notes[k], true).then(function (newNote) {
                if (newNote && newNote.visitId) {
                  onlyVisitId = newNote.visitId;
                }
                return $q.resolve();
              }, $q.reject);
            }
            return $q.resolve();
          }, $q.reject);
        };

      $rootScope.newJobIntermediateDeletionId = undefined;
      moreAPICalls.push({promise: formatJobBasics(jobBasics).then(function (formattedJob) {
        return $http.post(API_Base + 'job', formattedJob).then(function (response) {
          newJob = response.data;
          if (newJob && newJob.jobBasics && newJob.jobBasics.jobId) {
            $rootScope.newJobIntermediateDeletionId = newJob.jobBasics.jobId;
          }
          return response.data;
        }, function (err) {
          if (err && err.data) {
            err.errors = err.data.errors ? err.data.errors.join("<br>") : err.data.message || err.data;
          }
          return $q.reject(err);
        });
      }, $q.reject)});

      j = 0;
      if (chambers && chambers.length) {
        for (i = 0; i < chambers.length; i += 1, j += 1) {//Add all chambers, one-at-a-time.
          moreAPICalls.push({promise: waitThenCreateChamber(i, j)});

          if (chambers[i].notes && chambers[i].notes.length) {
            for (l = 0; l < chambers[i].notes.length; l += 1) {
              j += 1;
              moreAPICalls.push({promise: waitThenCreateChamberNote(l, j, i)});
              shouldAddWeather = true;
            }
          }
        }
      }
      if (contacts && contacts.length) {
        for (i = 0; i < contacts.length; i += 1, j += 1) {//Add all contacts, one-at-a-time.
          moreAPICalls.push({promise: waitThenCreateContact(i, j)});
        }
      }
      if (notes && notes.length) {
        for (i = 0; i < notes.length; i += 1, j += 1) {//Add all notes, one-at-a-time.
          moreAPICalls.push({promise: waitThenCreateNote(i, j)});
          shouldAddWeather = true;
        }
      }
      currentTime = Math.round((new Date()).getTime() / 1000);
      if (shouldAddWeather && jobBasics && jobBasics.startDateSecs && (currentTime - 3600 < jobBasics.startDateSecs) && (currentTime + 3600 > jobBasics.startDateSecs)) {
        moreAPICalls.push({promise: waitThenAddWeather(j)});
        j += 1;
      }

      return (moreAPICalls[moreAPICalls.length - 1]).promise.then(function () {
        $rootScope.newJobIntermediateDeletionId = undefined;
        Snackbar.show({template: "Job created", hide: 5000});
        return $q.resolve(newJob && newJob.jobBasics);
      }, $q.reject);
    };

    Job.update = function (jobBasics) {
      if (!jobBasics.jobId) {
        return $q.reject({title: "Error updating job", template: "Job ID is a required field"});
      }
      return formatJobBasics(jobBasics).then(function (formattedJob) {
        return $http.put(API_Base + 'job/' + jobBasics.jobId, formattedJob).then(function (response) {
          return response.data;
        }, function (err) {
          if (err && err.data) {
            err.errors = err.data.errors ? err.data.errors.join("<br>") : err.data.message || err.data;
          }
          return $q.reject(err);
        });
      }, $q.reject);
    };

    Job.delete = function (jobId) {
      if (!jobId) {
        return $q.reject({message: "No job ID found.", status: 400});
      }
      return $http.delete(API_Base.replace(/v1/g, 'v2') + 'job/' + jobId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.getList = function (current) {
      var extra = current ? '/current' : '';
      return $http.get(API_Base + 'job' + extra).then(function (response) {
        var i,
          totalUnseenAlertCount = 0;

        if (response && response.data && response.data.length) {
          for (i = 0; i < response.data.length; i += 1) {
            cleanJobOverview(response.data[i]);
            if (response.data[i] && response.data[i].unseenAlertCount) {
              totalUnseenAlertCount += response.data[i].unseenAlertCount;
            }
          }
        }
        $rootScope.unseenJobAlertCount = totalUnseenAlertCount;
        if (!$rootScope.unseenEquipmentAlertCount) {
          $rootScope.unseenEquipmentAlertCount = 0;
        }
        totalUnseenAlertCount = $rootScope.unseenEquipmentAlertCount + $rootScope.unseenJobAlertCount;
        if (totalUnseenAlertCount > 99) {
          totalUnseenAlertCount = 99;
        } else if (totalUnseenAlertCount < 0) {
          totalUnseenAlertCount = 0;
        }
        $rootScope.unseenTotalAlertCount = totalUnseenAlertCount;

        return response.data;
      }, $q.reject);
    };

    Job.assignEquipment2 = function (jobDetails) {
      var i,
        device,
        equipmentList = [],
        deviceList = $rootScope.deviceList,
        bleDehu,
        finishEquipment = function (k) {
          equipmentList[k].chamberAssignment = undefined;
          equipmentList[k].assignToJob = false;
          if (jobDetails) {
            equipmentList[k].jobId = jobDetails.jobId;
            equipmentList[k].jobName = jobDetails.jobName;

            if (deviceList) {
              if (deviceList.newer && deviceList.newer.length) {
                for (i = 0; i < deviceList.newer.length; i += 1) {
                  bleDehu = deviceList.newer[i] || {};
                  if (equipmentList[k].databaseId > -1 && bleDehu.serialNumber && bleDehu.serialNumber === equipmentList[k].serialNumber) {
                    bleDehu.jobName = jobDetails.jobName;
                  } else if (equipmentList[k].msensorId > -1 && ((bleDehu.serialNumber && bleDehu.serialNumber === equipmentList[k].uniqueEnoughID) ||
                             (bleDehu.msensorId === equipmentList[k].msensorId))) {
                    bleDehu.jobName = jobDetails.jobName;
                  }
                  if (equipmentList[k].beaconId > -1 && bleDehu.beaconId === equipmentList[k].beaconId) {
                    bleDehu.jobName = jobDetails.jobName;
                  }
                }
              }
              if (deviceList.older && deviceList.older.length) {
                for (i = 0; i < deviceList.older.length; i += 1) {
                  bleDehu = deviceList.older[i] || {};
                  if (equipmentList[k].databaseId > -1 && bleDehu.serialNumber && bleDehu.serialNumber === equipmentList[k].serialNumber) {
                    bleDehu.jobName = jobDetails.jobName;
                  } else if (equipmentList[k].msensorId > -1 && ((bleDehu.serialNumber && bleDehu.serialNumber === equipmentList[k].uniqueEnoughID) ||
                             (bleDehu.msensorId === equipmentList[k].msensorId))) {
                    bleDehu.jobName = jobDetails.jobName;
                  }
                  if (equipmentList[k].beaconId > -1 && bleDehu.beaconId === equipmentList[k].beaconId) {
                    bleDehu.jobName = jobDetails.jobName;
                  }
                }
              }
            }
          }
        },
        createOneEquipment = function (k) {
          if (!(equipmentList[k].chamberId > -1)) {
            return Job.createUnaffectedEquipment(jobDetails.jobId, equipmentList[k].msensorId > 0 && equipmentList[k].msensorId,
                equipmentList[k].iconCategory === 'Hygrometer' && equipmentList[k].hasBeaconRH && equipmentList[k].cequipId).then(function (newData) {
              finishEquipment(k);
              return $q.resolve(newData);
            }, $q.reject);
          }
          return Job.createEquipment2(equipmentList[k].chamberId, equipmentList[k]).then(function (newData) {
            finishEquipment(k);
            return $q.resolve(newData);
          }, $q.reject);
        },
        waitThenCreateEquipment = function (k) {
          return equipmentList[k + 1].promise.then(function () {
            return createOneEquipment(k);
          }, function (error) {
            return createOneEquipment(k).then(function () {
              return $q.reject(error);
            }, $q.reject);
          });
        };

      if (!$rootScope.choiceList || !$rootScope.choiceList.length) {
        return;
      }
      for (i = 0; i < $rootScope.choiceList.length; i += 1) {
        device = $rootScope.choiceList[i] || {};
        if (device.assignToJob && device.chamberAssignment) {
          if (device.chamberAssignment.chamberId > 0) {
            device.chamberId = device.chamberAssignment.chamberId;
            equipmentList.push(device);
          } else if (device.chamberAssignment.areaId > 0) {
            device.areaId = device.chamberAssignment.areaId;
            equipmentList.push(device);
          }
        }
      }
      if (!equipmentList.length) {
        return;
      }
      equipmentList.push({promise: $q.resolve()});
      for (i = equipmentList.length - 2; i >= 0; i -= 1) {//Add all equipment to their respective chambers, one-at-a-time.
        equipmentList[i].promise = waitThenCreateEquipment(i);
      }

      return equipmentList[0].promise.then(function (newData) {
        Snackbar.show({template: "Equipment assigned to job", hide: 5000});

        return $q.resolve(newData);
      }, $q.reject);
    };

    Job.assignEquipment = function (job, defaultChamber, jobToUpdate) {
      var i,
        device,
        equipmentList = [],
        cleanListOfAssignments = function () {
          var j;
          for (j = 0; j < $rootScope.deviceList.newer.length; j += 1) {
            $rootScope.deviceList.newer[j].assignToJob = undefined;
            $rootScope.deviceList.newer[j].chamberAssignment = undefined;
          }
        },
        createOneEquipment = function (k) {
          return Job.createEquipment(equipmentList[k].chamberId, {equipmentType: 'dehu', exSize: equipmentList[k].modelType === 'Dry Max' ? 'Large' : 'XL',
              dehuId: equipmentList[k].dehuId, equipmentName: equipmentList[k].equipmentName}).then(function (newData) {
            var m;
            equipmentList[k].devicePointer.jobName = job.jobName;

            if (jobToUpdate) {
              newData = newData || {};
              newData.isRemoved = false;
              determineEquipmentType(newData);
              newData.chamberId = equipmentList[k].chamberId;
              newData.chamberName = equipmentList[k].chamberName;

              if (jobToUpdate.equipmentList) {
                jobToUpdate.equipmentList.push(newData);
              }
              if (jobToUpdate.chambers) {
                for (m = 0; m < jobToUpdate.chambers.length; m += 1) {
                  if (jobToUpdate.chambers[m].equipment && jobToUpdate.chambers[m].chamberId === equipmentList[k].chamberId) {
                    jobToUpdate.chambers[m].equipment.push(newData);
                    jobToUpdate.chambers[m].graphable = true;
                    break;
                  }
                }
              }
            }
            return $q.resolve();
          }, $q.reject);
        },
        waitThenCreateEquipment = function (k) {
          return equipmentList[k + 1].promise.then(function () {
            return createOneEquipment(k);
          }, function (error) {
            return createOneEquipment(k).then(function () {
              return $q.reject(error);
            }, $q.reject);
          });
        };

      if (!$rootScope.deviceList || !$rootScope.deviceList.newer || !$rootScope.deviceList.newer.length) {
        return;
      }
      if (!job || isNaN(job.jobId) || job.jobId < 0) {
        cleanListOfAssignments();
        return;
      }

      for (i = 0; i < $rootScope.deviceList.newer.length; i += 1) {
        device = $rootScope.deviceList.newer[i];
        if (device.assignToJob && device.claimStatus === 'mine') {
          if (!(device.chamberAssignment && device.chamberAssignment.chamberId > 0)) {
            if (defaultChamber && defaultChamber.chamberId > 0) {
              device.chamberAssignment = defaultChamber;
            } else {
              device.chamberAssignment = {chamberId: undefined};
            }
          }
          equipmentList.push({chamberId: device.chamberAssignment.chamberId, chamberName: device.chamberAssignment.chamberName, modelType: device.modelType,
            dehuId: device.databaseId, equipmentName: device.name, address: device.address, serialNumber: device.serialNumber, devicePointer: device});
        }
      }
      if (!equipmentList.length) {
        return;
      }
      $ionicLoading.show();
      if (!defaultChamber || isNaN(defaultChamber.chamberId) || defaultChamber.chamberId <= 0) {//Create a new chamber if none exists for this job.
        equipmentList.push({promise: Job.createChamber(job.jobId).then(function (chamberId) {
          var j;
          for (j = 0; j < equipmentList.length; j += 1) {
            equipmentList[j].chamberId = chamberId;
            equipmentList[j].chamberName = "New Chamber";
          }
          return $q.resolve();
        }, $q.reject)});
      } else {
        equipmentList.push({promise: $q.resolve()});
      }

      for (i = equipmentList.length - 2; i >= 0; i -= 1) {//Add all equipment to their respective chambers, one-at-a-time.
        equipmentList[i].promise = waitThenCreateEquipment(i);
      }

      equipmentList[0].promise.then(function () {
        Snackbar.show({template: "Equipment assigned to job", hide: 5000});

        cleanListOfAssignments();
        if (jobToUpdate && jobToUpdate.equipmentList) {
          markExtraneousEquipment(jobToUpdate.equipmentList);
        }
        $timeout(function () {
          $ionicLoading.hide();
        }, 100);
      }, function (error) {
        cleanListOfAssignments();
        $rootScope.handleInternetErrorMessage("Could not assign equipment to job", error);
      });
    };

    Job.createCompanyEquipment = function (equipment) {
      return formatCompanyEquipment(equipment, false).then(function (formattedEquipment) {
        return $http.post(API_Base + 'cequip', formattedEquipment).then(function (response) {
          Snackbar.show({template: "Equipment added to company", hide: 5000});
          return response.data;
        }, $q.reject);
      }, $q.reject);
    };

    Job.updateCompanyEquipment = function (equipment) {
      if (!equipment || !equipment.cequipId) {
        return $q.reject({message: "No equipment ID found.", status: 400});
      }
      return formatCompanyEquipment(equipment, true).then(function (formattedEquipment) {
        return $http.put(API_Base + 'cequip/' + equipment.cequipId + formattedEquipment).then(function (response) {
          return response.data;
        }, $q.reject);
      }, $q.reject);
    };

    Job.createUnaffectedEquipment = function (jobId, msensorId, cequipId) {
      var formattedEquipment;
      if (!jobId || !(msensorId || cequipId)) {
        return $q.reject({message: "No job or equipment ID found.", status: 400});
      }
      if (msensorId) {
        formattedEquipment = "?msensorId=" + msensorId;
      } else {
        formattedEquipment = "?cequipId=" + cequipId;
      }

      return $http.post(API_Base + 'job/' + jobId + '/unaffected' + formattedEquipment).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.createEquipment2 = function (chamberId, equipment, isMoisture) {
      if (!chamberId) {
        return $q.reject({message: "No chamber ID found.", status: 400});
      }
      return formatEquipment2(equipment).then(function (formattedEquipment) {
        if (formattedEquipment && formattedEquipment.length) {
          if (isMoisture) {
            return $http.post(API_Base + 'chamber/' + chamberId + '/mequipment' + formattedEquipment).then(function (response) {
              return response.data;
            }, $q.reject);
          }
          return $http.post(API_Base.replace(/v1/g, 'v2') + 'chamber/' + chamberId + '/equipment' + formattedEquipment).then(function (response) {
            return response.data;
          }, $q.reject);
        }
        return $q.when();
      }, $q.reject);
    };

    Job.createEquipment = function (chamberId, equipment) {
      if (!chamberId) {
        return $q.reject({message: "No chamber ID found.", status: 400});
      }
      return formatEquipment(equipment).then(function (formattedEquipment) {
        if (formattedEquipment && formattedEquipment.length) {
          return $http.post(API_Base + 'chamber/' + chamberId + '/equipment' + formattedEquipment).then(function (response) {
            return response.data;
          }, $q.reject);
        }
        return $q.when();
      }, $q.reject);
    };

    Job.getEquipment = function (equipmentId) {
      if (!equipmentId) {
        return $q.reject({message: "No equipment ID found.", status: 400});
      }
      return $http.get(API_Base + 'equipment/' + equipmentId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.updateEquipment = function (equipmentId, equipment) {
      if (!equipmentId) {
        return $q.reject({message: "No equipment ID found.", status: 400});
      }
      return formatEquipment(equipment, true).then(function (formattedEquipment) {
        if (formattedEquipment && formattedEquipment.length) {
          return $http.put(API_Base + 'equipment/' + equipmentId + formattedEquipment).then(function (response) {
            return response.data;
          }, $q.reject);
        }
        return $q.when();
      }, $q.reject);
    };

    Job.deleteCompanyEquipment = function (equipmentId) {
      if (!equipmentId) {
        return $q.reject({message: "No equipment ID found.", status: 400});
      }
      return $http.delete(API_Base + 'cequip/' + equipmentId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.deleteEquipment = function (equipmentId) {
      if (!equipmentId) {
        return $q.reject({message: "No equipment ID found.", status: 400});
      }
      return $http.delete(API_Base + 'equipment/' + equipmentId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.deleteMoistureEquipment = function (equipmentId) {
      if (!equipmentId) {
        return $q.reject({message: "No equipment ID found.", status: 400});
      }
      return $http.delete(API_Base + 'mequipment/' + equipmentId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.createChamber = function (jobId, chamber) {
      if (!jobId) {
        return $q.reject({message: "No job ID found.", status: 400});
      }
      return formatChamber(chamber, true, true).then(function (formattedChamber) {
        return $http.post(API_Base.replace(/v1/g, 'v2') + 'job/' + jobId + '/chamber' + formattedChamber).then(function (response) {
          return response.data;
        }, $q.reject);
      });
    };

    Job.updateChamber = function (chamberId, chamber) {
      if (!chamberId) {
        return $q.reject({message: "No chamber ID found.", status: 400});
      }
      return formatChamber(chamber, false).then(function (formattedChamber) {
        return $http.put(API_Base + 'chamber/' + chamberId, formattedChamber).then(function (response) {
          return formatChamberSizes(chamber).then(function (formattedSizes) {
            if (formattedSizes) {
              return $http.put(API_Base + 'chamber/' + chamberId + '/size' + formattedSizes).then(function (response2) {
                return response2.data;
              }, $q.reject);
            }
            return response.data;
          });
        }, $q.reject);
      });
    };

    Job.getChamber = function (chamberId) {
      if (!chamberId) {
        return $q.reject({message: "No chamber ID found.", status: 400});
      }
      return $http.get(API_Base.replace(/v1/g, 'v2') + 'chamber/' + chamberId).then(function (response) {
        var i, j, thisEquipment;
        if (response && response.data && response.data.mequipment && response.data.mequipment.length) {
          if (!response.data.equipment) {
            response.data.equipment = [];
          }
          for (i = 0; i < response.data.mequipment.length; i += 1) {
            if (response.data.mequipment[i] && response.data.mequipment[i].msensorId && response.data.mequipment[i].msensorId > -1) {
              response.data.mequipment[i].model = "DrySENSE";
              response.data.mequipment[i].typeName = "Penetrating Meter";
              if (!response.data.mequipment[i].equipmentName) {
                response.data.mequipment[i].equipmentName = response.data.mequipment[i].sensorName || response.data.mequipment[i].label;
              }
              response.data.equipment.push(response.data.mequipment[i]);
            }
          }
        }
        if (response && response.data && response.data.equipment && response.data.equipment.length) {
          for (i = 0; i < response.data.equipment.length; i += 1) {
            thisEquipment = response.data.equipment[i] || {};
            if (thisEquipment.jobDayEnd > 0) {
              thisEquipment.isRemoved = true;
            } else {
              thisEquipment.isRemoved = false;
            }
            thisEquipment.isExtraneous = false;
            thisEquipment.hasBeacon = thisEquipment.beaconId > -1;
            determineEquipmentType(thisEquipment);
          }
          for (i = 0; i < response.data.equipment.length; i += 1) {
            if (response.data.equipment[i] && !response.data.equipment[i].isRemoved && response.data.equipment[i].msensorId && response.data.equipment[i].msensorId > -1) {
              for (j = 0; j < response.data.equipment.length; j += 1) {
                if (i !== j && response.data.equipment[j] && response.data.equipment[j].msensorId === response.data.equipment[i].msensorId) {
                  if (response.data.equipment[j].equipmentId && response.data.equipment[j].equipmentId > -1 && response.data.equipment[j].equipmentName) {
                    response.data.equipment[i].equipmentName = response.data.equipment[j].equipmentName;
                  }
                  if (!response.data.equipment[j].isRemoved && response.data.equipment[j].mequipId && response.data.equipment[j].mequipId > -1 &&
                      !(response.data.equipment[i].mequipId && response.data.equipment[i].mequipId > -1)) {
                    response.data.equipment[i].mequipId = response.data.equipment[j].mequipId;
                  }
                  if (response.data.equipment[j].equipmentId > 0 && (!(response.data.equipment[i].equipmentId > 0) || !response.data.equipment[j].isExtraneous)) {
                    response.data.equipment[i].isExtraneous = true;
                    break;
                  }
                }
              }
            }
          }
        }
        return response.data;
      }, $q.reject);
    };

    Job.deleteChamber = function (chamberId) {
      if (!chamberId) {
        return $q.reject({message: "No chamber ID found.", status: 400});
      }
      return $http.delete(API_Base + 'chamber/' + chamberId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.createContact = function (jobId, contact) {
      if (!jobId || isNaN(jobId)) {
        return $q.reject({message: "No job ID found.", status: 400});
      }
      return formatContact(contact, false).then(function (formattedContact) {
        formattedContact.jobId = parseInt(jobId, 10);
        return $http.post(API_Base + 'contact', formattedContact).then(function (response) {
          return response.data;
        }, $q.reject);
      });
    };

    Job.updateContact = function (contact) {
      if (!contact || !contact.contactId) {
        return $q.reject({message: "No contact ID found.", status: 400});
      }
      return formatContact(contact, true).then(function (formattedContact) {
        return $http.put(API_Base + 'contact/' + contact.contactId + formattedContact).then(function (response) {
          return response.data;
        }, $q.reject);
      });
    };

    Job.deleteContact = function (contactId) {
      if (!contactId) {
        return $q.reject({message: "No contact ID found.", status: 400});
      }
      return $http.delete(API_Base + 'contact/' + contactId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.createNote = function (note, hideSnackbar) {
      if (!note || !note.jobId) {
        return $q.reject({message: "No job ID found.", status: 400});
      }
      return Job.useOldVisitOrStartNew(note.jobId, note.sampleSecs, note.visitId).then(function (vId) {
        if (note.chamberId) {
          return $http.post(API_Base.replace(/v1/g, 'v2') + 'notes?jobId=' + note.jobId + '&visitId=' + vId + '&sampleSecs=' + note.sampleSecs +
                            '&chamberId=' + note.chamberId + '&note=' + encodeURIComponent(note.jobNote)).then(function (response) {
            if (!hideSnackbar) {
              Snackbar.show({template: "Changes saved", hide: 1500});
            }
            response.data = response.data || {};
            response.data.visitId = vId;
            return response.data;
          }, $q.reject);
        }
        return $http.post(API_Base.replace(/v1/g, 'v2') + 'notes?jobId=' + note.jobId + '&visitId=' + vId + '&sampleSecs=' + note.sampleSecs +
                          '&note=' + encodeURIComponent(note.jobNote)).then(function (response) {
          if (!hideSnackbar) {
            Snackbar.show({template: "Changes saved", hide: 1500});
          }
          response.data = response.data || {};
          response.data.visitId = vId;
          return response.data;
        }, $q.reject);
      }, $q.reject);
    };

    Job.updateNote = function (note) {
      if (!note || !note.jobNoteId) {
        return $q.reject({message: "No note ID found.", status: 400});
      }
      return $http.put(API_Base + 'notes/' + note.jobNoteId + '?sampleSecs=' + note.sampleSecs + '&note=' + encodeURIComponent(note.jobNote)).then(function (response) {
        Snackbar.show({template: "Changes saved", hide: 1500});
        return response.data;
      }, $q.reject);
    };

    Job.deleteNote = function (noteId) {
      if (!noteId) {
        return $q.reject({message: "No note ID found.", status: 400});
      }
      return $http.delete(API_Base + 'notes/' + noteId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.getDryingLog = function (jobId) {
      if (!jobId) {
        return $q.reject({message: "No job ID found.", status: 400});
      }
      return $http.get(API_Base.replace(/v1/g, 'v2') + 'job/' + jobId + '/reportstatus').then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.addBluetoothReading = function (equipmentId, measurementTime, jobId, visitId) {
      if (isNaN(equipmentId) || isNaN(measurementTime)) {
        return $q.reject({message: "Equipment ID and measurement time required.", status: 400});
      }

      return Job.useOldVisitOrStartNew(jobId, measurementTime, visitId).then(function (vId) {
        return $http.put(API_Base.replace(/v1/g, 'v2') + 'equipment/' + equipmentId + '/measure?sampleSecs=' + measurementTime + '&visitId=' + vId).then(function (response) {
          Snackbar.show({template: "Changes saved", hide: 1500});
          response.data = response.data || {};
          response.data.visitId = vId;
          return response.data;
        }, $q.reject);
      }, $q.reject);
    };

    Job.addBluetoothInletReading = function (chamberId, equipmentId, measurementTime, jobId, visitId, optionalRH, optionalTemp) {
      if (isNaN(chamberId) || isNaN(equipmentId) || isNaN(measurementTime)) {
        return $q.reject({message: "Chamber ID, equipment ID, and measurement time required.", status: 400});
      }
      var extras = "";
      if (!isNaN(optionalRH) && !isNaN(optionalTemp)) {
        extras = "&rh=" + optionalRH + "&temperature=" + optionalTemp;
      }

      return Job.useOldVisitOrStartNew(jobId, measurementTime, visitId).then(function (vId) {
        return $http.put(API_Base.replace(/v1/g, 'v2') + 'chamber/' + chamberId + '/affected?equipmentId=' + equipmentId + '&sampleSecs=' + measurementTime + '&visitId=' + vId + extras).then(function (response) {
          Snackbar.show({template: "Changes saved", hide: 1500});
          response.data = response.data || {};
          response.data.visitId = vId;
          return response.data;
        }, $q.reject);
      }, $q.reject);
    };

    Job.addBluetoothHVACReading = function (chamberId, equipmentId, measurementTime, jobId, visitId, optionalRH, optionalTemp) {
      if (!(chamberId > 0) || !(equipmentId > 0) || isNaN(measurementTime)) {
        return $q.reject({message: "Chamber ID, equipment ID, and measurement time required.", status: 400});
      }
      var extras = "";
      if (!isNaN(optionalRH) && !isNaN(optionalTemp)) {
        extras = "&rh=" + optionalRH + "&temperature=" + optionalTemp;
      }

      return Job.useOldVisitOrStartNew(jobId, measurementTime, visitId).then(function (vId) {
        return $http.put(API_Base.replace(/v1/g, 'v2') + 'chamber/' + chamberId + '/HVAC?equipmentId=' + equipmentId + '&sampleSecs=' + measurementTime + '&visitId=' + vId + extras).then(function (response) {
          Snackbar.show({template: "Changes saved", hide: 1500});
          response.data = response.data || {};
          response.data.visitId = vId;
          return response.data;
        }, $q.reject);
      }, $q.reject);
    };

    Job.addBluetoothUnaffectedReading = function (areaId, equipmentId, measurementTime, jobId, visitId, optionalRH, optionalTemp) {
      if (!(areaId > 0) || !(equipmentId > 0) || isNaN(measurementTime)) {
        return $q.reject({message: "Area ID, equipment ID, and measurement time required.", status: 400});
      }
      var extras = "";
      if (!isNaN(optionalRH) && !isNaN(optionalTemp)) {
        extras = "&rh=" + optionalRH + "&temperature=" + optionalTemp;
      }

      return Job.useOldVisitOrStartNew(jobId, measurementTime, visitId).then(function (vId) {
        return $http.put(API_Base.replace(/v1/g, 'v2') + 'area/' + areaId + '/unaffected?equipId=' + equipmentId + '&sampleSecs=' + measurementTime + '&visitId=' + vId + extras).then(function (response) {
          Snackbar.show({template: "Changes saved", hide: 1500});
          response.data = response.data || {};
          response.data.visitId = vId;
          return response.data;
        }, $q.reject);
      }, $q.reject);
    };

    Job.addAllBluetoothReadings = function (chamberId, measurementTime, jobId, visitId) {
      if (isNaN(chamberId) || isNaN(measurementTime)) {
        return $q.reject({message: "Chamber ID and measurement time required.", status: 400});
      }

      return Job.useOldVisitOrStartNew(jobId, measurementTime, visitId).then(function (vId) {
        return $http.put(API_Base.replace(/v1/g, 'v2') + 'chamber/' + chamberId + '/assign_measures?visitId=' + vId).then(function (response) {
          var addedMeasurements = (response.data && response.data.airMeasures) || [{}],
            i;
          if (response.data && response.data.moistureMeasures && response.data.moistureMeasures.length) {
            addedMeasurements = addedMeasurements.concat(response.data.moistureMeasures);
          }
          for (i = 0; i < addedMeasurements.length; i += 1) {
            if (addedMeasurements[i] && (addedMeasurements[i].status === 0 || addedMeasurements[i].status === 2)) {
              Snackbar.show({template: "Measurements added", hide: 1500});
              break;
            }
          }
          if (addedMeasurements[0]) {
            addedMeasurements[0].visitId = vId;
          }
          return addedMeasurements;
        }, $q.reject);
      }, $q.reject);
    };

    Job.addCurrentWeatherReading = function (measurement, changedName) {
      if (!measurement) {
        return $q.reject({message: "Could not create measurement with empty entry.", status: 400});
      }
      var initializationPromise = $q.resolve();

      if (changedName) {
        if (isNaN(measurement.areaId)) {
          initializationPromise = formatArea(measurement).then(function (formattedArea) {
            return $http.post(API_Base + 'area' + formattedArea).then(function (response) {
              measurement.areaId = response.data.areaId;
              return response.data;
            }, $q.reject);
          });
        } else {
          initializationPromise = formatArea(measurement).then(function (formattedArea) {
            return $http.put(API_Base + 'area/' + measurement.areaId + formattedArea).then(function (response) {
              return response.data;
            }, $q.reject);
          });
        }
      }

      return initializationPromise.then(function () {
        return Job.useOldVisitOrStartNew(measurement.jobId, measurement.sampleSecs, measurement.visitId).then(function (vId) {
          return $http.post(API_Base + 'area/' + measurement.areaId + '/outside?visitId=' + vId).then(function (response) {
            Snackbar.show({template: "Changes saved", hide: 1500});
            response.data = response.data || {};
            response.data.visitId = vId;
            return response.data;
          }, $q.reject);
        }, $q.reject);
      }, $q.reject);
    };

    Job.getHistoricalData = function (databaseId, startTime, endTime) {
      if (isNaN(databaseId) || isNaN(startTime) || isNaN(endTime)) {
        return $q.reject({message: "Dehu ID and time interval required.", status: 400});
      }
      return $http.get(API_Base + 'dehu/' + databaseId + '/data?scale=max&startSecs=' + Math.round(startTime) + '&endSecs=' + Math.round(endTime)).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.findEquipmentTypes = function () {
      return $http.get(API_Base + 'cequip/types').then(function (response) {
        var shuffledData = response.data || [],
          orderedData = [],
          typeLookup = {},
          foundOrder = {},
          aType = {},
          aSize = {},
          furtherLookup = {},
          validForDryTAGrh,
          i;

        for (i = 0; i < shuffledData.length; i += 1) {//Rearrange the data from Diane's server to be in a better format.
          if (shuffledData[i] && shuffledData[i].category) {
            validForDryTAGrh =  shuffledData[i].category === "Dehumidifier" || shuffledData[i].typeName.indexOf('Hygrometer') > -1;
            if (typeLookup.hasOwnProperty(shuffledData[i].category) && typeLookup[shuffledData[i].category].index > -1) {
              foundOrder = orderedData[typeLookup[shuffledData[i].category].index];
              furtherLookup = typeLookup[shuffledData[i].category].furtherLookup;
              if (furtherLookup.hasOwnProperty(shuffledData[i].equipType)) {
                aSize = {size: shuffledData[i].size, fullName: shuffledData[i].typeName};
                foundOrder.types[furtherLookup[shuffledData[i].equipType]].sizes.push(aSize);
              } else {
                aSize = {size: shuffledData[i].size, fullName: shuffledData[i].typeName};
                aType = {type: shuffledData[i].equipType, sizes: [aSize], sizeChoice: aSize, validForDryTAGrh: validForDryTAGrh};
                foundOrder.types.push(aType);
                if (!foundOrder.validForDryTAGrh && validForDryTAGrh) {
                  foundOrder.typeChoice = aType;
                }
                foundOrder.validForDryTAGrh = foundOrder.validForDryTAGrh || validForDryTAGrh;
                furtherLookup[shuffledData[i].equipType] = foundOrder.types.length - 1;
              }
            } else {
              aSize = {size: shuffledData[i].size, fullName: shuffledData[i].typeName};
              aType = {type: shuffledData[i].equipType, sizes: [aSize], sizeChoice: aSize, validForDryTAGrh: validForDryTAGrh};
              orderedData.push({category: shuffledData[i].category, types: [aType], typeChoice: aType, validForDryTAGrh: validForDryTAGrh});
              furtherLookup = {};
              furtherLookup[shuffledData[i].equipType] = 0;
              typeLookup[shuffledData[i].category] = {index: orderedData.length - 1, furtherLookup: furtherLookup};
            }
          } else if (shuffledData[i] && shuffledData[i].typeName) {
            if (!typeLookup.hasOwnProperty(shuffledData[i].typeName)) {
              aSize = {size: shuffledData[i].size, fullName: shuffledData[i].typeName};
              aType = {type: shuffledData[i].equipType, sizes: [aSize], sizeChoice: aSize, validForDryTAGrh: false};
              orderedData.push({category: shuffledData[i].typeName, types: [aType], typeChoice: aType, validForDryTAGrh: false});
              furtherLookup = {};
              furtherLookup[shuffledData[i].equipType] = 0;
              typeLookup[shuffledData[i].category] = {index: orderedData.length - 1, furtherLookup: furtherLookup};
            }
          }
        }
        return orderedData;
      }, $q.reject);
    };

    Job.findMaterialsAndTargets = function (chamberId) {
      if (!chamberId) {
        return $q.reject({message: "No chamber ID found.", status: 400});
      }
      return $http.get(API_Base + 'chamber/' + chamberId + '/mptargets').then(function (response) {
        var shuffledData = response.data || [],
          orderedData = [],
          locationLookup = {},
          aMaterial = {},
          i;

        for (i = 0; i < shuffledData.length; i += 1) {//Rearrange the data from Diane's server to be in a better format.
          if (shuffledData[i] && shuffledData[i].location) {
            if (locationLookup.hasOwnProperty(shuffledData[i].location)) {
              orderedData[locationLookup[shuffledData[i].location]].materials.push({material: shuffledData[i].material, target: shuffledData[i].target});
            } else {
              aMaterial = {material: shuffledData[i].material, target: shuffledData[i].target};
              orderedData.push({location: shuffledData[i].location, materials: [aMaterial], materialChoice: aMaterial});
              locationLookup[shuffledData[i].location] = orderedData.length - 1;
            }
          }
        }
        return orderedData;
      }, $q.reject);
    };

    Job.useOldVisitOrStartNew = function (jobId, startTime, visitId) {
      if (isNaN(visitId)) {
        if (!jobId || isNaN(startTime)) {
          return $q.reject({message: "Job ID and start time required.", status: 400});
        }
        return $http.post(API_Base + 'job/' + jobId + '/visit?initialSecs=' + startTime).then(function (response) {
          if ($rootScope.latestLogEntry && !($rootScope.latestLogEntry.visitId > 0 && response && response.data > 0)) {
            $rootScope.latestLogEntry.visitId = response.data;
          }
          return response.data;
        }, $q.reject);
      }
      return $q.resolve(visitId);
    };

    Job.updateMultipleMeasurements = function (measurements) {
      var i = 0,
        updateAMeasurement = function () {
          if (!measurements || !measurements.length || i >= measurements.length) {
            return $q.resolve();
          }
          if (measurements[i].jobMeasureId) {
            return createOrUpdateReading(measurements[i]).then(function () {
              i += 1;
              return updateAMeasurement();
            }, $q.reject);
          }
          i += 1;
          return updateAMeasurement();
        };

      return updateAMeasurement();
    };

    Job.addMoistureSite = function (chamberId, moisturePoint) {
      if (!chamberId) {
        return $q.reject({message: "No chamber ID found.", status: 400});
      }

      return formatMoisturePoint(moisturePoint).then(function (formattedMoisture) {
        return $http.post(API_Base + 'chamber/' + chamberId + '/mequipment' + formattedMoisture).then(function (response) {
          return response.data;
        }, $q.reject);
      });
    };

    Job.addBluetoothMoisture = function (equipmentId, measurementTime, jobId, visitId, moistureReading) {
      if (isNaN(equipmentId) || isNaN(measurementTime)) {
        return $q.reject({message: "Equipment ID and measurement time required.", status: 400});
      }

      return Job.useOldVisitOrStartNew(jobId, measurementTime, visitId).then(function (vId) {
        var extraBit = '';
        if (moistureReading >= 0) {
          extraBit = '&moisture=' + moistureReading;
        }
        return $http.put(API_Base + 'mequipment/' + equipmentId + '/measure?sampleSecs=' + measurementTime + '&visitId=' + vId + extraBit).then(function (response) {
          Snackbar.show({template: "Changes saved", hide: 1500});
          response.data = response.data || {};
          response.data.visitId = vId;
          return response.data;
        }, $q.reject);
      }, $q.reject);
    };

    Job.createOrUpdateMoisture = function (measure, changedMeasure, changedPoint) {
      var initializationPromise = $q.resolve();
      if (!measure || !measure.mequipId) {
        return $q.reject({message: "Could not create measurement with empty entry.", status: 400});
      }
      if (changedPoint) {
        initializationPromise = formatMoisturePoint(measure).then(function (formattedMoisture) {
          return $http.put(API_Base + 'mequipment/' + measure.mequipId + formattedMoisture).then(function (response) {
            if (response && response.data) {
              measure.location = response.data.location || measure.location;
              measure.material = response.data.material || measure.material;
              measure.label = response.data.label || measure.label;
              measure.target = response.data.target || measure.target;
            }
            return response.data;
          }, $q.reject);
        });
      }

      return initializationPromise.then(function () {
        if (changedMeasure) {
          return Job.useOldVisitOrStartNew(measure.jobId, measure.sampleSecs, measure.visitId).then(function (visitId) {
            measure.visitId = visitId;
            return formatMoisture(measure).then(function (formattedMeasure) {
              return $http.post(API_Base + 'mequipment/' + measure.mequipId + '/measure' + formattedMeasure).then(function (response) {
                Snackbar.show({template: "Changes saved", hide: 1500});
                response.data = response.data || {};
                response.data.visitId = visitId;
                return response.data;
              }, $q.reject);
            });
          });
        }
        Snackbar.show({template: "Changes saved", hide: 1500});
        return $q.resolve(measure);
      }, $q.reject);
    };

    Job.createOrUpdateMeasurement = function (measurement, changedMeasure, changedName) {
      var initializationPromise = $q.resolve();

      if (changedName && measurement) {
        if (isNaN(measurement.areaId)) {
          initializationPromise = formatArea(measurement).then(function (formattedArea) {
            return $http.post(API_Base + 'area' + formattedArea).then(function (response) {
              measurement.areaId = response.data.areaId;
              changedMeasure = true;
              return response.data;
            }, $q.reject);
          });
        } else {
          initializationPromise = formatArea(measurement).then(function (formattedArea) {
            return $http.put(API_Base + 'area/' + measurement.areaId + formattedArea).then(function (response) {
              return response.data;
            }, $q.reject);
          });
          // $timeout(function () {
          //   $ionicLoading.hide();
          // }, 100);
          // initializationPromise = $ionicPopup.confirm({
          //   title: "Change all reading names?",
          //   template: 'Do you want to change the name of all ' + measurement.measurementType + ' measurements for this job from "' +
          //     measurement.areaName + '" to "' + measurement.editName + '" or just this one, on this day?',
          //   okText: "ALL",
          //   okType: "right-button",
          //   cancelText: "ONLY ONE",
          //   cancelType: "left-button"
          // }).then(function (res) {
          //   $ionicLoading.show();
          //   if (res) {
          //     return formatArea(measurement).then(function (formattedArea) {
          //       return $http.put(API_Base + 'area/' + measurement.areaId + formattedArea).then(function (response) {
          //         return response.data;
          //       }, $q.reject);
          //     });
          //   }
          //   delete measurement.areaId;
          //   return Job.deleteMeasurement(measurement.jobMeasureId).then(function () {
          //     return formatArea(measurement).then(function (formattedArea) {
          //       return $http.post(API_Base + 'area' + formattedArea).then(function (response) {
          //         measurement.areaId = response.data.areaId;
          //         changedMeasure = true;
          //         return response.data;
          //       }, $q.reject);
          //     });
          //   }, $q.reject);
          // });
        }
      }

      return initializationPromise.then(function () {
        if (changedMeasure) {
          return createOrUpdateReading(measurement);
        }
        Snackbar.show({template: "Changes saved", hide: 1500});
        return $q.resolve(measurement);
      }, $q.reject);
    };

    Job.deleteMeasurement = function (measurementId) {
      if (!measurementId) {
        return $q.reject({message: "No measurement ID found.", status: 400});
      }
      return $http.delete(API_Base + 'measure/' + measurementId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.deleteMoistureReading = function (measurementId) {
      if (!measurementId) {
        return $q.reject({message: "No measurement ID found.", status: 400});
      }
      return $http.delete(API_Base + 'moisture/' + measurementId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.deleteVisit = function (visitId) {
      if (!visitId) {
        return $q.reject({message: "No visit ID found.", status: 400});
      }
      return $http.delete(API_Base + 'visit/' + visitId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.getRefreshedInventoryReport = function () {
      return $http.get(API_Base + 'inventory/equip/').then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.getInventoryReport = function () {
      return $http.get(API_Base + 'inventory/equip/last/').then(function (response) {
        if (!response.data || !response.data.length) {
          return Job.getRefreshedInventoryReport();
        }
        return response.data;
      }, $q.reject);
    };

    Job.createLocation = function (location) {
      return formatLocation(location).then(function (formattedLocation) {
        return $http.post(API_Base + 'inventory/location/fixed' + formattedLocation).then(function (response) {
          return response.data;
        }, $q.reject);
      });
    };

    Job.deleteLocation = function (locationId) {
      return $http.delete(API_Base + 'inventory/location/' + locationId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.createOrUpdateGateway = function (locationId, name, isExisting) {
      if (!locationId || !name) {
        return $q.reject({message: "Gateway UUID and name required.", status: 400});
      }
      if (isExisting) {
        return $http.put(Integration_Base + 'gateway/location/' + encodeURIComponent(locationId) + '?name=' + encodeURIComponent(name)).then(function (response) {
          return response.data;
        }, $q.reject);
      }
      return $http.post(Integration_Base + 'gateway/location?uuid=' + encodeURIComponent(locationId) + '&name=' + encodeURIComponent(name)).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.deleteGateway = function (locationId) {
      return $http.delete(Integration_Base + 'gateway/location/' + encodeURIComponent(locationId)).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.getAllInventoryLocations = function () {
      return $http.get(API_Base + 'inventory/summary/').then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.getInventoryLocations = function () {
      var i,
        list,
        output = {fixed: [], job: [], mobile: [], gateway: []};
      return $http.get(API_Base + 'inventory/location').then(function (response) {
        list = (response && response.data) || [];
        if (list && list.length) {
          for (i = 0; i < list.length; i += 1) {
            if (list[i]) {
              if (list[i].locationType === 'Fixed') {
                output.fixed.push(list[i]);
              } else if (list[i].locationType === 'Job') {
                output.job.push(list[i]);
              } else if (list[i].locationType === 'Mobile') {
                output.mobile.push(list[i]);
              } else if (list[i].locationType === 'Gateway') {
                output.gateway.push(list[i]);
              }
            }
          }
        }
        return output;
      }, $q.reject);
    };

    Job.updateInventoryStatus = function (newStatus, equipment) {
      var query = '?disposition=' + newStatus;
      if (equipment.dehuId > 0) {
        query += '&dehuId=' + equipment.dehuId;
      }
      if (equipment.cequipId > 0) {
        query += '&cequipId=' + equipment.cequipId;
      }
      if (equipment.msensorId > 0) {
        query += '&msensorId=' + equipment.msensorId;
      }
      return $http.put(API_Base + 'inventory/equip' + query).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    return Job;
  }];

  angular.module('ThermaStor')
    .service('Job', jobFactory);
}());
