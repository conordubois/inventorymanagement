(function () {
  'use strict';

  var AuthenticationDefinition = ['$q', '$http', '$localStorage', '$state', 'API_Base', '$rootScope', function ($q, $http, $localStorage, $state, API_Base, $rootScope) {
    var Authentication = {},
      token = {},
      ourAPIKey = 'aae9458c73b96c922536b8c3abd7aa69b39bbf0a',
      myWebsocket,
      websocketPingCount = 0,
      websocketState = 0, // 0 = closed, 1 = open but no token sent, 2 = open and token sent
      urlEncodedFormConfig = {
        transformRequest: function (obj) {// from http://stackoverflow.com/a/24964658/1148769
          var str = [], p;
          for (p in obj) {
            if (obj.hasOwnProperty(p)) {
              str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
          }
          return str.join("&");
        },
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      },
      authenticateWebsocket = function () {
        //console.log("authenticating?");
        if (myWebsocket && websocketState === 1 && token && token.accesstoken) {
          //console.log("authenticating.");
          myWebsocket.send('accesstoken=' + token.accesstoken);
          websocketState = 2;
        }
      },
      newWebsocket = function () {
        myWebsocket = new window.WebSocket('ws' + API_Base.slice(4) + 'ws_settings');

        myWebsocket.addEventListener("open", function () {
          console.log("websocket opened " + (new Date()).getTime());
          websocketState = 1;
          authenticateWebsocket();
        });

        myWebsocket.addEventListener('message', function (event) {
          var newData, newID, i;

          //console.log(".. " + JSON.stringify(event.data));
          if (event.data && event.data[0] === '.') {
            websocketPingCount = 0;
          } else if (event.data && event.data.slice(0, 10) === 'settings:{') {
            newData = JSON.parse(event.data.slice(9));
            $rootScope.$broadcast('dehu_update_' + newData.dehuId, newData);
          } else if (event.data && event.data.slice(0, 8) === 'dehu_id:') {
            newID = parseInt(event.data.slice(8), 10);
            for (i = 0; i < $rootScope.dehus.length; i += 1) {
              if ($rootScope.dehus[i].dehuId === newID) {//Check if dehumidifier is already present in side menu.
                return;
              }
            }
            //Dehumidifier.get(newID).then(function (response) {});
            $rootScope.getDehuList();//If newID not found, update entire side menu.
          }
        });

        myWebsocket.addEventListener("close", function () {
          console.log("websocket closed " + (new Date()).getTime());
          websocketState = 0;
          myWebsocket.close();
          setTimeout(function () {
            newWebsocket();
          }, 10000);
        });

        myWebsocket.addEventListener("error", function (event) {
          console.log("websocket error:", event);
        });
      },
      /**
       * Store token in memory if valid.
       * @param {object} {accesstoken, expires_in, refreshtoken} optionally wrapped in 'data' field
       * @return {promise} resolves with token object, rejects with $ionicPopup.alert template
       */
      setToken = function (newToken) {
        if (newToken.hasOwnProperty('data')) {
          newToken = newToken.data;
        }
        if (!newToken.accesstoken || !newToken.expires_in || !newToken.refreshtoken) {
          return $q.reject({title: 'Error', template: 'There was an error authorizing the user'});
        }

        token = newToken;
        token.expiry = (new Date()).getTime() + (newToken.expires_in * 1000); // calculate when the token will expire so we know to refresh it
        $localStorage.token = token;

        authenticateWebsocket();
        return $q.when(token);
      },
      /**
       * Get token from API using either password or previous token. On success, calls setToken and resolves with token.
       * @param  {Object} {refreshtoken (string) OR email (string), password (string)}, redirectLogin (boolean)
       * @return {promise} resolves with user object if successful
       */
      getToken = function (payload, redirectLogin) {
        if (websocketState > 1) {
          websocketState = 1;
        }
        payload.apikey = ourAPIKey;
        return $http.post(API_Base + 'oauth2/token', payload, urlEncodedFormConfig).then(function (response) {
          return setToken(response);
        }, function (err) {
          if (redirectLogin) {
            $rootScope.checkThenSignOut();
          }
          return $q.reject(err);
        });
      },
      isTokenExpired = function () {
        var expireDate, now;
        if (!$localStorage.token || $localStorage.token.expireOverride || !$localStorage.token.expiry) {
          return true;
        }
        expireDate = new Date($localStorage.token.expiry);
        now = new Date();
        return expireDate < now;
      };

    Authentication.apiKey = ourAPIKey;

    Authentication.isTokenExpired = function () {
      return isTokenExpired();
    };

    Authentication.forgotPassword = function (email) {
      return $http.post(API_Base + 'password_reset', {apikey: ourAPIKey, email: email}, urlEncodedFormConfig).then(function (response) {
        return $q.when(response);
      }, $q.reject);
    };

    Authentication.getAccessToken = function () {
      if (!token.accesstoken && $localStorage.token) {//Try to load from local if it exists.
        token = $localStorage.token;
      }
      return token.accesstoken;
    };

    Authentication.authenticateUser = function (user) {
      if (!user || !user.email || !user.password) {
        return $q.reject({title: 'Authentication failed', template: "Email or password missing"});
      }
      return getToken({'grant_type': 'password', 'username': user.email, 'password': user.password});
    };

    Authentication.invalidateToken = function () {
      if ($localStorage.token) {
        $localStorage.token.expireOverride = true;
      }
    };

    Authentication.login = function (lastErr) {
      console.log("authentication login");
      if (!isTokenExpired()) {
        token = $localStorage.token;
        return $q.when($localStorage.token);
      }
      if ($localStorage.token) {
        console.log("using refresh token");// + $localStorage.token.refreshtoken);
        return getToken({'grant_type': 'refresh_token', 'refreshtoken': $localStorage.token.refreshtoken}, true);
      }
      $state.go('login');
      return $q.reject(lastErr || 'Could not find token.');
    };

    Authentication.openWebsocket = function () {
      if (websocketState > 0 || !window.WebSocket) {
        return;
      }

      if (!myWebsocket) {
        setInterval(function () {
          //console.log("websocket state: " + myWebsocket.readyState);
          if (websocketPingCount < 1) {
            myWebsocket.send('.');//Ping the server.
            websocketPingCount += 1;
          } else {
            //console.log("websocket should close");
            websocketPingCount = 0;
            myWebsocket.close();
          }
        }, 60000);

        newWebsocket();

        // window.test = function () {
        //   console.log("closing");
        //   myWebsocket.close();
        // };
      }
    };

    Authentication.newUser = function (newUser) {
      return $http.post(API_Base + 'account?apikey=' + ourAPIKey, newUser).then(function (response) {
        //return setToken(response);
        return response.data;
      }, $q.reject);
    };

    Authentication.logOut = function (redirectLogin) {
      console.log("logging out!");
      //Clear localstorage variable and redirect to login page.
      delete $localStorage.token;
      token = {};
      if (redirectLogin) {
        delete $localStorage.userEmail;
        delete $rootScope.userName;
        delete $rootScope.companyName;
        $state.go('login');
      }
      if (!$localStorage.token) {
        return $q.when('logged out');
      }
      return $q.reject({title: 'Error', template: 'There was an error logging out'});
    };

    return Authentication;
  }];

  angular.module('ThermaStor')
    .service('Authentication', AuthenticationDefinition);
}());
