(function () {
  'use strict';
  var snackbarFactory = ['$timeout', '$q', '$ionicTemplateLoader', '$ionicBody', '$compile', function ($timeout, $q, $ionicTemplateLoader, $ionicBody, $compile) {
    var Snackbar = {},
      snackbarInstance,
      snackbarHideTimeout,
      toggleShown = function (doShow, hideDelay, html) {
        var children = snackbarInstance.element.children();

        if (doShow) {
          children.html(html);
          $compile(children.contents())(snackbarInstance.scope);
          snackbarInstance.element.removeClass('snackbar-disappear');
          snackbarInstance.element.addClass('snackbar-appear');
          snackbarInstance.isShown = true;

          if (!snackbarInstance.isVisible) {
            snackbarInstance.element.addClass('visible');
            snackbarInstance.isVisible = true;
          }

          if (hideDelay) {
            snackbarHideTimeout = $timeout(function () {
              toggleShown(false);

              snackbarHideTimeout = $timeout(function () {
                children = snackbarInstance.element.children();
                snackbarInstance.element.removeClass('visible');
                snackbarInstance.isVisible = false;
                children.html("");
              }, 350);
            }, hideDelay);
          }
        } else {
          snackbarInstance.element.removeClass('snackbar-appear');
          snackbarInstance.element.addClass('snackbar-disappear');
          snackbarInstance.isShown = false;
        }
      },
      SNACKBAR_TPL =
        '<div class="snackbar-container">' +
          '<div class="snackbar">' +
          '</div>' +
        '</div>';

    Snackbar.show = function (options) {
      options = options || {};
      var hideDelay = options.hide || 3000,
        creationPromise = $q.resolve(),
        templatePromise = options.templateUrl ? $ionicTemplateLoader.load(options.templateUrl) : $q.resolve(options.template || '');

      $timeout.cancel(snackbarHideTimeout);
      if (!snackbarInstance) {
        creationPromise = $ionicTemplateLoader.compile({
          template: SNACKBAR_TPL,
          appendTo: $ionicBody.get()
        }).then(function (self) {
          snackbarInstance = self;
        });
      }
      $q.all([templatePromise, creationPromise]).then(function (results) {
        var html = results[0];

        if (!options.omitImage) {
          html = '<span class="icon ion-checkmark balanced float-left pad-left"></span>' + html;
        }

        if (snackbarInstance.isShown) {
          toggleShown(false);

          snackbarHideTimeout = $timeout(function () {
            toggleShown(true, hideDelay, html);
          }, 350);
        } else {
          toggleShown(true, hideDelay, html);
        }
      });
    };

    return Snackbar;
  }];

  angular.module('ThermaStor')
    .service('Snackbar', snackbarFactory);
}());
