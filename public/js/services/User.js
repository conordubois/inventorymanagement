(function () {
  'use strict';
  var userFactory = ['$http', '$q', 'API_Base', 'Integration_Base', 'Authentication', '$localStorage', '$rootScope', 'Snackbar', 'Dehumidifier', function ($http, $q, API_Base, Integration_Base, Authentication, $localStorage, $rootScope, Snackbar, Dehumidifier) {
    var User = {},
      july2017Cutoff = 1500000000,
      getTimeString = function (timeSecs) {
        if (!timeSecs || timeSecs < july2017Cutoff) {
          return undefined;
        }

        var aDate = new Date(timeSecs * 1000),
          time = aDate.toLocaleTimeString();

        time = time.replace(/\u200E/g, '');//from https://stackoverflow.com/questions/17913681
        time = time.replace(/^([^\d]*\d{1,2}:\d{1,2}):\d{1,2}([^\d]*)$/, '$1$2');
        return time + " " + aDate.toLocaleDateString();
      },
      findTrueStatus = function (device) {
        if (device.errorStatus) {
          device.trueStatus = 'Err ' + device.errorStatus + ': ';
          if (device.errorStatus === 1) {
            device.trueStatus += 'equip GUID is missing';
          } else if (device.errorStatus === 2) {
            device.trueStatus += 'DryTAG attached to another equipment';
          } else if (device.errorStatus === 3) {
            device.trueStatus += 'DryTAG UUID does not exist';
          } else if (device.errorStatus === 4) {
            device.trueStatus += 'DryTAG owned by another company';
          } else if (device.errorStatus === 5) {
            device.trueStatus += 'BLE dehu S/N not found';
          } else if (device.errorStatus === 6) {
            device.trueStatus += 'BLE dehu S/N owned by another company';
          } else if (device.errorStatus === 7) {
            device.trueStatus += 'cannot change equip category';
          } else if (device.errorStatus === 8) {
            device.trueStatus += 'DryTAG UUID can only be changed by DryLINK';
          } else if (device.errorStatus === 9) {
            device.trueStatus += 'cannot change serial number for BLE factory equip';
          } else if (device.errorStatus === 10) {
            device.trueStatus += 'ownership of unit has been released by DryLINK';
          } else if (device.errorStatus === 11) {
            device.trueStatus += 'serial number is missing';
          } else if (device.errorStatus === 12) {
            device.trueStatus += 'previously assigned equip not found';
          } else if (device.errorStatus === 13) {
            device.trueStatus += 'serial number not unique for class/type';
          } else if (device.errorStatus === 14) {
            device.trueStatus += 'Site Guid is missing or not found';
          } else if (device.errorStatus === 15) {
            device.trueStatus += 'Equipment name is required';
          } else if (device.errorStatus === 16) {
            device.trueStatus += 'Equipment class is missing';
          } else {
            device.trueStatus += 'Error unknown';
          }
        } else if (device.syncStatus === -1) {//Why does Diane make some error statuses negative and others positive?
          device.trueStatus = 'Err - never synced';
        } else if (device.syncStatus === 3 || device.syncStatus === 4) {//Some questions will never have an answer.
          device.trueStatus = 'Err unknown - sync failed';
        } else {
          device.trueStatus = 0;
        }
      },
      requiredFields = ['email', 'pwd', 'companyName'],
      validFields = requiredFields.concat(['hasEmail', 'hasSMS', 'hasPush', 'isCelsius', 'isGrams', 'isMeters', 'phone', 'firstName', 'lastName', 'companyId', 'userType']),
      validCompanyFields = ['companyName', 'address', 'city', 'state', 'zip', 'phone'],
      validSyncField = ["dehuId", "cequipId", "msensorId", "equipName", "serialNumber", "equipStatus", "typeName", "equipClass",
        "equipType", "manufacturer", "model", "drytagUuid", "string"],
      formatCompany = function (company) {
        var i, payload;

        payload = angular.copy(company);

        for (i in payload) {
          if (payload.hasOwnProperty(i) && validCompanyFields.indexOf(i) === -1) {
            delete payload[i];
          }
        }
        return $q.when(payload);
      },
      formatSyncObject = function (syncObject) {
        var i, payload;

        payload = angular.copy(syncObject);
        for (i in payload) {
          if (payload.hasOwnProperty(i) && validSyncField.indexOf(i) === -1) {
            delete payload[i];
          }
        }

        return payload;
      },
      /**
      * formatUserObject
      * @param {object} user - user object from sign up or edit preferences forms
      * @return {object} payload - a formatted object wrapped in promise
      */
      formatUserObject = function (user) {
        var i, payload;
        // if (user.password !== user.confirmPassword) {
        //   return $q.reject({title: 'Invalid Password', template: '"Confirm Password" does not match "Password"'});
        // }

        if (!user) {
          return $q.reject({title: 'Could not complete operation', template: "User information not found"});
        }

        payload = angular.copy(user);
        if (payload.password) {
          payload.pwd = payload.password;
        }

        for (i in payload) {
          if (payload.hasOwnProperty(i) && validFields.indexOf(i) === -1) {
            delete payload[i];
          }
        }
        for (i = 0; i < requiredFields.length; i += 1) {
          if (!payload.hasOwnProperty(requiredFields[i]) && (requiredFields[i] !== 'companyName' || !payload.hasOwnProperty('companyId'))) {
            return $q.reject({title: "Error saving user", template: requiredFields[i] + " is a required field"});
          }
        }
        return $q.when(payload);
      };

    User.get = function () {
      return $http.get(API_Base + 'account').then(function (response) {
        var accountData = response.data || {};

        if (accountData.firstName && accountData.lastName) {
          $rootScope.userName = accountData.firstName + " " + accountData.lastName;
        } else if (accountData.firstName) {
          $rootScope.userName = accountData.firstName;
        } else if (accountData.lastName) {
          $rootScope.userName = accountData.lastName;
        } else if (accountData.email) {
          $rootScope.userName = accountData.email;
        }
        if (accountData.companyName) {
          $rootScope.companyName = accountData.companyName;
        }
        $localStorage.useMeters = accountData.isMeters;
        $localStorage.userName = $rootScope.userName;
        return accountData;
      }, $q.reject);
    };

    User.getPossibleIntegrations = function () {
      return $http.get(API_Base + 'thirdp/client/list?apikey=' + Authentication.apiKey).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    User.getExistingIntegrations = function () {
      return $http.get(API_Base + 'thirdp/company/key').then(function (response) {
        return response.data;
      }, $q.reject);
    };

    User.addIntegration = function (clientId, isNew) {
      if (isNew) {
        return $http.post(API_Base + 'thirdp/company/key?clientId=' + clientId).then(function (response) {
          return response.data;
        }, $q.reject);
      }
      return $http.put(API_Base + 'thirdp/company/key?clientId=' + clientId + '&isEnabled=true').then(function (response) {
        var message = response && response.data && response.data.message;
        if (message === "Success") {
          return {integrationReady: true};
        }
        return {integrationReady: false};
      }, $q.reject);
    };

    User.deleteIntegration  = function (clientId) {
      return $http.put(API_Base + 'thirdp/company/key?clientId=' + clientId + '&isEnabled=false').then(function (response) {
        var message = response && response.data && response.data.message;
        if (message === "Success") {
          return {integrationReady: true};
        }
        return {integrationReady: false};
      }, $q.reject);
    };

    User.getSyncStatus = function (clientId) {
      return $http.get(Integration_Base + 'syncequip?clientId=' + clientId).then(function (response) {
        var data = response.data || [],
          i,
          errors = [],
          synced = [];
        for (i = 0; i < data.length; i += 1) {
          if (data[i]) {
            findTrueStatus(data[i]);

            if (data[i].trueStatus) {
              errors.push(data[i]);
            } else {
              synced.push(data[i]);
            }
          }
        }
        return {errors: errors, synced: synced};
      }, $q.reject);
    };

    User.updateSyncStatus = function (clientId, updateObject) {
      var formattedObject = formatSyncObject(updateObject);
      return $http.post(Integration_Base + 'syncequip?clientId=' + clientId, formattedObject).then(function (response) {
        findTrueStatus(response.data);
        if (response.data && !response.data.trueStatus) {
          Snackbar.show({template: updateObject.equipName + " synced", hide: 2500});
        }
        return response.data;
      }, $q.reject);
    };

    User.getCompany = function (myId) {
      var companyRecords = {employees: [], me: [], invitations: [], emailList: []};

      return $http.get(API_Base + 'company').then(function (company) {
        if (company && company.data) {
          companyRecords.company = company.data;
          companyRecords.company.fullLocation = "";
          if (companyRecords.company.city) {
            companyRecords.company.fullLocation += companyRecords.company.city;
          }
          if (companyRecords.company.state) {
            if (companyRecords.company.fullLocation.length) {
              companyRecords.company.fullLocation += ", ";
            }
            companyRecords.company.fullLocation += companyRecords.company.state;
          }
          companyRecords.company.fullLocation += " " + companyRecords.company.zip;
        }

        return $http.get(API_Base + 'account/employee').then(function (employees) {
          var i;
          if (employees && employees.data && employees.data.length) {
            for (i = 0; i < employees.data.length; i += 1) {
              if (employees.data[i] && employees.data[i].email) {
                companyRecords.emailList.push(employees.data[i].email);
              }

              if (employees.data[i] && String(employees.data[i].accountId) === String(myId)) {
                companyRecords.me.push(employees.data[i]);
              } else {
                companyRecords.employees.push(employees.data[i]);
              }
            }
          }

          return $http.get(API_Base + 'invite').then(function (invitations) {
            var j,
              emailList = [],
              emailIndex,
              currentTime = (new Date()).getTime() / 1000;

            if (invitations && invitations.data && invitations.data.length) {
              for (j = 0; j < invitations.data.length; j += 1) {
                if (invitations.data[j] && invitations.data[j].inviteeEmail && invitations.data[j].inviteeEmail.length) {
                  emailIndex = emailList.indexOf(invitations.data[j].inviteeEmail);
                  if (emailIndex < 0) {
                    invitations.data[j].completed = companyRecords.emailList.indexOf(invitations.data[j].inviteeEmail) > -1;
                    companyRecords.invitations.push(invitations.data[j]);
                    emailList.push(invitations.data[j].inviteeEmail);
                  } else if (companyRecords.invitations[emailIndex].expireSecs < invitations.data[j].expireSecs) {
                    invitations.data[j].completed = companyRecords.invitations[emailIndex].completed;
                    companyRecords.invitations[emailIndex] = invitations.data[j];
                  }
                }
              }

              if (companyRecords.invitations && companyRecords.invitations.length) {
                for (j = 0; j < companyRecords.invitations.length; j += 1) {
                  if (companyRecords.invitations[j]) {
                    if (!companyRecords.invitations[j].expireSecs || companyRecords.invitations[j].expireSecs < currentTime) {
                      companyRecords.invitations[j].expired = true;
                    } else {
                      companyRecords.invitations[j].expired = false;
                    }
                  }
                }
              }
            }

            return $q.resolve(companyRecords);
          }, function () {
            return $q.resolve(companyRecords);
          });
        }, $q.reject);
      }, $q.reject);
    };

    User.updateCompany = function (company) {
      return formatCompany(company).then(function (formattedCompany) {
        return $http.put(API_Base + 'company', formattedCompany).then(function (response) {
          return response.data;
        }, $q.reject);
      });
    };

    User.getCompanyLogo = function () {
      return $http.get(API_Base + 'account/company_logo').then(function (response) {
        return response.data;
      }, $q.reject);
    };

    User.uploadCompanyLogo = function (newFile) {
      if (!newFile || !newFile.name) {
        return $q.reject({message: "Could not create upload logo without file.", status: 400});
      }
      var formData = new FormData();
      formData.append('file', newFile);

      return $http.post(API_Base + 'account/company_logo', formData, {
        headers: {'Content-Type': undefined},
        transformResponse: angular.identity
      }).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    User.getWeather = function () {
      if (!$localStorage.location || (!$localStorage.location.latitude && !$localStorage.location.longitude)) {
        return $q.reject(-1);
      }

      return $http.get(API_Base + 'weather?lat=' + $localStorage.location.latitude + '&lng=' + $localStorage.location.longitude).then(function (response) {
        if (!response || !response.data) {
          return $q.reject(0);
        }
        var allData = response.data,
          tC = (allData.temperature - 32.0) * 5.0 / 9.0,
          vaporPressure = (6.116441 * Math.pow(10, ((7.591386 * tC) / (tC + 240.7263)))) * allData.rh / 100;//These calculations are from the Sherpa project psychro.c library.

        allData.water =  4354.0 * vaporPressure / (998.0 - vaporPressure);//Measured in grains per pound.
        allData.dewpoint = 240.7263 / ((7.591386 / Math.log10(vaporPressure / 6.116441)) - 1.0);//Measured in Celsius.
        if (allData.isCelsius) {
          allData.temperature = tC;
        } else {
          allData.dewpoint = (allData.dewpoint * 9.0 / 5.0) + 32.0;
        }
        if (allData.isGrams) {
          allData.water /= 7.0;
        }
        return allData;
      }, function () {
        return $q.reject(0);
      });
    };

    User.authenticate = function (user) {
      return Authentication.authenticateUser(user).then(function () {
        return User.get();
      }, $q.reject);
    };

    User.create = function (user) {
      return formatUserObject(user).then(function (formattedUser) {
        return Authentication.newUser(formattedUser);
      }, $q.reject);
    };

    User.update = function (user) {
      return formatUserObject(user).then(function (formattedUser) {
        return $http.put(API_Base.replace(/v1/g, 'v2') + 'account', formattedUser).then(function (response) {
          return response.data;
        }, function (err) {
          if (err && err.data) {
            err.errors = err.data.errors ? err.data.errors.join("<br>") : err.data.message || err.data;
          }
          return $q.reject(err);
        });
      }, $q.reject);
    };

    User.cancelAccount = function (accountId) {
      return $http.delete(API_Base + 'account/' + accountId + '?apikey=' + Authentication.apiKey).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    User.retryEmail = function (emailAddress) {
      return $http.post(API_Base + 'account/email_verify?apikey=' + Authentication.apiKey + '&email=' + encodeURIComponent(emailAddress)).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    User.deleteUser = function () {
      return $http.delete(API_Base + 'account').then(function (response) {
        return response.data;
      }, $q.reject);
    };

    User.removeEmployee = function (employeeId) {
      return $http.delete(API_Base + 'account/employee/' + employeeId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    User.updateEmployee = function (employeeId, role) {
      if (role === 'owner') {
        return $http.put(API_Base + 'account/owner?employeeId=' + employeeId).then(function (response) {
          return response.data;
        }, $q.reject);
      }
      return $http.put(API_Base + 'account/employee/' + employeeId + "?role=" + role).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    User.sendInvitation = function (email, role) {
      return $http.post(API_Base + 'invite?email=' + encodeURIComponent(email) + "&role=" + role).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    User.checkForInvitations = function (email) {
      return $http.get(API_Base + 'invite/employee?apikey=' + Authentication.apiKey + '&email=' + encodeURIComponent(email)).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    User.getLegalStuff = function () {
      return $http.get(API_Base + 'account/consent').then(function (response) {
        var i,
          unfilteredStuff = (response && response.data) || [],
          legalStuff = [];

        for (i = 0; i < unfilteredStuff.length; i += 1) {
          if (unfilteredStuff[i] && unfilteredStuff[i].docId && unfilteredStuff[i].docId > -1 && unfilteredStuff[i].docText) {
            legalStuff.push(unfilteredStuff[i]);
          }
        }
        return legalStuff;
      }, $q.reject);
    };

    User.legalAgreement = function (legalId) {
      return $http.put(API_Base + 'account/consent/' + legalId).then(function (response) {
        return response && response.data;
      }, $q.reject);
    };

    User.alterJobAlerts = function (jobId, enable) {
      if (!jobId) {
        return $q.reject({message: "No job ID found.", status: 400});
      }

      return $http.put(API_Base + 'alerts/job/' + jobId + '/status?isEnabled=' + !!enable).then(function (response) {
        return response && response.data;
      }, $q.reject);
    };

    User.getJobAlerts = function (jobId) {
      if (!jobId) {
        return $q.reject({message: "No job ID found.", status: 400});
      }

      return $http.get(API_Base + 'alerts/job/' + jobId + '/status').then(function (response) {
        var status = (response && response.data) || {};
        return $http.get(API_Base.replace(/v1/g, 'v2') + 'alerts/job/' + jobId).then(function (response2) {
          var alertInfo = (response2 && response2.data) || {},
            unseenAlertCount = 0,
            offSettings = [],
            rhSettings = [],
            tempSettings = [],
            miscellaneousSettings = [],
            aDevice,
            i;

          if (!$localStorage.seenJobAlertIds || !$localStorage.seenJobAlertIds.length) {
            $localStorage.seenJobAlertIds = [];
          }
          if (alertInfo && alertInfo.settings && alertInfo.settings.length) {
            for (i = 0; i < alertInfo.settings.length; i += 1) {
              if (alertInfo.settings[i] && alertInfo.settings[i].ruleName && alertInfo.settings[i].ruleName.indexOf) {
                alertInfo.settings[i].ruleName = alertInfo.settings[i].ruleName.replace(/Dercrease/g, "Decrease");
                if (alertInfo.settings[i].ruleName === "Equipment Turned Off") {
                  alertInfo.settings[i].ruleText = "Send an alert if a piece of equipment is powered off when on a job.";
                } else if (alertInfo.settings[i].ruleName === "DryTAG Offline") {
                  alertInfo.settings[i].ruleText = "Send an alert if a DryTAG stops broadcasting information over Bluetooth for more than the duration below.";
                } else if (alertInfo.settings[i].ruleName === "Dehu Offline") {
                  alertInfo.settings[i].ruleText = "Send an alert if a DryMAX goes offline and stops uploading data for more than the duration below.";
                } else if (alertInfo.settings[i].ruleName === "Low Chamber RH") {
                  alertInfo.settings[i].ruleText = "Send an alert when the RH of the chamber goes below the threshold below according to Bluetooth equipment in the chamber.";
                } else if (alertInfo.settings[i].ruleName === "High Chamber RH") {
                  alertInfo.settings[i].ruleText = "Send an alert when the RH of the chamber goes above the threshold below according to Bluetooth equipment in the chamber.";
                } else if (alertInfo.settings[i].ruleName === "Low Chamber Temperature") {
                  alertInfo.settings[i].ruleText = "Send an alert when the temperature of the chamber goes below the threshold below according to Bluetooth equipment in the chamber.";
                } else if (alertInfo.settings[i].ruleName === "High Chamber Temperature") {
                  alertInfo.settings[i].ruleText = "Send an alert when the temperature of the chamber goes above the threshold below according to Bluetooth equipment in the chamber.";
                } else if (alertInfo.settings[i].ruleName === "Chamber GPP Slow Decrease") {
                  alertInfo.settings[i].ruleText = "Send an alert when the absolute humidity of the chamber does not go below the threshold below within the duration specified according to Bluetooth equipment in the chamber.";
                } else if (alertInfo.settings[i].ruleName === "Chamber Drying Goal Reached") {
                  alertInfo.settings[i].ruleText = "Send an alert when DrySENSE measurements show that the moisture content goal for the chamber has been reached.";
                }
                if (alertInfo.settings[i].ruleName.indexOf("Off") > -1) {
                  alertInfo.settings[i].convertedThreshold = alertInfo.settings[i].threshold;
                  offSettings.push(alertInfo.settings[i]);
                } else if (alertInfo.settings[i].ruleName.indexOf("RH") > -1) {
                  alertInfo.settings[i].convertedThreshold = alertInfo.settings[i].threshold;
                  alertInfo.settings[i].thresholdUnits = "%";
                  rhSettings.push(alertInfo.settings[i]);
                } else if (alertInfo.settings[i].ruleName.indexOf("Temperature") > -1) {
                  if (alertInfo.settings[i].threshold > -100) {
                    if ($localStorage.useCelsius) {
                      alertInfo.settings[i].convertedThreshold = Math.round(5 * (alertInfo.settings[i].threshold - 32) / 9);
                      alertInfo.settings[i].thresholdUnits = "°C";
                    } else {
                      alertInfo.settings[i].convertedThreshold = alertInfo.settings[i].threshold;
                      alertInfo.settings[i].thresholdUnits = "°F";
                    }
                  } else {
                    alertInfo.settings[i].convertedThreshold = alertInfo.settings[i].threshold;
                  }
                  tempSettings.push(alertInfo.settings[i]);
                } else {
                  if (alertInfo.settings[i].threshold > -100 && alertInfo.settings[i].ruleName.indexOf("GPP") > -1) {
                    if ($localStorage.useGrams) {
                      alertInfo.settings[i].convertedThreshold = Math.round(alertInfo.settings[i].threshold / 7);
                      alertInfo.settings[i].thresholdUnits = "g/kg";
                    } else {
                      alertInfo.settings[i].convertedThreshold = alertInfo.settings[i].threshold;
                      alertInfo.settings[i].thresholdUnits = "gpp";
                    }
                  } else {
                    alertInfo.settings[i].convertedThreshold = alertInfo.settings[i].threshold;
                  }
                  miscellaneousSettings.push(alertInfo.settings[i]);
                }
              }
            }
            alertInfo.settings = offSettings.concat(rhSettings, tempSettings, miscellaneousSettings);
          }
          if (alertInfo && alertInfo.equipment && alertInfo.equipment.length) {
            for (i = 0; i < alertInfo.equipment.length; i += 1) {
              if (alertInfo.equipment[i]) {
                if (alertInfo.equipment[i].beaconId && alertInfo.equipment[i].beaconId > -1) {
                  alertInfo.equipment[i].hasBeacon = true;
                }
                if (alertInfo.equipment[i].dehuId && alertInfo.equipment[i].dehuId > -1) {
                  aDevice = $rootScope.searchBluetoothListsForMatch('databaseId', alertInfo.equipment[i].dehuId);
                  if (aDevice && aDevice.modelType) {
                    alertInfo.equipment[i].modelType = aDevice.modelType;
                  } else if (alertInfo.equipment[i].typeName === "Dehu LGR - L") {
                    alertInfo.equipment[i].modelType = 'Dry Max';
                  } else {
                    alertInfo.equipment[i].modelType = 'Dry Max XL';
                  }
                }
                Dehumidifier.determineIconCategory(alertInfo.equipment[i]);
              }
            }
          }
          if (alertInfo && alertInfo.alerts && alertInfo.alerts.length) {
            for (i = 0; i < alertInfo.alerts.length; i += 1) {
              if (alertInfo.alerts[i]) {
                if (alertInfo.alerts[i].endSecs) {
                  alertInfo.alerts[i].shouldShow = false;
                  alertInfo.alerts[i].endTimeString = getTimeString(alertInfo.alerts[i].endSecs);
                } else {
                  alertInfo.alerts[i].shouldShow = true;
                  if (alertInfo.alerts[i].alertId && $localStorage.seenJobAlertIds.indexOf(alertInfo.alerts[i].alertId) < 0) {
                    unseenAlertCount += 1;
                    alertInfo.alerts[i].isNew = true;
                  }
                }
                if (alertInfo.alerts[i].conditionMetSecs) {
                  alertInfo.alerts[i].timeString = getTimeString(alertInfo.alerts[i].conditionMetSecs);
                }
                if (alertInfo.alerts[i].alertName === "Chamber Drying Goal Reached") {
                  alertInfo.alerts[i].isGood = true;
                }
              }
            }
          }
          if (unseenAlertCount > 99) {
            unseenAlertCount = 99;
          } else if (unseenAlertCount < 0) {
            unseenAlertCount = 0;
          }
          alertInfo.unseenAlertCount = unseenAlertCount;
          alertInfo.isEnabled = status.isEnabled;
          return alertInfo;
        }, $q.reject);
      }, $q.reject);
    };

    User.getAlerts = function (type) {
      if (type === 'equipment') {
        return $http.get(API_Base + 'alerts/equip').then(function (response) {
          var alertSettings = (response && response.data) || {},
            unseenAlertCount = 0,
            aDevice,
            i;

          if (!$localStorage.seenEquipmentAlertIds || !$localStorage.seenEquipmentAlertIds.length) {
            $localStorage.seenEquipmentAlertIds = [];
          }
          if (alertSettings && alertSettings.settings && alertSettings.settings.length) {
            for (i = 0; i < alertSettings.settings.length; i += 1) {
              if (alertSettings.settings[i]) {
                if (alertSettings.settings[i].ruleName === "Reservoir full") {
                  alertSettings.settings[i].realName = "Reservoir full";
                  alertSettings.settings[i].ruleText = "Send notifications when dehumidifier reservoirs are full.";
                } else if (alertSettings.settings[i].ruleName === "System Protect") {
                  alertSettings.settings[i].realName = "System Protection";
                  alertSettings.settings[i].ruleText = "Send notifications when dehumidifiers enter system protection mode because of high temperatures or low refrigerant.";
                } else if (alertSettings.settings[i].ruleName === "Battery Low") {
                  alertSettings.settings[i].realName = "Low Battery";
                  alertSettings.settings[i].ruleText = "Send notifications when DryTAG or DrySENSE batteries need to be replaced.";
                } else if (alertSettings.settings[i].ruleName === "Filter Change Due") {
                  alertSettings.settings[i].realName = "Filter Change";
                  alertSettings.settings[i].ruleText = "Send notifications when dehumidifier filters need to be replaced.";
                } else if (alertSettings.settings[i].ruleName === "Equipment Not Connected" || alertSettings.settings[i].ruleName === "Not Connected" ||
                           (alertSettings.settings[i].ruleName && alertSettings.settings[i].ruleName.indexOf && alertSettings.settings[i].ruleName.indexOf('Equipment Missing') > -1)) {
                  alertSettings.settings[i].realName = "Not Connected";
                  alertSettings.settings[i].ruleText = "Send notifications when equipment has not been scanned over Bluetooth for more than the number of days below.";
                } else {
                  alertSettings.settings[i].realName = alertSettings.settings[i].ruleName;
                }
              }
            }
          }
          if (alertSettings && alertSettings.alerts && alertSettings.alerts.length) {
            for (i = 0; i < alertSettings.alerts.length; i += 1) {
              if (alertSettings.alerts[i]) {
                if (alertSettings.alerts[i].beaconId && alertSettings.alerts[i].beaconId > -1) {
                  alertSettings.alerts[i].hasBeacon = true;
                }
                if (alertSettings.alerts[i].conditionMetSecs) {
                  alertSettings.alerts[i].timeString = getTimeString(alertSettings.alerts[i].conditionMetSecs);
                }
                if (alertSettings.alerts[i].eqAlertId && $localStorage.seenEquipmentAlertIds.indexOf(alertSettings.alerts[i].eqAlertId) < 0) {
                  unseenAlertCount += 1;
                  alertSettings.alerts[i].isNew = true;
                }
                if (alertSettings.alerts[i].dehuId && alertSettings.alerts[i].dehuId > -1) {
                  aDevice = $rootScope.searchBluetoothListsForMatch('databaseId', alertSettings.alerts[i].dehuId);
                  if (aDevice && aDevice.modelType) {
                    alertSettings.alerts[i].modelType = aDevice.modelType;
                  } else if (alertSettings.alerts[i].typeName === "Dehu LGR - L") {
                    alertSettings.alerts[i].modelType = 'Dry Max';
                  } else {
                    alertSettings.alerts[i].modelType = 'Dry Max XL';
                  }
                }
                Dehumidifier.determineIconCategory(alertSettings.alerts[i]);
              }
            }
          }
          $rootScope.unseenEquipmentAlertCount = unseenAlertCount;
          if (!$rootScope.unseenJobAlertCount) {
            $rootScope.unseenJobAlertCount = 0;
          }
          unseenAlertCount = $rootScope.unseenEquipmentAlertCount + $rootScope.unseenJobAlertCount;
          if (unseenAlertCount > 99) {
            unseenAlertCount = 99;
          } else if (unseenAlertCount < 0) {
            unseenAlertCount = 0;
          }
          $rootScope.unseenTotalAlertCount = unseenAlertCount;
          return alertSettings;
        }, $q.reject);
      }
      if (type === 'jobStatus') {
        return $http.get(API_Base + 'alerts/jobstatus').then(function (response) {
          var alertSettings = (response && response.data) || [],
            totalUnseenAlertCount = 0,
            unseenAlertCount = 0,
            i,
            j;

          if (!$localStorage.seenJobAlertIds || !$localStorage.seenJobAlertIds.length) {
            $localStorage.seenJobAlertIds = [];
          }
          if (alertSettings && alertSettings.length) {
            for (i = 0; i < alertSettings.length; i += 1) {
              if (alertSettings[i]) {
                unseenAlertCount = 0;
                if (alertSettings[i].alerts && alertSettings[i].alerts.length) {
                  for (j = 0; j < alertSettings[i].alerts.length; j += 1) {
                    if (alertSettings[i].alerts[j]) {
                      if (!alertSettings[i].alerts[j].endSecs) {
                        alertSettings[i].shouldShow = true;
                        if (alertSettings[i].alerts[j].alertId && $localStorage.seenJobAlertIds.indexOf(alertSettings[i].alerts[j].alertId) < 0) {
                          unseenAlertCount += 1;
                          alertSettings[i].alerts[j].isNew = true;
                        }
                      }
                      if (alertSettings[i].alerts[j].conditionMetSecs) {
                        alertSettings[i].alerts[j].timeString = getTimeString(alertSettings[i].alerts[j].conditionMetSecs);
                      }
                      if (alertSettings[i].alerts[j].alertName === "Chamber Drying Goal Reached") {
                        alertSettings[i].alerts[j].isGood = true;
                      }
                    }
                  }
                }
                if (unseenAlertCount > 99) {
                  unseenAlertCount = 99;
                } else if (unseenAlertCount < 0) {
                  unseenAlertCount = 0;
                }
                alertSettings[i].unseenAlertCount = unseenAlertCount;
                totalUnseenAlertCount += unseenAlertCount;
              }
            }
          }
          $rootScope.unseenJobAlertCount = totalUnseenAlertCount;
          if (!$rootScope.unseenEquipmentAlertCount) {
            $rootScope.unseenEquipmentAlertCount = 0;
          }
          unseenAlertCount = $rootScope.unseenEquipmentAlertCount + $rootScope.unseenJobAlertCount;
          if (unseenAlertCount > 99) {
            unseenAlertCount = 99;
          } else if (unseenAlertCount < 0) {
            unseenAlertCount = 0;
          }
          $rootScope.unseenTotalAlertCount = unseenAlertCount;
          return alertSettings;
        }, $q.reject);
      }
      if (type === 'management') {
        return $http.get(API_Base + 'alerts/jobmgmt').then(function (response) {
          var alertSettings = (response && response.data) || {},
            i;

          if (alertSettings && alertSettings.settings && alertSettings.settings.length) {
            for (i = 0; i < alertSettings.settings.length; i += 1) {
              if (alertSettings.settings[i]) {
                if (alertSettings.settings[i].ruleName === "Job Created") {
                  alertSettings.settings[i].realName = "New Job Started";
                  alertSettings.settings[i].ruleText = "Send notifications when jobs are created.";
                } else if (alertSettings.settings[i].ruleName === "Job Completed") {
                  alertSettings.settings[i].realName = "Job Completed";
                  alertSettings.settings[i].ruleText = "Send notifications when jobs are ended.";
                } else if (alertSettings.settings[i].ruleName && alertSettings.settings[i].ruleName.indexOf &&
                           alertSettings.settings[i].ruleName.indexOf('More than') > -1 && alertSettings.settings[i].ruleName.indexOf('Job Days') > -1) {
                  alertSettings.settings[i].realName = "Job Duration";
                  alertSettings.settings[i].ruleText = "Send notifications when jobs take longer than the number of days below.";
                } else {
                  alertSettings.settings[i].realName = alertSettings.settings[i].ruleName;
                }
              }
            }
          }
          return alertSettings;
        }, $q.reject);
      }
      return $q.resolve({});
    };

    User.updateAlertRule = function (alert, type) {
      var typeString = type === 'management' ? 'jobmgmt' : type === 'equipment' ? 'equip' : 'job';
      if (!alert || !alert.settingId || !(alert.settingId > -1)) {
        return $q.reject({message: "Setting ID required.", status: 400});
      }

      return $http.put(API_Base + 'alerts/' + typeString + '/rule/' + alert.settingId + '?isEnabled=' + alert.isEnabled + '&duration=' +
                       alert.duration + '&threshold=' + alert.threshold).then(function (response) {
        Snackbar.show({template: "Alert setting updated", hide: 2500});
        return (response && response.data) || {};
      }, $q.reject);
    };

    User.updateJobAlertEquipment = function (alert) {
      if (!alert || !alert.jobAlertEquipId) {
        return $q.reject({message: "Equipment ID required.", status: 400});
      }

      return $http.put(API_Base + 'alerts/job/equip/' + alert.jobAlertEquipId + '?isEnabled=' + alert.isEnabled).then(function (response) {
        return (response && response.data) || {};
      }, $q.reject);
    };

    User.updateAlertUsers = function (users, type) {
      var typeString = type === 'management' ? 'jobmgmt' : type === 'equipment' ? 'equip' : 'job',
        updateAUser = function (start) {
          var i;

          if (!users || !users.length || !(users.length > start)) {
            return $q.resolve();
          }
          for (i = start; i < users.length; i += 1) {
            if (users[i] && users[i].hasBeenChanged && users[i].alactId) {
              return $http.put(API_Base + 'alerts/' + typeString + '/account/' + users[i].alactId + '?isEnabled=' + users[i].isEnabled).then(function () {
                return updateAUser(i + 1);
              }, $q.reject);
            }
          }
          return $q.resolve();
        };
      if (!users || !users.length) {
        return $q.reject({message: "User list required.", status: 400});
      }

      return updateAUser(0).then(function () {
        Snackbar.show({template: "Alert users updated", hide: 2500});
        return $q.resolve();
      }, $q.reject);
    };

    return User;
  }];

  angular.module('ThermaStor')
    .service('User', userFactory);
}());
