(function () {
  'use strict';
  var dehuFactory = ['$http', 'API_Base', '$q', '$localStorage', '$rootScope', 'Authentication', 'Snackbar', function ($http, API_Base, $q, $localStorage, $rootScope, Authentication, Snackbar) {
    var Dehumidifier = {},
      uploadLocks = {},
      startCallback,
      successCallback,
      errorCallback,
      commandNames = ['pump', 'resetHours', 'rhOffset', 'tempOffset', 'setpoint', 'nameStart', 'nameEnd', 'fanMode', 'sleepMins', 'dehuState', 'offsetTime',
        'runTest', 'quietMode', 'xlMode', 'shutdownTime', 'startProgramming', 'programPowerBoard', 'storeCTSNumber', 'resetFloatFailure'],
      allFFString = String.fromCharCode(0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF),
      unfurlBase64Blocks = function (list) {
        var unfurledList = [],
          decodedChunk,
          decodedSample,
          i,
          j;

        if (!list || !list.length) {
          return [];
        }

        for (i = 0; i < list.length; i += 1) {
          decodedChunk = window.atob(list[i]);
          for (j = 0; j <= decodedChunk.length - 16; j += 16) {
            decodedSample = decodedChunk.slice(j, j + 16);
            if (decodedSample !== allFFString) {
              unfurledList.push(window.btoa(decodedSample));
            }
          }
        }
        return unfurledList;
      },
      myTrim = function (aString) {
        if (aString && aString.length) {
          return aString.replace(/\0/g, '');
        }
        return aString;
      },
      renameDrySENSEsamples = function (uploadObject, isOneHourDrySENSE) {
        var newObject = angular.copy(uploadObject);
        if (isOneHourDrySENSE) {
          newObject.oneHourSamples = newObject.samples;
          delete newObject.samples;
        }
        return newObject;
      },
      uploadHistoricalDataContinuously = function (device) {
        if (uploadLocks[device.address] === -1) {//Currently uploading data, already.
          return;
        }
        if (uploadLocks[device.address]) {
          clearTimeout(uploadLocks[device.address]);
        }
        uploadLocks[device.address] = -1;//Currently trying to upload data.

        if (device.checkUpload || device.downloading || !device.serialNumber || !device.timestamp || !device.data || !device.data.length) {
          if ((!device.timestamp || device.timestamp < 1) && device.data && device.data.length) {
            device.uploadQueue = [];
            device.data = [];
            device.settingsChanged = [];
            device.dataLength = undefined;
            if (errorCallback && ((typeof errorCallback) === "function")) {
              errorCallback(device, "negative timestamp -- deleting data");
            }
            uploadLocks[device.address] = setTimeout(function () {
              uploadHistoricalDataContinuously(device);
            }, 5000 + 10000 * Math.random());//Try again after about 10 seconds.
            return;
          }
          uploadLocks[device.address] = setTimeout(function () {
            uploadHistoricalDataContinuously(device);
          }, 1000 + 1000 * Math.random());//Try again after about a second.
          return;
        }
        console.log("upload data " + device.serialNumber + ", " + device.data.length);
        console.log(JSON.stringify(device.data));

        var data = {},
          aTime = {},
          endOfQueue = 0,
          cancelUpload = $q.defer(),
          uploadFinished = false,
          beginningOfData;
        if (startCallback && ((typeof startCallback) === "function")) {
          startCallback(device);
        }

        data.deviceId = device.serialNumber;
        if (device.uploadQueue && device.uploadQueue.length) {
          endOfQueue = device.uploadQueue.length - 1;
          data.samplingInterval = device.uploadQueue[endOfQueue].samplingMins || 15;
        } else {
          data.samplingInterval = 15;
        }
        data.times = [];
        if (device.settingsChanged && device.settingsChanged.length) {
          data.settingsChanged = unfurlBase64Blocks(device.settingsChanged);
        } else {
          data.settingsChanged = [];
        }
        for (0; endOfQueue >= 0 && device.uploadQueue && device.uploadQueue.length && device.uploadQueue[endOfQueue].samplingMins === data.samplingInterval; endOfQueue -= 1) {
          aTime = {timestamp: device.uploadQueue[endOfQueue].timestamp, jobHours: device.uploadQueue[endOfQueue].jobHours, defrostCycles: device.uploadQueue[endOfQueue].defrostCycles,
            pumpCycles: device.uploadQueue[endOfQueue].pumpCycles, sampleNo: device.uploadQueue[endOfQueue].sampleNo, displayBdVersion: myTrim(device.uploadQueue[endOfQueue].displayBdVersion),
            powerBdVersion: myTrim(device.uploadQueue[endOfQueue].powerBdVersion), lifetimeHours: device.uploadQueue[endOfQueue].lifetimeHours, isXL: device.uploadQueue[endOfQueue].isXL};
          if (device.uploadQueue[endOfQueue].location && (device.uploadQueue[endOfQueue].location.latitude || device.uploadQueue[endOfQueue].location.longitude)) {
            aTime.location = device.uploadQueue[endOfQueue].location;
          }
          data.times.push(aTime);
        }
        endOfQueue += 1;
        if (endOfQueue === 0) {
          beginningOfData = 0;
        } else {
          beginningOfData = device.uploadQueue[endOfQueue].dataStart;
        }
        data.data = device.data.slice(beginningOfData);

        setTimeout(function () {//If upload takes more than 60 seconds, cancel it and try again in about 5 minutes.
          if (!uploadFinished) {
            cancelUpload.resolve();
            if (errorCallback && ((typeof errorCallback) === "function")) {
              errorCallback(device, "upload cancelled due to unresponsive server");
            }
            uploadLocks[device.address] = setTimeout(function () {
              uploadHistoricalDataContinuously(device);
            }, 5 * 60000 + 30000 * Math.random());
            console.log("cancelling upload!", new Date());
          }
        }, 55000 + 10000 * Math.random());

        // console.log("sending " + JSON.stringify(data));
        $http.post(API_Base.replace(/v1/g, 'v2') + 'upload' + ($localStorage.myPhoneName ? '?phoneName=' + encodeURIComponent($localStorage.myPhoneName) : ''),
            data, {timeout: cancelUpload}).then(function (databaseInfo) {
          uploadFinished = true;
          if (databaseInfo && databaseInfo.data && databaseInfo.data.dehuId) {
            device.databaseId = databaseInfo.data.dehuId;
          }
          if (databaseInfo && databaseInfo.data && databaseInfo.data.claimStatus) {
            if (device.claimStatus === 'mine' && databaseInfo.data.claimStatus !== 'mine') {
              device.beaconUUID = undefined;
              device.beaconName = undefined;
              device.hasBeacon = false;
            }
            device.claimStatus = databaseInfo.data.claimStatus;
            if (device.claimStatus !== 'mine' && device.claimStatus !== 'theirs') {
              Dehumidifier.addClaimPossibility(device);
            }
          }

          device.settingsChanged = [];
          device.data = device.data.slice(0, beginningOfData);
          if (device.uploadQueue && device.uploadQueue.length) {
            device.uploadQueue = device.uploadQueue.slice(0, endOfQueue);
          }
          if (successCallback && ((typeof successCallback) === "function")) {
            successCallback(device);
          }

          if (device.data && device.data.length) {
            uploadLocks[device.address] = setTimeout(function () {
              uploadHistoricalDataContinuously(device);
            }, 25000 + 10000 * Math.random());//Send more data after about 30 seconds.
          } else {
            uploadLocks[device.address] = setTimeout(function () {
              uploadHistoricalDataContinuously(device);
            }, 15 * 60000 + 4 * 60000 * Math.random());//If app is still running, upload data again after at least 15 minutes.
          }

          if (databaseInfo && databaseInfo.data && device.name && (databaseInfo.data.dehuName !== device.name)) {
            Dehumidifier.postDehuName(device.databaseId, device.name);
          }
        }, function (err) {
          uploadFinished = true;
          if (errorCallback && ((typeof errorCallback) === "function")) {
            errorCallback(device, err.data || err);
          }
          if (err && err.status && err.status >= 400 && err.status < 500) {//Don't treat 400 errors the same as other error codes.
            uploadLocks[device.address] = setTimeout(function () {
              uploadHistoricalDataContinuously(device);
            }, 15 * 60000 + 4 * 60000 * Math.random());//If app is still running, upload data again after at least 15 minutes.
          } else {
            uploadLocks[device.address] = setTimeout(function () {
              uploadHistoricalDataContinuously(device);
            }, 55000 + 10000 * Math.random());//Try again after about 60 seconds.
          }
        });
      };

    Dehumidifier.setUploadCallbacks = function (startCB, successCB, errorCB) {
      startCallback = startCB;
      successCallback = successCB;
      errorCallback = errorCB;
    };

    //Start uploading data from dehumidifier at regular intervals and block more than one function from running at the same time for a given dehumidifier.
    Dehumidifier.uploadHistoricalData = function (device) {
      uploadHistoricalDataContinuously(device);
    };

    Dehumidifier.uploadHistoricalDrySENSEData = function (device) {
      if (uploadLocks[device.address] === -1) {//Currently uploading data, already.
        return;
      }
      if (uploadLocks[device.address]) {
        clearTimeout(uploadLocks[device.address]);
      }
      uploadLocks[device.address] = -1;//Currently trying to upload data.

      if (device.downloading || device.downloadFailed) {
        uploadLocks[device.address] = setTimeout(function () {
          Dehumidifier.uploadHistoricalDrySENSEData(device);
        }, 1000 + 1000 * Math.random());//Try again after about a second.
        return;
      }
      if (!device.uploadObject || !device.uploadObject.samples) {
        uploadLocks[device.address] = setTimeout(function () {
          Dehumidifier.uploadHistoricalDrySENSEData(device);
        }, 15 * 60000 + 4 * 60000 * Math.random());//If app is still running, upload data again after at least 15 minutes.
        return;
      }

      var cancelUpload = $q.defer(),
        uploadFinished = false;
      if (startCallback && ((typeof startCallback) === "function")) {
        startCallback(device);
      }

      setTimeout(function () {//If upload takes more than 60 seconds, cancel it and try again in about 5 minutes.
        if (!uploadFinished) {
          cancelUpload.resolve();
          if (errorCallback && ((typeof errorCallback) === "function")) {
            errorCallback(device, "upload cancelled due to unresponsive server");
          }
          uploadLocks[device.address] = setTimeout(function () {
            Dehumidifier.uploadHistoricalDrySENSEData(device);
          }, 5 * 60000 + 30000 * Math.random());
          console.log("cancelling upload!", new Date());
        }
      }, 55000 + 10000 * Math.random());

      Dehumidifier.checkDrySENSETime(device.uploadObject.deviceId).then(function (uploadSequenceNumber) {
        if (device.isOneHourDrySENSE) {
          if (uploadSequenceNumber >= device.downloadSequenceNumber && uploadSequenceNumber < device.downloadSequenceNumber + 72) {
            device.downloadSequenceNumber = uploadSequenceNumber;//Don't send data that is old but is less than 3 days old.
            device.uploadObject = undefined;
            return $q.reject();
          }
        } else if (uploadSequenceNumber >= device.downloadSequenceNumber && uploadSequenceNumber < device.downloadSequenceNumber + 12) {
          device.downloadSequenceNumber = uploadSequenceNumber;//Don't send data that is old but is less than 3 days old.
          device.uploadObject = undefined;
          return $q.reject();
        }
        return $q.resolve();
      }, $q.resolve).then(function () {
        $http.post(API_Base.replace(/v1/g, 'v3') + 'msensor/upload' + ($localStorage.myPhoneName ? '?phoneName=' + encodeURIComponent($localStorage.myPhoneName) : ''),
            renameDrySENSEsamples(device.uploadObject, device.isOneHourDrySENSE), {timeout: cancelUpload}).then(function (response) {
          var databaseInfo = (response && response.data) || {};
          console.log("upload successful", device.uploadObject.deviceId, databaseInfo);
          uploadFinished = true;
          device.claimStatus = databaseInfo.claimStatus;
          if (databaseInfo.sensorName) {
            device.name = databaseInfo.sensorName;
          }
          device.msensorId = databaseInfo.msensorId;
          if (device.claimStatus && device.claimStatus === 'mine') {
            $localStorage.neverShowDrySENSElist = false;
          }
          if (device.claimStatus && device.claimStatus !== 'mine' && device.claimStatus !== 'theirs') {
            Dehumidifier.addClaimPossibility(device);
          } else {
            device.uploadObject = undefined;//Only erase data if DrySENSE is claimed by the company, since otherwise it won't be stored by the server.
          }

          if (successCallback && ((typeof successCallback) === "function")) {
            successCallback(device);
          }

          uploadLocks[device.address] = setTimeout(function () {
            Dehumidifier.uploadHistoricalDrySENSEData(device);
          }, 15 * 60000 + 4 * 60000 * Math.random());//If app is still running, upload data again after at least 15 minutes.
        }, function (err) {
          uploadFinished = true;
          if (errorCallback && ((typeof errorCallback) === "function")) {
            errorCallback(device, err.data || err);
          }
          if (err && err.status && err.status >= 400 && err.status < 500) {//Don't treat 400 errors the same as other error codes.
            uploadLocks[device.address] = setTimeout(function () {
              Dehumidifier.uploadHistoricalDrySENSEData(device);
            }, 15 * 60000 + 4 * 60000 * Math.random());//If app is still running, upload data again after at least 15 minutes.
          } else {
            uploadLocks[device.address] = setTimeout(function () {
              Dehumidifier.uploadHistoricalDrySENSEData(device);
            }, 55000 + 10000 * Math.random());//Try again after about 60 seconds.
          }
        });
      }, function () {
        if (successCallback && ((typeof successCallback) === "function")) {
          successCallback(device);
        }
        uploadLocks[device.address] = setTimeout(function () {
          Dehumidifier.uploadHistoricalDrySENSEData(device);
        }, 15 * 60000 + 4 * 60000 * Math.random());//If app is still running, upload data again after at least 15 minutes.
      });
    };

    Dehumidifier.markDryTAGforOverride = function (device) {
      if (!device || device.modelType !== 'DryTAG') {
        return;
      }
      device.overrideNextTime = 2;
    };

    Dehumidifier.uploadDryTAG = function (device, recentBluetooth, overrideTimeWindow) {
      if (uploadLocks[device.address] === -1) {//Currently uploading data, already.
        return;
      }
      if (uploadLocks[device.address]) {
        clearTimeout(uploadLocks[device.address]);
      }
      uploadLocks[device.address] = -1;//Currently trying to upload data.

      if (!device.uploadQueue || !device.uploadQueue.length) {
        uploadLocks[device.address] = setTimeout(function () {
          Dehumidifier.uploadDryTAG(device);
        }, 15 * 60000 + 4 * 60000 * Math.random());//If app is still running, upload data again after at least 15 minutes.
        return;
      }
      if (device.overrideNextTime === 1) {
        device.overrideNextTime = undefined;
        overrideTimeWindow = true;
      }
      if (!overrideTimeWindow && device.lastTrueUploadTime && (new Date()).getTime() < device.lastTrueUploadTime + 15 * 60000) {
        //Don't upload more than once every 15 minutes, even if data is available in the upload queue.
        uploadLocks[device.address] = setTimeout(function () {
          Dehumidifier.uploadDryTAG(device);
        }, 30000 + 60000 * Math.random());
        return;
      }

      var cancelUpload = $q.defer(),
        uploadData = {},
        uploadFinished = false;
      if (startCallback && ((typeof startCallback) === "function")) {
        startCallback(device);
      }

      setTimeout(function () {//If upload takes more than 60 seconds, cancel it and try again in about 5 minutes.
        if (!uploadFinished) {
          cancelUpload.resolve();
          if (errorCallback && ((typeof errorCallback) === "function")) {
            errorCallback(device, "upload cancelled due to unresponsive server");
          }
          uploadLocks[device.address] = setTimeout(function () {
            Dehumidifier.uploadDryTAG(device);
          }, 5 * 60000 + 30000 * Math.random());
          console.log("cancelling upload!", new Date());
        }
      }, 55000 + 10000 * Math.random());

      uploadData.deviceId = device.validUUID;
      uploadData.deviceExt = device.validUUIDext;
      if (device.hasRHCapabilities) {
        uploadData.rhSamples = angular.copy(device.uploadQueue);
      } else {
        uploadData.samples = angular.copy(device.uploadQueue);
      }
      $http.post(API_Base.replace(/v1/g, 'v3') + 'beacon/upload' + ($localStorage.myPhoneName ? '?phoneName=' + encodeURIComponent($localStorage.myPhoneName) : ''),
          uploadData, {timeout: cancelUpload}).then(function (response) {
        var databaseInfo = (response && response.data) || {};
        //console.log("upload successful", uploadData.deviceId, databaseInfo);
        uploadFinished = true;
        device.lastTrueUploadTime = (new Date()).getTime();
        device.beaconId = databaseInfo.beaconId;
        device.claimStatus = databaseInfo.claimStatus;
        device.name = databaseInfo.name;
        device.dehuId = databaseInfo.dehuId;
        device.cequipId = databaseInfo.cequipId;
        if (device.claimStatus && device.claimStatus !== 'mine' && device.claimStatus !== 'theirs') {
          Dehumidifier.addClaimPossibility(device, recentBluetooth);
        }

        if (device.hasRHCapabilities) {
          device.uploadQueue = device.uploadQueue.slice(uploadData.rhSamples.length);
        } else {
          device.uploadQueue = device.uploadQueue.slice(uploadData.samples.length);
        }
        if (successCallback && ((typeof successCallback) === "function")) {
          successCallback(device);
        }

        if (device.uploadQueue && device.uploadQueue.length) {
          uploadLocks[device.address] = setTimeout(function () {
            Dehumidifier.uploadDryTAG(device);
          }, 25000 + 10000 * Math.random());//Send more data after about 30 seconds.
        } else {
          uploadLocks[device.address] = setTimeout(function () {
            Dehumidifier.uploadDryTAG(device);
          }, 15 * 60000 + 4 * 60000 * Math.random());//If app is still running, upload data again after at least 15 minutes.
        }
      }, function (err) {
        uploadFinished = true;
        if (errorCallback && ((typeof errorCallback) === "function")) {
          errorCallback(device, err.data || err);
        }
        if (err && err.status && err.status >= 400 && err.status < 500) {//Don't treat 400 errors the same as other error codes.
          uploadLocks[device.address] = setTimeout(function () {
            Dehumidifier.uploadDryTAG(device);
          }, 15 * 60000 + 4 * 60000 * Math.random());//If app is still running, upload data again after at least 15 minutes.
        } else {
          uploadLocks[device.address] = setTimeout(function () {
            Dehumidifier.uploadDryTAG(device);
          }, 55000 + 10000 * Math.random());//Try again after about 60 seconds.
        }
      });
    };

    Dehumidifier.checkDryTAGTime = function (beaconUUID) {
      if (!beaconUUID) {
        return $q.reject({message: "no DryTAG UUID", status: 400});
      }
      return $http.get(API_Base.replace(/v1/g, 'v2') + 'beacon/upload?deviceId=' + beaconUUID).then(function (newTime) {
        return newTime && newTime.data;
      }, $q.reject);
    };

    Dehumidifier.checkDrySENSETime = function (drySENSEID) {
      if (!drySENSEID) {
        return $q.reject({message: "no DrySENSE UUID", status: 400});
      }
      return $http.get(API_Base + 'msensor/upload?deviceId=' + drySENSEID).then(function (newTime) {
        return newTime && newTime.data;
      }, $q.reject);
    };

    Dehumidifier.checkUploadTime = function (device) {
      if (!device || !device.serialNumber) {
        return $q.reject({message: "no serial number", status: 400});
      }
      console.log("check upload time " + device.serialNumber);
      device.checkUpload = true;//Block any downloads or uploads until last timestamp retrieved from server or API call fails.
      return $http.get(API_Base + 'upload?deviceId=' + device.serialNumber).then(function (newTime) {
        console.log("new time " + JSON.stringify(newTime.data));
        if (typeof newTime.data === "number") {
          device.timestamp = newTime.data + 1;
        }
        device.checkUpload = false;
        return $q.resolve();
      }, function (err) {
        console.log("new time err " + JSON.stringify(err.data));
        device.checkUpload = false;
        return $q.reject(err);
      });
    };

    Dehumidifier.updateDrySENSEname = function (msensorId, name) {
      if (!msensorId) {
        return $q.reject({message: 'No database ID', status: 400});
      }

      return $http.put(API_Base + 'msensor/' + msensorId + '?name=' + encodeURIComponent(name)).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Dehumidifier.updateDrySENSEoffset = function (msensorId, rhOffset) {
      if (!msensorId) {
        return $q.reject({message: 'No database ID', status: 400});
      }

      return $http.put(API_Base + 'msensor/' + msensorId + '?rhOffset=' + Math.round(rhOffset)).then(function (response) {
        var aDevice = $rootScope.searchBluetoothListsForMatch('msensorId', msensorId);
        if (aDevice) {
          aDevice.serverRHoffset = rhOffset;
        }
        return response.data;
      }, $q.reject);
    };

    Dehumidifier.changeDryTAGBattery = function (drytagUuid, changeSecs) {
      if (!drytagUuid) {
        return $q.reject({message: 'No database ID', status: 400});
      }
      var query = "?deviceId=" + encodeURIComponent(drytagUuid);
      if (changeSecs) {
        query += "&changeSecs=" + encodeURIComponent(changeSecs);
      }
      return $http.put(API_Base + 'cequip/battery_change' + query).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Dehumidifier.postDehuName = function (databaseId, name, hoursPerFilter) {
      if (!databaseId) {
        return $q.reject({message: 'No database ID', status: 400});
      }
      if (!name && !hoursPerFilter) {
        return $q.reject({message: 'No info to post', status: 400});
      }

      var target = API_Base + 'dehu/' + databaseId + '?';
      if (name) {
        target += 'name=' + encodeURIComponent(name);
      }
      if (hoursPerFilter) {
        if (name) {
          target += '&';
        }
        target += 'hoursPerFilter=' + hoursPerFilter;
      }

      return $http.put(target).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Dehumidifier.addClaimPossibility = function (device, recentBluetooth) {
      var i,
        dehu;

      if (!device || (device.blockClaimingUntilBluetooth && device.modelType === 'DrySENSE')) {
        return;
      }
      if (device.modelType === 'DryTAG') {
        //console.log("this is the claim possibility", JSON.stringify(device));
        device.claimingDeferred = false;
        if ((device.dehuId && device.dehuId > -1) || (device.cequipId && device.cequipId > -1)) {
          if (device.dehuId && device.dehuId > -1 && $rootScope.claimPossibilities && $rootScope.claimPossibilities.length) {
            for (i = 0; i < $rootScope.claimPossibilities.length; i += 1) {
              dehu = $rootScope.claimPossibilities[i] || {};
              if (dehu.databaseId === device.dehuId) {
                dehu.beaconUUID = device.uuid;
                dehu.beaconName = device.name || device.uuid || device.address;
                dehu.hasBeacon = true;
                break;
              }
            }
          }
          if ($rootScope.associatedDryTAGPossibilities && $rootScope.associatedDryTAGPossibilities.length) {
            for (i = 0; i < $rootScope.associatedDryTAGPossibilities.length; i += 1) {
              if ($rootScope.associatedDryTAGPossibilities[i] && $rootScope.associatedDryTAGPossibilities[i].uuid === device.uuid) {
                $rootScope.newDehusAvailable = true;
                $rootScope.associatedDryTAGPossibilities[i] = device;
                return;
              }
            }
          } else {
            $rootScope.associatedDryTAGPossibilities = [];
          }
          if (recentBluetooth) {
            $rootScope.newDehusAvailable = true;
            $rootScope.associatedDryTAGPossibilities.push(device);
          } else {
            device.claimingDeferred = true;
          }
        } else {
          device.claimingDeferred = false;
          if ($rootScope.isolatedDryTAGPossibilities && $rootScope.isolatedDryTAGPossibilities.length) {
            for (i = 0; i < $rootScope.isolatedDryTAGPossibilities.length; i += 1) {
              if ($rootScope.isolatedDryTAGPossibilities[i] && $rootScope.isolatedDryTAGPossibilities[i].uuid === device.uuid) {
                $rootScope.isolatedDryTAGsAvailable = true;
                $rootScope.isolatedDryTAGPossibilities[i] = device;
                return;
              }
            }
          } else {
            $rootScope.isolatedDryTAGPossibilities = [];
          }
          if (recentBluetooth) {
            $rootScope.isolatedDryTAGsAvailable = true;
            $rootScope.isolatedDryTAGPossibilities.push(device);
          } else {
            device.claimingDeferred = true;
          }
        }
        return;
      }
      $rootScope.newDehusAvailable = true;
      if ($rootScope.associatedDryTAGPossibilities && $rootScope.associatedDryTAGPossibilities.length) {
        for (i = 0; i < $rootScope.associatedDryTAGPossibilities.length; i += 1) {
          dehu = $rootScope.associatedDryTAGPossibilities[i] || {};
          if (dehu.dehuId && dehu.dehuId > -1 && dehu.dehuId === device.databaseId) {
            device.beaconUUID = dehu.uuid;
            device.beaconName = dehu.name || dehu.uuid || dehu.address;
            device.hasBeacon = true;
            break;
          }
        }
      }
      if ($rootScope.claimPossibilities && $rootScope.claimPossibilities.length) {
        for (i = 0; i < $rootScope.claimPossibilities.length; i += 1) {
          if ($rootScope.claimPossibilities[i] && ((device.modelType === "DrySENSE" && $rootScope.claimPossibilities[i].address === device.address) ||
               (device.modelType !== "DrySENSE" && $rootScope.claimPossibilities[i].serialNumber === device.serialNumber))) {
            $rootScope.claimPossibilities[i] = device;
            return;
          }
        }
      } else {
        $rootScope.claimPossibilities = [];
      }
      $rootScope.claimPossibilities.push(device);
    };

    Dehumidifier.claimDryTAG = function (token, dehuId, cequipId) {
      if (!token) {
        return $q.reject({message: 'No claim token', status: 400});
      }
      var tokenString = "beacontoken=" + token;
      if (dehuId && dehuId > -1) {
        tokenString += "&dehuId=" + dehuId;
      }
      if (cequipId && cequipId > -1) {
        tokenString += "&cequipId=" + cequipId;
      }
      return $http.post(API_Base + 'beacon/claim?' + tokenString).then(function (response) {
        Snackbar.show({template: "Equipment added to company", hide: 5000});
        return response.data;
      }, $q.reject);
    };

    Dehumidifier.claimDrySENSE = function (token, firmwareVersion) {
      if (!token) {
        return $q.reject({message: 'No claim token', status: 400});
      }
      return $http.post(API_Base.replace(/v1/g, 'v2') + 'msensor/claim?msensortoken=' + token + '&firmwareVersion=' +
          encodeURIComponent(firmwareVersion.replace(/\0/g, ''))).then(function (response) {//or encodeURIComponent(token) or token.replace(/\//g, '%2F')
        $localStorage.neverShowDrySENSElist = false;
        Snackbar.show({template: "Equipment added to company", hide: 5000});
        return response.data;
      }, $q.reject);
    };

    Dehumidifier.claim = function (token) {
      if (!token) {
        return $q.reject({message: 'No claim token', status: 400});
      }
      return $http.post(API_Base + 'dehu/claim?dehutoken=' + token).then(function (response) {//or encodeURIComponent(token) or token.replace(/\//g, '%2F')
        Snackbar.show({template: "Equipment added to company", hide: 5000});
        return response.data;
      }, $q.reject);
    };

    Dehumidifier.resetFilter = function (databaseId) {
      if (!databaseId) {
        return $q.reject({message: 'No database ID', status: 400});
      }

      return $http.post(API_Base + 'dehu/' + databaseId + '/filter_reset').then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Dehumidifier.deleteDrySENSE = function (msensorId) {
      if (!msensorId || !(msensorId > -1)) {
        return $q.reject({message: 'No database ID', status: 400});
      }

      var i;
      if ($localStorage.leDeviceList) {
        if ($localStorage.leDeviceList.newer && $localStorage.leDeviceList.newer.length) {
          for (i = 0; i < $localStorage.leDeviceList.newer.length; i += 1) {
            if (String($localStorage.leDeviceList.newer[i].msensorId) === String(msensorId)) {
              $localStorage.leDeviceList.newer.splice(i, 1);
            }
          }
        }
        if ($localStorage.leDeviceList.older && $localStorage.leDeviceList.older.length) {
          for (i = 0; i < $localStorage.leDeviceList.older.length; i += 1) {
            if (String($localStorage.leDeviceList.older[i].msensorId) === String(msensorId)) {
              $localStorage.leDeviceList.older.splice(i, 1);
            }
          }
        }
      }

      return $http.delete(API_Base + 'msensor/' + msensorId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Dehumidifier.release = function (databaseId) {
      if (!databaseId || !(databaseId > -1)) {
        return $q.reject({message: 'No database ID', status: 400});
      }

      var i;
      if ($localStorage.leDeviceList) {
        if ($localStorage.leDeviceList.newer && $localStorage.leDeviceList.newer.length) {
          for (i = 0; i < $localStorage.leDeviceList.newer.length; i += 1) {
            if (String($localStorage.leDeviceList.newer[i].databaseId) === String(databaseId)) {
              $localStorage.leDeviceList.newer.splice(i, 1);
            }
          }
        }
        if ($localStorage.leDeviceList.older && $localStorage.leDeviceList.older.length) {
          for (i = 0; i < $localStorage.leDeviceList.older.length; i += 1) {
            if (String($localStorage.leDeviceList.older[i].databaseId) === String(databaseId)) {
              $localStorage.leDeviceList.older.splice(i, 1);
            }
          }
        }
      }

      return $http.delete(API_Base + 'dehu/' + databaseId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Dehumidifier.releaseDryTAG = function (beaconId, skipCloud) {
      if (!beaconId || !(beaconId > -1)) {
        return $q.reject({message: 'No beacon ID', status: 400});
      }

      var i;
      if ($localStorage.leDeviceList) {
        if ($localStorage.leDeviceList.newer && $localStorage.leDeviceList.newer.length) {
          for (i = 0; i < $localStorage.leDeviceList.newer.length; i += 1) {
            if (String($localStorage.leDeviceList.newer[i].beaconId) === String(beaconId)) {
              $localStorage.leDeviceList.newer.splice(i, 1);
            }
          }
        }
        if ($localStorage.leDeviceList.older && $localStorage.leDeviceList.older.length) {
          for (i = 0; i < $localStorage.leDeviceList.older.length; i += 1) {
            if (String($localStorage.leDeviceList.older[i].beaconId) === String(beaconId)) {
              $localStorage.leDeviceList.older.splice(i, 1);
            }
          }
        }
      }

      if (skipCloud) {
        return $q.resolve();
      }
      return $http.delete(API_Base.replace(/v1/g, 'v2') + 'beacon/' + beaconId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Dehumidifier.updateDryTAGoffset = function (beaconId, rhOffset) {
      if (!(beaconId > 0)) {
        return $q.reject({message: 'No beacon ID', status: 400});
      }

      return $http.put(API_Base + 'beacon/' + beaconId + "?rhOffset=" + Math.round(rhOffset)).then(function (response) {
        var aDevice = $rootScope.searchBluetoothListsForMatch('beaconId', beaconId);
        if (aDevice && aDevice.hasRHCapabilities) {
          aDevice.serverRHoffset = rhOffset;
        }
        return response.data;
      }, $q.reject);
    };

    Dehumidifier.getLastData = function (databaseId, isDehu, isDrySENSE) {
      if (!databaseId) {
        return $q.reject({message: 'No database ID', status: 400});
      }

      if (isDehu) {
        return $http.get(API_Base + 'dehu/' + databaseId).then(function (data) {
          return (data && data.data) || {};
        }, $q.reject);
      }
      if (isDrySENSE) {
        return $http.get(API_Base + 'msensor/' + databaseId).then(function (data) {
          var alteredData = (data && data.data) || {};
          if (alteredData.msensor && alteredData.lastReading) {//Diane is very consistent at being very inconsistent.
            if ((alteredData.msensor.latitude || alteredData.msensor.longitude) && !(alteredData.lastReading.latitude || alteredData.lastReading.longitude)) {
              alteredData.lastReading.latitude = alteredData.msensor.latitude;
              alteredData.lastReading.longitude = alteredData.msensor.longitude;
            }
            if (alteredData.msensor.lastAccessSecs && !alteredData.lastReading.sampleSecs) {
              alteredData.lastReading.sampleSecs = alteredData.msensor.lastAccessSecs;
            }
            if (alteredData.msensor.lastAccountName && !alteredData.lastReading.accountName) {
              alteredData.lastReading.accountName = alteredData.msensor.lastAccountName;
            }
          }
          return alteredData;
        }, $q.reject);
      }
      return $http.get(API_Base + 'cequip/' + databaseId).then(function (data) {
        return (data && data.data) || {};
      }, $q.reject);
    };

    Dehumidifier.getAllDrySENSEs = function () {
      return $http.get(API_Base + 'msensor').then(function (data) {
        var newList = (data && data.data) || [],
          cleanedList = [],
          deviceList = $localStorage.deviceList || $rootScope.deviceList,
          cloudDehu,
          bleDehu,
          i,
          j;

        for (i = 0; i < newList.length; i += 1) {
          cloudDehu = {};
          if (newList[i] && newList[i].msensor) {
            $localStorage.neverShowDrySENSElist = false;
            cloudDehu.serialNumber = newList[i].msensor.deviceId;
            cloudDehu.dehuName = newList[i].msensor.sensorName;
            cloudDehu.msensorId = newList[i].msensor.msensorId;
            cloudDehu.modelType = "DrySENSE";
            cloudDehu.typeName = newList[i].msensor.typeName;
            cloudDehu.lastSeen = 'elsewhere';
            cloudDehu.hasBeacon = false;
            if (newList[i].mpInfo && newList[i].mpInfo.jobId > 0) {
              cloudDehu.jobId = newList[i].mpInfo.jobId;
              cloudDehu.jobName = newList[i].mpInfo.jobName;
              cloudDehu.label = newList[i].mpInfo.label;
              cloudDehu.mequipId = newList[i].mpInfo.mequipmentId;
              cloudDehu.equipmentId = -1;
              cloudDehu.chamberId = newList[i].mpInfo.chamberId;
              cloudDehu.chamberName = newList[i].mpInfo.chamberName;
            } else if (newList[i].jobInfo && newList[i].jobInfo.jobId > 0) {
              cloudDehu.jobId = newList[i].jobInfo.jobId;
              cloudDehu.jobName = newList[i].jobInfo.jobName;
              cloudDehu.label = "";
              cloudDehu.mequipId = -1;
              cloudDehu.equipmentId = newList[i].jobInfo.equipmentId;
              cloudDehu.chamberId = newList[i].jobInfo.chamberId;
              cloudDehu.chamberName = newList[i].jobInfo.chamberName;
            } else {
              cloudDehu.jobId = -1;
              cloudDehu.jobName = "";
              cloudDehu.label = "";
              cloudDehu.mequipId = -1;
              cloudDehu.equipmentId = -1;
              cloudDehu.chamberId = -1;
              cloudDehu.chamberName = "";
            }

            if (deviceList) {
              if (deviceList.newer && deviceList.newer.length) {
                for (j = 0; j < deviceList.newer.length; j += 1) {
                  bleDehu = deviceList.newer[j];

                  if (cloudDehu.msensorId > -1 && (cloudDehu.msensorId === bleDehu.msensorId || (cloudDehu.serialNumber && cloudDehu.serialNumber === bleDehu.uniqueEnoughID))) {
                    bleDehu.msensorId = cloudDehu.msensorId;
                    cloudDehu.address = bleDehu.address;
                    cloudDehu.lastSeen = 'nearby';
                    if (!cloudDehu.lastRssi) {
                      cloudDehu.lastRssi = bleDehu.lastRssi;
                    }
                    bleDehu.jobName = cloudDehu.jobName;
                    bleDehu.jobId = cloudDehu.jobId;
                    bleDehu.claimStatus = "mine";
                    bleDehu.name = cloudDehu.dehuName;
                    bleDehu.typeName = cloudDehu.typeName;
                  }
                }
              }
              if (deviceList.older && deviceList.older.length) {
                for (j = 0; j < deviceList.older.length; j += 1) {
                  bleDehu = deviceList.older[j];

                  if (cloudDehu.msensorId > -1 && (cloudDehu.msensorId === bleDehu.msensorId || (cloudDehu.serialNumber && cloudDehu.serialNumber === bleDehu.uniqueEnoughID))) {
                    bleDehu.msensorId = cloudDehu.msensorId;
                    cloudDehu.address = bleDehu.address;
                    bleDehu.jobName = cloudDehu.jobName;
                    bleDehu.jobId = cloudDehu.jobId;
                    bleDehu.claimStatus = "mine";
                    bleDehu.name = cloudDehu.dehuName;
                    bleDehu.typeName = cloudDehu.typeName;
                  }
                }
              }
            }
            Dehumidifier.determineIconCategory(cloudDehu);
            cleanedList.push(cloudDehu);
          }
        }

        return cleanedList;
      }, $q.reject);
    };

    Dehumidifier.getDehuListAndReorganize = function () {
      return $http.get(API_Base + 'cequip/all').then(function (data) {
        var newList = (data && data.data) || [],
          cleanedList = [],
          deviceList = $localStorage.deviceList || $rootScope.deviceList,
          cloudDehu,
          bleDehu,
          foundTheCloud,
          i,
          j;

        for (i = 0; i < newList.length; i += 1) {
          cloudDehu = newList[i];
          if (cloudDehu && cloudDehu.deviceId) {
            if (cloudDehu.model === "dryxl" || cloudDehu.model === "Dry Max XL") {
              cloudDehu.model = "Dry Max XL";
            } else if (cloudDehu.model === "dry80" || cloudDehu.model === "Dry Max") {
              cloudDehu.model = "Dry Max";
            } else if (cloudDehu.model === "DrySense" || cloudDehu.model === "DrySENSE") {
              cloudDehu.model = "DrySENSE";
              $localStorage.neverShowDrySENSElist = false;
            }
            cleanedList.push({serialNumber: cloudDehu.deviceId, dehuName: cloudDehu.equipName, databaseId: cloudDehu.dehuId, cequipId: cloudDehu.cequipId, ctsSerialNumber: cloudDehu.serialNumber, beaconId: cloudDehu.beaconId, msensorId: cloudDehu.msensorId,
              jobName: cloudDehu.jobName, jobId: cloudDehu.jobId, modelType: cloudDehu.model, typeName: cloudDehu.typeName, lastSeen: 'elsewhere', hasBeacon: cloudDehu.beaconId > -1, hasBeaconRH: cloudDehu.hasBeaconRH});//Translate between Diane's naming conventions and my own.
          }
        }

        if (deviceList) {
          if (deviceList.newer && deviceList.newer.length) {
            for (i = 0; i < deviceList.newer.length; i += 1) {
              bleDehu = deviceList.newer[i];
              foundTheCloud = false;
              for (j = 0; j < cleanedList.length; j += 1) {
                cloudDehu = cleanedList[j];
                if (cloudDehu.databaseId && cloudDehu.databaseId > -1 && bleDehu.serialNumber === cloudDehu.serialNumber) {//Exchange data between the lists so that they are more complete.
                  foundTheCloud = true;
                  bleDehu.databaseId = cloudDehu.databaseId;//Give the cloud-generated database id to the list generated over Bluetooth.
                  cloudDehu.address = bleDehu.address;//And give the Bluetooth address (which will be device-specific on iOS) to the cloud-generated dehu list.
                  cloudDehu.lastSeen = 'nearby';
                  cloudDehu.lastRssi = bleDehu.lastRssi;
                  cloudDehu.otaLock = bleDehu.otaLock;
                  bleDehu.jobName = cloudDehu.jobName;
                  bleDehu.jobId = cloudDehu.jobId;
                  bleDehu.claimStatus = "mine";
                  bleDehu.hasBeacon = cloudDehu.hasBeacon;
                  if (bleDehu.modelType) {
                    cloudDehu.modelType = bleDehu.modelType;
                  } else if (cloudDehu.modelType) {
                    bleDehu.modelType = cloudDehu.modelType;
                  }
                  if (cloudDehu.ctsSerialNumber && (cloudDehu.ctsSerialNumber !== bleDehu.ctsSerialNumber)) {
                    bleDehu.shouldUpdateCTSnumber = true;
                  }
                  bleDehu.ctsSerialNumber = cloudDehu.ctsSerialNumber;

                  if (!cloudDehu.beaconId || !(cloudDehu.beaconId > -1)) {
                    bleDehu.beaconUUID = undefined;
                    bleDehu.beaconName = undefined;
                  }

                  cloudDehu.bleCopy = bleDehu;
                } else if (cloudDehu.beaconId && cloudDehu.beaconId > -1 && cloudDehu.beaconId === bleDehu.beaconId) {
                  foundTheCloud = true;
                  bleDehu.dehuId = cloudDehu.databaseId;//Associate the DryTAG with whatever dehu or equipment it's attached to.
                  bleDehu.cequipId = cloudDehu.cequipId;
                  if (!cloudDehu.address) {
                    cloudDehu.address = bleDehu.address;//The Bluetooth address isn't that useful for DryTAGs but we can exchange it, anyway.
                  }
                  cloudDehu.lastSeen = 'nearby';
                  if (!cloudDehu.lastRssi) {
                    cloudDehu.lastRssi = bleDehu.lastRssi;
                  }
                  bleDehu.jobName = cloudDehu.jobName;
                  bleDehu.jobId = cloudDehu.jobId;
                  bleDehu.claimStatus = "mine";
                  bleDehu.name = cloudDehu.dehuName;
                  if (bleDehu.dehuId && bleDehu.dehuId > -1) {
                    bleDehu.typeName = cloudDehu.modelType;
                  } else {
                    bleDehu.typeName = cloudDehu.typeName;
                  }
                  cloudDehu.hasBeaconRH = cloudDehu.hasBeaconRH || bleDehu.hasRHCapabilities;
                  Dehumidifier.determineIconCategory(bleDehu);

                  if (!cloudDehu.bleCopy) {
                    cloudDehu.bleCopy = bleDehu;
                  }
                } else if (cloudDehu.msensorId && cloudDehu.msensorId > -1 && (cloudDehu.msensorId === bleDehu.msensorId ||
                           (cloudDehu.serialNumber && cloudDehu.serialNumber === bleDehu.uniqueEnoughID))) {
                  foundTheCloud = true;
                  bleDehu.msensorId = cloudDehu.msensorId;
                  cloudDehu.address = bleDehu.address;
                  cloudDehu.lastSeen = 'nearby';
                  if (!cloudDehu.lastRssi) {
                    cloudDehu.lastRssi = bleDehu.lastRssi;
                  }
                  bleDehu.jobName = cloudDehu.jobName;
                  bleDehu.jobId = cloudDehu.jobId;
                  bleDehu.claimStatus = "mine";
                  bleDehu.name = cloudDehu.dehuName;
                  bleDehu.typeName = cloudDehu.typeName;
                  Dehumidifier.determineIconCategory(bleDehu);

                  if (!cloudDehu.bleCopy) {
                    cloudDehu.bleCopy = bleDehu;
                  }
                }
              }
              if (!foundTheCloud && bleDehu.claimStatus === "mine") {
                bleDehu.claimStatus = undefined;
                if (bleDehu.modelType === 'DryTAG') {
                  bleDehu.dehuId = undefined;
                  bleDehu.cequipId = undefined;
                }
              }
            }
          }
          if (deviceList.older && deviceList.older.length) {
            for (i = 0; i < deviceList.older.length; i += 1) {
              bleDehu = deviceList.older[i];
              foundTheCloud = false;
              for (j = 0; j < cleanedList.length; j += 1) {
                cloudDehu = cleanedList[j];
                if (cloudDehu.databaseId && cloudDehu.databaseId > -1 && bleDehu.serialNumber === cloudDehu.serialNumber) {
                  foundTheCloud = true;
                  bleDehu.databaseId = cloudDehu.databaseId;
                  cloudDehu.address = bleDehu.address;
                  cloudDehu.otaLock = bleDehu.otaLock;
                  bleDehu.jobName = cloudDehu.jobName;
                  bleDehu.jobId = cloudDehu.jobId;
                  bleDehu.claimStatus = "mine";
                  bleDehu.hasBeacon = cloudDehu.hasBeacon;
                  if (bleDehu.modelType) {
                    cloudDehu.modelType = bleDehu.modelType;
                  } else if (cloudDehu.modelType) {
                    bleDehu.modelType = cloudDehu.modelType;
                  }
                  if (cloudDehu.ctsSerialNumber && (cloudDehu.ctsSerialNumber !== bleDehu.ctsSerialNumber)) {
                    bleDehu.shouldUpdateCTSnumber = true;
                  }
                  bleDehu.ctsSerialNumber = cloudDehu.ctsSerialNumber;

                  if (!cloudDehu.beaconId || !(cloudDehu.beaconId > -1)) {
                    bleDehu.beaconUUID = undefined;
                    bleDehu.beaconName = undefined;
                  }

                  if (!cloudDehu.bleCopy) {
                    cloudDehu.bleCopy = bleDehu;
                  }
                } else if (cloudDehu.beaconId && cloudDehu.beaconId > -1 && cloudDehu.beaconId === bleDehu.beaconId) {
                  foundTheCloud = true;
                  bleDehu.dehuId = cloudDehu.databaseId;
                  bleDehu.cequipId = cloudDehu.cequipId;
                  if (!cloudDehu.address) {
                    cloudDehu.address = bleDehu.address;
                  }
                  bleDehu.jobName = cloudDehu.jobName;
                  bleDehu.jobId = cloudDehu.jobId;
                  bleDehu.claimStatus = "mine";
                  bleDehu.name = cloudDehu.dehuName;
                  if (bleDehu.dehuId && bleDehu.dehuId > -1) {
                    bleDehu.typeName = cloudDehu.modelType;
                  } else {
                    bleDehu.typeName = cloudDehu.typeName;
                  }
                  cloudDehu.hasBeaconRH = cloudDehu.hasBeaconRH || bleDehu.hasRHCapabilities;
                  Dehumidifier.determineIconCategory(bleDehu);

                  if (!cloudDehu.bleCopy) {
                    cloudDehu.bleCopy = bleDehu;
                  }
                } else if (cloudDehu.msensorId && cloudDehu.msensorId > -1 && (cloudDehu.msensorId === bleDehu.msensorId ||
                           (cloudDehu.serialNumber && cloudDehu.serialNumber === bleDehu.uniqueEnoughID))) {
                  foundTheCloud = true;
                  bleDehu.msensorId = cloudDehu.msensorId;
                  cloudDehu.address = bleDehu.address;
                  bleDehu.jobName = cloudDehu.jobName;
                  bleDehu.jobId = cloudDehu.jobId;
                  bleDehu.claimStatus = "mine";
                  bleDehu.name = cloudDehu.dehuName;
                  bleDehu.typeName = cloudDehu.typeName;
                  Dehumidifier.determineIconCategory(bleDehu);

                  if (!cloudDehu.bleCopy) {
                    cloudDehu.bleCopy = bleDehu;
                  }
                }
              }
              if (!foundTheCloud && bleDehu.claimStatus === "mine") {
                bleDehu.claimStatus = undefined;
                if (bleDehu.modelType === 'DryTAG') {
                  bleDehu.dehuId = undefined;
                  bleDehu.cequipId = undefined;
                }
              }
            }
          }
        }
        for (i = 0; i < cleanedList.length; i += 1) {
          Dehumidifier.determineIconCategory(cleanedList[i]);
        }
        return cleanedList;
      }, $q.reject);
    };

    Dehumidifier.reverseIconCategoryLookup = function (iconCategory) {
      var thisEquipment = {};

      if (iconCategory === 'Dry Max XL') {
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-phoenix-without DryTAG-DryMAX-XL.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-phoenix-DryTAG-DryMAX-XL.png";
      } else if (iconCategory === 'Dry Max') {
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-phoenix-without DryTAG-DryMax-LGR.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-phoenix-DryTAG-DryMax-LGR.png";
      } else if (iconCategory === 'DrySENSE') {
        thisEquipment.nonDryTAGiconURL = "img/DrySENSE_icon.png";
        thisEquipment.dryTAGiconURL = "img/DrySENSE_icon.png";
      } else if (iconCategory === 'AirMAX') {
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-phoenix-without DryTAG-Airmax.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-phoenix-DryTAG-Airmax.png";
      } else if (iconCategory === 'Dehu') {
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-Dehu.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-Dehu.png";
      } else if (iconCategory === 'Drying System') {
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-Floor-Dryer.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-Floor-Dryer.png";
      } else if (iconCategory === 'Heater') {
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-Heater.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-Heater.png";
      } else if (iconCategory === 'A/C') {
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-AC-Unit.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-AC-Unit.png";
      } else if (iconCategory === 'Fan') {
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-Air-Mover.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-Air-Mover.png";
      } else if (iconCategory === 'Splitter Box') {
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-Splitter.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-Splitter.png";
      } else if (iconCategory === 'Air Scrubber') {
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-HEPA-Air-Scrubber.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-HEPA-Air-Scrubber.png";
      } else if (iconCategory === 'Hygrometer') {
        thisEquipment.nonDryTAGiconURL = "img/ThermoHygrometer.png";
        thisEquipment.dryTAGiconURL = "img/DryTAG_RH-standalone.png";
      } else {
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-Custom.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-Custom.png";
      }
      return thisEquipment;
    };

    Dehumidifier.determineIconCategory = function (thisEquipment) {
      if (!thisEquipment) {
        thisEquipment = {};
      }
      if (!thisEquipment.typeName) {
        thisEquipment.typeName = '';
      }

      if (thisEquipment.modelType === 'Dry Max XL' || thisEquipment.typeName === 'Dry Max XL') {
        thisEquipment.iconCategory = 'Dry Max XL';
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-phoenix-without DryTAG-DryMAX-XL.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-phoenix-DryTAG-DryMAX-XL.png";
      } else if (thisEquipment.modelType === 'Dry Max' || thisEquipment.typeName === 'Dry Max') {
        thisEquipment.iconCategory = 'Dry Max';
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-phoenix-without DryTAG-DryMax-LGR.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-phoenix-DryTAG-DryMax-LGR.png";
      } else if (thisEquipment.modelType === 'DrySense' || thisEquipment.modelType === 'DrySENSE') {
        thisEquipment.modelType = 'DrySENSE';
        thisEquipment.iconCategory = 'DrySENSE';
        thisEquipment.nonDryTAGiconURL = "img/DrySENSE_icon.png";
        thisEquipment.dryTAGiconURL = "img/DrySENSE_icon.png";
      } else if (thisEquipment.typeName.indexOf('Dehu') > -1 || thisEquipment.typeName.indexOf('Dessicant') > -1 || thisEquipment.typeName.indexOf('Desiccant') > -1) {
        thisEquipment.iconCategory = 'Dehu';
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-Dehu.png";
        if (thisEquipment.hasBeaconRH || thisEquipment.hasRHCapabilities) {
          thisEquipment.dryTAGiconURL = "img/DryTAG_RH-dehu.png";
        } else {
          thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-Dehu.png";
        }
      } else if (thisEquipment.typeName.indexOf('Drying System') > -1) {
        thisEquipment.iconCategory = 'Drying System';
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-Floor-Dryer.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-Floor-Dryer.png";
      } else if (thisEquipment.typeName.indexOf('Heater') > -1) {
        thisEquipment.iconCategory = 'Heater';
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-Heater.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-Heater.png";
      } else if (thisEquipment.typeName.indexOf('A/C') > -1 || thisEquipment.typeName.indexOf('onditioner') > -1) {
        thisEquipment.iconCategory = 'A/C';
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-AC-Unit.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-AC-Unit.png";
      } else if (thisEquipment.typeName.indexOf('Fan') > -1 || thisEquipment.typeName.indexOf('Air Mover') > -1 || thisEquipment.typeName.indexOf('Vane Axial') > -1 ||
                 thisEquipment.typeName.indexOf('Blower') > -1) {
        if (thisEquipment.modelType && (thisEquipment.modelType.toLowerCase().indexOf('airmax') > -1 || thisEquipment.modelType.toLowerCase().indexOf('air max') > -1)) {
          thisEquipment.iconCategory = 'AirMAX';
          thisEquipment.nonDryTAGiconURL = "img/equipment-icon-phoenix-without DryTAG-Airmax.png";
          thisEquipment.dryTAGiconURL = "img/equipment-icon-phoenix-DryTAG-Airmax.png";
        } else {
          thisEquipment.iconCategory = 'Fan';
          thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-Air-Mover.png";
          thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-Air-Mover.png";
        }
      } else if (thisEquipment.typeName.indexOf('Power Distribution') > -1 || thisEquipment.typeName.indexOf('Splitter Box') > -1) {
        thisEquipment.iconCategory = 'Splitter Box';
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-Splitter.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-Splitter.png";
      } else if (thisEquipment.typeName.indexOf('HEPA') > -1 || thisEquipment.typeName.indexOf('Scrubber') > -1) {
        thisEquipment.iconCategory = 'Air Scrubber';
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-HEPA-Air-Scrubber.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-HEPA-Air-Scrubber.png";
      } else if (thisEquipment.msensorId && thisEquipment.msensorId > -1) {
        thisEquipment.modelType = 'DrySENSE';
        thisEquipment.iconCategory = 'DrySENSE';
        thisEquipment.nonDryTAGiconURL = "img/DrySENSE_icon.png";
        thisEquipment.dryTAGiconURL = "img/DrySENSE_icon.png";
      } else if (thisEquipment.typeName.indexOf('Hygrometer') > -1) {
        thisEquipment.iconCategory = 'Hygrometer';
        thisEquipment.nonDryTAGiconURL = "img/ThermoHygrometer.png";
        thisEquipment.dryTAGiconURL = "img/DryTAG_RH-standalone.png";
      } else {
        thisEquipment.iconCategory = 'Unknown';
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-Custom.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-Custom.png";
      }
      return thisEquipment;
    };

    Dehumidifier.getLatestDisplayFirmware = function (isBGM13p) {
      if (isBGM13p) {
        return $http.get(API_Base + 'info/BGM13P_display_firmware?apikey=' + Authentication.apiKey).then(function (data) {
          return data && data.data && data.data.message;
        }, $q.reject);
      }
      return $http.get(API_Base + 'info/display_firmware?apikey=' + Authentication.apiKey).then(function (data) {
        return data && data.data && data.data.message;
      }, $q.reject);
    };

    Dehumidifier.getLatestPowerFirmware = function () {
      // return $http.get("templates/drymax_power_special_0.3.2_test_bootload.hex").then(function (data) {
      //    return data && data.data;
      //  }, $q.reject);
      return $http.get(API_Base + 'info/power_firmware?apikey=' + Authentication.apiKey).then(function (data) {
        return data && data.data && data.data.message;
      }, $q.reject);
    };

    Dehumidifier.getLatestDisplayVersion = function () {
      return $http.get(API_Base + 'info/display_version?apikey=' + Authentication.apiKey).then(function (data) {
        return $http.get(API_Base + 'info/BGM13P_display_version?apikey=' + Authentication.apiKey).then(function (data2) {
          return {bgm111: data && data.data && data.data.message, bgm13p: data2 && data2.data && data2.data.message};
        }, $q.reject);
      }, $q.reject);
    };

    Dehumidifier.getLatestPowerVersion = function () {
      return $http.get(API_Base + 'info/power_version?apikey=' + Authentication.apiKey).then(function (data) {
        return data && data.data && data.data.message;
      }, $q.reject);
    };

    Dehumidifier.encryptCommand = function (deviceId, command) {
      var binaryCommand = new Array(14),
        time,
        target,
        offset = 0,
        i;

      if (command.time !== undefined) {
        time = command.time || 0;
        binaryCommand[0] = 0xFF & (time >> 24);
        binaryCommand[1] = 0xFF & (time >> 16);
        binaryCommand[2] = 0xFF & (time >> 8);
        binaryCommand[3] = 0xFF & time;
        offset += 4;
      }
      if (command.name) {
        target = commandNames.indexOf(command.name);
        if (target < 0) {
          return $q.reject({status: 403, message: 'Invalid command name: ' + command.name});
        }

        binaryCommand[offset] = 0xFF & target;
        offset += 1;
      }
      if (command.value) {
        if (typeof command.value === 'string') {
          for (i = 0; i < command.value.length && i < 9; i += 1) {
            binaryCommand[i + offset] = 0xFF & command.value.charCodeAt(i);
          }
        } else if (typeof command.value === 'number') {//Two byte numbers are the default--otherwise, use int arrays for 1, 4, or 8 byte numbers.
          binaryCommand[offset] = 0xFF & (command.value >> 8);
          binaryCommand[offset + 1] = 0xFF & command.value;
        } else if (command.value.length) {
          for (i = 0; i < command.value.length && i < 9; i += 1) {
            binaryCommand[i + offset] = 0xFF & command.value[i];
          }
        }
      }
      return $http.put(API_Base + 'command', {deviceId: deviceId, command: window.btoa(String.fromCharCode.apply(null, binaryCommand))}).then(function (data) {
        if (data && data.data) {
          if (data.data.status !== undefined && data.data.status !== 0) {
            return $q.reject(data.data);
          }
          return data.data;
        }
        return $q.reject();
      }, $q.reject);
    };

    return Dehumidifier;
  }];

  angular.module('ThermaStor')
    .service('Dehumidifier', dehuFactory);
}());