var gm_authFailure;

(function () {
  'use strict';
  var appRun = [
    '$ionicPlatform',
    '$rootScope',
    '$ionicLoading',
    '$http',
    '$timeout',
    'Authentication',
    // 'miLogger',
    '$ionicPopup',
    '$ionicPopover',
    '$localStorage',
    '$state',
    '$q',
    'Job',
    function ($ionicPlatform, $rootScope, $ionicLoading, $http, $timeout, Authentication, /*miLogger,*/ $ionicPopup, $ionicPopover, $localStorage, $state, $q, Job) {
      var stateTimeout = {},
        googleApiKey = "AIzaSyD26vJwvZ2dL8MLPKs416Cf0HlcYiCtnxY",
        myStringify = function (x) {
          if (!x) {
            return x;
          }
          return JSON.stringify(x).replace(/"/g, "");
        },
        $mainContainer;

      $ionicPlatform.ready(function () {
        if (ionic.Platform.isIOS()) {//iOS doesn't handle :focus selectors correctly--they work when the app is first started but not after multiple pages with inputs have been navigated to
          $rootScope.iOSFocusElement = function (event) {//however, ng-focus and ng-blur work fine on iOS--so it's necessary to manage things in a clunkier way.
            if (event && event.target && event.target.className) {
              if (event.target.className.indexOf("has-focus") < 0) {
                event.target.className += ' has-focus';
              }
            }
          };
          $rootScope.iOSUnfocusElement = function (event) {
            if (event && event.target && event.target.className) {
              if (event.target.className.indexOf("has-focus") >= 0) {
                event.target.className = event.target.className.replace("has-focus", "");
              }
            }
          };
        } else {
          $rootScope.iOSFocusElement = function () {
          };

          $rootScope.iOSUnfocusElement = function () {
          };
        }

        try {
          Capacitor.Plugins.Keyboard.setAccessoryBarVisible({isVisible: true});
        } catch (ignore) {
          //console.log("Keyboard accessory bar not functioning properly.");
        }

        $rootScope.useBackUpAddresses = false;
        gm_authFailure = function () {
          console.log("Google autocomplete failed");
          $rootScope.useBackUpAddresses = true;
        };

        if (!$rootScope.userName) {
          $rootScope.userName = $localStorage.userName;
        }
      });
      /*jslint unparam: true*/
      $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
        console.log("stateChangeError", error);
        $ionicLoading.hide();
        $rootScope.cancelTimeout();
      });
      /*jslint unparam:false */
      $rootScope.lastStateChangeTime = 0;
      $rootScope.$on('$stateChangeStart', function (e, toState) {
        $rootScope.lastStateChangeTime = (new Date()).getTime();
        if ($rootScope.closePopup  && $rootScope.closePopup.close) {
          $rootScope.closePopup.close();
        }
        stateTimeout = $timeout(function () { //If transition takes longer than 5 seconds, timeout.
          $ionicLoading.hide();
          // $ionicPopup.alert({'title': 'Timed Out', 'template': 'Communication with the server timed out. Please check your connection and try again.'});
          angular.forEach($http.pendingRequests, function (req) {
            if (req.abort) {
              req.abort.resolve();
            }
          });
        }, 5000);
      });
      // $interval(function () {
      //   if ((new Date()).getTime() - $rootScope.lastStateChangeTime > 1800000) {//If no user activity has occurred in the last 30 minutes, turn on a screensaver.
      //     $rootScope.showScreensaver = true;
      //   }
      // }, 600000);
      // $interval(function () {
      //   if ($rootScope.showScreensaver) {
      //     $rootScope.screensaverImageLocation = {left: Math.round(Math.random() * 70) + "%", top: Math.round(Math.random() * 80) + "%"};
      //   }
      // }, 10000);
      $rootScope.hideScreensaver = function () {
        $rootScope.showScreensaver = false;
        $rootScope.lastStateChangeTime = (new Date()).getTime();
      };
      $rootScope.$on('$stateChangeSuccess', function (e, toState) {
        $rootScope.cancelTimeout();
      });
      $rootScope.hideKeyboard = function () {
        if (window.cordova && window.cordova.plugins.Keyboard) {
          window.cordova.plugins.Keyboard.close();
        }
      };
      $rootScope.cancelTimeout = function () {
        $timeout.cancel(stateTimeout);
      };
      $rootScope.closeLoading = function () {
        $ionicLoading.hide();
        angular.forEach($http.pendingRequests, function (req) {
          if (req.abort) {
            req.abort.resolve();
          }
        });
      };
      $rootScope.preLoginErrorMessage = function (messageTitle, err) {
        $timeout(function () {
          $ionicLoading.hide();
        }, 100);
        if (err && err.title && err.template) {
          $ionicPopup.alert(err);
        } else if (!err || !err.status || err.status < 1) {
          $ionicPopup.alert({title: 'Could not connect to server', template: 'Check your internet connection.'});
        } else {
          $ionicPopup.alert({
            title: messageTitle,
            template: myStringify(err.message || (err.data && err.data.message) || err.data || err)
          });
        }
      };
      $rootScope.internetIsOkay = function (err) {
        if (!$localStorage.token) {
          if (!$rootScope.onlyLogoutPopup) {
            $rootScope.onlyLogoutPopup = $ionicPopup.confirm({
              title: "Could not connect to server",
              template: "You need to sign in to your account.",
              okText: "LOG IN",
              okType: "right-button",
              cancelText: "CANCEL",
              cancelType: "left-button"
            });
            $rootScope.onlyLogoutPopup.then(function (res) {
              Authentication.logOut(res);
              delete $rootScope.onlyLogoutPopup;
            });
          }
          return 0;
        }
        if (!err || !err.status || err.status < 1) {
          $ionicPopup.alert({title: 'Could not connect to server', template: 'Check your internet connection.'});
          return 0;
        }
        return 1;
      };
      $rootScope.handleInternetErrorMessage = function (messageTitle, err) {
        var message,
          parsedJSON;
        $timeout(function () {
          $ionicLoading.hide();
        }, 100);
        if ($rootScope.internetIsOkay(err)) {
          message = myStringify(err.message || (err.data && err.data.message));
          if (!message && err.data) {
            try {
              parsedJSON = JSON.parse(err.data);
              message = myStringify(parsedJSON.message || parsedJSON);
            } catch (ignore) {
            }
          }
          if (!message) {
            message = myStringify(err.data || err);
          }
          if (((message.indexOf("ccount not found or ") > -1 || message.indexOf("ccount entry not found") > -1) && message.indexOf("disabled") > -1) ||
              (message.indexOf("Invalid account") > -1 && message.indexOf("not associated with company") > -1)) {
            message = 'Problem unknown. Contact Therma-Stor support if the following message doesn\'t make sense: <br><br>"' + message + '"';
          }
          $ionicPopup.alert({
            title: messageTitle,
            template: message
          });
        }
      };
      delete $rootScope.onlyLogoutPopup;
      $rootScope.checkThenSignOut = function () {
        if ($state.current.url !== '/welcome' && $state.current.url !== '/enterEmail' &&
            $state.current.url !== '/signUp' && $state.current.url !== '/signupWait' && $state.current.url !== '/login' && $state.current.url !== '/forgotPassword' && $state.current.url !== '/troubleshoot' &&
            $state.current.url !== '/dehuExplanation' && $state.current.url !== '/initialAddUsers') {
          if (!$rootScope.onlyLogoutPopup) {
            $rootScope.onlyLogoutPopup = $ionicPopup.confirm({
              title: "You are logged out",
              template: "Internet access and a user account are required to access this functionality. Do you want to log in?",
              okText: "LOG IN",
              okType: "right-button",
              cancelText: "CANCEL",
              cancelType: "left-button"
            });
            $rootScope.onlyLogoutPopup.then(function (res) {
              Authentication.logOut(res);
              delete $rootScope.onlyLogoutPopup;
            });
          }
        }
      };
      $rootScope.signOutPopover = function ($event) {
        var popoverPromise;
        popoverPromise = $ionicPopover.fromTemplateUrl('views/Main/accountPopover.html');
        popoverPromise.then(function (popover) {
          $rootScope.popover = popover;
          $rootScope.popover.show($event);
        });
      };
      $rootScope.goToStateFromPopover = function (destination) {
        if ($rootScope.popover) {
          $rootScope.popover.hide();
          $rootScope.popover = undefined;
        }
        $state.go(destination);
      };
      $rootScope.signOut = function () {
        if ($rootScope.popover) {
          $rootScope.popover.hide();
          $rootScope.popover = undefined;
        }
        Authentication.logOut(true);
        window.onerror = function () {};
      };
      $rootScope.reloadAutoComplete = function () {
        var newAutoComplete;

        try {
          if (google.maps.places.Autocomplete) {
            return;
          }
        } catch (e) {
          console.log("reloading autocomplete API");
        }

        newAutoComplete = document.createElement('script');
        newAutoComplete.type = "text/javascript";
        newAutoComplete.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyAwC3yLYo3LqAkk-EA6ZE6-k0F1xEbaBd4&libraries=places";
        document.head.appendChild(newAutoComplete);
      };
      $rootScope.openInNewWindow = function (url, silent) {
        if (silent || (silent === 0 && $localStorage.mapsInfoRead && $localStorage.userEmail && $localStorage.mapsInfoRead.indexOf($localStorage.userEmail) !== -1)) {
          window.open(url, '_system', 'location=yes');
        } else if (silent === 0) {
          $rootScope.pester = {silence: false};
          $ionicPopup.confirm({
            title: "External link",
            scope: $rootScope,
            template: '<p>Map will be opened in another window.</p>' +
              '<ion-checkbox checked="false" ng-model="$root.pester.silence">Don\'t show again</ion-checkbox>',
            okText: "OK",
            okType: "right-button",
            cancelText: "CANCEL",
            cancelType: "left-button"
          }).then(function (res) {
            if (res) {
              if (!$localStorage.mapsInfoRead) {
                $localStorage.mapsInfoRead = [];
              }
              if ($rootScope.pester.silence) {
                $localStorage.mapsInfoRead.push($localStorage.userEmail);
              }
              window.open(url, '_system', 'location=yes');
            }
          });
        } else {
          $ionicPopup.alert({'title': 'External link', 'template': 'Opening a separate browser window to go to ' + url}).then(function () {
            window.open(url, '_system', 'location=yes');
          });
        }
      };
      $rootScope.openExternalMap = function (latitude, longitude) {
        if (latitude || longitude) {
          if (ionic.Platform.isAndroid()) {
            $rootScope.openInNewWindow("geo:0,0?q=" + latitude + "," + longitude, 0);
          } else if (ionic.Platform.isIOS()) {
            $rootScope.openInNewWindow("maps://?q=" + latitude + "," + longitude, 0);
          } else {
            $rootScope.openInNewWindow("https://maps.google.com/?q=" + latitude + "," + longitude, 0);
          }
        }
      };
      $rootScope.addressLocations = {};
      $rootScope.openExternalAddressedMap = function (address) {
        if (ionic.Platform.isAndroid()) {
          $rootScope.openInNewWindow("geo:0,0?q=" + encodeURIComponent(address), 0);
        } else if (ionic.Platform.isIOS()) {
          $rootScope.openInNewWindow("maps://?q=" + encodeURIComponent(address), 0);
        } else {
          $rootScope.openInNewWindow("https://maps.google.com/?q=" + encodeURIComponent(address), 0);
        }
      };
      $rootScope.mapsAreCancelled = 0;
      $rootScope.cancelMapRendering = function () {//Cancel rendering new maps for at least the next 4 seconds.
        $rootScope.mapsAreCancelled = (new Date()).getTime();
        setTimeout(function () {
          if ((new Date()).getTime() - $rootScope.mapsAreCancelled > 3000) {
            $rootScope.mapsAreCancelled = 0;
          }
        }, 4000);
      };
      $rootScope.uncancelMapRendering = function () {
        $rootScope.mapsAreCancelled = 0;
      };
      $rootScope.renderMap = function (mapId, address) {
        var myElement = mapId && document.getElementById(mapId),
          deferredPromise = $q.defer();

        if (myElement) {
          if (myElement.className && myElement.className.indexOf("rendered") > -1) {//Do not redraw maps that are already there, to minimize Google API calls.
            if ($rootScope.mapsAreCancelled) {
              deferredPromise.reject();
            } else {
              deferredPromise.resolve();
            }
          } else {
            myElement.className = myElement.className + " rendered";
            if (address) {
              myElement.src = "https://www.google.com/maps/embed/v1/place?zoom=13&key=" + googleApiKey + "&q=" + encodeURIComponent(address);
            } else {
              myElement.src = "https://www.google.com/maps/embed/v1/place?zoom=13&key=" + googleApiKey + "&q=" + $localStorage.location.latitude + "," + localStorage.location.longitude;
            }

            setTimeout(function () {
              if ($rootScope.mapsAreCancelled) {
                deferredPromise.reject();
              } else {
                deferredPromise.resolve();
              }
            }, 200);
          }
        } else {
          if ($rootScope.mapsAreCancelled) {
            deferredPromise.reject();
          } else {
            deferredPromise.resolve();
          }
        }
        return deferredPromise.promise;
      };
    }],
    appResolve = {
      // authUser: ['Authentication', '$state', '$q',
      //   function (Authentication, $state, $q) {
      //     return Authentication.login()
      //       .then($q.when, function (result) {
      //         console.log(result);
      //         return $state.go('login');
      //       });
      //   }]
    },
    appConfig = ['$stateProvider', '$urlRouterProvider', '$httpProvider', '$ionicLoadingConfig', '$ionicConfigProvider', 'ionicDatePickerProvider', '$compileProvider',
      function ($stateProvider, $urlRouterProvider, $httpProvider, $ionicLoadingConfig, $ionicConfigProvider, ionicDatePickerProvider, $compileProvider) {
        $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|capacitor):/);
        $ionicConfigProvider.tabs.position('bottom');
        $ionicConfigProvider.tabs.style('standard');
        $ionicConfigProvider.views.swipeBackEnabled(false);//Prevents white screen on iOS when swiping from the left side of the screen.
        $stateProvider
          .state('app', {
            url: "/app",
            abstract: true,
            templateUrl: "templates/menu.html",
            controller: "appController as appVM",
            resolve: appResolve
          });
        //If none of the above states are matched, use this as the fallback.
        $urlRouterProvider.otherwise('/login');
        /// date picker options & config
        var datePickerObj = {
          inputDate: new Date(),
          titleLabel: 'Select a Date',
          setLabel: 'SET',
          closeLabel: 'CLOSE',
          mondayFirst: false,
          weeksList: ["S", "M", "T", "W", "T", "F", "S"],
          monthsList: ["Jan", "Feb", "Mar", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"],
          templateType: 'popup',
          from: new Date(2017, 7, 13),
          to: new Date(),
          showTodayButton: false,
          dateFormat: 'dd MMMM yyyy',
          closeOnSelect: false,
          disableWeekdays: []
        };
        ionicDatePickerProvider.configDatePicker(datePickerObj);
        //$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|content|sms|tel|file):/);
        $httpProvider.interceptors.push(['$q', '$injector', function ($q, $injector) {
          return {
            request: function (config) {
              //Add an http aborter.
              var Authentication,
                $rootScope = $injector.get('$rootScope'),
                token,
                // oldTokenLoc,
                // endTokenLoc,
                // afterToken,
                abort = $q.defer();
              config.abort = abort;
              config.timeout = abort.promise;

              if (config.url.indexOf('.html') === -1 && config.url.indexOf('apikey') === -1 && config.headers["Content-Type"] !== "application/x-www-form-urlencoded") {
                //Add token onto end of request.
                Authentication = $injector.get('Authentication');
                token = Authentication.getAccessToken();
                if (!token) {//Don't send a request to server if it has no token.
                  $rootScope.checkThenSignOut();
                  return $q.reject("Did not send a request to " + config.url + " because no token was found.");
                }
                config.headers['X-Auth-Token'] = token;
                // oldTokenLoc = config.url.indexOf('accesstoken=');
                // if (oldTokenLoc !== -1) {
                //   //If there's already a token in the url (because the packet is being re-tried),
                //   //replace it with the new one rather than tacking another on at the end.
                //   endTokenLoc = config.url.indexOf('&', oldTokenLoc);
                //   if (endTokenLoc === -1) {
                //     afterToken = "";
                //   } else {
                //     afterToken = config.url.slice(endTokenLoc);
                //   }
                //   config.url = config.url.slice(0, oldTokenLoc + 12) + token + afterToken;
                //   return config;
                // }
                // if (config.url.indexOf('?') === -1) {
                //   config.url += "?";
                // } else {
                //   config.url += "&";
                // }
                // config.url += "accesstoken=" + encodeURIComponent(token);
              }
              return config;
            },

            responseError: function (response) {
              var Authentication = $injector.get('Authentication'),
                $rootScope = $injector.get('$rootScope');
              if (response.status === 401) {
                console.log("401!");
                if (response.config.url.indexOf('oauth2/token') !== -1) {
                  console.log("invalid credentials " + response.config.url + " ... " + response.config.data.grant_type);
                  //If refresh token isn't being accepted, then token should be invalidated.
                  $rootScope.checkThenSignOut();
                  return $q.reject(response);
                }
                Authentication.invalidateToken();//Invalidate access token;
                return Authentication.login(response.data).then(function () {
                  var $http = $injector.get('$http');
                  console.log("retrying", response.config);
                  return $http(response.config);//Send the last http request again after updating token.
                }, $q.reject);
              }
              if (response.status === -1) {
                console.log("-1 error: " + JSON.stringify(response));
              } else {
                console.log("other error: " + response.status);
              }
              return $q.reject(response);
            }
          };
        }]);
        //Customize ionicLoading, add cancel button.
        $ionicLoadingConfig.template = '<ion-spinner class="spinner-assertive huge"></ion-spinner><h3>Loading...</h3><button class="button button-block button-transparent icon icon-right ion-close-circled" ng-click="closeLoading()">Cancel</button>';
      }],
    // appCtrl = [
    //   'Authentication',
    //   'miLogger',
    //   function (Authentication, miLogger) {
    //     miLogger.setDefaultPayload({'accessToken': Authentication.getAccessToken()});
    //   }];
    appCtrl = [function () {}];
  angular.module('User', []);
  angular.module('Dehumidifier', []);
  angular.module('ThermaStor', [
    'ionic',
    'ngStorage',
    // 'miAnalytics',
    'ionic-datepicker',
    'User',
    'Dehumidifier'
  ])
    .config(appConfig)
    .controller('appController', appCtrl)
    .run(appRun)
    .value('Integration_Base', 'https://integrationservicep.azurewebsites.net/v1/')
    .value('API_Base', 'https://restoreservice.thermastor.com/v1/')//'https://restoreservicep.azurewebsites.net/v1/' or 'https://rservice.azurewebsites.net/v1/' or 'https://tsiot.thermastor.com:8443/' or 'https://restorestaging.azurewebsites.net/v1/')
    .directive('ngEnter', function () {//From https://gist.github.com/EpokK/5884263
      return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
          if (event.which === 13) {
            scope.$apply(function () {
              scope.$eval(attrs.ngEnter);
            });

            event.preventDefault();
          }
        });
      };
    })
    .directive('expanding', ['$timeout', function ($timeout) {//https://stackoverflow.com/questions/17772260/textarea-auto-height
      return {
        restrict: 'A',
        link: function ($scope, element) {
          var resize = function () {
            if (!element[0].style.height || (element[0].scrollHeight > $scope.lastTextAreaHeight)) {
              element[0].style.height = (element[0].scrollHeight + 6) + "px";
            }
            $scope.lastTextAreaHeight = element[0].scrollHeight;
          };
          $scope.lastTextAreaHeight = element[0].style.height;
          element.on("input change", resize);
          $timeout(resize, 0);
        }
      };
    }]);
}());
(function () {
  'use strict';

  var AuthenticationDefinition = ['$q', '$http', '$localStorage', '$state', 'API_Base', '$rootScope', function ($q, $http, $localStorage, $state, API_Base, $rootScope) {
    var Authentication = {},
      token = {},
      ourAPIKey = 'aae9458c73b96c922536b8c3abd7aa69b39bbf0a',
      myWebsocket,
      websocketPingCount = 0,
      websocketState = 0, // 0 = closed, 1 = open but no token sent, 2 = open and token sent
      urlEncodedFormConfig = {
        transformRequest: function (obj) {// from http://stackoverflow.com/a/24964658/1148769
          var str = [], p;
          for (p in obj) {
            if (obj.hasOwnProperty(p)) {
              str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
          }
          return str.join("&");
        },
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      },
      authenticateWebsocket = function () {
        //console.log("authenticating?");
        if (myWebsocket && websocketState === 1 && token && token.accesstoken) {
          //console.log("authenticating.");
          myWebsocket.send('accesstoken=' + token.accesstoken);
          websocketState = 2;
        }
      },
      newWebsocket = function () {
        myWebsocket = new window.WebSocket('ws' + API_Base.slice(4) + 'ws_settings');

        myWebsocket.addEventListener("open", function () {
          console.log("websocket opened " + (new Date()).getTime());
          websocketState = 1;
          authenticateWebsocket();
        });

        myWebsocket.addEventListener('message', function (event) {
          var newData, newID, i;

          //console.log(".. " + JSON.stringify(event.data));
          if (event.data && event.data[0] === '.') {
            websocketPingCount = 0;
          } else if (event.data && event.data.slice(0, 10) === 'settings:{') {
            newData = JSON.parse(event.data.slice(9));
            $rootScope.$broadcast('dehu_update_' + newData.dehuId, newData);
          } else if (event.data && event.data.slice(0, 8) === 'dehu_id:') {
            newID = parseInt(event.data.slice(8), 10);
            for (i = 0; i < $rootScope.dehus.length; i += 1) {
              if ($rootScope.dehus[i].dehuId === newID) {//Check if dehumidifier is already present in side menu.
                return;
              }
            }
            //Dehumidifier.get(newID).then(function (response) {});
            $rootScope.getDehuList();//If newID not found, update entire side menu.
          }
        });

        myWebsocket.addEventListener("close", function () {
          console.log("websocket closed " + (new Date()).getTime());
          websocketState = 0;
          myWebsocket.close();
          setTimeout(function () {
            newWebsocket();
          }, 10000);
        });

        myWebsocket.addEventListener("error", function (event) {
          console.log("websocket error:", event);
        });
      },
      /**
       * Store token in memory if valid.
       * @param {object} {accesstoken, expires_in, refreshtoken} optionally wrapped in 'data' field
       * @return {promise} resolves with token object, rejects with $ionicPopup.alert template
       */
      setToken = function (newToken) {
        if (newToken.hasOwnProperty('data')) {
          newToken = newToken.data;
        }
        if (!newToken.accesstoken || !newToken.expires_in || !newToken.refreshtoken) {
          return $q.reject({title: 'Error', template: 'There was an error authorizing the user'});
        }

        token = newToken;
        token.expiry = (new Date()).getTime() + (newToken.expires_in * 1000); // calculate when the token will expire so we know to refresh it
        $localStorage.token = token;

        authenticateWebsocket();
        return $q.when(token);
      },
      /**
       * Get token from API using either password or previous token. On success, calls setToken and resolves with token.
       * @param  {Object} {refreshtoken (string) OR email (string), password (string)}, redirectLogin (boolean)
       * @return {promise} resolves with user object if successful
       */
      getToken = function (payload, redirectLogin) {
        if (websocketState > 1) {
          websocketState = 1;
        }
        payload.apikey = ourAPIKey;
        return $http.post(API_Base + 'oauth2/token', payload, urlEncodedFormConfig).then(function (response) {
          return setToken(response);
        }, function (err) {
          if (redirectLogin) {
            $rootScope.checkThenSignOut();
          }
          return $q.reject(err);
        });
      },
      isTokenExpired = function () {
        var expireDate, now;
        if (!$localStorage.token || $localStorage.token.expireOverride || !$localStorage.token.expiry) {
          return true;
        }
        expireDate = new Date($localStorage.token.expiry);
        now = new Date();
        return expireDate < now;
      };

    Authentication.apiKey = ourAPIKey;

    Authentication.isTokenExpired = function () {
      return isTokenExpired();
    };

    Authentication.forgotPassword = function (email) {
      return $http.post(API_Base + 'password_reset', {apikey: ourAPIKey, email: email}, urlEncodedFormConfig).then(function (response) {
        return $q.when(response);
      }, $q.reject);
    };

    Authentication.getAccessToken = function () {
      if (!token.accesstoken && $localStorage.token) {//Try to load from local if it exists.
        token = $localStorage.token;
      }
      return token.accesstoken;
    };

    Authentication.authenticateUser = function (user) {
      if (!user || !user.email || !user.password) {
        return $q.reject({title: 'Authentication failed', template: "Email or password missing"});
      }
      return getToken({'grant_type': 'password', 'username': user.email, 'password': user.password});
    };

    Authentication.invalidateToken = function () {
      if ($localStorage.token) {
        $localStorage.token.expireOverride = true;
      }
    };

    Authentication.login = function (lastErr) {
      console.log("authentication login");
      if (!isTokenExpired()) {
        token = $localStorage.token;
        return $q.when($localStorage.token);
      }
      if ($localStorage.token) {
        console.log("using refresh token");// + $localStorage.token.refreshtoken);
        return getToken({'grant_type': 'refresh_token', 'refreshtoken': $localStorage.token.refreshtoken}, true);
      }
      $state.go('login');
      return $q.reject(lastErr || 'Could not find token.');
    };

    Authentication.openWebsocket = function () {
      if (websocketState > 0 || !window.WebSocket) {
        return;
      }

      if (!myWebsocket) {
        setInterval(function () {
          //console.log("websocket state: " + myWebsocket.readyState);
          if (websocketPingCount < 1) {
            myWebsocket.send('.');//Ping the server.
            websocketPingCount += 1;
          } else {
            //console.log("websocket should close");
            websocketPingCount = 0;
            myWebsocket.close();
          }
        }, 60000);

        newWebsocket();

        // window.test = function () {
        //   console.log("closing");
        //   myWebsocket.close();
        // };
      }
    };

    Authentication.newUser = function (newUser) {
      return $http.post(API_Base + 'account?apikey=' + ourAPIKey, newUser).then(function (response) {
        //return setToken(response);
        return response.data;
      }, $q.reject);
    };

    Authentication.logOut = function (redirectLogin) {
      console.log("logging out!");
      //Clear localstorage variable and redirect to login page.
      delete $localStorage.token;
      token = {};
      if (redirectLogin) {
        delete $localStorage.userEmail;
        delete $rootScope.userName;
        delete $rootScope.companyName;
        $state.go('login');
      }
      if (!$localStorage.token) {
        return $q.when('logged out');
      }
      return $q.reject({title: 'Error', template: 'There was an error logging out'});
    };

    return Authentication;
  }];

  angular.module('ThermaStor')
    .service('Authentication', AuthenticationDefinition);
}());

(function () {
  'use strict';
  var dehuFactory = ['$http', 'API_Base', '$q', '$localStorage', '$rootScope', 'Authentication', 'Snackbar', function ($http, API_Base, $q, $localStorage, $rootScope, Authentication, Snackbar) {
    var Dehumidifier = {},
      uploadLocks = {},
      startCallback,
      successCallback,
      errorCallback,
      commandNames = ['pump', 'resetHours', 'rhOffset', 'tempOffset', 'setpoint', 'nameStart', 'nameEnd', 'fanMode', 'sleepMins', 'dehuState', 'offsetTime',
        'runTest', 'quietMode', 'xlMode', 'shutdownTime', 'startProgramming', 'programPowerBoard', 'storeCTSNumber', 'resetFloatFailure'],
      allFFString = String.fromCharCode(0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF),
      unfurlBase64Blocks = function (list) {
        var unfurledList = [],
          decodedChunk,
          decodedSample,
          i,
          j;

        if (!list || !list.length) {
          return [];
        }

        for (i = 0; i < list.length; i += 1) {
          decodedChunk = window.atob(list[i]);
          for (j = 0; j <= decodedChunk.length - 16; j += 16) {
            decodedSample = decodedChunk.slice(j, j + 16);
            if (decodedSample !== allFFString) {
              unfurledList.push(window.btoa(decodedSample));
            }
          }
        }
        return unfurledList;
      },
      myTrim = function (aString) {
        if (aString && aString.length) {
          return aString.replace(/\0/g, '');
        }
        return aString;
      },
      renameDrySENSEsamples = function (uploadObject, isOneHourDrySENSE) {
        var newObject = angular.copy(uploadObject);
        if (isOneHourDrySENSE) {
          newObject.oneHourSamples = newObject.samples;
          delete newObject.samples;
        }
        return newObject;
      },
      uploadHistoricalDataContinuously = function (device) {
        if (uploadLocks[device.address] === -1) {//Currently uploading data, already.
          return;
        }
        if (uploadLocks[device.address]) {
          clearTimeout(uploadLocks[device.address]);
        }
        uploadLocks[device.address] = -1;//Currently trying to upload data.

        if (device.checkUpload || device.downloading || !device.serialNumber || !device.timestamp || !device.data || !device.data.length) {
          if ((!device.timestamp || device.timestamp < 1) && device.data && device.data.length) {
            device.uploadQueue = [];
            device.data = [];
            device.settingsChanged = [];
            device.dataLength = undefined;
            if (errorCallback && ((typeof errorCallback) === "function")) {
              errorCallback(device, "negative timestamp -- deleting data");
            }
            uploadLocks[device.address] = setTimeout(function () {
              uploadHistoricalDataContinuously(device);
            }, 5000 + 10000 * Math.random());//Try again after about 10 seconds.
            return;
          }
          uploadLocks[device.address] = setTimeout(function () {
            uploadHistoricalDataContinuously(device);
          }, 1000 + 1000 * Math.random());//Try again after about a second.
          return;
        }
        console.log("upload data " + device.serialNumber + ", " + device.data.length);
        console.log(JSON.stringify(device.data));

        var data = {},
          aTime = {},
          endOfQueue = 0,
          cancelUpload = $q.defer(),
          uploadFinished = false,
          beginningOfData;
        if (startCallback && ((typeof startCallback) === "function")) {
          startCallback(device);
        }

        data.deviceId = device.serialNumber;
        if (device.uploadQueue && device.uploadQueue.length) {
          endOfQueue = device.uploadQueue.length - 1;
          data.samplingInterval = device.uploadQueue[endOfQueue].samplingMins || 15;
        } else {
          data.samplingInterval = 15;
        }
        data.times = [];
        if (device.settingsChanged && device.settingsChanged.length) {
          data.settingsChanged = unfurlBase64Blocks(device.settingsChanged);
        } else {
          data.settingsChanged = [];
        }
        for (0; endOfQueue >= 0 && device.uploadQueue && device.uploadQueue.length && device.uploadQueue[endOfQueue].samplingMins === data.samplingInterval; endOfQueue -= 1) {
          aTime = {timestamp: device.uploadQueue[endOfQueue].timestamp, jobHours: device.uploadQueue[endOfQueue].jobHours, defrostCycles: device.uploadQueue[endOfQueue].defrostCycles,
            pumpCycles: device.uploadQueue[endOfQueue].pumpCycles, sampleNo: device.uploadQueue[endOfQueue].sampleNo, displayBdVersion: myTrim(device.uploadQueue[endOfQueue].displayBdVersion),
            powerBdVersion: myTrim(device.uploadQueue[endOfQueue].powerBdVersion), lifetimeHours: device.uploadQueue[endOfQueue].lifetimeHours, isXL: device.uploadQueue[endOfQueue].isXL};
          if (device.uploadQueue[endOfQueue].location && (device.uploadQueue[endOfQueue].location.latitude || device.uploadQueue[endOfQueue].location.longitude)) {
            aTime.location = device.uploadQueue[endOfQueue].location;
          }
          data.times.push(aTime);
        }
        endOfQueue += 1;
        if (endOfQueue === 0) {
          beginningOfData = 0;
        } else {
          beginningOfData = device.uploadQueue[endOfQueue].dataStart;
        }
        data.data = device.data.slice(beginningOfData);

        setTimeout(function () {//If upload takes more than 60 seconds, cancel it and try again in about 5 minutes.
          if (!uploadFinished) {
            cancelUpload.resolve();
            if (errorCallback && ((typeof errorCallback) === "function")) {
              errorCallback(device, "upload cancelled due to unresponsive server");
            }
            uploadLocks[device.address] = setTimeout(function () {
              uploadHistoricalDataContinuously(device);
            }, 5 * 60000 + 30000 * Math.random());
            console.log("cancelling upload!", new Date());
          }
        }, 55000 + 10000 * Math.random());

        // console.log("sending " + JSON.stringify(data));
        $http.post(API_Base.replace(/v1/g, 'v2') + 'upload' + ($localStorage.myPhoneName ? '?phoneName=' + encodeURIComponent($localStorage.myPhoneName) : ''),
            data, {timeout: cancelUpload}).then(function (databaseInfo) {
          uploadFinished = true;
          if (databaseInfo && databaseInfo.data && databaseInfo.data.dehuId) {
            device.databaseId = databaseInfo.data.dehuId;
          }
          if (databaseInfo && databaseInfo.data && databaseInfo.data.claimStatus) {
            if (device.claimStatus === 'mine' && databaseInfo.data.claimStatus !== 'mine') {
              device.beaconUUID = undefined;
              device.beaconName = undefined;
              device.hasBeacon = false;
            }
            device.claimStatus = databaseInfo.data.claimStatus;
            if (device.claimStatus !== 'mine' && device.claimStatus !== 'theirs') {
              Dehumidifier.addClaimPossibility(device);
            }
          }

          device.settingsChanged = [];
          device.data = device.data.slice(0, beginningOfData);
          if (device.uploadQueue && device.uploadQueue.length) {
            device.uploadQueue = device.uploadQueue.slice(0, endOfQueue);
          }
          if (successCallback && ((typeof successCallback) === "function")) {
            successCallback(device);
          }

          if (device.data && device.data.length) {
            uploadLocks[device.address] = setTimeout(function () {
              uploadHistoricalDataContinuously(device);
            }, 25000 + 10000 * Math.random());//Send more data after about 30 seconds.
          } else {
            uploadLocks[device.address] = setTimeout(function () {
              uploadHistoricalDataContinuously(device);
            }, 15 * 60000 + 4 * 60000 * Math.random());//If app is still running, upload data again after at least 15 minutes.
          }

          if (databaseInfo && databaseInfo.data && device.name && (databaseInfo.data.dehuName !== device.name)) {
            Dehumidifier.postDehuName(device.databaseId, device.name);
          }
        }, function (err) {
          uploadFinished = true;
          if (errorCallback && ((typeof errorCallback) === "function")) {
            errorCallback(device, err.data || err);
          }
          if (err && err.status && err.status >= 400 && err.status < 500) {//Don't treat 400 errors the same as other error codes.
            uploadLocks[device.address] = setTimeout(function () {
              uploadHistoricalDataContinuously(device);
            }, 15 * 60000 + 4 * 60000 * Math.random());//If app is still running, upload data again after at least 15 minutes.
          } else {
            uploadLocks[device.address] = setTimeout(function () {
              uploadHistoricalDataContinuously(device);
            }, 55000 + 10000 * Math.random());//Try again after about 60 seconds.
          }
        });
      };

    Dehumidifier.setUploadCallbacks = function (startCB, successCB, errorCB) {
      startCallback = startCB;
      successCallback = successCB;
      errorCallback = errorCB;
    };

    //Start uploading data from dehumidifier at regular intervals and block more than one function from running at the same time for a given dehumidifier.
    Dehumidifier.uploadHistoricalData = function (device) {
      uploadHistoricalDataContinuously(device);
    };

    Dehumidifier.uploadHistoricalDrySENSEData = function (device) {
      if (uploadLocks[device.address] === -1) {//Currently uploading data, already.
        return;
      }
      if (uploadLocks[device.address]) {
        clearTimeout(uploadLocks[device.address]);
      }
      uploadLocks[device.address] = -1;//Currently trying to upload data.

      if (device.downloading || device.downloadFailed) {
        uploadLocks[device.address] = setTimeout(function () {
          Dehumidifier.uploadHistoricalDrySENSEData(device);
        }, 1000 + 1000 * Math.random());//Try again after about a second.
        return;
      }
      if (!device.uploadObject || !device.uploadObject.samples) {
        uploadLocks[device.address] = setTimeout(function () {
          Dehumidifier.uploadHistoricalDrySENSEData(device);
        }, 15 * 60000 + 4 * 60000 * Math.random());//If app is still running, upload data again after at least 15 minutes.
        return;
      }

      var cancelUpload = $q.defer(),
        uploadFinished = false;
      if (startCallback && ((typeof startCallback) === "function")) {
        startCallback(device);
      }

      setTimeout(function () {//If upload takes more than 60 seconds, cancel it and try again in about 5 minutes.
        if (!uploadFinished) {
          cancelUpload.resolve();
          if (errorCallback && ((typeof errorCallback) === "function")) {
            errorCallback(device, "upload cancelled due to unresponsive server");
          }
          uploadLocks[device.address] = setTimeout(function () {
            Dehumidifier.uploadHistoricalDrySENSEData(device);
          }, 5 * 60000 + 30000 * Math.random());
          console.log("cancelling upload!", new Date());
        }
      }, 55000 + 10000 * Math.random());

      Dehumidifier.checkDrySENSETime(device.uploadObject.deviceId).then(function (uploadSequenceNumber) {
        if (device.isOneHourDrySENSE) {
          if (uploadSequenceNumber >= device.downloadSequenceNumber && uploadSequenceNumber < device.downloadSequenceNumber + 72) {
            device.downloadSequenceNumber = uploadSequenceNumber;//Don't send data that is old but is less than 3 days old.
            device.uploadObject = undefined;
            return $q.reject();
          }
        } else if (uploadSequenceNumber >= device.downloadSequenceNumber && uploadSequenceNumber < device.downloadSequenceNumber + 12) {
          device.downloadSequenceNumber = uploadSequenceNumber;//Don't send data that is old but is less than 3 days old.
          device.uploadObject = undefined;
          return $q.reject();
        }
        return $q.resolve();
      }, $q.resolve).then(function () {
        $http.post(API_Base.replace(/v1/g, 'v3') + 'msensor/upload' + ($localStorage.myPhoneName ? '?phoneName=' + encodeURIComponent($localStorage.myPhoneName) : ''),
            renameDrySENSEsamples(device.uploadObject, device.isOneHourDrySENSE), {timeout: cancelUpload}).then(function (response) {
          var databaseInfo = (response && response.data) || {};
          console.log("upload successful", device.uploadObject.deviceId, databaseInfo);
          uploadFinished = true;
          device.claimStatus = databaseInfo.claimStatus;
          if (databaseInfo.sensorName) {
            device.name = databaseInfo.sensorName;
          }
          device.msensorId = databaseInfo.msensorId;
          if (device.claimStatus && device.claimStatus === 'mine') {
            $localStorage.neverShowDrySENSElist = false;
          }
          if (device.claimStatus && device.claimStatus !== 'mine' && device.claimStatus !== 'theirs') {
            Dehumidifier.addClaimPossibility(device);
          } else {
            device.uploadObject = undefined;//Only erase data if DrySENSE is claimed by the company, since otherwise it won't be stored by the server.
          }

          if (successCallback && ((typeof successCallback) === "function")) {
            successCallback(device);
          }

          uploadLocks[device.address] = setTimeout(function () {
            Dehumidifier.uploadHistoricalDrySENSEData(device);
          }, 15 * 60000 + 4 * 60000 * Math.random());//If app is still running, upload data again after at least 15 minutes.
        }, function (err) {
          uploadFinished = true;
          if (errorCallback && ((typeof errorCallback) === "function")) {
            errorCallback(device, err.data || err);
          }
          if (err && err.status && err.status >= 400 && err.status < 500) {//Don't treat 400 errors the same as other error codes.
            uploadLocks[device.address] = setTimeout(function () {
              Dehumidifier.uploadHistoricalDrySENSEData(device);
            }, 15 * 60000 + 4 * 60000 * Math.random());//If app is still running, upload data again after at least 15 minutes.
          } else {
            uploadLocks[device.address] = setTimeout(function () {
              Dehumidifier.uploadHistoricalDrySENSEData(device);
            }, 55000 + 10000 * Math.random());//Try again after about 60 seconds.
          }
        });
      }, function () {
        if (successCallback && ((typeof successCallback) === "function")) {
          successCallback(device);
        }
        uploadLocks[device.address] = setTimeout(function () {
          Dehumidifier.uploadHistoricalDrySENSEData(device);
        }, 15 * 60000 + 4 * 60000 * Math.random());//If app is still running, upload data again after at least 15 minutes.
      });
    };

    Dehumidifier.markDryTAGforOverride = function (device) {
      if (!device || device.modelType !== 'DryTAG') {
        return;
      }
      device.overrideNextTime = 2;
    };

    Dehumidifier.uploadDryTAG = function (device, recentBluetooth, overrideTimeWindow) {
      if (uploadLocks[device.address] === -1) {//Currently uploading data, already.
        return;
      }
      if (uploadLocks[device.address]) {
        clearTimeout(uploadLocks[device.address]);
      }
      uploadLocks[device.address] = -1;//Currently trying to upload data.

      if (!device.uploadQueue || !device.uploadQueue.length) {
        uploadLocks[device.address] = setTimeout(function () {
          Dehumidifier.uploadDryTAG(device);
        }, 15 * 60000 + 4 * 60000 * Math.random());//If app is still running, upload data again after at least 15 minutes.
        return;
      }
      if (device.overrideNextTime === 1) {
        device.overrideNextTime = undefined;
        overrideTimeWindow = true;
      }
      if (!overrideTimeWindow && device.lastTrueUploadTime && (new Date()).getTime() < device.lastTrueUploadTime + 15 * 60000) {
        //Don't upload more than once every 15 minutes, even if data is available in the upload queue.
        uploadLocks[device.address] = setTimeout(function () {
          Dehumidifier.uploadDryTAG(device);
        }, 30000 + 60000 * Math.random());
        return;
      }

      var cancelUpload = $q.defer(),
        uploadData = {},
        uploadFinished = false;
      if (startCallback && ((typeof startCallback) === "function")) {
        startCallback(device);
      }

      setTimeout(function () {//If upload takes more than 60 seconds, cancel it and try again in about 5 minutes.
        if (!uploadFinished) {
          cancelUpload.resolve();
          if (errorCallback && ((typeof errorCallback) === "function")) {
            errorCallback(device, "upload cancelled due to unresponsive server");
          }
          uploadLocks[device.address] = setTimeout(function () {
            Dehumidifier.uploadDryTAG(device);
          }, 5 * 60000 + 30000 * Math.random());
          console.log("cancelling upload!", new Date());
        }
      }, 55000 + 10000 * Math.random());

      uploadData.deviceId = device.validUUID;
      uploadData.deviceExt = device.validUUIDext;
      if (device.hasRHCapabilities) {
        uploadData.rhSamples = angular.copy(device.uploadQueue);
      } else {
        uploadData.samples = angular.copy(device.uploadQueue);
      }
      $http.post(API_Base.replace(/v1/g, 'v3') + 'beacon/upload' + ($localStorage.myPhoneName ? '?phoneName=' + encodeURIComponent($localStorage.myPhoneName) : ''),
          uploadData, {timeout: cancelUpload}).then(function (response) {
        var databaseInfo = (response && response.data) || {};
        //console.log("upload successful", uploadData.deviceId, databaseInfo);
        uploadFinished = true;
        device.lastTrueUploadTime = (new Date()).getTime();
        device.beaconId = databaseInfo.beaconId;
        device.claimStatus = databaseInfo.claimStatus;
        device.name = databaseInfo.name;
        device.dehuId = databaseInfo.dehuId;
        device.cequipId = databaseInfo.cequipId;
        if (device.claimStatus && device.claimStatus !== 'mine' && device.claimStatus !== 'theirs') {
          Dehumidifier.addClaimPossibility(device, recentBluetooth);
        }

        if (device.hasRHCapabilities) {
          device.uploadQueue = device.uploadQueue.slice(uploadData.rhSamples.length);
        } else {
          device.uploadQueue = device.uploadQueue.slice(uploadData.samples.length);
        }
        if (successCallback && ((typeof successCallback) === "function")) {
          successCallback(device);
        }

        if (device.uploadQueue && device.uploadQueue.length) {
          uploadLocks[device.address] = setTimeout(function () {
            Dehumidifier.uploadDryTAG(device);
          }, 25000 + 10000 * Math.random());//Send more data after about 30 seconds.
        } else {
          uploadLocks[device.address] = setTimeout(function () {
            Dehumidifier.uploadDryTAG(device);
          }, 15 * 60000 + 4 * 60000 * Math.random());//If app is still running, upload data again after at least 15 minutes.
        }
      }, function (err) {
        uploadFinished = true;
        if (errorCallback && ((typeof errorCallback) === "function")) {
          errorCallback(device, err.data || err);
        }
        if (err && err.status && err.status >= 400 && err.status < 500) {//Don't treat 400 errors the same as other error codes.
          uploadLocks[device.address] = setTimeout(function () {
            Dehumidifier.uploadDryTAG(device);
          }, 15 * 60000 + 4 * 60000 * Math.random());//If app is still running, upload data again after at least 15 minutes.
        } else {
          uploadLocks[device.address] = setTimeout(function () {
            Dehumidifier.uploadDryTAG(device);
          }, 55000 + 10000 * Math.random());//Try again after about 60 seconds.
        }
      });
    };

    Dehumidifier.checkDryTAGTime = function (beaconUUID) {
      if (!beaconUUID) {
        return $q.reject({message: "no DryTAG UUID", status: 400});
      }
      return $http.get(API_Base.replace(/v1/g, 'v2') + 'beacon/upload?deviceId=' + beaconUUID).then(function (newTime) {
        return newTime && newTime.data;
      }, $q.reject);
    };

    Dehumidifier.checkDrySENSETime = function (drySENSEID) {
      if (!drySENSEID) {
        return $q.reject({message: "no DrySENSE UUID", status: 400});
      }
      return $http.get(API_Base + 'msensor/upload?deviceId=' + drySENSEID).then(function (newTime) {
        return newTime && newTime.data;
      }, $q.reject);
    };

    Dehumidifier.checkUploadTime = function (device) {
      if (!device || !device.serialNumber) {
        return $q.reject({message: "no serial number", status: 400});
      }
      console.log("check upload time " + device.serialNumber);
      device.checkUpload = true;//Block any downloads or uploads until last timestamp retrieved from server or API call fails.
      return $http.get(API_Base + 'upload?deviceId=' + device.serialNumber).then(function (newTime) {
        console.log("new time " + JSON.stringify(newTime.data));
        if (typeof newTime.data === "number") {
          device.timestamp = newTime.data + 1;
        }
        device.checkUpload = false;
        return $q.resolve();
      }, function (err) {
        console.log("new time err " + JSON.stringify(err.data));
        device.checkUpload = false;
        return $q.reject(err);
      });
    };

    Dehumidifier.updateDrySENSEname = function (msensorId, name) {
      if (!msensorId) {
        return $q.reject({message: 'No database ID', status: 400});
      }

      return $http.put(API_Base + 'msensor/' + msensorId + '?name=' + encodeURIComponent(name)).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Dehumidifier.updateDrySENSEoffset = function (msensorId, rhOffset) {
      if (!msensorId) {
        return $q.reject({message: 'No database ID', status: 400});
      }

      return $http.put(API_Base + 'msensor/' + msensorId + '?rhOffset=' + Math.round(rhOffset)).then(function (response) {
        var aDevice = $rootScope.searchBluetoothListsForMatch('msensorId', msensorId);
        if (aDevice) {
          aDevice.serverRHoffset = rhOffset;
        }
        return response.data;
      }, $q.reject);
    };

    Dehumidifier.changeDryTAGBattery = function (drytagUuid, changeSecs) {
      if (!drytagUuid) {
        return $q.reject({message: 'No database ID', status: 400});
      }
      var query = "?deviceId=" + encodeURIComponent(drytagUuid);
      if (changeSecs) {
        query += "&changeSecs=" + encodeURIComponent(changeSecs);
      }
      return $http.put(API_Base + 'cequip/battery_change' + query).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Dehumidifier.postDehuName = function (databaseId, name, hoursPerFilter) {
      if (!databaseId) {
        return $q.reject({message: 'No database ID', status: 400});
      }
      if (!name && !hoursPerFilter) {
        return $q.reject({message: 'No info to post', status: 400});
      }

      var target = API_Base + 'dehu/' + databaseId + '?';
      if (name) {
        target += 'name=' + encodeURIComponent(name);
      }
      if (hoursPerFilter) {
        if (name) {
          target += '&';
        }
        target += 'hoursPerFilter=' + hoursPerFilter;
      }

      return $http.put(target).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Dehumidifier.addClaimPossibility = function (device, recentBluetooth) {
      var i,
        dehu;

      if (!device || (device.blockClaimingUntilBluetooth && device.modelType === 'DrySENSE')) {
        return;
      }
      if (device.modelType === 'DryTAG') {
        //console.log("this is the claim possibility", JSON.stringify(device));
        device.claimingDeferred = false;
        if ((device.dehuId && device.dehuId > -1) || (device.cequipId && device.cequipId > -1)) {
          if (device.dehuId && device.dehuId > -1 && $rootScope.claimPossibilities && $rootScope.claimPossibilities.length) {
            for (i = 0; i < $rootScope.claimPossibilities.length; i += 1) {
              dehu = $rootScope.claimPossibilities[i] || {};
              if (dehu.databaseId === device.dehuId) {
                dehu.beaconUUID = device.uuid;
                dehu.beaconName = device.name || device.uuid || device.address;
                dehu.hasBeacon = true;
                break;
              }
            }
          }
          if ($rootScope.associatedDryTAGPossibilities && $rootScope.associatedDryTAGPossibilities.length) {
            for (i = 0; i < $rootScope.associatedDryTAGPossibilities.length; i += 1) {
              if ($rootScope.associatedDryTAGPossibilities[i] && $rootScope.associatedDryTAGPossibilities[i].uuid === device.uuid) {
                $rootScope.newDehusAvailable = true;
                $rootScope.associatedDryTAGPossibilities[i] = device;
                return;
              }
            }
          } else {
            $rootScope.associatedDryTAGPossibilities = [];
          }
          if (recentBluetooth) {
            $rootScope.newDehusAvailable = true;
            $rootScope.associatedDryTAGPossibilities.push(device);
          } else {
            device.claimingDeferred = true;
          }
        } else {
          device.claimingDeferred = false;
          if ($rootScope.isolatedDryTAGPossibilities && $rootScope.isolatedDryTAGPossibilities.length) {
            for (i = 0; i < $rootScope.isolatedDryTAGPossibilities.length; i += 1) {
              if ($rootScope.isolatedDryTAGPossibilities[i] && $rootScope.isolatedDryTAGPossibilities[i].uuid === device.uuid) {
                $rootScope.isolatedDryTAGsAvailable = true;
                $rootScope.isolatedDryTAGPossibilities[i] = device;
                return;
              }
            }
          } else {
            $rootScope.isolatedDryTAGPossibilities = [];
          }
          if (recentBluetooth) {
            $rootScope.isolatedDryTAGsAvailable = true;
            $rootScope.isolatedDryTAGPossibilities.push(device);
          } else {
            device.claimingDeferred = true;
          }
        }
        return;
      }
      $rootScope.newDehusAvailable = true;
      if ($rootScope.associatedDryTAGPossibilities && $rootScope.associatedDryTAGPossibilities.length) {
        for (i = 0; i < $rootScope.associatedDryTAGPossibilities.length; i += 1) {
          dehu = $rootScope.associatedDryTAGPossibilities[i] || {};
          if (dehu.dehuId && dehu.dehuId > -1 && dehu.dehuId === device.databaseId) {
            device.beaconUUID = dehu.uuid;
            device.beaconName = dehu.name || dehu.uuid || dehu.address;
            device.hasBeacon = true;
            break;
          }
        }
      }
      if ($rootScope.claimPossibilities && $rootScope.claimPossibilities.length) {
        for (i = 0; i < $rootScope.claimPossibilities.length; i += 1) {
          if ($rootScope.claimPossibilities[i] && ((device.modelType === "DrySENSE" && $rootScope.claimPossibilities[i].address === device.address) ||
               (device.modelType !== "DrySENSE" && $rootScope.claimPossibilities[i].serialNumber === device.serialNumber))) {
            $rootScope.claimPossibilities[i] = device;
            return;
          }
        }
      } else {
        $rootScope.claimPossibilities = [];
      }
      $rootScope.claimPossibilities.push(device);
    };

    Dehumidifier.claimDryTAG = function (token, dehuId, cequipId) {
      if (!token) {
        return $q.reject({message: 'No claim token', status: 400});
      }
      var tokenString = "beacontoken=" + token;
      if (dehuId && dehuId > -1) {
        tokenString += "&dehuId=" + dehuId;
      }
      if (cequipId && cequipId > -1) {
        tokenString += "&cequipId=" + cequipId;
      }
      return $http.post(API_Base + 'beacon/claim?' + tokenString).then(function (response) {
        Snackbar.show({template: "Equipment added to company", hide: 5000});
        return response.data;
      }, $q.reject);
    };

    Dehumidifier.claimDrySENSE = function (token, firmwareVersion) {
      if (!token) {
        return $q.reject({message: 'No claim token', status: 400});
      }
      return $http.post(API_Base.replace(/v1/g, 'v2') + 'msensor/claim?msensortoken=' + token + '&firmwareVersion=' +
          encodeURIComponent(firmwareVersion.replace(/\0/g, ''))).then(function (response) {//or encodeURIComponent(token) or token.replace(/\//g, '%2F')
        $localStorage.neverShowDrySENSElist = false;
        Snackbar.show({template: "Equipment added to company", hide: 5000});
        return response.data;
      }, $q.reject);
    };

    Dehumidifier.claim = function (token) {
      if (!token) {
        return $q.reject({message: 'No claim token', status: 400});
      }
      return $http.post(API_Base + 'dehu/claim?dehutoken=' + token).then(function (response) {//or encodeURIComponent(token) or token.replace(/\//g, '%2F')
        Snackbar.show({template: "Equipment added to company", hide: 5000});
        return response.data;
      }, $q.reject);
    };

    Dehumidifier.resetFilter = function (databaseId) {
      if (!databaseId) {
        return $q.reject({message: 'No database ID', status: 400});
      }

      return $http.post(API_Base + 'dehu/' + databaseId + '/filter_reset').then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Dehumidifier.deleteDrySENSE = function (msensorId) {
      if (!msensorId || !(msensorId > -1)) {
        return $q.reject({message: 'No database ID', status: 400});
      }

      var i;
      if ($localStorage.leDeviceList) {
        if ($localStorage.leDeviceList.newer && $localStorage.leDeviceList.newer.length) {
          for (i = 0; i < $localStorage.leDeviceList.newer.length; i += 1) {
            if (String($localStorage.leDeviceList.newer[i].msensorId) === String(msensorId)) {
              $localStorage.leDeviceList.newer.splice(i, 1);
            }
          }
        }
        if ($localStorage.leDeviceList.older && $localStorage.leDeviceList.older.length) {
          for (i = 0; i < $localStorage.leDeviceList.older.length; i += 1) {
            if (String($localStorage.leDeviceList.older[i].msensorId) === String(msensorId)) {
              $localStorage.leDeviceList.older.splice(i, 1);
            }
          }
        }
      }

      return $http.delete(API_Base + 'msensor/' + msensorId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Dehumidifier.release = function (databaseId) {
      if (!databaseId || !(databaseId > -1)) {
        return $q.reject({message: 'No database ID', status: 400});
      }

      var i;
      if ($localStorage.leDeviceList) {
        if ($localStorage.leDeviceList.newer && $localStorage.leDeviceList.newer.length) {
          for (i = 0; i < $localStorage.leDeviceList.newer.length; i += 1) {
            if (String($localStorage.leDeviceList.newer[i].databaseId) === String(databaseId)) {
              $localStorage.leDeviceList.newer.splice(i, 1);
            }
          }
        }
        if ($localStorage.leDeviceList.older && $localStorage.leDeviceList.older.length) {
          for (i = 0; i < $localStorage.leDeviceList.older.length; i += 1) {
            if (String($localStorage.leDeviceList.older[i].databaseId) === String(databaseId)) {
              $localStorage.leDeviceList.older.splice(i, 1);
            }
          }
        }
      }

      return $http.delete(API_Base + 'dehu/' + databaseId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Dehumidifier.releaseDryTAG = function (beaconId, skipCloud) {
      if (!beaconId || !(beaconId > -1)) {
        return $q.reject({message: 'No beacon ID', status: 400});
      }

      var i;
      if ($localStorage.leDeviceList) {
        if ($localStorage.leDeviceList.newer && $localStorage.leDeviceList.newer.length) {
          for (i = 0; i < $localStorage.leDeviceList.newer.length; i += 1) {
            if (String($localStorage.leDeviceList.newer[i].beaconId) === String(beaconId)) {
              $localStorage.leDeviceList.newer.splice(i, 1);
            }
          }
        }
        if ($localStorage.leDeviceList.older && $localStorage.leDeviceList.older.length) {
          for (i = 0; i < $localStorage.leDeviceList.older.length; i += 1) {
            if (String($localStorage.leDeviceList.older[i].beaconId) === String(beaconId)) {
              $localStorage.leDeviceList.older.splice(i, 1);
            }
          }
        }
      }

      if (skipCloud) {
        return $q.resolve();
      }
      return $http.delete(API_Base.replace(/v1/g, 'v2') + 'beacon/' + beaconId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Dehumidifier.updateDryTAGoffset = function (beaconId, rhOffset) {
      if (!(beaconId > 0)) {
        return $q.reject({message: 'No beacon ID', status: 400});
      }

      return $http.put(API_Base + 'beacon/' + beaconId + "?rhOffset=" + Math.round(rhOffset)).then(function (response) {
        var aDevice = $rootScope.searchBluetoothListsForMatch('beaconId', beaconId);
        if (aDevice && aDevice.hasRHCapabilities) {
          aDevice.serverRHoffset = rhOffset;
        }
        return response.data;
      }, $q.reject);
    };

    Dehumidifier.getLastData = function (databaseId, isDehu, isDrySENSE) {
      if (!databaseId) {
        return $q.reject({message: 'No database ID', status: 400});
      }

      if (isDehu) {
        return $http.get(API_Base + 'dehu/' + databaseId).then(function (data) {
          return (data && data.data) || {};
        }, $q.reject);
      }
      if (isDrySENSE) {
        return $http.get(API_Base + 'msensor/' + databaseId).then(function (data) {
          var alteredData = (data && data.data) || {};
          if (alteredData.msensor && alteredData.lastReading) {//Diane is very consistent at being very inconsistent.
            if ((alteredData.msensor.latitude || alteredData.msensor.longitude) && !(alteredData.lastReading.latitude || alteredData.lastReading.longitude)) {
              alteredData.lastReading.latitude = alteredData.msensor.latitude;
              alteredData.lastReading.longitude = alteredData.msensor.longitude;
            }
            if (alteredData.msensor.lastAccessSecs && !alteredData.lastReading.sampleSecs) {
              alteredData.lastReading.sampleSecs = alteredData.msensor.lastAccessSecs;
            }
            if (alteredData.msensor.lastAccountName && !alteredData.lastReading.accountName) {
              alteredData.lastReading.accountName = alteredData.msensor.lastAccountName;
            }
          }
          return alteredData;
        }, $q.reject);
      }
      return $http.get(API_Base + 'cequip/' + databaseId).then(function (data) {
        return (data && data.data) || {};
      }, $q.reject);
    };

    Dehumidifier.getAllDrySENSEs = function () {
      return $http.get(API_Base + 'msensor').then(function (data) {
        var newList = (data && data.data) || [],
          cleanedList = [],
          deviceList = $localStorage.deviceList || $rootScope.deviceList,
          cloudDehu,
          bleDehu,
          i,
          j;

        for (i = 0; i < newList.length; i += 1) {
          cloudDehu = {};
          if (newList[i] && newList[i].msensor) {
            $localStorage.neverShowDrySENSElist = false;
            cloudDehu.serialNumber = newList[i].msensor.deviceId;
            cloudDehu.dehuName = newList[i].msensor.sensorName;
            cloudDehu.msensorId = newList[i].msensor.msensorId;
            cloudDehu.modelType = "DrySENSE";
            cloudDehu.typeName = newList[i].msensor.typeName;
            cloudDehu.lastSeen = 'elsewhere';
            cloudDehu.hasBeacon = false;
            if (newList[i].mpInfo && newList[i].mpInfo.jobId > 0) {
              cloudDehu.jobId = newList[i].mpInfo.jobId;
              cloudDehu.jobName = newList[i].mpInfo.jobName;
              cloudDehu.label = newList[i].mpInfo.label;
              cloudDehu.mequipId = newList[i].mpInfo.mequipmentId;
              cloudDehu.equipmentId = -1;
              cloudDehu.chamberId = newList[i].mpInfo.chamberId;
              cloudDehu.chamberName = newList[i].mpInfo.chamberName;
            } else if (newList[i].jobInfo && newList[i].jobInfo.jobId > 0) {
              cloudDehu.jobId = newList[i].jobInfo.jobId;
              cloudDehu.jobName = newList[i].jobInfo.jobName;
              cloudDehu.label = "";
              cloudDehu.mequipId = -1;
              cloudDehu.equipmentId = newList[i].jobInfo.equipmentId;
              cloudDehu.chamberId = newList[i].jobInfo.chamberId;
              cloudDehu.chamberName = newList[i].jobInfo.chamberName;
            } else {
              cloudDehu.jobId = -1;
              cloudDehu.jobName = "";
              cloudDehu.label = "";
              cloudDehu.mequipId = -1;
              cloudDehu.equipmentId = -1;
              cloudDehu.chamberId = -1;
              cloudDehu.chamberName = "";
            }

            if (deviceList) {
              if (deviceList.newer && deviceList.newer.length) {
                for (j = 0; j < deviceList.newer.length; j += 1) {
                  bleDehu = deviceList.newer[j];

                  if (cloudDehu.msensorId > -1 && (cloudDehu.msensorId === bleDehu.msensorId || (cloudDehu.serialNumber && cloudDehu.serialNumber === bleDehu.uniqueEnoughID))) {
                    bleDehu.msensorId = cloudDehu.msensorId;
                    cloudDehu.address = bleDehu.address;
                    cloudDehu.lastSeen = 'nearby';
                    if (!cloudDehu.lastRssi) {
                      cloudDehu.lastRssi = bleDehu.lastRssi;
                    }
                    bleDehu.jobName = cloudDehu.jobName;
                    bleDehu.jobId = cloudDehu.jobId;
                    bleDehu.claimStatus = "mine";
                    bleDehu.name = cloudDehu.dehuName;
                    bleDehu.typeName = cloudDehu.typeName;
                  }
                }
              }
              if (deviceList.older && deviceList.older.length) {
                for (j = 0; j < deviceList.older.length; j += 1) {
                  bleDehu = deviceList.older[j];

                  if (cloudDehu.msensorId > -1 && (cloudDehu.msensorId === bleDehu.msensorId || (cloudDehu.serialNumber && cloudDehu.serialNumber === bleDehu.uniqueEnoughID))) {
                    bleDehu.msensorId = cloudDehu.msensorId;
                    cloudDehu.address = bleDehu.address;
                    bleDehu.jobName = cloudDehu.jobName;
                    bleDehu.jobId = cloudDehu.jobId;
                    bleDehu.claimStatus = "mine";
                    bleDehu.name = cloudDehu.dehuName;
                    bleDehu.typeName = cloudDehu.typeName;
                  }
                }
              }
            }
            Dehumidifier.determineIconCategory(cloudDehu);
            cleanedList.push(cloudDehu);
          }
        }

        return cleanedList;
      }, $q.reject);
    };

    Dehumidifier.getDehuListAndReorganize = function () {
      return $http.get(API_Base + 'cequip/all').then(function (data) {
        var newList = (data && data.data) || [],
          cleanedList = [],
          deviceList = $localStorage.deviceList || $rootScope.deviceList,
          cloudDehu,
          bleDehu,
          foundTheCloud,
          i,
          j;

        for (i = 0; i < newList.length; i += 1) {
          cloudDehu = newList[i];
          if (cloudDehu && cloudDehu.deviceId) {
            if (cloudDehu.model === "dryxl" || cloudDehu.model === "Dry Max XL") {
              cloudDehu.model = "Dry Max XL";
            } else if (cloudDehu.model === "dry80" || cloudDehu.model === "Dry Max") {
              cloudDehu.model = "Dry Max";
            } else if (cloudDehu.model === "DrySense" || cloudDehu.model === "DrySENSE") {
              cloudDehu.model = "DrySENSE";
              $localStorage.neverShowDrySENSElist = false;
            }
            cleanedList.push({serialNumber: cloudDehu.deviceId, dehuName: cloudDehu.equipName, databaseId: cloudDehu.dehuId, cequipId: cloudDehu.cequipId, ctsSerialNumber: cloudDehu.serialNumber, beaconId: cloudDehu.beaconId, msensorId: cloudDehu.msensorId,
              jobName: cloudDehu.jobName, jobId: cloudDehu.jobId, modelType: cloudDehu.model, typeName: cloudDehu.typeName, lastSeen: 'elsewhere', hasBeacon: cloudDehu.beaconId > -1, hasBeaconRH: cloudDehu.hasBeaconRH});//Translate between Diane's naming conventions and my own.
          }
        }

        if (deviceList) {
          if (deviceList.newer && deviceList.newer.length) {
            for (i = 0; i < deviceList.newer.length; i += 1) {
              bleDehu = deviceList.newer[i];
              foundTheCloud = false;
              for (j = 0; j < cleanedList.length; j += 1) {
                cloudDehu = cleanedList[j];
                if (cloudDehu.databaseId && cloudDehu.databaseId > -1 && bleDehu.serialNumber === cloudDehu.serialNumber) {//Exchange data between the lists so that they are more complete.
                  foundTheCloud = true;
                  bleDehu.databaseId = cloudDehu.databaseId;//Give the cloud-generated database id to the list generated over Bluetooth.
                  cloudDehu.address = bleDehu.address;//And give the Bluetooth address (which will be device-specific on iOS) to the cloud-generated dehu list.
                  cloudDehu.lastSeen = 'nearby';
                  cloudDehu.lastRssi = bleDehu.lastRssi;
                  cloudDehu.otaLock = bleDehu.otaLock;
                  bleDehu.jobName = cloudDehu.jobName;
                  bleDehu.jobId = cloudDehu.jobId;
                  bleDehu.claimStatus = "mine";
                  bleDehu.hasBeacon = cloudDehu.hasBeacon;
                  if (bleDehu.modelType) {
                    cloudDehu.modelType = bleDehu.modelType;
                  } else if (cloudDehu.modelType) {
                    bleDehu.modelType = cloudDehu.modelType;
                  }
                  if (cloudDehu.ctsSerialNumber && (cloudDehu.ctsSerialNumber !== bleDehu.ctsSerialNumber)) {
                    bleDehu.shouldUpdateCTSnumber = true;
                  }
                  bleDehu.ctsSerialNumber = cloudDehu.ctsSerialNumber;

                  if (!cloudDehu.beaconId || !(cloudDehu.beaconId > -1)) {
                    bleDehu.beaconUUID = undefined;
                    bleDehu.beaconName = undefined;
                  }

                  cloudDehu.bleCopy = bleDehu;
                } else if (cloudDehu.beaconId && cloudDehu.beaconId > -1 && cloudDehu.beaconId === bleDehu.beaconId) {
                  foundTheCloud = true;
                  bleDehu.dehuId = cloudDehu.databaseId;//Associate the DryTAG with whatever dehu or equipment it's attached to.
                  bleDehu.cequipId = cloudDehu.cequipId;
                  if (!cloudDehu.address) {
                    cloudDehu.address = bleDehu.address;//The Bluetooth address isn't that useful for DryTAGs but we can exchange it, anyway.
                  }
                  cloudDehu.lastSeen = 'nearby';
                  if (!cloudDehu.lastRssi) {
                    cloudDehu.lastRssi = bleDehu.lastRssi;
                  }
                  bleDehu.jobName = cloudDehu.jobName;
                  bleDehu.jobId = cloudDehu.jobId;
                  bleDehu.claimStatus = "mine";
                  bleDehu.name = cloudDehu.dehuName;
                  if (bleDehu.dehuId && bleDehu.dehuId > -1) {
                    bleDehu.typeName = cloudDehu.modelType;
                  } else {
                    bleDehu.typeName = cloudDehu.typeName;
                  }
                  cloudDehu.hasBeaconRH = cloudDehu.hasBeaconRH || bleDehu.hasRHCapabilities;
                  Dehumidifier.determineIconCategory(bleDehu);

                  if (!cloudDehu.bleCopy) {
                    cloudDehu.bleCopy = bleDehu;
                  }
                } else if (cloudDehu.msensorId && cloudDehu.msensorId > -1 && (cloudDehu.msensorId === bleDehu.msensorId ||
                           (cloudDehu.serialNumber && cloudDehu.serialNumber === bleDehu.uniqueEnoughID))) {
                  foundTheCloud = true;
                  bleDehu.msensorId = cloudDehu.msensorId;
                  cloudDehu.address = bleDehu.address;
                  cloudDehu.lastSeen = 'nearby';
                  if (!cloudDehu.lastRssi) {
                    cloudDehu.lastRssi = bleDehu.lastRssi;
                  }
                  bleDehu.jobName = cloudDehu.jobName;
                  bleDehu.jobId = cloudDehu.jobId;
                  bleDehu.claimStatus = "mine";
                  bleDehu.name = cloudDehu.dehuName;
                  bleDehu.typeName = cloudDehu.typeName;
                  Dehumidifier.determineIconCategory(bleDehu);

                  if (!cloudDehu.bleCopy) {
                    cloudDehu.bleCopy = bleDehu;
                  }
                }
              }
              if (!foundTheCloud && bleDehu.claimStatus === "mine") {
                bleDehu.claimStatus = undefined;
                if (bleDehu.modelType === 'DryTAG') {
                  bleDehu.dehuId = undefined;
                  bleDehu.cequipId = undefined;
                }
              }
            }
          }
          if (deviceList.older && deviceList.older.length) {
            for (i = 0; i < deviceList.older.length; i += 1) {
              bleDehu = deviceList.older[i];
              foundTheCloud = false;
              for (j = 0; j < cleanedList.length; j += 1) {
                cloudDehu = cleanedList[j];
                if (cloudDehu.databaseId && cloudDehu.databaseId > -1 && bleDehu.serialNumber === cloudDehu.serialNumber) {
                  foundTheCloud = true;
                  bleDehu.databaseId = cloudDehu.databaseId;
                  cloudDehu.address = bleDehu.address;
                  cloudDehu.otaLock = bleDehu.otaLock;
                  bleDehu.jobName = cloudDehu.jobName;
                  bleDehu.jobId = cloudDehu.jobId;
                  bleDehu.claimStatus = "mine";
                  bleDehu.hasBeacon = cloudDehu.hasBeacon;
                  if (bleDehu.modelType) {
                    cloudDehu.modelType = bleDehu.modelType;
                  } else if (cloudDehu.modelType) {
                    bleDehu.modelType = cloudDehu.modelType;
                  }
                  if (cloudDehu.ctsSerialNumber && (cloudDehu.ctsSerialNumber !== bleDehu.ctsSerialNumber)) {
                    bleDehu.shouldUpdateCTSnumber = true;
                  }
                  bleDehu.ctsSerialNumber = cloudDehu.ctsSerialNumber;

                  if (!cloudDehu.beaconId || !(cloudDehu.beaconId > -1)) {
                    bleDehu.beaconUUID = undefined;
                    bleDehu.beaconName = undefined;
                  }

                  if (!cloudDehu.bleCopy) {
                    cloudDehu.bleCopy = bleDehu;
                  }
                } else if (cloudDehu.beaconId && cloudDehu.beaconId > -1 && cloudDehu.beaconId === bleDehu.beaconId) {
                  foundTheCloud = true;
                  bleDehu.dehuId = cloudDehu.databaseId;
                  bleDehu.cequipId = cloudDehu.cequipId;
                  if (!cloudDehu.address) {
                    cloudDehu.address = bleDehu.address;
                  }
                  bleDehu.jobName = cloudDehu.jobName;
                  bleDehu.jobId = cloudDehu.jobId;
                  bleDehu.claimStatus = "mine";
                  bleDehu.name = cloudDehu.dehuName;
                  if (bleDehu.dehuId && bleDehu.dehuId > -1) {
                    bleDehu.typeName = cloudDehu.modelType;
                  } else {
                    bleDehu.typeName = cloudDehu.typeName;
                  }
                  cloudDehu.hasBeaconRH = cloudDehu.hasBeaconRH || bleDehu.hasRHCapabilities;
                  Dehumidifier.determineIconCategory(bleDehu);

                  if (!cloudDehu.bleCopy) {
                    cloudDehu.bleCopy = bleDehu;
                  }
                } else if (cloudDehu.msensorId && cloudDehu.msensorId > -1 && (cloudDehu.msensorId === bleDehu.msensorId ||
                           (cloudDehu.serialNumber && cloudDehu.serialNumber === bleDehu.uniqueEnoughID))) {
                  foundTheCloud = true;
                  bleDehu.msensorId = cloudDehu.msensorId;
                  cloudDehu.address = bleDehu.address;
                  bleDehu.jobName = cloudDehu.jobName;
                  bleDehu.jobId = cloudDehu.jobId;
                  bleDehu.claimStatus = "mine";
                  bleDehu.name = cloudDehu.dehuName;
                  bleDehu.typeName = cloudDehu.typeName;
                  Dehumidifier.determineIconCategory(bleDehu);

                  if (!cloudDehu.bleCopy) {
                    cloudDehu.bleCopy = bleDehu;
                  }
                }
              }
              if (!foundTheCloud && bleDehu.claimStatus === "mine") {
                bleDehu.claimStatus = undefined;
                if (bleDehu.modelType === 'DryTAG') {
                  bleDehu.dehuId = undefined;
                  bleDehu.cequipId = undefined;
                }
              }
            }
          }
        }
        for (i = 0; i < cleanedList.length; i += 1) {
          Dehumidifier.determineIconCategory(cleanedList[i]);
        }
        return cleanedList;
      }, $q.reject);
    };

    Dehumidifier.reverseIconCategoryLookup = function (iconCategory) {
      var thisEquipment = {};

      if (iconCategory === 'Dry Max XL') {
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-phoenix-without DryTAG-DryMAX-XL.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-phoenix-DryTAG-DryMAX-XL.png";
      } else if (iconCategory === 'Dry Max') {
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-phoenix-without DryTAG-DryMax-LGR.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-phoenix-DryTAG-DryMax-LGR.png";
      } else if (iconCategory === 'DrySENSE') {
        thisEquipment.nonDryTAGiconURL = "img/DrySENSE_icon.png";
        thisEquipment.dryTAGiconURL = "img/DrySENSE_icon.png";
      } else if (iconCategory === 'AirMAX') {
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-phoenix-without DryTAG-Airmax.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-phoenix-DryTAG-Airmax.png";
      } else if (iconCategory === 'Dehu') {
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-Dehu.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-Dehu.png";
      } else if (iconCategory === 'Drying System') {
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-Floor-Dryer.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-Floor-Dryer.png";
      } else if (iconCategory === 'Heater') {
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-Heater.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-Heater.png";
      } else if (iconCategory === 'A/C') {
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-AC-Unit.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-AC-Unit.png";
      } else if (iconCategory === 'Fan') {
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-Air-Mover.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-Air-Mover.png";
      } else if (iconCategory === 'Splitter Box') {
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-Splitter.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-Splitter.png";
      } else if (iconCategory === 'Air Scrubber') {
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-HEPA-Air-Scrubber.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-HEPA-Air-Scrubber.png";
      } else if (iconCategory === 'Hygrometer') {
        thisEquipment.nonDryTAGiconURL = "img/ThermoHygrometer.png";
        thisEquipment.dryTAGiconURL = "img/DryTAG_RH-standalone.png";
      } else {
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-Custom.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-Custom.png";
      }
      return thisEquipment;
    };

    Dehumidifier.determineIconCategory = function (thisEquipment) {
      if (!thisEquipment) {
        thisEquipment = {};
      }
      if (!thisEquipment.typeName) {
        thisEquipment.typeName = '';
      }

      if (thisEquipment.modelType === 'Dry Max XL' || thisEquipment.typeName === 'Dry Max XL') {
        thisEquipment.iconCategory = 'Dry Max XL';
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-phoenix-without DryTAG-DryMAX-XL.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-phoenix-DryTAG-DryMAX-XL.png";
      } else if (thisEquipment.modelType === 'Dry Max' || thisEquipment.typeName === 'Dry Max') {
        thisEquipment.iconCategory = 'Dry Max';
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-phoenix-without DryTAG-DryMax-LGR.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-phoenix-DryTAG-DryMax-LGR.png";
      } else if (thisEquipment.modelType === 'DrySense' || thisEquipment.modelType === 'DrySENSE') {
        thisEquipment.modelType = 'DrySENSE';
        thisEquipment.iconCategory = 'DrySENSE';
        thisEquipment.nonDryTAGiconURL = "img/DrySENSE_icon.png";
        thisEquipment.dryTAGiconURL = "img/DrySENSE_icon.png";
      } else if (thisEquipment.typeName.indexOf('Dehu') > -1 || thisEquipment.typeName.indexOf('Dessicant') > -1 || thisEquipment.typeName.indexOf('Desiccant') > -1) {
        thisEquipment.iconCategory = 'Dehu';
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-Dehu.png";
        if (thisEquipment.hasBeaconRH || thisEquipment.hasRHCapabilities) {
          thisEquipment.dryTAGiconURL = "img/DryTAG_RH-dehu.png";
        } else {
          thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-Dehu.png";
        }
      } else if (thisEquipment.typeName.indexOf('Drying System') > -1) {
        thisEquipment.iconCategory = 'Drying System';
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-Floor-Dryer.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-Floor-Dryer.png";
      } else if (thisEquipment.typeName.indexOf('Heater') > -1) {
        thisEquipment.iconCategory = 'Heater';
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-Heater.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-Heater.png";
      } else if (thisEquipment.typeName.indexOf('A/C') > -1 || thisEquipment.typeName.indexOf('onditioner') > -1) {
        thisEquipment.iconCategory = 'A/C';
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-AC-Unit.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-AC-Unit.png";
      } else if (thisEquipment.typeName.indexOf('Fan') > -1 || thisEquipment.typeName.indexOf('Air Mover') > -1 || thisEquipment.typeName.indexOf('Vane Axial') > -1 ||
                 thisEquipment.typeName.indexOf('Blower') > -1) {
        if (thisEquipment.modelType && (thisEquipment.modelType.toLowerCase().indexOf('airmax') > -1 || thisEquipment.modelType.toLowerCase().indexOf('air max') > -1)) {
          thisEquipment.iconCategory = 'AirMAX';
          thisEquipment.nonDryTAGiconURL = "img/equipment-icon-phoenix-without DryTAG-Airmax.png";
          thisEquipment.dryTAGiconURL = "img/equipment-icon-phoenix-DryTAG-Airmax.png";
        } else {
          thisEquipment.iconCategory = 'Fan';
          thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-Air-Mover.png";
          thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-Air-Mover.png";
        }
      } else if (thisEquipment.typeName.indexOf('Power Distribution') > -1 || thisEquipment.typeName.indexOf('Splitter Box') > -1) {
        thisEquipment.iconCategory = 'Splitter Box';
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-Splitter.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-Splitter.png";
      } else if (thisEquipment.typeName.indexOf('HEPA') > -1 || thisEquipment.typeName.indexOf('Scrubber') > -1) {
        thisEquipment.iconCategory = 'Air Scrubber';
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-HEPA-Air-Scrubber.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-HEPA-Air-Scrubber.png";
      } else if (thisEquipment.msensorId && thisEquipment.msensorId > -1) {
        thisEquipment.modelType = 'DrySENSE';
        thisEquipment.iconCategory = 'DrySENSE';
        thisEquipment.nonDryTAGiconURL = "img/DrySENSE_icon.png";
        thisEquipment.dryTAGiconURL = "img/DrySENSE_icon.png";
      } else if (thisEquipment.typeName.indexOf('Hygrometer') > -1) {
        thisEquipment.iconCategory = 'Hygrometer';
        thisEquipment.nonDryTAGiconURL = "img/ThermoHygrometer.png";
        thisEquipment.dryTAGiconURL = "img/DryTAG_RH-standalone.png";
      } else {
        thisEquipment.iconCategory = 'Unknown';
        thisEquipment.nonDryTAGiconURL = "img/equipment-icon-nobrand-without DryTAG-Custom.png";
        thisEquipment.dryTAGiconURL = "img/equipment-icon-nobrand-DryTAG-Custom.png";
      }
      return thisEquipment;
    };

    Dehumidifier.getLatestDisplayFirmware = function (isBGM13p) {
      if (isBGM13p) {
        return $http.get(API_Base + 'info/BGM13P_display_firmware?apikey=' + Authentication.apiKey).then(function (data) {
          return data && data.data && data.data.message;
        }, $q.reject);
      }
      return $http.get(API_Base + 'info/display_firmware?apikey=' + Authentication.apiKey).then(function (data) {
        return data && data.data && data.data.message;
      }, $q.reject);
    };

    Dehumidifier.getLatestPowerFirmware = function () {
      // return $http.get("templates/drymax_power_special_0.3.2_test_bootload.hex").then(function (data) {
      //    return data && data.data;
      //  }, $q.reject);
      return $http.get(API_Base + 'info/power_firmware?apikey=' + Authentication.apiKey).then(function (data) {
        return data && data.data && data.data.message;
      }, $q.reject);
    };

    Dehumidifier.getLatestDisplayVersion = function () {
      return $http.get(API_Base + 'info/display_version?apikey=' + Authentication.apiKey).then(function (data) {
        return $http.get(API_Base + 'info/BGM13P_display_version?apikey=' + Authentication.apiKey).then(function (data2) {
          return {bgm111: data && data.data && data.data.message, bgm13p: data2 && data2.data && data2.data.message};
        }, $q.reject);
      }, $q.reject);
    };

    Dehumidifier.getLatestPowerVersion = function () {
      return $http.get(API_Base + 'info/power_version?apikey=' + Authentication.apiKey).then(function (data) {
        return data && data.data && data.data.message;
      }, $q.reject);
    };

    Dehumidifier.encryptCommand = function (deviceId, command) {
      var binaryCommand = new Array(14),
        time,
        target,
        offset = 0,
        i;

      if (command.time !== undefined) {
        time = command.time || 0;
        binaryCommand[0] = 0xFF & (time >> 24);
        binaryCommand[1] = 0xFF & (time >> 16);
        binaryCommand[2] = 0xFF & (time >> 8);
        binaryCommand[3] = 0xFF & time;
        offset += 4;
      }
      if (command.name) {
        target = commandNames.indexOf(command.name);
        if (target < 0) {
          return $q.reject({status: 403, message: 'Invalid command name: ' + command.name});
        }

        binaryCommand[offset] = 0xFF & target;
        offset += 1;
      }
      if (command.value) {
        if (typeof command.value === 'string') {
          for (i = 0; i < command.value.length && i < 9; i += 1) {
            binaryCommand[i + offset] = 0xFF & command.value.charCodeAt(i);
          }
        } else if (typeof command.value === 'number') {//Two byte numbers are the default--otherwise, use int arrays for 1, 4, or 8 byte numbers.
          binaryCommand[offset] = 0xFF & (command.value >> 8);
          binaryCommand[offset + 1] = 0xFF & command.value;
        } else if (command.value.length) {
          for (i = 0; i < command.value.length && i < 9; i += 1) {
            binaryCommand[i + offset] = 0xFF & command.value[i];
          }
        }
      }
      return $http.put(API_Base + 'command', {deviceId: deviceId, command: window.btoa(String.fromCharCode.apply(null, binaryCommand))}).then(function (data) {
        if (data && data.data) {
          if (data.data.status !== undefined && data.data.status !== 0) {
            return $q.reject(data.data);
          }
          return data.data;
        }
        return $q.reject();
      }, $q.reject);
    };

    return Dehumidifier;
  }];

  angular.module('ThermaStor')
    .service('Dehumidifier', dehuFactory);
}());
(function () {
  'use strict';
  var snackbarFactory = ['$timeout', '$q', '$ionicTemplateLoader', '$ionicBody', '$compile', function ($timeout, $q, $ionicTemplateLoader, $ionicBody, $compile) {
    var Snackbar = {},
      snackbarInstance,
      snackbarHideTimeout,
      toggleShown = function (doShow, hideDelay, html) {
        var children = snackbarInstance.element.children();

        if (doShow) {
          children.html(html);
          $compile(children.contents())(snackbarInstance.scope);
          snackbarInstance.element.removeClass('snackbar-disappear');
          snackbarInstance.element.addClass('snackbar-appear');
          snackbarInstance.isShown = true;

          if (!snackbarInstance.isVisible) {
            snackbarInstance.element.addClass('visible');
            snackbarInstance.isVisible = true;
          }

          if (hideDelay) {
            snackbarHideTimeout = $timeout(function () {
              toggleShown(false);

              snackbarHideTimeout = $timeout(function () {
                children = snackbarInstance.element.children();
                snackbarInstance.element.removeClass('visible');
                snackbarInstance.isVisible = false;
                children.html("");
              }, 350);
            }, hideDelay);
          }
        } else {
          snackbarInstance.element.removeClass('snackbar-appear');
          snackbarInstance.element.addClass('snackbar-disappear');
          snackbarInstance.isShown = false;
        }
      },
      SNACKBAR_TPL =
        '<div class="snackbar-container">' +
          '<div class="snackbar">' +
          '</div>' +
        '</div>';

    Snackbar.show = function (options) {
      options = options || {};
      var hideDelay = options.hide || 3000,
        creationPromise = $q.resolve(),
        templatePromise = options.templateUrl ? $ionicTemplateLoader.load(options.templateUrl) : $q.resolve(options.template || '');

      $timeout.cancel(snackbarHideTimeout);
      if (!snackbarInstance) {
        creationPromise = $ionicTemplateLoader.compile({
          template: SNACKBAR_TPL,
          appendTo: $ionicBody.get()
        }).then(function (self) {
          snackbarInstance = self;
        });
      }
      $q.all([templatePromise, creationPromise]).then(function (results) {
        var html = results[0];

        if (!options.omitImage) {
          html = '<span class="icon ion-checkmark balanced float-left pad-left"></span>' + html;
        }

        if (snackbarInstance.isShown) {
          toggleShown(false);

          snackbarHideTimeout = $timeout(function () {
            toggleShown(true, hideDelay, html);
          }, 350);
        } else {
          toggleShown(true, hideDelay, html);
        }
      });
    };

    return Snackbar;
  }];

  angular.module('ThermaStor')
    .service('Snackbar', snackbarFactory);
}());

(function () {
  'use strict';
  var userFactory = ['$http', '$q', 'API_Base', 'Integration_Base', 'Authentication', '$localStorage', '$rootScope', 'Snackbar', 'Dehumidifier', function ($http, $q, API_Base, Integration_Base, Authentication, $localStorage, $rootScope, Snackbar, Dehumidifier) {
    var User = {},
      july2017Cutoff = 1500000000,
      getTimeString = function (timeSecs) {
        if (!timeSecs || timeSecs < july2017Cutoff) {
          return undefined;
        }

        var aDate = new Date(timeSecs * 1000),
          time = aDate.toLocaleTimeString();

        time = time.replace(/\u200E/g, '');//from https://stackoverflow.com/questions/17913681
        time = time.replace(/^([^\d]*\d{1,2}:\d{1,2}):\d{1,2}([^\d]*)$/, '$1$2');
        return time + " " + aDate.toLocaleDateString();
      },
      findTrueStatus = function (device) {
        if (device.errorStatus) {
          device.trueStatus = 'Err ' + device.errorStatus + ': ';
          if (device.errorStatus === 1) {
            device.trueStatus += 'equip GUID is missing';
          } else if (device.errorStatus === 2) {
            device.trueStatus += 'DryTAG attached to another equipment';
          } else if (device.errorStatus === 3) {
            device.trueStatus += 'DryTAG UUID does not exist';
          } else if (device.errorStatus === 4) {
            device.trueStatus += 'DryTAG owned by another company';
          } else if (device.errorStatus === 5) {
            device.trueStatus += 'BLE dehu S/N not found';
          } else if (device.errorStatus === 6) {
            device.trueStatus += 'BLE dehu S/N owned by another company';
          } else if (device.errorStatus === 7) {
            device.trueStatus += 'cannot change equip category';
          } else if (device.errorStatus === 8) {
            device.trueStatus += 'DryTAG UUID can only be changed by DryLINK';
          } else if (device.errorStatus === 9) {
            device.trueStatus += 'cannot change serial number for BLE factory equip';
          } else if (device.errorStatus === 10) {
            device.trueStatus += 'ownership of unit has been released by DryLINK';
          } else if (device.errorStatus === 11) {
            device.trueStatus += 'serial number is missing';
          } else if (device.errorStatus === 12) {
            device.trueStatus += 'previously assigned equip not found';
          } else if (device.errorStatus === 13) {
            device.trueStatus += 'serial number not unique for class/type';
          } else if (device.errorStatus === 14) {
            device.trueStatus += 'Site Guid is missing or not found';
          } else if (device.errorStatus === 15) {
            device.trueStatus += 'Equipment name is required';
          } else if (device.errorStatus === 16) {
            device.trueStatus += 'Equipment class is missing';
          } else {
            device.trueStatus += 'Error unknown';
          }
        } else if (device.syncStatus === -1) {//Why does Diane make some error statuses negative and others positive?
          device.trueStatus = 'Err - never synced';
        } else if (device.syncStatus === 3 || device.syncStatus === 4) {//Some questions will never have an answer.
          device.trueStatus = 'Err unknown - sync failed';
        } else {
          device.trueStatus = 0;
        }
      },
      requiredFields = ['email', 'pwd', 'companyName'],
      validFields = requiredFields.concat(['hasEmail', 'hasSMS', 'hasPush', 'isCelsius', 'isGrams', 'isMeters', 'phone', 'firstName', 'lastName', 'companyId', 'userType']),
      validCompanyFields = ['companyName', 'address', 'city', 'state', 'zip', 'phone'],
      validSyncField = ["dehuId", "cequipId", "msensorId", "equipName", "serialNumber", "equipStatus", "typeName", "equipClass",
        "equipType", "manufacturer", "model", "drytagUuid", "string"],
      formatCompany = function (company) {
        var i, payload;

        payload = angular.copy(company);

        for (i in payload) {
          if (payload.hasOwnProperty(i) && validCompanyFields.indexOf(i) === -1) {
            delete payload[i];
          }
        }
        return $q.when(payload);
      },
      formatSyncObject = function (syncObject) {
        var i, payload;

        payload = angular.copy(syncObject);
        for (i in payload) {
          if (payload.hasOwnProperty(i) && validSyncField.indexOf(i) === -1) {
            delete payload[i];
          }
        }

        return payload;
      },
      /**
      * formatUserObject
      * @param {object} user - user object from sign up or edit preferences forms
      * @return {object} payload - a formatted object wrapped in promise
      */
      formatUserObject = function (user) {
        var i, payload;
        // if (user.password !== user.confirmPassword) {
        //   return $q.reject({title: 'Invalid Password', template: '"Confirm Password" does not match "Password"'});
        // }

        if (!user) {
          return $q.reject({title: 'Could not complete operation', template: "User information not found"});
        }

        payload = angular.copy(user);
        if (payload.password) {
          payload.pwd = payload.password;
        }

        for (i in payload) {
          if (payload.hasOwnProperty(i) && validFields.indexOf(i) === -1) {
            delete payload[i];
          }
        }
        for (i = 0; i < requiredFields.length; i += 1) {
          if (!payload.hasOwnProperty(requiredFields[i]) && (requiredFields[i] !== 'companyName' || !payload.hasOwnProperty('companyId'))) {
            return $q.reject({title: "Error saving user", template: requiredFields[i] + " is a required field"});
          }
        }
        return $q.when(payload);
      };

    User.get = function () {
      return $http.get(API_Base + 'account').then(function (response) {
        var accountData = response.data || {};

        if (accountData.firstName && accountData.lastName) {
          $rootScope.userName = accountData.firstName + " " + accountData.lastName;
        } else if (accountData.firstName) {
          $rootScope.userName = accountData.firstName;
        } else if (accountData.lastName) {
          $rootScope.userName = accountData.lastName;
        } else if (accountData.email) {
          $rootScope.userName = accountData.email;
        }
        if (accountData.companyName) {
          $rootScope.companyName = accountData.companyName;
        }
        $localStorage.useMeters = accountData.isMeters;
        $localStorage.userName = $rootScope.userName;
        return accountData;
      }, $q.reject);
    };

    User.getPossibleIntegrations = function () {
      return $http.get(API_Base + 'thirdp/client/list?apikey=' + Authentication.apiKey).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    User.getExistingIntegrations = function () {
      return $http.get(API_Base + 'thirdp/company/key').then(function (response) {
        return response.data;
      }, $q.reject);
    };

    User.addIntegration = function (clientId, isNew) {
      if (isNew) {
        return $http.post(API_Base + 'thirdp/company/key?clientId=' + clientId).then(function (response) {
          return response.data;
        }, $q.reject);
      }
      return $http.put(API_Base + 'thirdp/company/key?clientId=' + clientId + '&isEnabled=true').then(function (response) {
        var message = response && response.data && response.data.message;
        if (message === "Success") {
          return {integrationReady: true};
        }
        return {integrationReady: false};
      }, $q.reject);
    };

    User.deleteIntegration  = function (clientId) {
      return $http.put(API_Base + 'thirdp/company/key?clientId=' + clientId + '&isEnabled=false').then(function (response) {
        var message = response && response.data && response.data.message;
        if (message === "Success") {
          return {integrationReady: true};
        }
        return {integrationReady: false};
      }, $q.reject);
    };

    User.getSyncStatus = function (clientId) {
      return $http.get(Integration_Base + 'syncequip?clientId=' + clientId).then(function (response) {
        var data = response.data || [],
          i,
          errors = [],
          synced = [];
        for (i = 0; i < data.length; i += 1) {
          if (data[i]) {
            findTrueStatus(data[i]);

            if (data[i].trueStatus) {
              errors.push(data[i]);
            } else {
              synced.push(data[i]);
            }
          }
        }
        return {errors: errors, synced: synced};
      }, $q.reject);
    };

    User.updateSyncStatus = function (clientId, updateObject) {
      var formattedObject = formatSyncObject(updateObject);
      return $http.post(Integration_Base + 'syncequip?clientId=' + clientId, formattedObject).then(function (response) {
        findTrueStatus(response.data);
        if (response.data && !response.data.trueStatus) {
          Snackbar.show({template: updateObject.equipName + " synced", hide: 2500});
        }
        return response.data;
      }, $q.reject);
    };

    User.getCompany = function (myId) {
      var companyRecords = {employees: [], me: [], invitations: [], emailList: []};

      return $http.get(API_Base + 'company').then(function (company) {
        if (company && company.data) {
          companyRecords.company = company.data;
          companyRecords.company.fullLocation = "";
          if (companyRecords.company.city) {
            companyRecords.company.fullLocation += companyRecords.company.city;
          }
          if (companyRecords.company.state) {
            if (companyRecords.company.fullLocation.length) {
              companyRecords.company.fullLocation += ", ";
            }
            companyRecords.company.fullLocation += companyRecords.company.state;
          }
          companyRecords.company.fullLocation += " " + companyRecords.company.zip;
        }

        return $http.get(API_Base + 'account/employee').then(function (employees) {
          var i;
          if (employees && employees.data && employees.data.length) {
            for (i = 0; i < employees.data.length; i += 1) {
              if (employees.data[i] && employees.data[i].email) {
                companyRecords.emailList.push(employees.data[i].email);
              }

              if (employees.data[i] && String(employees.data[i].accountId) === String(myId)) {
                companyRecords.me.push(employees.data[i]);
              } else {
                companyRecords.employees.push(employees.data[i]);
              }
            }
          }

          return $http.get(API_Base + 'invite').then(function (invitations) {
            var j,
              emailList = [],
              emailIndex,
              currentTime = (new Date()).getTime() / 1000;

            if (invitations && invitations.data && invitations.data.length) {
              for (j = 0; j < invitations.data.length; j += 1) {
                if (invitations.data[j] && invitations.data[j].inviteeEmail && invitations.data[j].inviteeEmail.length) {
                  emailIndex = emailList.indexOf(invitations.data[j].inviteeEmail);
                  if (emailIndex < 0) {
                    invitations.data[j].completed = companyRecords.emailList.indexOf(invitations.data[j].inviteeEmail) > -1;
                    companyRecords.invitations.push(invitations.data[j]);
                    emailList.push(invitations.data[j].inviteeEmail);
                  } else if (companyRecords.invitations[emailIndex].expireSecs < invitations.data[j].expireSecs) {
                    invitations.data[j].completed = companyRecords.invitations[emailIndex].completed;
                    companyRecords.invitations[emailIndex] = invitations.data[j];
                  }
                }
              }

              if (companyRecords.invitations && companyRecords.invitations.length) {
                for (j = 0; j < companyRecords.invitations.length; j += 1) {
                  if (companyRecords.invitations[j]) {
                    if (!companyRecords.invitations[j].expireSecs || companyRecords.invitations[j].expireSecs < currentTime) {
                      companyRecords.invitations[j].expired = true;
                    } else {
                      companyRecords.invitations[j].expired = false;
                    }
                  }
                }
              }
            }

            return $q.resolve(companyRecords);
          }, function () {
            return $q.resolve(companyRecords);
          });
        }, $q.reject);
      }, $q.reject);
    };

    User.updateCompany = function (company) {
      return formatCompany(company).then(function (formattedCompany) {
        return $http.put(API_Base + 'company', formattedCompany).then(function (response) {
          return response.data;
        }, $q.reject);
      });
    };

    User.getCompanyLogo = function () {
      return $http.get(API_Base + 'account/company_logo').then(function (response) {
        return response.data;
      }, $q.reject);
    };

    User.uploadCompanyLogo = function (newFile) {
      if (!newFile || !newFile.name) {
        return $q.reject({message: "Could not create upload logo without file.", status: 400});
      }
      var formData = new FormData();
      formData.append('file', newFile);

      return $http.post(API_Base + 'account/company_logo', formData, {
        headers: {'Content-Type': undefined},
        transformResponse: angular.identity
      }).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    User.getWeather = function () {
      if (!$localStorage.location || (!$localStorage.location.latitude && !$localStorage.location.longitude)) {
        return $q.reject(-1);
      }

      return $http.get(API_Base + 'weather?lat=' + $localStorage.location.latitude + '&lng=' + $localStorage.location.longitude).then(function (response) {
        if (!response || !response.data) {
          return $q.reject(0);
        }
        var allData = response.data,
          tC = (allData.temperature - 32.0) * 5.0 / 9.0,
          vaporPressure = (6.116441 * Math.pow(10, ((7.591386 * tC) / (tC + 240.7263)))) * allData.rh / 100;//These calculations are from the Sherpa project psychro.c library.

        allData.water =  4354.0 * vaporPressure / (998.0 - vaporPressure);//Measured in grains per pound.
        allData.dewpoint = 240.7263 / ((7.591386 / Math.log10(vaporPressure / 6.116441)) - 1.0);//Measured in Celsius.
        if (allData.isCelsius) {
          allData.temperature = tC;
        } else {
          allData.dewpoint = (allData.dewpoint * 9.0 / 5.0) + 32.0;
        }
        if (allData.isGrams) {
          allData.water /= 7.0;
        }
        return allData;
      }, function () {
        return $q.reject(0);
      });
    };

    User.authenticate = function (user) {
      return Authentication.authenticateUser(user).then(function () {
        return User.get();
      }, $q.reject);
    };

    User.create = function (user) {
      return formatUserObject(user).then(function (formattedUser) {
        return Authentication.newUser(formattedUser);
      }, $q.reject);
    };

    User.update = function (user) {
      return formatUserObject(user).then(function (formattedUser) {
        return $http.put(API_Base.replace(/v1/g, 'v2') + 'account', formattedUser).then(function (response) {
          return response.data;
        }, function (err) {
          if (err && err.data) {
            err.errors = err.data.errors ? err.data.errors.join("<br>") : err.data.message || err.data;
          }
          return $q.reject(err);
        });
      }, $q.reject);
    };

    User.cancelAccount = function (accountId) {
      return $http.delete(API_Base + 'account/' + accountId + '?apikey=' + Authentication.apiKey).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    User.retryEmail = function (emailAddress) {
      return $http.post(API_Base + 'account/email_verify?apikey=' + Authentication.apiKey + '&email=' + encodeURIComponent(emailAddress)).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    User.deleteUser = function () {
      return $http.delete(API_Base + 'account').then(function (response) {
        return response.data;
      }, $q.reject);
    };

    User.removeEmployee = function (employeeId) {
      return $http.delete(API_Base + 'account/employee/' + employeeId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    User.updateEmployee = function (employeeId, role) {
      if (role === 'owner') {
        return $http.put(API_Base + 'account/owner?employeeId=' + employeeId).then(function (response) {
          return response.data;
        }, $q.reject);
      }
      return $http.put(API_Base + 'account/employee/' + employeeId + "?role=" + role).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    User.sendInvitation = function (email, role) {
      return $http.post(API_Base + 'invite?email=' + encodeURIComponent(email) + "&role=" + role).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    User.checkForInvitations = function (email) {
      return $http.get(API_Base + 'invite/employee?apikey=' + Authentication.apiKey + '&email=' + encodeURIComponent(email)).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    User.getLegalStuff = function () {
      return $http.get(API_Base + 'account/consent').then(function (response) {
        var i,
          unfilteredStuff = (response && response.data) || [],
          legalStuff = [];

        for (i = 0; i < unfilteredStuff.length; i += 1) {
          if (unfilteredStuff[i] && unfilteredStuff[i].docId && unfilteredStuff[i].docId > -1 && unfilteredStuff[i].docText) {
            legalStuff.push(unfilteredStuff[i]);
          }
        }
        return legalStuff;
      }, $q.reject);
    };

    User.legalAgreement = function (legalId) {
      return $http.put(API_Base + 'account/consent/' + legalId).then(function (response) {
        return response && response.data;
      }, $q.reject);
    };

    User.alterJobAlerts = function (jobId, enable) {
      if (!jobId) {
        return $q.reject({message: "No job ID found.", status: 400});
      }

      return $http.put(API_Base + 'alerts/job/' + jobId + '/status?isEnabled=' + !!enable).then(function (response) {
        return response && response.data;
      }, $q.reject);
    };

    User.getJobAlerts = function (jobId) {
      if (!jobId) {
        return $q.reject({message: "No job ID found.", status: 400});
      }

      return $http.get(API_Base + 'alerts/job/' + jobId + '/status').then(function (response) {
        var status = (response && response.data) || {};
        return $http.get(API_Base.replace(/v1/g, 'v2') + 'alerts/job/' + jobId).then(function (response2) {
          var alertInfo = (response2 && response2.data) || {},
            unseenAlertCount = 0,
            offSettings = [],
            rhSettings = [],
            tempSettings = [],
            miscellaneousSettings = [],
            aDevice,
            i;

          if (!$localStorage.seenJobAlertIds || !$localStorage.seenJobAlertIds.length) {
            $localStorage.seenJobAlertIds = [];
          }
          if (alertInfo && alertInfo.settings && alertInfo.settings.length) {
            for (i = 0; i < alertInfo.settings.length; i += 1) {
              if (alertInfo.settings[i] && alertInfo.settings[i].ruleName && alertInfo.settings[i].ruleName.indexOf) {
                alertInfo.settings[i].ruleName = alertInfo.settings[i].ruleName.replace(/Dercrease/g, "Decrease");
                if (alertInfo.settings[i].ruleName === "Equipment Turned Off") {
                  alertInfo.settings[i].ruleText = "Send an alert if a piece of equipment is powered off when on a job.";
                } else if (alertInfo.settings[i].ruleName === "DryTAG Offline") {
                  alertInfo.settings[i].ruleText = "Send an alert if a DryTAG stops broadcasting information over Bluetooth for more than the duration below.";
                } else if (alertInfo.settings[i].ruleName === "Dehu Offline") {
                  alertInfo.settings[i].ruleText = "Send an alert if a DryMAX goes offline and stops uploading data for more than the duration below.";
                } else if (alertInfo.settings[i].ruleName === "Low Chamber RH") {
                  alertInfo.settings[i].ruleText = "Send an alert when the RH of the chamber goes below the threshold below according to Bluetooth equipment in the chamber.";
                } else if (alertInfo.settings[i].ruleName === "High Chamber RH") {
                  alertInfo.settings[i].ruleText = "Send an alert when the RH of the chamber goes above the threshold below according to Bluetooth equipment in the chamber.";
                } else if (alertInfo.settings[i].ruleName === "Low Chamber Temperature") {
                  alertInfo.settings[i].ruleText = "Send an alert when the temperature of the chamber goes below the threshold below according to Bluetooth equipment in the chamber.";
                } else if (alertInfo.settings[i].ruleName === "High Chamber Temperature") {
                  alertInfo.settings[i].ruleText = "Send an alert when the temperature of the chamber goes above the threshold below according to Bluetooth equipment in the chamber.";
                } else if (alertInfo.settings[i].ruleName === "Chamber GPP Slow Decrease") {
                  alertInfo.settings[i].ruleText = "Send an alert when the absolute humidity of the chamber does not go below the threshold below within the duration specified according to Bluetooth equipment in the chamber.";
                } else if (alertInfo.settings[i].ruleName === "Chamber Drying Goal Reached") {
                  alertInfo.settings[i].ruleText = "Send an alert when DrySENSE measurements show that the moisture content goal for the chamber has been reached.";
                }
                if (alertInfo.settings[i].ruleName.indexOf("Off") > -1) {
                  alertInfo.settings[i].convertedThreshold = alertInfo.settings[i].threshold;
                  offSettings.push(alertInfo.settings[i]);
                } else if (alertInfo.settings[i].ruleName.indexOf("RH") > -1) {
                  alertInfo.settings[i].convertedThreshold = alertInfo.settings[i].threshold;
                  alertInfo.settings[i].thresholdUnits = "%";
                  rhSettings.push(alertInfo.settings[i]);
                } else if (alertInfo.settings[i].ruleName.indexOf("Temperature") > -1) {
                  if (alertInfo.settings[i].threshold > -100) {
                    if ($localStorage.useCelsius) {
                      alertInfo.settings[i].convertedThreshold = Math.round(5 * (alertInfo.settings[i].threshold - 32) / 9);
                      alertInfo.settings[i].thresholdUnits = "°C";
                    } else {
                      alertInfo.settings[i].convertedThreshold = alertInfo.settings[i].threshold;
                      alertInfo.settings[i].thresholdUnits = "°F";
                    }
                  } else {
                    alertInfo.settings[i].convertedThreshold = alertInfo.settings[i].threshold;
                  }
                  tempSettings.push(alertInfo.settings[i]);
                } else {
                  if (alertInfo.settings[i].threshold > -100 && alertInfo.settings[i].ruleName.indexOf("GPP") > -1) {
                    if ($localStorage.useGrams) {
                      alertInfo.settings[i].convertedThreshold = Math.round(alertInfo.settings[i].threshold / 7);
                      alertInfo.settings[i].thresholdUnits = "g/kg";
                    } else {
                      alertInfo.settings[i].convertedThreshold = alertInfo.settings[i].threshold;
                      alertInfo.settings[i].thresholdUnits = "gpp";
                    }
                  } else {
                    alertInfo.settings[i].convertedThreshold = alertInfo.settings[i].threshold;
                  }
                  miscellaneousSettings.push(alertInfo.settings[i]);
                }
              }
            }
            alertInfo.settings = offSettings.concat(rhSettings, tempSettings, miscellaneousSettings);
          }
          if (alertInfo && alertInfo.equipment && alertInfo.equipment.length) {
            for (i = 0; i < alertInfo.equipment.length; i += 1) {
              if (alertInfo.equipment[i]) {
                if (alertInfo.equipment[i].beaconId && alertInfo.equipment[i].beaconId > -1) {
                  alertInfo.equipment[i].hasBeacon = true;
                }
                if (alertInfo.equipment[i].dehuId && alertInfo.equipment[i].dehuId > -1) {
                  aDevice = $rootScope.searchBluetoothListsForMatch('databaseId', alertInfo.equipment[i].dehuId);
                  if (aDevice && aDevice.modelType) {
                    alertInfo.equipment[i].modelType = aDevice.modelType;
                  } else if (alertInfo.equipment[i].typeName === "Dehu LGR - L") {
                    alertInfo.equipment[i].modelType = 'Dry Max';
                  } else {
                    alertInfo.equipment[i].modelType = 'Dry Max XL';
                  }
                }
                Dehumidifier.determineIconCategory(alertInfo.equipment[i]);
              }
            }
          }
          if (alertInfo && alertInfo.alerts && alertInfo.alerts.length) {
            for (i = 0; i < alertInfo.alerts.length; i += 1) {
              if (alertInfo.alerts[i]) {
                if (alertInfo.alerts[i].endSecs) {
                  alertInfo.alerts[i].shouldShow = false;
                  alertInfo.alerts[i].endTimeString = getTimeString(alertInfo.alerts[i].endSecs);
                } else {
                  alertInfo.alerts[i].shouldShow = true;
                  if (alertInfo.alerts[i].alertId && $localStorage.seenJobAlertIds.indexOf(alertInfo.alerts[i].alertId) < 0) {
                    unseenAlertCount += 1;
                    alertInfo.alerts[i].isNew = true;
                  }
                }
                if (alertInfo.alerts[i].conditionMetSecs) {
                  alertInfo.alerts[i].timeString = getTimeString(alertInfo.alerts[i].conditionMetSecs);
                }
                if (alertInfo.alerts[i].alertName === "Chamber Drying Goal Reached") {
                  alertInfo.alerts[i].isGood = true;
                }
              }
            }
          }
          if (unseenAlertCount > 99) {
            unseenAlertCount = 99;
          } else if (unseenAlertCount < 0) {
            unseenAlertCount = 0;
          }
          alertInfo.unseenAlertCount = unseenAlertCount;
          alertInfo.isEnabled = status.isEnabled;
          return alertInfo;
        }, $q.reject);
      }, $q.reject);
    };

    User.getAlerts = function (type) {
      if (type === 'equipment') {
        return $http.get(API_Base + 'alerts/equip').then(function (response) {
          var alertSettings = (response && response.data) || {},
            unseenAlertCount = 0,
            aDevice,
            i;

          if (!$localStorage.seenEquipmentAlertIds || !$localStorage.seenEquipmentAlertIds.length) {
            $localStorage.seenEquipmentAlertIds = [];
          }
          if (alertSettings && alertSettings.settings && alertSettings.settings.length) {
            for (i = 0; i < alertSettings.settings.length; i += 1) {
              if (alertSettings.settings[i]) {
                if (alertSettings.settings[i].ruleName === "Reservoir full") {
                  alertSettings.settings[i].realName = "Reservoir full";
                  alertSettings.settings[i].ruleText = "Send notifications when dehumidifier reservoirs are full.";
                } else if (alertSettings.settings[i].ruleName === "System Protect") {
                  alertSettings.settings[i].realName = "System Protection";
                  alertSettings.settings[i].ruleText = "Send notifications when dehumidifiers enter system protection mode because of high temperatures or low refrigerant.";
                } else if (alertSettings.settings[i].ruleName === "Battery Low") {
                  alertSettings.settings[i].realName = "Low Battery";
                  alertSettings.settings[i].ruleText = "Send notifications when DryTAG or DrySENSE batteries need to be replaced.";
                } else if (alertSettings.settings[i].ruleName === "Filter Change Due") {
                  alertSettings.settings[i].realName = "Filter Change";
                  alertSettings.settings[i].ruleText = "Send notifications when dehumidifier filters need to be replaced.";
                } else if (alertSettings.settings[i].ruleName === "Equipment Not Connected" || alertSettings.settings[i].ruleName === "Not Connected" ||
                           (alertSettings.settings[i].ruleName && alertSettings.settings[i].ruleName.indexOf && alertSettings.settings[i].ruleName.indexOf('Equipment Missing') > -1)) {
                  alertSettings.settings[i].realName = "Not Connected";
                  alertSettings.settings[i].ruleText = "Send notifications when equipment has not been scanned over Bluetooth for more than the number of days below.";
                } else {
                  alertSettings.settings[i].realName = alertSettings.settings[i].ruleName;
                }
              }
            }
          }
          if (alertSettings && alertSettings.alerts && alertSettings.alerts.length) {
            for (i = 0; i < alertSettings.alerts.length; i += 1) {
              if (alertSettings.alerts[i]) {
                if (alertSettings.alerts[i].beaconId && alertSettings.alerts[i].beaconId > -1) {
                  alertSettings.alerts[i].hasBeacon = true;
                }
                if (alertSettings.alerts[i].conditionMetSecs) {
                  alertSettings.alerts[i].timeString = getTimeString(alertSettings.alerts[i].conditionMetSecs);
                }
                if (alertSettings.alerts[i].eqAlertId && $localStorage.seenEquipmentAlertIds.indexOf(alertSettings.alerts[i].eqAlertId) < 0) {
                  unseenAlertCount += 1;
                  alertSettings.alerts[i].isNew = true;
                }
                if (alertSettings.alerts[i].dehuId && alertSettings.alerts[i].dehuId > -1) {
                  aDevice = $rootScope.searchBluetoothListsForMatch('databaseId', alertSettings.alerts[i].dehuId);
                  if (aDevice && aDevice.modelType) {
                    alertSettings.alerts[i].modelType = aDevice.modelType;
                  } else if (alertSettings.alerts[i].typeName === "Dehu LGR - L") {
                    alertSettings.alerts[i].modelType = 'Dry Max';
                  } else {
                    alertSettings.alerts[i].modelType = 'Dry Max XL';
                  }
                }
                Dehumidifier.determineIconCategory(alertSettings.alerts[i]);
              }
            }
          }
          $rootScope.unseenEquipmentAlertCount = unseenAlertCount;
          if (!$rootScope.unseenJobAlertCount) {
            $rootScope.unseenJobAlertCount = 0;
          }
          unseenAlertCount = $rootScope.unseenEquipmentAlertCount + $rootScope.unseenJobAlertCount;
          if (unseenAlertCount > 99) {
            unseenAlertCount = 99;
          } else if (unseenAlertCount < 0) {
            unseenAlertCount = 0;
          }
          $rootScope.unseenTotalAlertCount = unseenAlertCount;
          return alertSettings;
        }, $q.reject);
      }
      if (type === 'jobStatus') {
        return $http.get(API_Base + 'alerts/jobstatus').then(function (response) {
          var alertSettings = (response && response.data) || [],
            totalUnseenAlertCount = 0,
            unseenAlertCount = 0,
            i,
            j;

          if (!$localStorage.seenJobAlertIds || !$localStorage.seenJobAlertIds.length) {
            $localStorage.seenJobAlertIds = [];
          }
          if (alertSettings && alertSettings.length) {
            for (i = 0; i < alertSettings.length; i += 1) {
              if (alertSettings[i]) {
                unseenAlertCount = 0;
                if (alertSettings[i].alerts && alertSettings[i].alerts.length) {
                  for (j = 0; j < alertSettings[i].alerts.length; j += 1) {
                    if (alertSettings[i].alerts[j]) {
                      if (!alertSettings[i].alerts[j].endSecs) {
                        alertSettings[i].shouldShow = true;
                        if (alertSettings[i].alerts[j].alertId && $localStorage.seenJobAlertIds.indexOf(alertSettings[i].alerts[j].alertId) < 0) {
                          unseenAlertCount += 1;
                          alertSettings[i].alerts[j].isNew = true;
                        }
                      }
                      if (alertSettings[i].alerts[j].conditionMetSecs) {
                        alertSettings[i].alerts[j].timeString = getTimeString(alertSettings[i].alerts[j].conditionMetSecs);
                      }
                      if (alertSettings[i].alerts[j].alertName === "Chamber Drying Goal Reached") {
                        alertSettings[i].alerts[j].isGood = true;
                      }
                    }
                  }
                }
                if (unseenAlertCount > 99) {
                  unseenAlertCount = 99;
                } else if (unseenAlertCount < 0) {
                  unseenAlertCount = 0;
                }
                alertSettings[i].unseenAlertCount = unseenAlertCount;
                totalUnseenAlertCount += unseenAlertCount;
              }
            }
          }
          $rootScope.unseenJobAlertCount = totalUnseenAlertCount;
          if (!$rootScope.unseenEquipmentAlertCount) {
            $rootScope.unseenEquipmentAlertCount = 0;
          }
          unseenAlertCount = $rootScope.unseenEquipmentAlertCount + $rootScope.unseenJobAlertCount;
          if (unseenAlertCount > 99) {
            unseenAlertCount = 99;
          } else if (unseenAlertCount < 0) {
            unseenAlertCount = 0;
          }
          $rootScope.unseenTotalAlertCount = unseenAlertCount;
          return alertSettings;
        }, $q.reject);
      }
      if (type === 'management') {
        return $http.get(API_Base + 'alerts/jobmgmt').then(function (response) {
          var alertSettings = (response && response.data) || {},
            i;

          if (alertSettings && alertSettings.settings && alertSettings.settings.length) {
            for (i = 0; i < alertSettings.settings.length; i += 1) {
              if (alertSettings.settings[i]) {
                if (alertSettings.settings[i].ruleName === "Job Created") {
                  alertSettings.settings[i].realName = "New Job Started";
                  alertSettings.settings[i].ruleText = "Send notifications when jobs are created.";
                } else if (alertSettings.settings[i].ruleName === "Job Completed") {
                  alertSettings.settings[i].realName = "Job Completed";
                  alertSettings.settings[i].ruleText = "Send notifications when jobs are ended.";
                } else if (alertSettings.settings[i].ruleName && alertSettings.settings[i].ruleName.indexOf &&
                           alertSettings.settings[i].ruleName.indexOf('More than') > -1 && alertSettings.settings[i].ruleName.indexOf('Job Days') > -1) {
                  alertSettings.settings[i].realName = "Job Duration";
                  alertSettings.settings[i].ruleText = "Send notifications when jobs take longer than the number of days below.";
                } else {
                  alertSettings.settings[i].realName = alertSettings.settings[i].ruleName;
                }
              }
            }
          }
          return alertSettings;
        }, $q.reject);
      }
      return $q.resolve({});
    };

    User.updateAlertRule = function (alert, type) {
      var typeString = type === 'management' ? 'jobmgmt' : type === 'equipment' ? 'equip' : 'job';
      if (!alert || !alert.settingId || !(alert.settingId > -1)) {
        return $q.reject({message: "Setting ID required.", status: 400});
      }

      return $http.put(API_Base + 'alerts/' + typeString + '/rule/' + alert.settingId + '?isEnabled=' + alert.isEnabled + '&duration=' +
                       alert.duration + '&threshold=' + alert.threshold).then(function (response) {
        Snackbar.show({template: "Alert setting updated", hide: 2500});
        return (response && response.data) || {};
      }, $q.reject);
    };

    User.updateJobAlertEquipment = function (alert) {
      if (!alert || !alert.jobAlertEquipId) {
        return $q.reject({message: "Equipment ID required.", status: 400});
      }

      return $http.put(API_Base + 'alerts/job/equip/' + alert.jobAlertEquipId + '?isEnabled=' + alert.isEnabled).then(function (response) {
        return (response && response.data) || {};
      }, $q.reject);
    };

    User.updateAlertUsers = function (users, type) {
      var typeString = type === 'management' ? 'jobmgmt' : type === 'equipment' ? 'equip' : 'job',
        updateAUser = function (start) {
          var i;

          if (!users || !users.length || !(users.length > start)) {
            return $q.resolve();
          }
          for (i = start; i < users.length; i += 1) {
            if (users[i] && users[i].hasBeenChanged && users[i].alactId) {
              return $http.put(API_Base + 'alerts/' + typeString + '/account/' + users[i].alactId + '?isEnabled=' + users[i].isEnabled).then(function () {
                return updateAUser(i + 1);
              }, $q.reject);
            }
          }
          return $q.resolve();
        };
      if (!users || !users.length) {
        return $q.reject({message: "User list required.", status: 400});
      }

      return updateAUser(0).then(function () {
        Snackbar.show({template: "Alert users updated", hide: 2500});
        return $q.resolve();
      }, $q.reject);
    };

    return User;
  }];

  angular.module('ThermaStor')
    .service('User', userFactory);
}());

(function () {
  'use strict';
  var jobFactory = ['$http', '$q', 'API_Base', 'Integration_Base', '$rootScope', '$ionicLoading', '$timeout', '$localStorage', 'Snackbar', 'Dehumidifier', function ($http, $q, API_Base, Integration_Base, $rootScope, $ionicLoading, $timeout, $localStorage, Snackbar, Dehumidifier) {
    var Job = {},
      locationFields = ['name', 'address', 'city', 'state', 'zip'],
      requiredFields = ['jobName', 'startDateSecs', 'address', 'zip'],
      validFields = requiredFields.concat(['city', 'state', 'insuranceName', 'claimNumber', 'description', 'endDateSecs']),
      validSizeFields = ['ceilingSqFt', 'wallSqFt', 'floorSqFt', 'ceilingWetPercent', 'wallWetPercent', 'floorWetPercent', 'cubicFt'],
      july2017Cutoff = 1500000000,
      getTimeString = function (timeSecs) {
        if (!timeSecs || timeSecs < july2017Cutoff) {
          return '---';
        }

        var aDate = new Date(timeSecs * 1000),
          time = aDate.toLocaleTimeString();

        time = time.replace(/\u200E/g, '');//from https://stackoverflow.com/questions/17913681
        time = time.replace(/^([^\d]*\d{1,2}:\d{1,2}):\d{1,2}([^\d]*)$/, '$1$2');
        return time;
      },
      formatLocation = function (location) {
        var i,
          payload = {},
          urlEncoded = [];

        payload = angular.copy(location);

        for (i in payload) {
          if (payload.hasOwnProperty(i) && locationFields.indexOf(i) === -1) {
            delete payload[i];
          }
        }
        // for (i = 0; i < locationFields.length; i += 1) {
        //   if (!payload.hasOwnProperty(locationFields[i])) {
        //     return $q.reject({title: "Error updating job", template: locationFields[i] + " is a required field"});
        //   }
        // }
        for (i in payload) {
          if (payload.hasOwnProperty(i)) {
            urlEncoded.push(encodeURIComponent(i) + "=" + encodeURIComponent(payload[i]));
          }
        }

        if (urlEncoded.length) {
          return $q.when("?" + urlEncoded.join("&"));
        }
        return $q.when("");
      },
      formatJobBasics = function (jobBasics) {
        var i, payload;

        payload = angular.copy(jobBasics);

        for (i in payload) {
          if (payload.hasOwnProperty(i) && validFields.indexOf(i) === -1) {
            delete payload[i];
          }
        }
        for (i = 0; i < requiredFields.length; i += 1) {
          if (!payload.hasOwnProperty(requiredFields[i])) {
            return $q.reject({title: "Error updating job", template: requiredFields[i] + " is a required field"});
          }
        }
        return $q.when(payload);
      },
      formatChamberSizes = function (chamber) {
        var i,
          payload = {},
          urlEncoded = [];

        for (i in chamber) {
          if (chamber.hasOwnProperty(i) && validSizeFields.indexOf(i) !== -1 && !isNaN(chamber[i])) {
            payload[i] = chamber[i];
          }
        }
        for (i in payload) {
          if (payload.hasOwnProperty(i)) {
            urlEncoded.push(encodeURIComponent(i) + "=" + encodeURIComponent(payload[i]));
          }
        }

        if (urlEncoded.length) {
          return $q.when("?" + urlEncoded.join("&"));
        }
        return $q.when();
      },
      formatChamber = function (chamber, useUrlEncoding, includeSizes) {
        var i,
          payload = {},
          urlEncoded = [];

        if (chamber && chamber.chamberName) {
          payload.chamberName = chamber.chamberName;
        } else {
          payload.chamberName = "New Chamber";
        }
        if (chamber && chamber.category) {
          payload.category = chamber.category;
        } else {
          payload.category = "1";
        }
        if (chamber && chamber.classification) {
          if (useUrlEncoding) {
            payload.class = chamber.classification;//Diane calls this variable "class" sometimes and other times "classification".
          } else {
            payload.classification = chamber.classification;
          }
        } else {
          if (useUrlEncoding) {
            payload.class = "1";
          } else {
            payload.classification = "1";
          }
        }

        if (!useUrlEncoding) {
          return $q.when(payload);
        }

        for (i in payload) {
          if (payload.hasOwnProperty(i)) {
            urlEncoded.push(encodeURIComponent(i) + "=" + encodeURIComponent(payload[i]));
          }
        }

        if (includeSizes) {
          return formatChamberSizes(chamber).then(function (formattedSizes) {
            if (formattedSizes && formattedSizes.length) {
              return $q.when(formattedSizes + '&' + urlEncoded.join("&"));
            }
            return $q.when("?" + urlEncoded.join("&"));
          });
        }
        return $q.when("?" + urlEncoded.join("&"));
      },
      formatContact = function (contact, useUrlEncoding) {
        var i,
          payload = {},
          urlEncoded = [];

        if (contact && contact.contactName) {
          payload.contactName = contact.contactName;
        } else {
          payload.contactName = "";
        }
        if (useUrlEncoding) {
          payload.name = payload.contactName;//Diane calls this variable "name" sometimes and other times "contactName".
          delete payload.contactName;
        }
        if (contact && contact.phone) {
          payload.phone = contact.phone;
        } else {
          payload.phone = "";
        }

        if (!useUrlEncoding) {
          return $q.when(payload);
        }

        for (i in payload) {
          if (payload.hasOwnProperty(i)) {
            urlEncoded.push(encodeURIComponent(i) + "=" + encodeURIComponent(payload[i]));
          }
        }

        return $q.when("?" + urlEncoded.join("&"));
      },
      formatCompanyEquipment = function (equipment, useUrlEncoding) {
        var i,
          payload = {},
          urlEncoded = [];

        equipment = equipment || {};
        if (equipment.fullName) {
          payload.typeName = equipment.fullName;
        }
        if (equipment.equipmentName) {
          payload.equipName = equipment.equipmentName;
        }
        if (equipment.manufacturer) {
          payload.manufacturer = equipment.manufacturer;
        }
        if (equipment.model) {
          payload.model = equipment.model;
        }
        if (equipment.serialNumber) {
          payload.serialNumber = equipment.serialNumber;
        }

        if (!useUrlEncoding) {
          return $q.when(payload);
        }

        for (i in payload) {
          if (payload.hasOwnProperty(i)) {
            urlEncoded.push(encodeURIComponent(i) + "=" + encodeURIComponent(payload[i]));
          }
        }

        return $q.when("?" + urlEncoded.join("&"));
      },
      formatEquipment2 = function (equipment) {
        var i,
          payload = {},
          urlEncoded = [];

        if (equipment.databaseId > -1) {
          payload.dehuId = equipment.databaseId;
        }
        if (equipment.msensorId > -1) {
          payload.msensorId = equipment.msensorId;
          if (equipment.location) {
            payload.location = equipment.location;
          }
          if (equipment.material) {
            payload.material = equipment.material;
          }
          if (equipment.target) {
            payload.target = equipment.target;
          }
        }
        if (equipment.cequipId > -1) {
          payload.cequipId = equipment.cequipId;
        }

        for (i in payload) {
          if (payload.hasOwnProperty(i)) {
            urlEncoded.push(encodeURIComponent(i) + "=" + encodeURIComponent(payload[i]));
          }
        }

        if (urlEncoded.length) {
          return $q.when("?" + urlEncoded.join("&"));
        }
        return $q.when("");
      },
      formatEquipment = function (equipment, suppressRetro) {
        var i,
          payload = {},
          urlEncoded = [];

        //Diane has different names for the put and post fields of equipment versus the get fields, for whatever unfathomable reason.
        if (equipment.equipmentType) {
          payload.equipType = equipment.equipmentType;
        } else if (!suppressRetro) {
          payload.equipType = 'retrodehu';
        }
        if (equipment.equipmentName) {
          payload.equipName = equipment.equipmentName;
        }
        if (equipment.exSize) {
          payload.equipSize = equipment.exSize;
        }
        if (equipment.location) {
          payload.location = equipment.location;
        } else {
          payload.location = "";
        }
        if (!isNaN(equipment.dehuId)) {
          payload.dehuId = equipment.dehuId;
        }

        for (i in payload) {
          if (payload.hasOwnProperty(i)) {
            urlEncoded.push(encodeURIComponent(i) + "=" + encodeURIComponent(payload[i]));
          }
        }

        if (urlEncoded.length) {
          return $q.when("?" + urlEncoded.join("&"));
        }
        return $q.when("");
      },
      formatArea = function (area) {
        var i,
          payload = {},
          urlEncoded = [];

        payload.areaType = area.measurementType;
        payload.areaName = area.editName;
        payload.jobId = area.jobId;

        for (i in payload) {
          if (payload.hasOwnProperty(i)) {
            urlEncoded.push(encodeURIComponent(i) + "=" + encodeURIComponent(payload[i]));
          }
        }

        if (urlEncoded.length) {
          return $q.when("?" + urlEncoded.join("&"));
        }
        return $q.when("");
      },
      formatMeasurement = function (measurement) {
        var i,
          payload = {},
          urlEncoded = [];

        if (measurement.measurementType) {
          payload.measuretype = measurement.measurementType;
        }
        payload.rh = measurement.editRh;
        payload.temperature = measurement.editTemp;
        payload.sampleSecs = measurement.sampleSecs;
        payload.visitId = measurement.visitId;

        for (i in payload) {
          if (payload.hasOwnProperty(i)) {
            urlEncoded.push(encodeURIComponent(i) + "=" + encodeURIComponent(payload[i]));
          }
        }

        if (urlEncoded.length) {
          return $q.when("?" + urlEncoded.join("&"));
        }
        return $q.when("");
      },
      formatMoisture = function (measurement) {
        var i,
          payload = {},
          urlEncoded = [];

        payload.moisture = measurement.editMoisture;
        payload.sampleSecs = measurement.sampleSecs;
        payload.visitId = measurement.visitId;

        for (i in payload) {
          if (payload.hasOwnProperty(i)) {
            urlEncoded.push(encodeURIComponent(i) + "=" + encodeURIComponent(payload[i]));
          }
        }

        if (urlEncoded.length) {
          return $q.when("?" + urlEncoded.join("&"));
        }
        return $q.when("");
      },
      formatMoisturePoint = function (measurement) {
        var i,
          payload = {},
          urlEncoded = [];

        payload.location = measurement.editLocation;
        payload.material = measurement.editMaterial;
        payload.target = measurement.editTarget;

        for (i in payload) {
          if (payload.hasOwnProperty(i)) {
            urlEncoded.push(encodeURIComponent(i) + "=" + encodeURIComponent(payload[i]));
          }
        }

        if (urlEncoded.length) {
          return $q.when("?" + urlEncoded.join("&"));
        }
        return $q.when("");
      },
      createOrUpdateReading = function (measurement) {
        // if (measurement && measurement.jobMeasureId) {
        //   return formatMeasurement(measurement).then(function (formattedMeasure) {
        //     return $http.put(API_Base + 'measure/' + measurement.jobMeasureId + formattedMeasure).then(function (response) {
        //       return response.data;
        //     }, $q.reject);
        //   });
        // }
        if (measurement && measurement.measurementType) {
          return Job.useOldVisitOrStartNew(measurement.jobId, measurement.sampleSecs, measurement.visitId).then(function (visitId) {
            measurement.visitId = visitId;

            if (measurement.measurementType === 'HVAC' && measurement.chamberId > -1) {
              delete measurement.measurementType;
              return formatMeasurement(measurement).then(function (formattedMeasure) {
                return $http.post(API_Base + 'chamber/' + measurement.chamberId + '/HVAC' + formattedMeasure).then(function (response) {
                  Snackbar.show({template: "Changes saved", hide: 1500});
                  response.data = response.data || {};
                  response.data.visitId = visitId;
                  return response.data;
                }, $q.reject);
              });
            }
            if (measurement.measurementType === 'outside' || measurement.measurementType === 'unaffected' || measurement.measurementType === 'HVAC') {
              if (isNaN(measurement.areaId)) {
                return $q.reject({message: "Could not create measurement for unknown area.", status: 400});
              }
              return formatMeasurement(measurement).then(function (formattedMeasure) {
                return $http.post(API_Base.replace(/v1/g, 'v2') + 'area/' + measurement.areaId + '/measure' + formattedMeasure).then(function (response) {
                  Snackbar.show({template: "Changes saved", hide: 1500});
                  response.data = response.data || {};
                  response.data.visitId = visitId;
                  return response.data;
                }, $q.reject);
              });
            }
            if (measurement.measurementType === 'affected') {
              if (isNaN(measurement.chamberId)) {
                return $q.reject({message: "Could not create measurement for unknown equipment.", status: 400});
              }
              return formatMeasurement(measurement).then(function (formattedMeasure) {
                return $http.post(API_Base.replace(/v1/g, 'v2') + 'chamber/' + measurement.chamberId + '/affected' + formattedMeasure).then(function (response) {
                  Snackbar.show({template: "Changes saved", hide: 1500});
                  response.data = response.data || {};
                  response.data.visitId = visitId;
                  return response.data;
                }, $q.reject);
              });
            }
            if (measurement.measurementType === 'dehu_out' || measurement.measurementType === 'other_air') {
              if (isNaN(measurement.equipmentId)) {
                return $q.reject({message: "Could not create measurement for unknown equipment.", status: 400});
              }
              return formatMeasurement(measurement).then(function (formattedMeasure) {
                return $http.post(API_Base.replace(/v1/g, 'v2') + 'equipment/' + measurement.equipmentId + '/measure' + formattedMeasure).then(function (response) {
                  Snackbar.show({template: "Changes saved", hide: 1500});
                  response.data = response.data || {};
                  response.data.visitId = visitId;
                  return response.data;
                }, $q.reject);
              });
            }
            return $q.reject({message: "Could not create measurement with unknown type.", status: 400});
          }, $q.reject);
        }
        return $q.reject({message: "Could not create measurement with empty entry.", status: 400});
      },
      cleanJobOverview = function (thisJob) {
        var unseenAlertCount = 0,
          i;

        thisJob.mapId = "map-" + thisJob.jobId;
        if (thisJob.endDateSecs && thisJob.endDateSecs >= thisJob.startDateSecs) {
          thisJob.status = "COMPLETE";
          thisJob.endDate = (new Date(thisJob.endDateSecs * 1000)).toLocaleDateString();
        } else {
          thisJob.status = "ACTIVE";
          thisJob.endDate = "Current";
        }
        thisJob.startDate = (new Date(thisJob.startDateSecs * 1000)).toLocaleDateString();
        if (thisJob.city) {
          thisJob.address2 = thisJob.city;
        }
        if (thisJob.state) {
          if (thisJob.address2) {
            thisJob.address2 += ", ";
          } else {
            thisJob.address2 = "";
          }
          thisJob.address2 += thisJob.state;
        }
        if (thisJob.zip) {
          if (thisJob.address2) {
            thisJob.address2 += " " + thisJob.zip;
          } else {
            thisJob.address2 = thisJob.zip;
          }
        }
        if (!$localStorage.seenJobAlertIds || !$localStorage.seenJobAlertIds.length) {
          $localStorage.seenJobAlertIds = [];
        }
        if (thisJob.activeAlerts && thisJob.activeAlerts.length) {
          for (i = 0; i < thisJob.activeAlerts.length; i += 1) {
            if (thisJob.activeAlerts[i] && $localStorage.seenJobAlertIds.indexOf(thisJob.activeAlerts[i]) < 0) {
              unseenAlertCount += 1;
            }
          }
        }
        if (unseenAlertCount > 99) {
          unseenAlertCount = 99;
        } else if (unseenAlertCount < 0) {
          unseenAlertCount = 0;
        }
        thisJob.unseenAlertCount = unseenAlertCount;
      },
      determineEquipmentType = function (thisEquipment) {
        var aDevice;
        if (!thisEquipment) {
          return {};
        }

        if (thisEquipment.dehuId > -1) {
          aDevice = $rootScope.searchBluetoothListsForMatch('databaseId', thisEquipment.dehuId);
          if (aDevice && aDevice.modelType) {
            thisEquipment.modelType = aDevice.modelType;
          } else {
            thisEquipment.modelType = thisEquipment.model;
          }
        } else if (thisEquipment.model && thisEquipment.model.indexOf('Dry Max') > -1) {
          thisEquipment.modelType = 'Unknown';
          thisEquipment.typeName = 'Unknown';
        } else {
          thisEquipment.modelType = thisEquipment.model;
        }
        return Dehumidifier.determineIconCategory(thisEquipment);
      },
      markExtraneousEquipment = function (equipmentList) {
        var i, j;
        if (equipmentList && equipmentList.length) {//Multiple equipment id's can be generated for a single dehumidifier if the user removes and re-adds them to a job.
          for (i = 0; i < equipmentList.length; i += 1) {//Mark the older ones as extraneous so that they can be hidden on the job equipment page.
            if (equipmentList[i]) {
              equipmentList[i].isExtraneous = false;
              if (equipmentList[i].msensorId && equipmentList[i].msensorId > -1) {
                for (j = 0; j < equipmentList.length; j += 1) {//Merge together equipment lists from the atmospheric-based and moisture-based lists that the server provides.
                  if (i !== j && equipmentList[j] && equipmentList[j].msensorId === equipmentList[i].msensorId) {
                    if (!(equipmentList[j].equipmentId > 0) && equipmentList[j].equipmentId > 0 && equipmentList[j].equipmentName) {
                      equipmentList[i].equipmentName = equipmentList[j].equipmentName;
                    }
                    if (!equipmentList[j].isRemoved && equipmentList[j].mequipId > 0 && !(equipmentList[i].mequipId > 0)) {
                      equipmentList[i].mequipId = equipmentList[j].mequipId;
                    }
                    if ((equipmentList[i].isRemoved && (!equipmentList[j].isRemoved || (equipmentList[j].equipmentId > 0 && !(equipmentList[i].equipmentId > 0) && !equipmentList[j].isExtraneous) ||
                         equipmentList[j].jobDayEnd > equipmentList[i].jobDayEnd || (equipmentList[j].jobDayEnd === equipmentList[i].jobDayEnd && !equipmentList[j].isExtraneous))) ||
                        (!equipmentList[i].isRemoved && !equipmentList[j].isRemoved && equipmentList[j].equipmentId > 0 && (!(equipmentList[i].equipmentId > 0) || !equipmentList[j].isExtraneous))) {
                      equipmentList[i].isExtraneous = true;
                      break;
                    }
                  }
                }
              } else if (equipmentList[i].isRemoved && (equipmentList[i].dehuId || (equipmentList[i].equipmentData && equipmentList[i].equipmentData.dehuId))) {
                for (j = 0; j < equipmentList.length; j += 1) {
                  if (i !== j && equipmentList[j] && ((equipmentList[j].equipmentData && equipmentList[i].equipmentData.dehuId === equipmentList[j].equipmentData.dehuId &&
                      (equipmentList[i].equipmentData.dehuId >= 0  || (equipmentList[i].equipmentData.equipmentType === equipmentList[j].equipmentData.equipmentType &&
                        equipmentList[i].equipmentData.equipmentName === equipmentList[j].equipmentData.equipmentName)) &&
                      (!equipmentList[j].isRemoved || equipmentList[j].equipmentData.jobDayEnd > equipmentList[i].equipmentData.jobDayEnd ||
                       (equipmentList[j].equipmentData.jobDayEnd === equipmentList[i].equipmentData.jobDayEnd && !equipmentList[j].isExtraneous))) ||
                      (equipmentList[j].dehuId && equipmentList[j].dehuId === equipmentList[i].dehuId && (equipmentList[i].dehuId >= 0 ||
                       (equipmentList[i].cequipId >= 0 && equipmentList[i].cequipId === equipmentList[j].cequipId) || ((!equipmentList[i].cequipId || equipmentList[i].cequipId < 0) &&
                          (equipmentList[i].modelType === equipmentList[j].modelType && equipmentList[i].equipmentName === equipmentList[j].equipmentName))) &&
                      (!equipmentList[j].isRemoved || equipmentList[j].jobDayEnd > equipmentList[i].jobDayEnd ||
                          (equipmentList[j].jobDayEnd === equipmentList[i].jobDayEnd && !equipmentList[j].isExtraneous))))) {
                    equipmentList[i].isExtraneous = true;
                    break;
                  }
                }
              }
            }
          }
        }
      };

    Job.convertArea = function (area, metricToImperial) {
      if (!area || isNaN(area)) {
        return area;
      }
      if (area < 0) {
        return 0;
      }
      if (!$localStorage.useMeters) {
        return Math.round(area);
      }
      if (!metricToImperial) {
        return Math.round(area * 0.092903);
      }
      return Math.round(area / 0.092903);
    };

    Job.convertVolume = function (volume, metricToImperial) {
      if (!volume || isNaN(volume)) {
        return volume;
      }
      if (volume < 0) {
        return 0;
      }
      if (!$localStorage.useMeters) {
        return Math.round(volume);
      }
      if (!metricToImperial) {
        return Math.round(volume * 0.0283168);
      }
      return Math.round(volume / 0.0283168);
    };

    Job.updateProgressBar = function (anEntry) {//Update the progress bar associated with moisture readings on the drying log page.
      if (!anEntry) {
        return {};
      }

      if (anEntry.initialMoisture > anEntry.target) {
        anEntry.percentDone = 100 * (anEntry.initialMoisture - anEntry.moistureValue) / (anEntry.initialMoisture - anEntry.target);
      } else if (anEntry.moistureValue > anEntry.target) {
        anEntry.percentDone = 5;
      } else {
        anEntry.percentDone = 100;
      }
      if (anEntry.percentDone < 5) {
        anEntry.percentDone = 5;
      } else if (anEntry.percentDone > 100) {
        anEntry.percentDone = 100;
      }
      anEntry.isGreen = false;
      anEntry.isYellow = false;
      anEntry.isOrange = false;
      anEntry.isRed = false;
      if (anEntry.percentDone >= 100) {
        anEntry.isGreen = true;
      } else if (anEntry.percentDone >= 66) {
        anEntry.isYellow = true;
      } else if (anEntry.percentDone >= 33) {
        anEntry.isOrange = true;
      } else {
        anEntry.isRed = true;
      }
      return anEntry;
    };

    Job.updateOtherTargets = function (newTarget, chamberId, material, location) {
      //All moisture points with the same material (and location?) in a given chamber will have the same moisture target.
      //If one get updated, then they all need to be updated.
      var i;

      if (isNaN(newTarget) || isNaN(chamberId) || !material || !location) {
        return;
      }
      if ($rootScope.latestMoisturePoints && $rootScope.latestMoisturePoints.length) {
        for (i = 0; i < $rootScope.latestMoisturePoints.length; i += 1) {
          if ($rootScope.latestMoisturePoints[i] && $rootScope.latestMoisturePoints[i].chamberId === chamberId &&
              $rootScope.latestMoisturePoints[i].material === material) {// && $rootScope.latestMoisturePoints[i].location === location) {
            $rootScope.latestMoisturePoints[i].target = newTarget;
          }
        }
      }
      if ($rootScope.latestLogEntry && $rootScope.latestLogEntry.moistureMeasures && $rootScope.latestLogEntry.moistureMeasures.length) {
        for (i = 0; i < $rootScope.latestLogEntry.moistureMeasures.length; i += 1) {
          if ($rootScope.latestLogEntry.moistureMeasures[i] && $rootScope.latestLogEntry.moistureMeasures[i].chamberId === chamberId &&
              $rootScope.latestLogEntry.moistureMeasures[i].material === material) {// && $rootScope.latestLogEntry.moistureMeasures[i].location === location) {
            $rootScope.latestLogEntry.moistureMeasures[i].target = newTarget;
          }
        }
      }
    };

    Job.minimumDateSecs = july2017Cutoff;//Only dates after July 2017 will be allowed for jobs and drying log entries.

    Job.getJob2 = function (jobId) {
      if (!jobId) {
        return $q.reject({message: "No job ID found.", status: 400});
      }

      // Job.getJob(jobId).then(function (response) {
      //   console.log("here's the original", response);
      // });
      return $http.get(API_Base.replace(/v1/g, 'v2') + 'job/' + jobId).then(function (response) {
        var i, j, graphable,
          thisEquipment,
          allData,
          anEntry,
          entryTime,
          chamberLookup = {},
          dehuLookup = {},
          equipmentLookup = {},
          areaLookup = {},
          unaffectedList = [],
          moistureLookup = {},
          aChamber,
          abbreviatedDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

        allData = (response && response.data) || {};
        if (allData.jobBasics) {
          cleanJobOverview(allData.jobBasics);
        }
        if (allData.chambers) {
          for (i = 0; i < allData.chambers.length; i += 1) {
            if (allData.chambers[i]) {
              allData.chambers[i].equipment = [];
              if (allData.chambers[i].chamberId) {
                chamberLookup[allData.chambers[i].chamberId] = i;
              }
            }
          }
        }
        if (allData.areas) {
          for (i = 0; i < allData.areas.length; i += 1) {
            if (allData.areas[i] && allData.areas[i].areaId) {
              allData.areas[i].equipment = [];
              areaLookup[allData.areas[i].areaId] = i;
              if (allData.areas[i].areaType === "unaffected") {
                unaffectedList.push(allData.areas[i]);
              }
            }
          }
        }
        if (allData.mequipment) {
          if (!allData.equipment) {
            allData.equipment = [];
          }
          for (i = 0; i < allData.mequipment.length; i += 1) {
            if (allData.mequipment[i] && allData.mequipment[i].mequipId) {
              moistureLookup[allData.mequipment[i].mequipId] = i;
            }
            if (allData.mequipment[i] && allData.mequipment[i].msensorId && allData.mequipment[i].msensorId > -1) {
              allData.mequipment[i].model = "DrySENSE";
              allData.mequipment[i].typeName = "Penetrating Meter";
              if (!allData.mequipment[i].equipmentName) {
                allData.mequipment[i].equipmentName = allData.mequipment[i].sensorName || allData.mequipment[i].label;
              }
              allData.equipment.push(allData.mequipment[i]);
            } else if (allData.mequipment[i]) {
              allData.mequipment[i].isRemoved = (allData.mequipment[i].jobDayEnd > 0);
              allData.mequipment[i].sensorName = undefined;
            }
          }
        }

        allData.equipmentList = allData.equipment;
        delete allData.equipment;
        if (allData.equipmentList) {
          for (i = 0; i < allData.equipmentList.length; i += 1) {
            thisEquipment = allData.equipmentList[i] || {};
            if (thisEquipment.equipmentId) {
              equipmentLookup[thisEquipment.equipmentId] = i;
            }
            if (thisEquipment.jobDayEnd > 0) {
              thisEquipment.dayCompleted = (new Date(allData.jobBasics.startDateSecs * 1000 +
                (thisEquipment.jobDayEnd - 1) * 24 * 60 * 60 * 1000)).toLocaleDateString();
              thisEquipment.isRemoved = true;
            } else {
              thisEquipment.isRemoved = false;
            }
            thisEquipment.hasBeacon = thisEquipment.beaconId > -1;
            determineEquipmentType(thisEquipment);

            if (thisEquipment.chamberId === -1 && (thisEquipment.modelType === 'DrySENSE' ||
                (thisEquipment.iconCategory === 'Hygrometer' && thisEquipment.hasBeaconRH))) {
              for (j = 0; j < unaffectedList.length; j += 1) {
                if (unaffectedList[j]) {
                  thisEquipment.chamberName = unaffectedList[j].areaName;
                  thisEquipment.areaId = unaffectedList[j].areaId;
                  unaffectedList[j].equipment.push(thisEquipment);
                }
              }
            } else if (thisEquipment.chamberId && chamberLookup.hasOwnProperty(thisEquipment.chamberId)) {
              aChamber = allData.chambers[chamberLookup[thisEquipment.chamberId]] || {};
              thisEquipment.chamberClass = 'Category ' + aChamber.category +  ' Class ' + aChamber.classification;
              thisEquipment.chamberName = aChamber.chamberName;
              aChamber.equipment.push(thisEquipment);
            }
          }
        }
        markExtraneousEquipment(allData.equipmentList);
        if (allData.equipmentList) {
          for (i = 0; i < allData.equipmentList.length; i += 1) {
            if (allData.equipmentList[i] && allData.equipmentList[i].dehuId && !allData.equipmentList[i].isExtraneous) {
              dehuLookup[allData.equipmentList[i].dehuId] = i;
            }
          }
        }
        if (allData.chambers) {
          for (i = 0; i < allData.chambers.length; i += 1) {
            if (allData.chambers[i]) {
              graphable = false;
              if (allData.chambers[i].equipment && allData.chambers[i].equipment.length) {
                for (j = 0; j < allData.chambers[i].equipment.length; j += 1) {
                  if (allData.chambers[i].equipment[j] && !isNaN(allData.chambers[i].equipment[j].dehuId) && allData.chambers[i].equipment[j].dehuId > -1 &&
                      !isNaN(allData.chambers[i].equipment[j].equipmentId) && allData.chambers[i].equipment[j].equipmentId > -1) {
                    graphable = true;
                    break;
                  }
                }
              }
              allData.chambers[i].graphable = graphable;
            }
          }
        }

        if (allData.visits) {
          for (i = 0; i < allData.visits.length; i += 1) {
            if (allData.visits[i]) {
              entryTime = new Date(allData.visits[i].startSecs * 1000);
              allData.visits[i].startTime = getTimeString(allData.visits[i].startSecs);
              allData.visits[i].endTime = getTimeString(allData.visits[i].endSecs);
              allData.visits[i].date = abbreviatedDays[entryTime.getDay()] + ' ' + entryTime.toLocaleDateString();

              if (allData.visits[i].airMeasures && allData.visits[i].airMeasures.length) {
                for (j = 0; j < allData.visits[i].airMeasures.length; j += 1) {
                  anEntry = allData.visits[i].airMeasures[j] || {};
                  if (anEntry.dehuId && anEntry.dehuId > -1 && dehuLookup.hasOwnProperty(anEntry.dehuId)) {
                    thisEquipment = allData.equipmentList[dehuLookup[anEntry.dehuId]] || {};
                    anEntry.name = thisEquipment.equipmentName;
                    anEntry.location = thisEquipment.location;
                  } else if (anEntry.equipmentId && anEntry.equipmentId > -1 && equipmentLookup.hasOwnProperty(anEntry.equipmentId)) {
                    thisEquipment = allData.equipmentList[equipmentLookup[anEntry.equipmentId]] || {};
                    anEntry.name = thisEquipment.equipmentName;
                    anEntry.location = thisEquipment.location;
                    anEntry.hasBeaconRH = thisEquipment.hasBeaconRH;
                    anEntry.msensorId = thisEquipment.msensorId;
                  } else if (anEntry.measurementType === 'outside' && anEntry.areaId && areaLookup.hasOwnProperty(anEntry.areaId)) {
                    thisEquipment = allData.areas[areaLookup[anEntry.areaId]] || {};
                    anEntry.name = thisEquipment.areaName;
                  }

                  if (!anEntry.sampleSecs || anEntry.sampleSecs < Job.minimumDateSecs) {
                    allData.visits[i].airMeasures.splice(j, 1);
                    j -= 1;
                  } else {
                    entryTime = new Date(anEntry.sampleSecs * 1000);
                    anEntry.time = entryTime.toLocaleTimeString();
                    anEntry.graphable = (anEntry.measurementType === 'affected') || (anEntry.measurementType === 'dehu_out' && anEntry.dehuId && anEntry.dehuId > -1);
                    if (anEntry.measurementType === "dehu_in") {
                      anEntry.measurementType = "affected";
                    } else if (anEntry.measurementType === 'heater' || anEntry.measurementType === 'airconditioner') {
                      anEntry.measurementType = 'other_air';
                    } else if (anEntry.measurementType === "dehu" || anEntry.measurementType === "retrodehu") {
                      anEntry.measurementType = "dehu_out";
                    }
                  }
                }
              }
              if (allData.visits[i].moistureMeasures && allData.visits[i].moistureMeasures.length) {
                for (j = 0; j <  allData.visits[i].moistureMeasures.length; j += 1) {
                  anEntry = allData.visits[i].moistureMeasures[j] || {};
                  if (anEntry.mequipId && moistureLookup.hasOwnProperty(anEntry.mequipId)) {
                    thisEquipment = allData.mequipment[moistureLookup[anEntry.mequipId]] || {};
                    anEntry.location = thisEquipment.location;
                    anEntry.material = thisEquipment.material;
                    anEntry.msensorId = thisEquipment.msensorId;
                    anEntry.sensorName = thisEquipment.sensorName;

                    if (thisEquipment.initialMoisture) {
                      anEntry.initialMoisture = thisEquipment.initialMoisture;
                    } else {
                      anEntry.initialMoisture = anEntry.moistureValue;
                      thisEquipment.initialMoisture = anEntry.moistureValue;
                    }
                    Job.updateProgressBar(anEntry);
                  }
                }
              }
            }
          }
        }

        return allData;
      }, $q.reject);
    };

    Job.getJob = function (jobId) {
      if (!jobId) {
        return $q.reject({message: "No job ID found.", status: 400});
      }
      return $http.get(API_Base + 'job/' + jobId).then(function (response) {
        var i, j, k, isRemoved, graphable, aDevice,
          defaultDate = Job.minimumDateSecs,
          abbreviatedDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
          addEntry = function (entryList, entry) {
            var entryTime;

            if (!entryList || !entry || isNaN(entry.jobDay) || !entry.sampleSecs || entry.sampleSecs < Job.minimumDateSecs) {
              return;
            }
            entryTime = new Date(entry.sampleSecs * 1000);
            entry.time = entryTime.toLocaleTimeString();
            entry.graphable = (entry.measurementType === 'affected') || (entry.measurementType === 'dehu_out' && entry.dehuId && entry.dehuId > -1);
            if (!entryList[entry.jobDay]) {
              entryList[entry.jobDay] = {day: "Day " + entry.jobDay, date: entryTime.toLocaleDateString(), sampleSecs: entry.sampleSecs,
                time: abbreviatedDays[entryTime.getDay()] + ' ' + entryTime.toLocaleTimeString(), jobId: jobId, measures: [], notes: []};
            }
            entryList[entry.jobDay].measures.push(entry);
          },
          addNote = function (entryList, note) {
            var entryTime;

            if (!entryList || !note) {
              return;
            }
            if (note.sampleSecs && note.sampleSecs > defaultDate) {
              note.jobDay = Math.round((note.sampleSecs - defaultDate) / (24 * 60 * 60)) + 1;
            } else {
              note.sampleSecs = defaultDate + 24 * 60 * 60 * (note.jobDay - 1);
            }
            if (!(note.jobDay > 0)) {
              note.jobDay = 1;
            }
            entryTime = new Date(note.sampleSecs * 1000);
            note.time = entryTime.toLocaleTimeString();
            if (!entryList[note.jobDay]) {
              entryList[note.jobDay] = {day: "Day " + note.jobDay, date: entryTime.toLocaleDateString(), sampleSecs: note.sampleSecs,
                time: abbreviatedDays[entryTime.getDay()] + ' ' + entryTime.toLocaleTimeString(), jobId: jobId, measures: [], notes: []};
            }
            entryList[note.jobDay].notes.push(note);
          };

        if (response && response.data && response.data.jobBasics) {
          cleanJobOverview(response.data.jobBasics);
          if (response.data.jobBasics.startDateSecs) {
            defaultDate = response.data.jobBasics.startDateSecs;
          }
        }
        response.data.equipmentList = [];
        response.data.entryList = {};
        if (response && response.data && response.data.areas && response.data.areas.length) {//Drying log entries and job equipment come from the server in fragmented parts.
          for (i = 0; i < response.data.areas.length; i += 1) {//They need to be found, sorted, and merged into usable form after each Get /job/jobId request.
            if (response.data.areas[i].measures && response.data.areas[i].measures.length) {
              for (j = 0; j < response.data.areas[i].measures.length; j += 1) {
                response.data.areas[i].measures[j].areaName = response.data.areas[i].areaName;
                addEntry(response.data.entryList, response.data.areas[i].measures[j]);
              }
            }
          }
        }
        if (response && response.data && response.data.chambers && response.data.chambers.length) {
          for (i = 0; i < response.data.chambers.length; i += 1) {
            graphable = false;
            if (response.data.chambers[i].equipment && response.data.chambers[i].equipment.length) {
              for (j = 0; j < response.data.chambers[i].equipment.length; j += 1) {
                if (response.data.chambers[i].equipment[j] && !isNaN(response.data.chambers[i].equipment[j].dehuId) && response.data.chambers[i].equipment[j].dehuId > -1 &&
                    !isNaN(response.data.chambers[i].equipment[j].equipmentId) && response.data.chambers[i].equipment[j].equipmentId > -1) {
                  graphable = true;
                }

                isRemoved = false;
                if (response.data.chambers[i].equipment[j]  && response.data.chambers[i].equipment[j].jobDayEnd > 0) {
                  response.data.chambers[i].equipment[j].dayCompleted = (new Date(response.data.jobBasics.startDateSecs * 1000 +
                    (response.data.chambers[i].equipment[j].jobDayEnd - 1) * 24 * 60 * 60 * 1000)).toLocaleDateString();
                  isRemoved = true;
                }
                response.data.chambers[i].equipment[j].isRemoved = isRemoved;
                if (response.data.chambers[i].equipment[j].dehuId > -1) {
                  aDevice = $rootScope.searchBluetoothListsForMatch('databaseId', response.data.chambers[i].equipment[j].dehuId);
                  if (aDevice && aDevice.modelType) {
                    response.data.chambers[i].equipment[j].modelType = aDevice.modelType;
                  } else {
                    if (response.data.chambers[i].equipment[j].exSize === 'XL') {
                      response.data.chambers[i].equipment[j].modelType = "Dry Max XL";
                    } else if (response.data.chambers[i].equipment[j].exSize === 'Large') {
                      response.data.chambers[i].equipment[j].modelType = "Dry Max";
                    }
                  }
                }
                response.data.equipmentList.push({isRemoved: isRemoved, chamberId: response.data.chambers[i].chamberId,
                  chamberName: response.data.chambers[i].chamberName, equipmentData: response.data.chambers[i].equipment[j],
                  chamberClass: 'Category ' + response.data.chambers[i].category +  ' Class ' + response.data.chambers[i].classification});

                if (response.data.chambers[i].equipment[j].measures && response.data.chambers[i].equipment[j].measures.length) {
                  for (k = 0; k < response.data.chambers[i].equipment[j].measures.length; k += 1) {
                    response.data.chambers[i].equipment[j].measures[k].chamberName = response.data.chambers[i].chamberName;
                    response.data.chambers[i].equipment[j].measures[k].chamberId = response.data.chambers[i].chamberId;
                    response.data.chambers[i].equipment[j].measures[k].location = response.data.chambers[i].equipment[j].location;
                    response.data.chambers[i].equipment[j].measures[k].equipmentName = response.data.chambers[i].equipment[j].equipmentName;
                    response.data.chambers[i].equipment[j].measures[k].areaName = response.data.chambers[i].equipment[j].equipmentName;
                    response.data.chambers[i].equipment[j].measures[k].dehuId = response.data.chambers[i].equipment[j].dehuId;
                    if (response.data.chambers[i].equipment[j].measures[k].measurementType === "dehu_in") {
                      response.data.chambers[i].equipment[j].measures[k].measurementType = "affected";
                    }
                    addEntry(response.data.entryList, response.data.chambers[i].equipment[j].measures[k]);
                  }
                }
              }
            }
            response.data.chambers[i].graphable = graphable;

            if (response.data.chambers[i].affectedArea && response.data.chambers[i].affectedArea.measures && response.data.chambers[i].affectedArea.measures.length) {
              for (k = 0; k < response.data.chambers[i].affectedArea.measures.length; k += 1) {
                response.data.chambers[i].affectedArea.measures[k].chamberName = response.data.chambers[i].chamberName;
                response.data.chambers[i].affectedArea.measures[k].chamberId = response.data.chambers[i].chamberId;
                response.data.chambers[i].affectedArea.measures[k].areaName = response.data.chambers[i].affectedArea.areaName;
                if (response.data.chambers[i].affectedArea.measures[k].equipmentId > -1 && response.data.chambers[i].equipment && response.data.chambers[i].equipment.length) {
                  for (j = 0; j < response.data.chambers[i].equipment.length; j += 1) {
                    if (response.data.chambers[i].equipment[j] && response.data.chambers[i].equipment[j].equipmentId === response.data.chambers[i].affectedArea.measures[k].equipmentId) {
                      response.data.chambers[i].affectedArea.measures[k].areaName = response.data.chambers[i].equipment[j].equipmentName;//If an inlet measurement comes from a Bluetooth-enabled dehumidifer,
                      response.data.chambers[i].affectedArea.measures[k].location = response.data.chambers[i].equipment[j].location;//Change the name of the affected area measurement to match its source.
                    }
                  }
                }
                addEntry(response.data.entryList, response.data.chambers[i].affectedArea.measures[k]);
              }
            }
          }
        }

        if (response && response.data && response.data.notes && response.data.notes.length) {
          for (i = 0; i < response.data.notes.length; i += 1) {
            addNote(response.data.entryList, response.data.notes[i]);
          }
        }

        markExtraneousEquipment(response.data.equipmentList);

        return response.data;
      }, $q.reject);
    };

    Job.create = function (jobBasics, chambers, contacts, notes) {
      var i, j, l,
        moreAPICalls = [],
        newJob = {},
        existingChamberIndex = 0,
        onlyVisitId,
        shouldAddWeather = false,
        currentTime,
        waitThenCreateChamber = function (k, m) {
          return moreAPICalls[m].promise.then(function () {
            if (!newJob.chambers || !newJob.chambers.length || newJob.chambers.length <= existingChamberIndex) {
              return Job.createChamber(newJob.jobBasics && newJob.jobBasics.jobId, chambers[k]).then(function (newChamber) {
                if (newChamber && newChamber.chamberId) {
                  chambers[k].chamberId = newChamber.chamberId;
                }
                return $q.resolve();
              }, $q.reject);
            }
            return Job.updateChamber(newJob.chambers[k].chamberId, chambers[k]).then(function () {
              chambers[k].chamberId = newJob.chambers[k].chamberId;
              existingChamberIndex += 1;
              return $q.resolve();
            }, $q.reject);
          }, $q.reject);
        },
        waitThenCreateContact = function (k, m) {
          return moreAPICalls[m].promise.then(function () {
            if (contacts[k] && (contacts[k].phone || contacts[k].contactName) && newJob.jobBasics && newJob.jobBasics.jobId) {
              return Job.createContact(newJob.jobBasics.jobId, contacts[k]);
            }
            return $q.resolve();
          }, $q.reject);
        },
        waitThenAddWeather = function (m) {
          return moreAPICalls[m].promise.then(function () {
            if (onlyVisitId && onlyVisitId > -1 && newJob && newJob.jobBasics && newJob.jobBasics.jobId && newJob.areas && newJob.areas.length) {
              for (i = 0; i < newJob.areas.length; i += 1) {
                if (newJob.areas[i] && newJob.areas[i].areaType === "outside" && newJob.areas[i].areaId && newJob.areas[i].areaId > -1) {
                  return Job.addCurrentWeatherReading({jobId: newJob.jobBasics.jobId, visitId: onlyVisitId, sampleSecs: currentTime,
                    areaId: newJob.areas[i].areaId}).then($q.resolve, $q.resolve);
                }
              }
            }
            return $q.resolve();
          }, $q.reject);
        },
        waitThenCreateChamberNote = function (k, m, n) {
          return moreAPICalls[m].promise.then(function () {
            if (chambers[n].notes[k] && chambers[n].notes[k].jobNote && chambers[n].chamberId && newJob && newJob.jobBasics && newJob.jobBasics.jobId) {
              chambers[n].notes[k].jobId = newJob.jobBasics.jobId;
              chambers[n].notes[k].sampleSecs = newJob.jobBasics.startDateSecs;
              chambers[n].notes[k].chamberId = chambers[n].chamberId;
              chambers[n].notes[k].visitId = onlyVisitId;
              return Job.createNote(chambers[n].notes[k], true).then(function (newNote) {
                if (newNote && newNote.visitId) {
                  onlyVisitId = newNote.visitId;
                }
                return $q.resolve();
              }, $q.reject);
            }
            return $q.resolve();
          }, $q.reject);
        },
        waitThenCreateNote = function (k, m) {
          return moreAPICalls[m].promise.then(function () {
            if (notes[k] && notes[k].jobNote && newJob && newJob.jobBasics && newJob.jobBasics.jobId) {
              notes[k].jobId = newJob.jobBasics.jobId;
              notes[k].sampleSecs = newJob.jobBasics.startDateSecs;
              notes[k].visitId = onlyVisitId;
              return Job.createNote(notes[k], true).then(function (newNote) {
                if (newNote && newNote.visitId) {
                  onlyVisitId = newNote.visitId;
                }
                return $q.resolve();
              }, $q.reject);
            }
            return $q.resolve();
          }, $q.reject);
        };

      $rootScope.newJobIntermediateDeletionId = undefined;
      moreAPICalls.push({promise: formatJobBasics(jobBasics).then(function (formattedJob) {
        return $http.post(API_Base + 'job', formattedJob).then(function (response) {
          newJob = response.data;
          if (newJob && newJob.jobBasics && newJob.jobBasics.jobId) {
            $rootScope.newJobIntermediateDeletionId = newJob.jobBasics.jobId;
          }
          return response.data;
        }, function (err) {
          if (err && err.data) {
            err.errors = err.data.errors ? err.data.errors.join("<br>") : err.data.message || err.data;
          }
          return $q.reject(err);
        });
      }, $q.reject)});

      j = 0;
      if (chambers && chambers.length) {
        for (i = 0; i < chambers.length; i += 1, j += 1) {//Add all chambers, one-at-a-time.
          moreAPICalls.push({promise: waitThenCreateChamber(i, j)});

          if (chambers[i].notes && chambers[i].notes.length) {
            for (l = 0; l < chambers[i].notes.length; l += 1) {
              j += 1;
              moreAPICalls.push({promise: waitThenCreateChamberNote(l, j, i)});
              shouldAddWeather = true;
            }
          }
        }
      }
      if (contacts && contacts.length) {
        for (i = 0; i < contacts.length; i += 1, j += 1) {//Add all contacts, one-at-a-time.
          moreAPICalls.push({promise: waitThenCreateContact(i, j)});
        }
      }
      if (notes && notes.length) {
        for (i = 0; i < notes.length; i += 1, j += 1) {//Add all notes, one-at-a-time.
          moreAPICalls.push({promise: waitThenCreateNote(i, j)});
          shouldAddWeather = true;
        }
      }
      currentTime = Math.round((new Date()).getTime() / 1000);
      if (shouldAddWeather && jobBasics && jobBasics.startDateSecs && (currentTime - 3600 < jobBasics.startDateSecs) && (currentTime + 3600 > jobBasics.startDateSecs)) {
        moreAPICalls.push({promise: waitThenAddWeather(j)});
        j += 1;
      }

      return (moreAPICalls[moreAPICalls.length - 1]).promise.then(function () {
        $rootScope.newJobIntermediateDeletionId = undefined;
        Snackbar.show({template: "Job created", hide: 5000});
        return $q.resolve(newJob && newJob.jobBasics);
      }, $q.reject);
    };

    Job.update = function (jobBasics) {
      if (!jobBasics.jobId) {
        return $q.reject({title: "Error updating job", template: "Job ID is a required field"});
      }
      return formatJobBasics(jobBasics).then(function (formattedJob) {
        return $http.put(API_Base + 'job/' + jobBasics.jobId, formattedJob).then(function (response) {
          return response.data;
        }, function (err) {
          if (err && err.data) {
            err.errors = err.data.errors ? err.data.errors.join("<br>") : err.data.message || err.data;
          }
          return $q.reject(err);
        });
      }, $q.reject);
    };

    Job.delete = function (jobId) {
      if (!jobId) {
        return $q.reject({message: "No job ID found.", status: 400});
      }
      return $http.delete(API_Base.replace(/v1/g, 'v2') + 'job/' + jobId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.getList = function (current) {
      var extra = current ? '/current' : '';
      return $http.get(API_Base + 'job' + extra).then(function (response) {
        var i,
          totalUnseenAlertCount = 0;

        if (response && response.data && response.data.length) {
          for (i = 0; i < response.data.length; i += 1) {
            cleanJobOverview(response.data[i]);
            if (response.data[i] && response.data[i].unseenAlertCount) {
              totalUnseenAlertCount += response.data[i].unseenAlertCount;
            }
          }
        }
        $rootScope.unseenJobAlertCount = totalUnseenAlertCount;
        if (!$rootScope.unseenEquipmentAlertCount) {
          $rootScope.unseenEquipmentAlertCount = 0;
        }
        totalUnseenAlertCount = $rootScope.unseenEquipmentAlertCount + $rootScope.unseenJobAlertCount;
        if (totalUnseenAlertCount > 99) {
          totalUnseenAlertCount = 99;
        } else if (totalUnseenAlertCount < 0) {
          totalUnseenAlertCount = 0;
        }
        $rootScope.unseenTotalAlertCount = totalUnseenAlertCount;

        return response.data;
      }, $q.reject);
    };

    Job.assignEquipment2 = function (jobDetails) {
      var i,
        device,
        equipmentList = [],
        deviceList = $rootScope.deviceList,
        bleDehu,
        finishEquipment = function (k) {
          equipmentList[k].chamberAssignment = undefined;
          equipmentList[k].assignToJob = false;
          if (jobDetails) {
            equipmentList[k].jobId = jobDetails.jobId;
            equipmentList[k].jobName = jobDetails.jobName;

            if (deviceList) {
              if (deviceList.newer && deviceList.newer.length) {
                for (i = 0; i < deviceList.newer.length; i += 1) {
                  bleDehu = deviceList.newer[i] || {};
                  if (equipmentList[k].databaseId > -1 && bleDehu.serialNumber && bleDehu.serialNumber === equipmentList[k].serialNumber) {
                    bleDehu.jobName = jobDetails.jobName;
                  } else if (equipmentList[k].msensorId > -1 && ((bleDehu.serialNumber && bleDehu.serialNumber === equipmentList[k].uniqueEnoughID) ||
                             (bleDehu.msensorId === equipmentList[k].msensorId))) {
                    bleDehu.jobName = jobDetails.jobName;
                  }
                  if (equipmentList[k].beaconId > -1 && bleDehu.beaconId === equipmentList[k].beaconId) {
                    bleDehu.jobName = jobDetails.jobName;
                  }
                }
              }
              if (deviceList.older && deviceList.older.length) {
                for (i = 0; i < deviceList.older.length; i += 1) {
                  bleDehu = deviceList.older[i] || {};
                  if (equipmentList[k].databaseId > -1 && bleDehu.serialNumber && bleDehu.serialNumber === equipmentList[k].serialNumber) {
                    bleDehu.jobName = jobDetails.jobName;
                  } else if (equipmentList[k].msensorId > -1 && ((bleDehu.serialNumber && bleDehu.serialNumber === equipmentList[k].uniqueEnoughID) ||
                             (bleDehu.msensorId === equipmentList[k].msensorId))) {
                    bleDehu.jobName = jobDetails.jobName;
                  }
                  if (equipmentList[k].beaconId > -1 && bleDehu.beaconId === equipmentList[k].beaconId) {
                    bleDehu.jobName = jobDetails.jobName;
                  }
                }
              }
            }
          }
        },
        createOneEquipment = function (k) {
          if (!(equipmentList[k].chamberId > -1)) {
            return Job.createUnaffectedEquipment(jobDetails.jobId, equipmentList[k].msensorId > 0 && equipmentList[k].msensorId,
                equipmentList[k].iconCategory === 'Hygrometer' && equipmentList[k].hasBeaconRH && equipmentList[k].cequipId).then(function (newData) {
              finishEquipment(k);
              return $q.resolve(newData);
            }, $q.reject);
          }
          return Job.createEquipment2(equipmentList[k].chamberId, equipmentList[k]).then(function (newData) {
            finishEquipment(k);
            return $q.resolve(newData);
          }, $q.reject);
        },
        waitThenCreateEquipment = function (k) {
          return equipmentList[k + 1].promise.then(function () {
            return createOneEquipment(k);
          }, function (error) {
            return createOneEquipment(k).then(function () {
              return $q.reject(error);
            }, $q.reject);
          });
        };

      if (!$rootScope.choiceList || !$rootScope.choiceList.length) {
        return;
      }
      for (i = 0; i < $rootScope.choiceList.length; i += 1) {
        device = $rootScope.choiceList[i] || {};
        if (device.assignToJob && device.chamberAssignment) {
          if (device.chamberAssignment.chamberId > 0) {
            device.chamberId = device.chamberAssignment.chamberId;
            equipmentList.push(device);
          } else if (device.chamberAssignment.areaId > 0) {
            device.areaId = device.chamberAssignment.areaId;
            equipmentList.push(device);
          }
        }
      }
      if (!equipmentList.length) {
        return;
      }
      equipmentList.push({promise: $q.resolve()});
      for (i = equipmentList.length - 2; i >= 0; i -= 1) {//Add all equipment to their respective chambers, one-at-a-time.
        equipmentList[i].promise = waitThenCreateEquipment(i);
      }

      return equipmentList[0].promise.then(function (newData) {
        Snackbar.show({template: "Equipment assigned to job", hide: 5000});

        return $q.resolve(newData);
      }, $q.reject);
    };

    Job.assignEquipment = function (job, defaultChamber, jobToUpdate) {
      var i,
        device,
        equipmentList = [],
        cleanListOfAssignments = function () {
          var j;
          for (j = 0; j < $rootScope.deviceList.newer.length; j += 1) {
            $rootScope.deviceList.newer[j].assignToJob = undefined;
            $rootScope.deviceList.newer[j].chamberAssignment = undefined;
          }
        },
        createOneEquipment = function (k) {
          return Job.createEquipment(equipmentList[k].chamberId, {equipmentType: 'dehu', exSize: equipmentList[k].modelType === 'Dry Max' ? 'Large' : 'XL',
              dehuId: equipmentList[k].dehuId, equipmentName: equipmentList[k].equipmentName}).then(function (newData) {
            var m;
            equipmentList[k].devicePointer.jobName = job.jobName;

            if (jobToUpdate) {
              newData = newData || {};
              newData.isRemoved = false;
              determineEquipmentType(newData);
              newData.chamberId = equipmentList[k].chamberId;
              newData.chamberName = equipmentList[k].chamberName;

              if (jobToUpdate.equipmentList) {
                jobToUpdate.equipmentList.push(newData);
              }
              if (jobToUpdate.chambers) {
                for (m = 0; m < jobToUpdate.chambers.length; m += 1) {
                  if (jobToUpdate.chambers[m].equipment && jobToUpdate.chambers[m].chamberId === equipmentList[k].chamberId) {
                    jobToUpdate.chambers[m].equipment.push(newData);
                    jobToUpdate.chambers[m].graphable = true;
                    break;
                  }
                }
              }
            }
            return $q.resolve();
          }, $q.reject);
        },
        waitThenCreateEquipment = function (k) {
          return equipmentList[k + 1].promise.then(function () {
            return createOneEquipment(k);
          }, function (error) {
            return createOneEquipment(k).then(function () {
              return $q.reject(error);
            }, $q.reject);
          });
        };

      if (!$rootScope.deviceList || !$rootScope.deviceList.newer || !$rootScope.deviceList.newer.length) {
        return;
      }
      if (!job || isNaN(job.jobId) || job.jobId < 0) {
        cleanListOfAssignments();
        return;
      }

      for (i = 0; i < $rootScope.deviceList.newer.length; i += 1) {
        device = $rootScope.deviceList.newer[i];
        if (device.assignToJob && device.claimStatus === 'mine') {
          if (!(device.chamberAssignment && device.chamberAssignment.chamberId > 0)) {
            if (defaultChamber && defaultChamber.chamberId > 0) {
              device.chamberAssignment = defaultChamber;
            } else {
              device.chamberAssignment = {chamberId: undefined};
            }
          }
          equipmentList.push({chamberId: device.chamberAssignment.chamberId, chamberName: device.chamberAssignment.chamberName, modelType: device.modelType,
            dehuId: device.databaseId, equipmentName: device.name, address: device.address, serialNumber: device.serialNumber, devicePointer: device});
        }
      }
      if (!equipmentList.length) {
        return;
      }
      $ionicLoading.show();
      if (!defaultChamber || isNaN(defaultChamber.chamberId) || defaultChamber.chamberId <= 0) {//Create a new chamber if none exists for this job.
        equipmentList.push({promise: Job.createChamber(job.jobId).then(function (chamberId) {
          var j;
          for (j = 0; j < equipmentList.length; j += 1) {
            equipmentList[j].chamberId = chamberId;
            equipmentList[j].chamberName = "New Chamber";
          }
          return $q.resolve();
        }, $q.reject)});
      } else {
        equipmentList.push({promise: $q.resolve()});
      }

      for (i = equipmentList.length - 2; i >= 0; i -= 1) {//Add all equipment to their respective chambers, one-at-a-time.
        equipmentList[i].promise = waitThenCreateEquipment(i);
      }

      equipmentList[0].promise.then(function () {
        Snackbar.show({template: "Equipment assigned to job", hide: 5000});

        cleanListOfAssignments();
        if (jobToUpdate && jobToUpdate.equipmentList) {
          markExtraneousEquipment(jobToUpdate.equipmentList);
        }
        $timeout(function () {
          $ionicLoading.hide();
        }, 100);
      }, function (error) {
        cleanListOfAssignments();
        $rootScope.handleInternetErrorMessage("Could not assign equipment to job", error);
      });
    };

    Job.createCompanyEquipment = function (equipment) {
      return formatCompanyEquipment(equipment, false).then(function (formattedEquipment) {
        return $http.post(API_Base + 'cequip', formattedEquipment).then(function (response) {
          Snackbar.show({template: "Equipment added to company", hide: 5000});
          return response.data;
        }, $q.reject);
      }, $q.reject);
    };

    Job.updateCompanyEquipment = function (equipment) {
      if (!equipment || !equipment.cequipId) {
        return $q.reject({message: "No equipment ID found.", status: 400});
      }
      return formatCompanyEquipment(equipment, true).then(function (formattedEquipment) {
        return $http.put(API_Base + 'cequip/' + equipment.cequipId + formattedEquipment).then(function (response) {
          return response.data;
        }, $q.reject);
      }, $q.reject);
    };

    Job.createUnaffectedEquipment = function (jobId, msensorId, cequipId) {
      var formattedEquipment;
      if (!jobId || !(msensorId || cequipId)) {
        return $q.reject({message: "No job or equipment ID found.", status: 400});
      }
      if (msensorId) {
        formattedEquipment = "?msensorId=" + msensorId;
      } else {
        formattedEquipment = "?cequipId=" + cequipId;
      }

      return $http.post(API_Base + 'job/' + jobId + '/unaffected' + formattedEquipment).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.createEquipment2 = function (chamberId, equipment, isMoisture) {
      if (!chamberId) {
        return $q.reject({message: "No chamber ID found.", status: 400});
      }
      return formatEquipment2(equipment).then(function (formattedEquipment) {
        if (formattedEquipment && formattedEquipment.length) {
          if (isMoisture) {
            return $http.post(API_Base + 'chamber/' + chamberId + '/mequipment' + formattedEquipment).then(function (response) {
              return response.data;
            }, $q.reject);
          }
          return $http.post(API_Base.replace(/v1/g, 'v2') + 'chamber/' + chamberId + '/equipment' + formattedEquipment).then(function (response) {
            return response.data;
          }, $q.reject);
        }
        return $q.when();
      }, $q.reject);
    };

    Job.createEquipment = function (chamberId, equipment) {
      if (!chamberId) {
        return $q.reject({message: "No chamber ID found.", status: 400});
      }
      return formatEquipment(equipment).then(function (formattedEquipment) {
        if (formattedEquipment && formattedEquipment.length) {
          return $http.post(API_Base + 'chamber/' + chamberId + '/equipment' + formattedEquipment).then(function (response) {
            return response.data;
          }, $q.reject);
        }
        return $q.when();
      }, $q.reject);
    };

    Job.getEquipment = function (equipmentId) {
      if (!equipmentId) {
        return $q.reject({message: "No equipment ID found.", status: 400});
      }
      return $http.get(API_Base + 'equipment/' + equipmentId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.updateEquipment = function (equipmentId, equipment) {
      if (!equipmentId) {
        return $q.reject({message: "No equipment ID found.", status: 400});
      }
      return formatEquipment(equipment, true).then(function (formattedEquipment) {
        if (formattedEquipment && formattedEquipment.length) {
          return $http.put(API_Base + 'equipment/' + equipmentId + formattedEquipment).then(function (response) {
            return response.data;
          }, $q.reject);
        }
        return $q.when();
      }, $q.reject);
    };

    Job.deleteCompanyEquipment = function (equipmentId) {
      if (!equipmentId) {
        return $q.reject({message: "No equipment ID found.", status: 400});
      }
      return $http.delete(API_Base + 'cequip/' + equipmentId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.deleteEquipment = function (equipmentId) {
      if (!equipmentId) {
        return $q.reject({message: "No equipment ID found.", status: 400});
      }
      return $http.delete(API_Base + 'equipment/' + equipmentId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.deleteMoistureEquipment = function (equipmentId) {
      if (!equipmentId) {
        return $q.reject({message: "No equipment ID found.", status: 400});
      }
      return $http.delete(API_Base + 'mequipment/' + equipmentId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.createChamber = function (jobId, chamber) {
      if (!jobId) {
        return $q.reject({message: "No job ID found.", status: 400});
      }
      return formatChamber(chamber, true, true).then(function (formattedChamber) {
        return $http.post(API_Base.replace(/v1/g, 'v2') + 'job/' + jobId + '/chamber' + formattedChamber).then(function (response) {
          return response.data;
        }, $q.reject);
      });
    };

    Job.updateChamber = function (chamberId, chamber) {
      if (!chamberId) {
        return $q.reject({message: "No chamber ID found.", status: 400});
      }
      return formatChamber(chamber, false).then(function (formattedChamber) {
        return $http.put(API_Base + 'chamber/' + chamberId, formattedChamber).then(function (response) {
          return formatChamberSizes(chamber).then(function (formattedSizes) {
            if (formattedSizes) {
              return $http.put(API_Base + 'chamber/' + chamberId + '/size' + formattedSizes).then(function (response2) {
                return response2.data;
              }, $q.reject);
            }
            return response.data;
          });
        }, $q.reject);
      });
    };

    Job.getChamber = function (chamberId) {
      if (!chamberId) {
        return $q.reject({message: "No chamber ID found.", status: 400});
      }
      return $http.get(API_Base.replace(/v1/g, 'v2') + 'chamber/' + chamberId).then(function (response) {
        var i, j, thisEquipment;
        if (response && response.data && response.data.mequipment && response.data.mequipment.length) {
          if (!response.data.equipment) {
            response.data.equipment = [];
          }
          for (i = 0; i < response.data.mequipment.length; i += 1) {
            if (response.data.mequipment[i] && response.data.mequipment[i].msensorId && response.data.mequipment[i].msensorId > -1) {
              response.data.mequipment[i].model = "DrySENSE";
              response.data.mequipment[i].typeName = "Penetrating Meter";
              if (!response.data.mequipment[i].equipmentName) {
                response.data.mequipment[i].equipmentName = response.data.mequipment[i].sensorName || response.data.mequipment[i].label;
              }
              response.data.equipment.push(response.data.mequipment[i]);
            }
          }
        }
        if (response && response.data && response.data.equipment && response.data.equipment.length) {
          for (i = 0; i < response.data.equipment.length; i += 1) {
            thisEquipment = response.data.equipment[i] || {};
            if (thisEquipment.jobDayEnd > 0) {
              thisEquipment.isRemoved = true;
            } else {
              thisEquipment.isRemoved = false;
            }
            thisEquipment.isExtraneous = false;
            thisEquipment.hasBeacon = thisEquipment.beaconId > -1;
            determineEquipmentType(thisEquipment);
          }
          for (i = 0; i < response.data.equipment.length; i += 1) {
            if (response.data.equipment[i] && !response.data.equipment[i].isRemoved && response.data.equipment[i].msensorId && response.data.equipment[i].msensorId > -1) {
              for (j = 0; j < response.data.equipment.length; j += 1) {
                if (i !== j && response.data.equipment[j] && response.data.equipment[j].msensorId === response.data.equipment[i].msensorId) {
                  if (response.data.equipment[j].equipmentId && response.data.equipment[j].equipmentId > -1 && response.data.equipment[j].equipmentName) {
                    response.data.equipment[i].equipmentName = response.data.equipment[j].equipmentName;
                  }
                  if (!response.data.equipment[j].isRemoved && response.data.equipment[j].mequipId && response.data.equipment[j].mequipId > -1 &&
                      !(response.data.equipment[i].mequipId && response.data.equipment[i].mequipId > -1)) {
                    response.data.equipment[i].mequipId = response.data.equipment[j].mequipId;
                  }
                  if (response.data.equipment[j].equipmentId > 0 && (!(response.data.equipment[i].equipmentId > 0) || !response.data.equipment[j].isExtraneous)) {
                    response.data.equipment[i].isExtraneous = true;
                    break;
                  }
                }
              }
            }
          }
        }
        return response.data;
      }, $q.reject);
    };

    Job.deleteChamber = function (chamberId) {
      if (!chamberId) {
        return $q.reject({message: "No chamber ID found.", status: 400});
      }
      return $http.delete(API_Base + 'chamber/' + chamberId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.createContact = function (jobId, contact) {
      if (!jobId || isNaN(jobId)) {
        return $q.reject({message: "No job ID found.", status: 400});
      }
      return formatContact(contact, false).then(function (formattedContact) {
        formattedContact.jobId = parseInt(jobId, 10);
        return $http.post(API_Base + 'contact', formattedContact).then(function (response) {
          return response.data;
        }, $q.reject);
      });
    };

    Job.updateContact = function (contact) {
      if (!contact || !contact.contactId) {
        return $q.reject({message: "No contact ID found.", status: 400});
      }
      return formatContact(contact, true).then(function (formattedContact) {
        return $http.put(API_Base + 'contact/' + contact.contactId + formattedContact).then(function (response) {
          return response.data;
        }, $q.reject);
      });
    };

    Job.deleteContact = function (contactId) {
      if (!contactId) {
        return $q.reject({message: "No contact ID found.", status: 400});
      }
      return $http.delete(API_Base + 'contact/' + contactId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.createNote = function (note, hideSnackbar) {
      if (!note || !note.jobId) {
        return $q.reject({message: "No job ID found.", status: 400});
      }
      return Job.useOldVisitOrStartNew(note.jobId, note.sampleSecs, note.visitId).then(function (vId) {
        if (note.chamberId) {
          return $http.post(API_Base.replace(/v1/g, 'v2') + 'notes?jobId=' + note.jobId + '&visitId=' + vId + '&sampleSecs=' + note.sampleSecs +
                            '&chamberId=' + note.chamberId + '&note=' + encodeURIComponent(note.jobNote)).then(function (response) {
            if (!hideSnackbar) {
              Snackbar.show({template: "Changes saved", hide: 1500});
            }
            response.data = response.data || {};
            response.data.visitId = vId;
            return response.data;
          }, $q.reject);
        }
        return $http.post(API_Base.replace(/v1/g, 'v2') + 'notes?jobId=' + note.jobId + '&visitId=' + vId + '&sampleSecs=' + note.sampleSecs +
                          '&note=' + encodeURIComponent(note.jobNote)).then(function (response) {
          if (!hideSnackbar) {
            Snackbar.show({template: "Changes saved", hide: 1500});
          }
          response.data = response.data || {};
          response.data.visitId = vId;
          return response.data;
        }, $q.reject);
      }, $q.reject);
    };

    Job.updateNote = function (note) {
      if (!note || !note.jobNoteId) {
        return $q.reject({message: "No note ID found.", status: 400});
      }
      return $http.put(API_Base + 'notes/' + note.jobNoteId + '?sampleSecs=' + note.sampleSecs + '&note=' + encodeURIComponent(note.jobNote)).then(function (response) {
        Snackbar.show({template: "Changes saved", hide: 1500});
        return response.data;
      }, $q.reject);
    };

    Job.deleteNote = function (noteId) {
      if (!noteId) {
        return $q.reject({message: "No note ID found.", status: 400});
      }
      return $http.delete(API_Base + 'notes/' + noteId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.getDryingLog = function (jobId) {
      if (!jobId) {
        return $q.reject({message: "No job ID found.", status: 400});
      }
      return $http.get(API_Base.replace(/v1/g, 'v2') + 'job/' + jobId + '/reportstatus').then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.addBluetoothReading = function (equipmentId, measurementTime, jobId, visitId) {
      if (isNaN(equipmentId) || isNaN(measurementTime)) {
        return $q.reject({message: "Equipment ID and measurement time required.", status: 400});
      }

      return Job.useOldVisitOrStartNew(jobId, measurementTime, visitId).then(function (vId) {
        return $http.put(API_Base.replace(/v1/g, 'v2') + 'equipment/' + equipmentId + '/measure?sampleSecs=' + measurementTime + '&visitId=' + vId).then(function (response) {
          Snackbar.show({template: "Changes saved", hide: 1500});
          response.data = response.data || {};
          response.data.visitId = vId;
          return response.data;
        }, $q.reject);
      }, $q.reject);
    };

    Job.addBluetoothInletReading = function (chamberId, equipmentId, measurementTime, jobId, visitId, optionalRH, optionalTemp) {
      if (isNaN(chamberId) || isNaN(equipmentId) || isNaN(measurementTime)) {
        return $q.reject({message: "Chamber ID, equipment ID, and measurement time required.", status: 400});
      }
      var extras = "";
      if (!isNaN(optionalRH) && !isNaN(optionalTemp)) {
        extras = "&rh=" + optionalRH + "&temperature=" + optionalTemp;
      }

      return Job.useOldVisitOrStartNew(jobId, measurementTime, visitId).then(function (vId) {
        return $http.put(API_Base.replace(/v1/g, 'v2') + 'chamber/' + chamberId + '/affected?equipmentId=' + equipmentId + '&sampleSecs=' + measurementTime + '&visitId=' + vId + extras).then(function (response) {
          Snackbar.show({template: "Changes saved", hide: 1500});
          response.data = response.data || {};
          response.data.visitId = vId;
          return response.data;
        }, $q.reject);
      }, $q.reject);
    };

    Job.addBluetoothHVACReading = function (chamberId, equipmentId, measurementTime, jobId, visitId, optionalRH, optionalTemp) {
      if (!(chamberId > 0) || !(equipmentId > 0) || isNaN(measurementTime)) {
        return $q.reject({message: "Chamber ID, equipment ID, and measurement time required.", status: 400});
      }
      var extras = "";
      if (!isNaN(optionalRH) && !isNaN(optionalTemp)) {
        extras = "&rh=" + optionalRH + "&temperature=" + optionalTemp;
      }

      return Job.useOldVisitOrStartNew(jobId, measurementTime, visitId).then(function (vId) {
        return $http.put(API_Base.replace(/v1/g, 'v2') + 'chamber/' + chamberId + '/HVAC?equipmentId=' + equipmentId + '&sampleSecs=' + measurementTime + '&visitId=' + vId + extras).then(function (response) {
          Snackbar.show({template: "Changes saved", hide: 1500});
          response.data = response.data || {};
          response.data.visitId = vId;
          return response.data;
        }, $q.reject);
      }, $q.reject);
    };

    Job.addBluetoothUnaffectedReading = function (areaId, equipmentId, measurementTime, jobId, visitId, optionalRH, optionalTemp) {
      if (!(areaId > 0) || !(equipmentId > 0) || isNaN(measurementTime)) {
        return $q.reject({message: "Area ID, equipment ID, and measurement time required.", status: 400});
      }
      var extras = "";
      if (!isNaN(optionalRH) && !isNaN(optionalTemp)) {
        extras = "&rh=" + optionalRH + "&temperature=" + optionalTemp;
      }

      return Job.useOldVisitOrStartNew(jobId, measurementTime, visitId).then(function (vId) {
        return $http.put(API_Base.replace(/v1/g, 'v2') + 'area/' + areaId + '/unaffected?equipId=' + equipmentId + '&sampleSecs=' + measurementTime + '&visitId=' + vId + extras).then(function (response) {
          Snackbar.show({template: "Changes saved", hide: 1500});
          response.data = response.data || {};
          response.data.visitId = vId;
          return response.data;
        }, $q.reject);
      }, $q.reject);
    };

    Job.addAllBluetoothReadings = function (chamberId, measurementTime, jobId, visitId) {
      if (isNaN(chamberId) || isNaN(measurementTime)) {
        return $q.reject({message: "Chamber ID and measurement time required.", status: 400});
      }

      return Job.useOldVisitOrStartNew(jobId, measurementTime, visitId).then(function (vId) {
        return $http.put(API_Base.replace(/v1/g, 'v2') + 'chamber/' + chamberId + '/assign_measures?visitId=' + vId).then(function (response) {
          var addedMeasurements = (response.data && response.data.airMeasures) || [{}],
            i;
          if (response.data && response.data.moistureMeasures && response.data.moistureMeasures.length) {
            addedMeasurements = addedMeasurements.concat(response.data.moistureMeasures);
          }
          for (i = 0; i < addedMeasurements.length; i += 1) {
            if (addedMeasurements[i] && (addedMeasurements[i].status === 0 || addedMeasurements[i].status === 2)) {
              Snackbar.show({template: "Measurements added", hide: 1500});
              break;
            }
          }
          if (addedMeasurements[0]) {
            addedMeasurements[0].visitId = vId;
          }
          return addedMeasurements;
        }, $q.reject);
      }, $q.reject);
    };

    Job.addCurrentWeatherReading = function (measurement, changedName) {
      if (!measurement) {
        return $q.reject({message: "Could not create measurement with empty entry.", status: 400});
      }
      var initializationPromise = $q.resolve();

      if (changedName) {
        if (isNaN(measurement.areaId)) {
          initializationPromise = formatArea(measurement).then(function (formattedArea) {
            return $http.post(API_Base + 'area' + formattedArea).then(function (response) {
              measurement.areaId = response.data.areaId;
              return response.data;
            }, $q.reject);
          });
        } else {
          initializationPromise = formatArea(measurement).then(function (formattedArea) {
            return $http.put(API_Base + 'area/' + measurement.areaId + formattedArea).then(function (response) {
              return response.data;
            }, $q.reject);
          });
        }
      }

      return initializationPromise.then(function () {
        return Job.useOldVisitOrStartNew(measurement.jobId, measurement.sampleSecs, measurement.visitId).then(function (vId) {
          return $http.post(API_Base + 'area/' + measurement.areaId + '/outside?visitId=' + vId).then(function (response) {
            Snackbar.show({template: "Changes saved", hide: 1500});
            response.data = response.data || {};
            response.data.visitId = vId;
            return response.data;
          }, $q.reject);
        }, $q.reject);
      }, $q.reject);
    };

    Job.getHistoricalData = function (databaseId, startTime, endTime) {
      if (isNaN(databaseId) || isNaN(startTime) || isNaN(endTime)) {
        return $q.reject({message: "Dehu ID and time interval required.", status: 400});
      }
      return $http.get(API_Base + 'dehu/' + databaseId + '/data?scale=max&startSecs=' + Math.round(startTime) + '&endSecs=' + Math.round(endTime)).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.findEquipmentTypes = function () {
      return $http.get(API_Base + 'cequip/types').then(function (response) {
        var shuffledData = response.data || [],
          orderedData = [],
          typeLookup = {},
          foundOrder = {},
          aType = {},
          aSize = {},
          furtherLookup = {},
          validForDryTAGrh,
          i;

        for (i = 0; i < shuffledData.length; i += 1) {//Rearrange the data from Diane's server to be in a better format.
          if (shuffledData[i] && shuffledData[i].category) {
            validForDryTAGrh =  shuffledData[i].category === "Dehumidifier" || shuffledData[i].typeName.indexOf('Hygrometer') > -1;
            if (typeLookup.hasOwnProperty(shuffledData[i].category) && typeLookup[shuffledData[i].category].index > -1) {
              foundOrder = orderedData[typeLookup[shuffledData[i].category].index];
              furtherLookup = typeLookup[shuffledData[i].category].furtherLookup;
              if (furtherLookup.hasOwnProperty(shuffledData[i].equipType)) {
                aSize = {size: shuffledData[i].size, fullName: shuffledData[i].typeName};
                foundOrder.types[furtherLookup[shuffledData[i].equipType]].sizes.push(aSize);
              } else {
                aSize = {size: shuffledData[i].size, fullName: shuffledData[i].typeName};
                aType = {type: shuffledData[i].equipType, sizes: [aSize], sizeChoice: aSize, validForDryTAGrh: validForDryTAGrh};
                foundOrder.types.push(aType);
                if (!foundOrder.validForDryTAGrh && validForDryTAGrh) {
                  foundOrder.typeChoice = aType;
                }
                foundOrder.validForDryTAGrh = foundOrder.validForDryTAGrh || validForDryTAGrh;
                furtherLookup[shuffledData[i].equipType] = foundOrder.types.length - 1;
              }
            } else {
              aSize = {size: shuffledData[i].size, fullName: shuffledData[i].typeName};
              aType = {type: shuffledData[i].equipType, sizes: [aSize], sizeChoice: aSize, validForDryTAGrh: validForDryTAGrh};
              orderedData.push({category: shuffledData[i].category, types: [aType], typeChoice: aType, validForDryTAGrh: validForDryTAGrh});
              furtherLookup = {};
              furtherLookup[shuffledData[i].equipType] = 0;
              typeLookup[shuffledData[i].category] = {index: orderedData.length - 1, furtherLookup: furtherLookup};
            }
          } else if (shuffledData[i] && shuffledData[i].typeName) {
            if (!typeLookup.hasOwnProperty(shuffledData[i].typeName)) {
              aSize = {size: shuffledData[i].size, fullName: shuffledData[i].typeName};
              aType = {type: shuffledData[i].equipType, sizes: [aSize], sizeChoice: aSize, validForDryTAGrh: false};
              orderedData.push({category: shuffledData[i].typeName, types: [aType], typeChoice: aType, validForDryTAGrh: false});
              furtherLookup = {};
              furtherLookup[shuffledData[i].equipType] = 0;
              typeLookup[shuffledData[i].category] = {index: orderedData.length - 1, furtherLookup: furtherLookup};
            }
          }
        }
        return orderedData;
      }, $q.reject);
    };

    Job.findMaterialsAndTargets = function (chamberId) {
      if (!chamberId) {
        return $q.reject({message: "No chamber ID found.", status: 400});
      }
      return $http.get(API_Base + 'chamber/' + chamberId + '/mptargets').then(function (response) {
        var shuffledData = response.data || [],
          orderedData = [],
          locationLookup = {},
          aMaterial = {},
          i;

        for (i = 0; i < shuffledData.length; i += 1) {//Rearrange the data from Diane's server to be in a better format.
          if (shuffledData[i] && shuffledData[i].location) {
            if (locationLookup.hasOwnProperty(shuffledData[i].location)) {
              orderedData[locationLookup[shuffledData[i].location]].materials.push({material: shuffledData[i].material, target: shuffledData[i].target});
            } else {
              aMaterial = {material: shuffledData[i].material, target: shuffledData[i].target};
              orderedData.push({location: shuffledData[i].location, materials: [aMaterial], materialChoice: aMaterial});
              locationLookup[shuffledData[i].location] = orderedData.length - 1;
            }
          }
        }
        return orderedData;
      }, $q.reject);
    };

    Job.useOldVisitOrStartNew = function (jobId, startTime, visitId) {
      if (isNaN(visitId)) {
        if (!jobId || isNaN(startTime)) {
          return $q.reject({message: "Job ID and start time required.", status: 400});
        }
        return $http.post(API_Base + 'job/' + jobId + '/visit?initialSecs=' + startTime).then(function (response) {
          if ($rootScope.latestLogEntry && !($rootScope.latestLogEntry.visitId > 0 && response && response.data > 0)) {
            $rootScope.latestLogEntry.visitId = response.data;
          }
          return response.data;
        }, $q.reject);
      }
      return $q.resolve(visitId);
    };

    Job.updateMultipleMeasurements = function (measurements) {
      var i = 0,
        updateAMeasurement = function () {
          if (!measurements || !measurements.length || i >= measurements.length) {
            return $q.resolve();
          }
          if (measurements[i].jobMeasureId) {
            return createOrUpdateReading(measurements[i]).then(function () {
              i += 1;
              return updateAMeasurement();
            }, $q.reject);
          }
          i += 1;
          return updateAMeasurement();
        };

      return updateAMeasurement();
    };

    Job.addMoistureSite = function (chamberId, moisturePoint) {
      if (!chamberId) {
        return $q.reject({message: "No chamber ID found.", status: 400});
      }

      return formatMoisturePoint(moisturePoint).then(function (formattedMoisture) {
        return $http.post(API_Base + 'chamber/' + chamberId + '/mequipment' + formattedMoisture).then(function (response) {
          return response.data;
        }, $q.reject);
      });
    };

    Job.addBluetoothMoisture = function (equipmentId, measurementTime, jobId, visitId, moistureReading) {
      if (isNaN(equipmentId) || isNaN(measurementTime)) {
        return $q.reject({message: "Equipment ID and measurement time required.", status: 400});
      }

      return Job.useOldVisitOrStartNew(jobId, measurementTime, visitId).then(function (vId) {
        var extraBit = '';
        if (moistureReading >= 0) {
          extraBit = '&moisture=' + moistureReading;
        }
        return $http.put(API_Base + 'mequipment/' + equipmentId + '/measure?sampleSecs=' + measurementTime + '&visitId=' + vId + extraBit).then(function (response) {
          Snackbar.show({template: "Changes saved", hide: 1500});
          response.data = response.data || {};
          response.data.visitId = vId;
          return response.data;
        }, $q.reject);
      }, $q.reject);
    };

    Job.createOrUpdateMoisture = function (measure, changedMeasure, changedPoint) {
      var initializationPromise = $q.resolve();
      if (!measure || !measure.mequipId) {
        return $q.reject({message: "Could not create measurement with empty entry.", status: 400});
      }
      if (changedPoint) {
        initializationPromise = formatMoisturePoint(measure).then(function (formattedMoisture) {
          return $http.put(API_Base + 'mequipment/' + measure.mequipId + formattedMoisture).then(function (response) {
            if (response && response.data) {
              measure.location = response.data.location || measure.location;
              measure.material = response.data.material || measure.material;
              measure.label = response.data.label || measure.label;
              measure.target = response.data.target || measure.target;
            }
            return response.data;
          }, $q.reject);
        });
      }

      return initializationPromise.then(function () {
        if (changedMeasure) {
          return Job.useOldVisitOrStartNew(measure.jobId, measure.sampleSecs, measure.visitId).then(function (visitId) {
            measure.visitId = visitId;
            return formatMoisture(measure).then(function (formattedMeasure) {
              return $http.post(API_Base + 'mequipment/' + measure.mequipId + '/measure' + formattedMeasure).then(function (response) {
                Snackbar.show({template: "Changes saved", hide: 1500});
                response.data = response.data || {};
                response.data.visitId = visitId;
                return response.data;
              }, $q.reject);
            });
          });
        }
        Snackbar.show({template: "Changes saved", hide: 1500});
        return $q.resolve(measure);
      }, $q.reject);
    };

    Job.createOrUpdateMeasurement = function (measurement, changedMeasure, changedName) {
      var initializationPromise = $q.resolve();

      if (changedName && measurement) {
        if (isNaN(measurement.areaId)) {
          initializationPromise = formatArea(measurement).then(function (formattedArea) {
            return $http.post(API_Base + 'area' + formattedArea).then(function (response) {
              measurement.areaId = response.data.areaId;
              changedMeasure = true;
              return response.data;
            }, $q.reject);
          });
        } else {
          initializationPromise = formatArea(measurement).then(function (formattedArea) {
            return $http.put(API_Base + 'area/' + measurement.areaId + formattedArea).then(function (response) {
              return response.data;
            }, $q.reject);
          });
          // $timeout(function () {
          //   $ionicLoading.hide();
          // }, 100);
          // initializationPromise = $ionicPopup.confirm({
          //   title: "Change all reading names?",
          //   template: 'Do you want to change the name of all ' + measurement.measurementType + ' measurements for this job from "' +
          //     measurement.areaName + '" to "' + measurement.editName + '" or just this one, on this day?',
          //   okText: "ALL",
          //   okType: "right-button",
          //   cancelText: "ONLY ONE",
          //   cancelType: "left-button"
          // }).then(function (res) {
          //   $ionicLoading.show();
          //   if (res) {
          //     return formatArea(measurement).then(function (formattedArea) {
          //       return $http.put(API_Base + 'area/' + measurement.areaId + formattedArea).then(function (response) {
          //         return response.data;
          //       }, $q.reject);
          //     });
          //   }
          //   delete measurement.areaId;
          //   return Job.deleteMeasurement(measurement.jobMeasureId).then(function () {
          //     return formatArea(measurement).then(function (formattedArea) {
          //       return $http.post(API_Base + 'area' + formattedArea).then(function (response) {
          //         measurement.areaId = response.data.areaId;
          //         changedMeasure = true;
          //         return response.data;
          //       }, $q.reject);
          //     });
          //   }, $q.reject);
          // });
        }
      }

      return initializationPromise.then(function () {
        if (changedMeasure) {
          return createOrUpdateReading(measurement);
        }
        Snackbar.show({template: "Changes saved", hide: 1500});
        return $q.resolve(measurement);
      }, $q.reject);
    };

    Job.deleteMeasurement = function (measurementId) {
      if (!measurementId) {
        return $q.reject({message: "No measurement ID found.", status: 400});
      }
      return $http.delete(API_Base + 'measure/' + measurementId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.deleteMoistureReading = function (measurementId) {
      if (!measurementId) {
        return $q.reject({message: "No measurement ID found.", status: 400});
      }
      return $http.delete(API_Base + 'moisture/' + measurementId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.deleteVisit = function (visitId) {
      if (!visitId) {
        return $q.reject({message: "No visit ID found.", status: 400});
      }
      return $http.delete(API_Base + 'visit/' + visitId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.getRefreshedInventoryReport = function () {
      return $http.get(API_Base + 'inventory/equip/').then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.getInventoryReport = function () {
      return $http.get(API_Base + 'inventory/equip/last/').then(function (response) {
        if (!response.data || !response.data.length) {
          return Job.getRefreshedInventoryReport();
        }
        return response.data;
      }, $q.reject);
    };

    Job.createLocation = function (location) {
      return formatLocation(location).then(function (formattedLocation) {
        return $http.post(API_Base + 'inventory/location/fixed' + formattedLocation).then(function (response) {
          return response.data;
        }, $q.reject);
      });
    };

    Job.deleteLocation = function (locationId) {
      return $http.delete(API_Base + 'inventory/location/' + locationId).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.createOrUpdateGateway = function (locationId, name, isExisting) {
      if (!locationId || !name) {
        return $q.reject({message: "Gateway UUID and name required.", status: 400});
      }
      if (isExisting) {
        return $http.put(Integration_Base + 'gateway/location/' + encodeURIComponent(locationId) + '?name=' + encodeURIComponent(name)).then(function (response) {
          return response.data;
        }, $q.reject);
      }
      return $http.post(Integration_Base + 'gateway/location?uuid=' + encodeURIComponent(locationId) + '&name=' + encodeURIComponent(name)).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.deleteGateway = function (locationId) {
      return $http.delete(Integration_Base + 'gateway/location/' + encodeURIComponent(locationId)).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.getAllInventoryLocations = function () {
      return $http.get(API_Base + 'inventory/summary/').then(function (response) {
        return response.data;
      }, $q.reject);
    };

    Job.getInventoryLocations = function () {
      var i,
        list,
        output = {fixed: [], job: [], mobile: [], gateway: []};
      return $http.get(API_Base + 'inventory/location').then(function (response) {
        list = (response && response.data) || [];
        if (list && list.length) {
          for (i = 0; i < list.length; i += 1) {
            if (list[i]) {
              if (list[i].locationType === 'Fixed') {
                output.fixed.push(list[i]);
              } else if (list[i].locationType === 'Job') {
                output.job.push(list[i]);
              } else if (list[i].locationType === 'Mobile') {
                output.mobile.push(list[i]);
              } else if (list[i].locationType === 'Gateway') {
                output.gateway.push(list[i]);
              }
            }
          }
        }
        return output;
      }, $q.reject);
    };

    Job.updateInventoryStatus = function (newStatus, equipment) {
      var query = '?disposition=' + newStatus;
      if (equipment.dehuId > 0) {
        query += '&dehuId=' + equipment.dehuId;
      }
      if (equipment.cequipId > 0) {
        query += '&cequipId=' + equipment.cequipId;
      }
      if (equipment.msensorId > 0) {
        query += '&msensorId=' + equipment.msensorId;
      }
      return $http.put(API_Base + 'inventory/equip' + query).then(function (response) {
        return response.data;
      }, $q.reject);
    };

    return Job;
  }];

  angular.module('ThermaStor')
    .service('Job', jobFactory);
}());

(function () {
  'use strict';
  var homeConfig = ['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('app.home', {
        url: '/dashboard',
        views: {
          main: {
            templateUrl: "views/Main/home.html",
            controller: 'HomeController'
          }
        }
      });
  }],
    homeCtrl = ['$state', '$scope', '$ionicHistory', '$ionicPopup', '$ionicPopover', 'User', 'Job', 'Dehumidifier', '$localStorage', '$ionicSideMenuDelegate', '$rootScope', '$ionicLoading', '$timeout', 'ionicDatePicker', '$filter', '$q',
      function ($state, $scope, $ionicHistory, $ionicPopup, $ionicPopover, User, Job, Dehumidifier, $localStorage, $ionicSideMenuDelegate, $rootScope, $ionicLoading, $timeout, ionicDatePicker, $filter, $q) {
        var july2017Cutoff = 1500000000,
          oct2096Cutoff = 4000000000,
          k,
          loadingMessage = function (message, cancelFunction) {
            if (!cancelFunction) {
              return {template: '<ion-spinner class="spinner-assertive huge"></ion-spinner><h3>' + message + '</h3>', delay: 100};
            }
            return {template: '<ion-spinner class="spinner-assertive huge"></ion-spinner><h3>' + message +
              '</h3><button class="button button-block button-transparent icon icon-right ion-close-circled" ng-click="' + cancelFunction + '()">Cancel</button>', delay: 100};
          },
          refreshThePage = function (inventory) {
            var i,
              milliampHoursUsed,
              countsSinceBatteryChanged,
              vibrationsSinceBatteryChanged,
              smallestDate,
              largestDate,
              aRefreshTimestamp,
              equipmentTypes = [],
              locations = {};
            $ionicLoading.hide();
            if (!inventory || !inventory.length) {
              inventory = [];
            }
            for (i = 0; i < inventory.length; i += 1) {
              if (inventory[i] && inventory[i].equipName) {
                if (!aRefreshTimestamp && inventory[i].tstampSecs) {
                  aRefreshTimestamp = new Date(inventory[i].tstampSecs * 1000);
                }
                if (inventory[i].drytagUuid) {
                  if (inventory[i].batteryStatus < 2.3) {
                    inventory[i].dryTAGbattery = "< 10%";
                  } else {
                    countsSinceBatteryChanged = inventory[i].lastSeqCount - inventory[i].newBatterySeqCount;
                    vibrationsSinceBatteryChanged = inventory[i].lastVibSeqCount - inventory[i].newBatteryVibSeqCount;
                    if (countsSinceBatteryChanged < 0) {
                      countsSinceBatteryChanged = 0;
                    }
                    if (vibrationsSinceBatteryChanged < 0) {
                      vibrationsSinceBatteryChanged = 0;
                    }
                    milliampHoursUsed = vibrationsSinceBatteryChanged * 0.000078125 + countsSinceBatteryChanged * 0.000057292;
                    if (milliampHoursUsed >= 418.5) {
                      inventory[i].dryTAGbattery = "< 10%";
                    } else if (milliampHoursUsed < 0) {
                      inventory[i].dryTAGbattery = "100%";
                    } else {
                      inventory[i].dryTAGbattery = Math.round(100 - milliampHoursUsed / 4.65) + "%";
                    }
                  }
                }
                if (inventory[i].lastScanSecs && inventory[i].lastScanSecs > july2017Cutoff) {
                  if (inventory[i].lastScanSecs > oct2096Cutoff && Date.now() < oct2096Cutoff * 1000) {
                    inventory[i].lastScanSecs = Math.round(inventory[i].lastScanSecs / 1000);
                  }
                  if (inventory[i].lastScanSecs > july2017Cutoff) {
                    if (!smallestDate || inventory[i].lastScanSecs < smallestDate) {
                      smallestDate = inventory[i].lastScanSecs;
                    }
                    if (!largestDate || inventory[i].lastScanSecs > largestDate) {
                      largestDate = inventory[i].lastScanSecs;
                    }
                  }
                }
                if (inventory[i].equipType && equipmentTypes.indexOf(inventory[i].equipType) < 0) {
                  equipmentTypes.push(inventory[i].equipType);
                }
                if (inventory[i].lastScanLocation) {
                  if (!locations[inventory[i].lastScanLocation]) {
                    locations[inventory[i].lastScanLocation] = {toggled: false};
                  }
                }
                inventory[i].isInDateRange = true;
              }
            }
            if (!aRefreshTimestamp || ((new Date()).getTime() - aRefreshTimestamp.getTime() > (1000 * 3600 * 24 * 7))) {
              //If last refresh date is more than a week old, trigger a refresh automatically
              //(since server only does the daily auto-updates for a month after user clicks the button).
              $scope.shouldRefreshFurther = true;
            }
            $scope.updatedTime = "Updated " + aRefreshTimestamp.toLocaleDateString() + " at " + aRefreshTimestamp.toLocaleTimeString();
            $scope.smallestDate = smallestDate;
            $scope.largestDate = largestDate;
            $scope.absoluteSmallestDate = $scope.smallestDate;
            $scope.absoluteLargestDate = $scope.largestDate;
            $scope.locationList = locations;
            $scope.equipmentTypeList = equipmentTypes;
            $scope.inventory = inventory;
          };

        $scope.equipmentFilter = {isInDateRange: true};
        if (!$localStorage.columnPreferences) {
          $localStorage.columnPreferences = {equipName: true, status: true, lastScanSecs: true, lastScanLocation: true, equipType: true};
        }
        $scope.columnPreferences = $localStorage.columnPreferences;
        if (!$localStorage.columnList) {
          $localStorage.columnList = [
            { displayName: 'Equipment Name',
              serverName: 'equipName',
              allowFilter: true,
              allowSort: true,
              increaseWidth: true },
            { displayName: 'Status',
              serverName: 'status',
              allowFilter: true,
              allowSort: true,
              isStatus: true,
              increaseWidth: false },
            { displayName: 'Last Read Time',
              serverName: 'lastScanSecs',
              allowFilter: false,
              allowSort: true,
              isDateRange: true,
              increaseWidth: true },
            { displayName: 'Location',
              serverName: 'lastScanLocation',
              allowFilter: true,
              allowSort: true,
              increaseWidth: false },
            { displayName: 'Location Coordinates',
              serverName: 'coordinates',
              allowFilter: false,
              allowSort: false,
              isMapLocation: true,
              increaseWidth: false },
            { displayName: 'Job',
              serverName: 'lastJobLocation',
              allowFilter: true,
              allowSort: true,
              increaseWidth: false },
            { displayName: 'Type',
              serverName: 'equipType',
              allowFilter: true,
              allowSort: true,
              increaseWidth: false },
            { displayName: 'DryTAG UUID',
              serverName: 'drytagUuid',
              allowFilter: true,
              allowSort: true,
              increaseWidth: true },
            { displayName: 'Serial Number',
              serverName: 'serialNumber',
              allowFilter: true,
              allowSort: true,
              increaseWidth: true },
            { displayName: 'Last User',
              serverName: 'lastUser',
              allowFilter: true,
              allowSort: true,
              increaseWidth: false },
            { displayName: 'Warning',
              serverName: 'warning',
              allowFilter: true,
              allowSort: true,
              increaseWidth: true }
          ];
        }
        if (!$localStorage.columnMigration || $localStorage.columnMigration < 1) {
          //The dryTAGbattery field wasn't present originally when the website was first published
          //so we need to do a migration of the data structure to make sure it updates for browsers
          //that have the old format stored.
          $localStorage.columnMigration = 1;
          $localStorage.columnList.push({ displayName: 'DryTAG Battery',
              serverName: 'dryTAGbattery',
              allowFilter: true,
              allowSort: true,
              increaseWidth: true });
        }
        if ($localStorage.columnList && $localStorage.columnList.length && (!$localStorage.columnMigration || $localStorage.columnMigration < 2)) {
          $localStorage.columnMigration = 2;
          for (k = 0; k < $localStorage.columnList.length; k += 1) {
            if ($localStorage.columnList[k].serverName === 'lastScanLocation') {
              $localStorage.columnList[k].increaseWidthModerate = true;
            }
          }
        }
        if ($localStorage.columnList && $localStorage.columnList.length && (!$localStorage.columnMigration || $localStorage.columnMigration < 3)) {
          $localStorage.columnMigration = 3;
          for (k = 0; k < $localStorage.columnList.length; k += 1) {
            if ($localStorage.columnList[k].serverName === 'lastScanLocation') {
              $localStorage.columnList[k].isLocation = true;
            }
          }
        }
        $scope.columnList = $localStorage.columnList;

        $scope.toggleCheckboxes = function () {
          var boxCheckCount = 0,
            dryTagChecked = false,
            onlyInactive = true,
            i;

          if ($scope.inventory && $scope.inventory.length) {
            for (i = 0; i < $scope.inventory.length; i += 1) {
              if ($scope.inventory[i] && $scope.inventory[i].isChecked) {
                boxCheckCount += 1;
                if ($scope.inventory[i].drytagUuid) {
                  dryTagChecked = true;
                }
                if ($scope.inventory[i].status !== 'Inactive') {
                  onlyInactive = false;
                }
              }
            }
          }
          $scope.checkingCount = boxCheckCount;
          $scope.showFloatingBottom = boxCheckCount > 0;
          $scope.canChangeBatteries = dryTagChecked;
          $scope.onlyInactiveChecked = onlyInactive;
        };

        $scope.clearCheckboxes = function () {
          var i;
          if ($scope.inventory && $scope.inventory.length) {
            for (i = 0; i < $scope.inventory.length; i += 1) {
              if ($scope.inventory[i]) {
                $scope.inventory[i].isChecked = false;
              }
            }
          }
          $scope.showFloatingBottom = false;
          $scope.canChangeBatteries = false;
          $scope.checkingCount = 0;
        };

        $scope.highlightCheckboxes = function () {
          var boxCheckCount = 0,
            dryTagChecked = false,
            onlyInactive = true,
            i;
          if ($scope.inventory && $scope.inventory.length) {
            for (i = 0; i < $scope.inventory.length; i += 1) {
              if ($scope.inventory[i]) {
                boxCheckCount += 1;
                $scope.inventory[i].isChecked = true;
                if ($scope.inventory[i].drytagUuid) {
                  dryTagChecked = true;
                }
                if ($scope.inventory[i].status !== 'Inactive') {
                  onlyInactive = false;
                }
              }
            }
          }
          $scope.checkingCount = boxCheckCount;
          $scope.showFloatingBottom = true;
          $scope.canChangeBatteries = dryTagChecked;
          $scope.onlyInactiveChecked = onlyInactive;
        };

        $scope.bulkBattery = function () {
          var i,
            initialPromise = $q.defer(),
            promises = [initialPromise.promise],
            promiseClosure = function (previousPromise, currentDevice) {
              promises.push(previousPromise.then(function () {
                return Dehumidifier.changeDryTAGBattery(currentDevice.drytagUuid);
              }, $q.reject));
            };
          $ionicLoading.show();
          if ($scope.inventory && $scope.inventory.length) {
            for (i = 0; i < $scope.inventory.length; i += 1) {
              if ($scope.inventory[i] && $scope.inventory[i].drytagUuid && $scope.inventory[i].isChecked) {
                promiseClosure(promises[promises.length - 1], $scope.inventory[i]);
              }
            }
          }
          initialPromise.resolve();
          promises[promises.length - 1].then(function () {
            if ($scope.inventory && $scope.inventory.length) {
              for (i = 0; i < $scope.inventory.length; i += 1) {
                if ($scope.inventory[i] && $scope.inventory[i].isChecked) {
                  if ($scope.inventory[i].drytagUuid) {
                    $scope.inventory[i].dryTAGbattery = "100%";
                  }
                  $scope.inventory[i].isChecked = false;
                }
              }
            }
            $scope.showFloatingBottom = false;
            $ionicPopup.alert({title: 'Battery Changed',
              template: 'The battery status of these DryTAGs have been reset to 100%. You will be warned to change their battery again once enough time passes.'});
            $timeout(function () {
              $ionicLoading.hide();
            }, 100);
          }, function (err) {
            $rootScope.handleInternetErrorMessage("Could not edit battery status", err);
          });
        };

        $scope.bulkInactive = function () {
          var i,
            initialPromise = $q.defer(),
            newStatus = $scope.onlyInactiveChecked ? 'Active' : 'Inactive',
            newDeviceStatus = newStatus === 'Active' ? 'Untagged' : newStatus,//Use untagged as a placeholder since it will be made invisible.
            promises = [initialPromise.promise],
            promiseClosure = function (previousPromise, currentDevice) {
              promises.push(previousPromise.then(function () {
                return Job.updateInventoryStatus(newStatus, currentDevice).then(function () {
                  currentDevice.status = newDeviceStatus;
                });
              }, $q.reject));
            };
          $ionicLoading.show();
          if ($scope.inventory && $scope.inventory.length) {
            for (i = 0; i < $scope.inventory.length; i += 1) {
              if ($scope.inventory[i] && $scope.inventory[i].isChecked && $scope.inventory[i].status !== newStatus) {
                promiseClosure(promises[promises.length - 1], $scope.inventory[i]);
              }
            }
          }
          initialPromise.resolve();
          promises[promises.length - 1].then(function () {
            if ($scope.inventory && $scope.inventory.length) {
              for (i = 0; i < $scope.inventory.length; i += 1) {
                if ($scope.inventory[i]) {
                  $scope.inventory[i].isChecked = false;
                }
              }
            }
            $scope.showFloatingBottom = false;
            $ionicPopup.alert({title: 'Status changed',
              template: 'These pieces of equipment have now been marked as ' + newStatus});
            $timeout(function () {
              $ionicLoading.hide();
            }, 100);
          }, function (err) {
            $rootScope.handleInternetErrorMessage("Could not update equipment status", err);
          });
        };

        $scope.resetBattery = function (aDevice) {
          if ($rootScope.popover) {
            $rootScope.popover.hide();
            $rootScope.popover = undefined;
          }

          Dehumidifier.changeDryTAGBattery(aDevice.drytagUuid).then(function () {
            aDevice.dryTAGbattery = "100%";
            $ionicPopup.alert({title: 'Battery Changed',
              template: 'The battery status of this DryTAG has been reset to 100%. You will be warned to change the battery again once enough time passes.'});
          }, function (err) {
            $rootScope.handleInternetErrorMessage("Could not edit battery status", err);
          });
        };

        $scope.setInactiveStatus = function (shouldBeInactive, aDevice) {
          if ($rootScope.popover) {
            $rootScope.popover.hide();
            $rootScope.popover = undefined;
          }

          Job.updateInventoryStatus(shouldBeInactive ? 'Inactive' : 'Active', aDevice).then(function () {
            aDevice.status = shouldBeInactive ? 'Inactive' : 'Untagged';//Use untagged as a placeholder since it will be made invisible.
          }, function (err) {
            $rootScope.handleInternetErrorMessage("Could not update equipment status", err);
          });
        };

        $scope.checkColumnPreferences = function (column) {
          if (column && column.serverName && $scope.columnPreferences[column.serverName]) {
            return true;
          }
          return false;
        };

        $scope.download = function (content, fileName) {
          //From https://stackoverflow.com/questions/14964035
          var hiddenElement = document.createElement('a');

          if (navigator.msSaveBlob) { // IE10
            navigator.msSaveBlob(new Blob([content], {
              type: 'text/csv;charset=utf-8;'
            }), fileName);
          } else if ('download' in hiddenElement) { //html5 A[download]
            hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(content);
            hiddenElement.target = '_blank';

            hiddenElement.download = fileName;
            hiddenElement.click();
          } else {
            location.href = 'data:application/octet-stream,' + encodeURIComponent(content);
          }
        };

        $scope.export = function () {
          var columns = $filter('filter')($scope.columnList, $scope.checkColumnPreferences),
            devices = $filter('filter')($scope.inventory, $scope.fullFilter),
            i,
            j,
            csvData = [[]],
            info,
            csv = '';

          if (!columns || !columns.length) {
            $ionicPopup.alert({title: 'Could not generate CSV', template: 'No data to export.'});
            return;
          }
          for (i = 0; i < columns.length; i += 1) {
            info = columns[i].displayName || '';
            info = info.replace(/"/g, '');
            csvData[0].push(('"' + info + '"') || '');
          }
          for (j = 0; j < devices.length; j += 1) {
            csvData.push([]);
            if (devices[j]) {
              for (i = 0; i < columns.length; i += 1) {
                if (columns[i].isMapLocation) {
                  csvData[csvData.length - 1].push('"' + (devices[j].latitude || 0) + ',' +  (devices[j].longitude || 0) + '"');
                } else if (columns[i].isDateRange) {
                  info = $scope.formatDate(devices[j][columns[i].serverName]);
                  if (info) {
                    csvData[csvData.length - 1].push('"' + info + '"');
                  } else {
                    csvData[csvData.length - 1].push('');
                  }
                } else {
                  info = devices[j][columns[i].serverName];
                  if (info === undefined || info === null) {
                    csvData[csvData.length - 1].push('');
                  } else {
                    info = String(info);
                    info = info.replace(/"/g, '');
                    csvData[csvData.length - 1].push('"' + info + '"');
                  }
                }
              }
            }
          }
          for (i = 0; i < csvData.length; i += 1) {
            csv += csvData[i].join(',') + '\n';
          }
          $scope.download(csv, 'inventory.csv');
        };

        $scope.chooseColumns = function ($event) {
          var popoverPromise;
          popoverPromise = $ionicPopover.fromTemplateUrl('views/Main/columnChoice.html', {scope: $scope});
          popoverPromise.then(function (popover) {
            $rootScope.popover = popover;
            $rootScope.popover.show($event);
          });
        };

        $scope.countColumns = function () {
          var i,
            count = 0;
          for (i in $scope.columnPreferences) {
            if ($scope.columnPreferences.hasOwnProperty(i) && $scope.columnPreferences) {
              count += 1;
            }
          }
          return count;
        };

        $scope.columnMoveUp = function (index) {
          var temp;
          if (index <= 0) {
            return;
          }
          temp = $scope.columnList[index - 1];
          $scope.columnList[index - 1] = $scope.columnList[index];
          $scope.columnList[index] = temp;
        };

        $scope.columnMoveDown = function (index) {
          var temp;
          if (index >= $scope.columnList.length - 1) {
            return;
          }
          temp = $scope.columnList[index + 1];
          $scope.columnList[index + 1] = $scope.columnList[index];
          $scope.columnList[index] = temp;
        };

        $scope.go = function (destination) {
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $state.go(destination);
        };

        $scope.canClearFilters = function () {
          var i;
          if ($scope.timeFiltering || $scope.tagFilter !== undefined) {
            return true;
          }
          if (!$scope.equipmentFilter) {
            return false;
          }
          for (i in $scope.equipmentFilter) {
            if ($scope.equipmentFilter.hasOwnProperty(i) && i !== 'isInDateRange' && $scope.equipmentFilter[i]) {
              return true;
            }
          }
          return false;
        };

        $scope.clearFilters = function () {
          var i;
          $scope.equipmentFilter = {isInDateRange: true};
          if ($scope.locationList) {
            for (i in $scope.locationList) {
              if ($scope.locationList.hasOwnProperty(i) && $scope.locationList[i]) {
                $scope.locationList[i].toggled = false;
              }
            }
          }
          if ($scope.inventory && $scope.inventory.length) {
            for (i = 0; i < $scope.inventory.length; i += 1) {
              if ($scope.inventory[i]) {
                $scope.inventory[i].isInDateRange = true;
              }
            }
          }
          $scope.tagFilter = undefined;
          $scope.timeFiltering = false;
          $scope.smallestDate = $scope.absoluteSmallestDate;
          $scope.largestDate = $scope.absoluteLargestDate;
          $scope.strictLocationFilter = undefined;
        };

        $scope.toggleOptions = function (value) {
          $scope.hideSideOptions = value;
        };

        $scope.toggleStatus = function (newStatus) {
          if (!$scope.equipmentFilter) {
            $scope.equipmentFilter = {isInDateRange: true};
          }
          if ($scope.equipmentFilter.status === newStatus) {
            $scope.equipmentFilter.status = '';
          } else {
            $scope.equipmentFilter.status = newStatus;
          }
        };

        $scope.toggleTagged = function (newToggling) {
          if (newToggling === $scope.tagFilter) {
            $scope.tagFilter = undefined;
          } else {
            $scope.tagFilter = newToggling;
          }
        };

        $scope.fullFilter = function (device) {
          var possibleDevice;
          if ($scope.tagFilter !== undefined) {
            if (($scope.tagFilter && !device.drytagUuid) || (!$scope.tagFilter && device.drytagUuid)) {
              return false;
            }
          }
          if ($scope.strictLocationFilter && $scope.strictLocationFilter !== device.lastScanLocation) {
            return false;
          }
          possibleDevice = $filter('filter')([device], $scope.equipmentFilter);
          if (possibleDevice && possibleDevice.length) {
            return true;
          }
          return false;
        };

        $scope.fullFilterSelect = function (property, value, reverse) {
          return function (device) {
            if (!device) {
              return false;
            }
            if (property) {
              if (reverse) {
                if (device[property]) {
                  return false;
                }
                if (value && device[property] === value) {
                  return false;
                }
              } else {
                if (!device[property]) {
                  return false;
                }
                if (value && device[property] !== value) {
                  return false;
                }
              }
            }
            return $scope.fullFilter(device);
          };
        };

        $scope.setLocationFilter = function () {
          if (!$scope.equipmentFilter) {
            $scope.equipmentFilter = {isInDateRange: true};
          }
        };

        $scope.unsetLocationFilter = function (isLocation) {
          var i;
          if (isLocation) {
            if ($scope.locationList) {
              for (i in $scope.locationList) {
                if ($scope.locationList.hasOwnProperty(i) && $scope.locationList[i]) {
                  $scope.locationList[i].toggled = false;
                }
              }
            }
            $scope.strictLocationFilter = undefined;
          }
        };

        $scope.toggleEquipmentType = function (equipmentType) {
          if (!$scope.equipmentFilter) {
            $scope.equipmentFilter = {isInDateRange: true};
          }
          if ($scope.equipmentFilter.equipType === equipmentType) {
            $scope.equipmentFilter.equipType = '';
          } else {
            $scope.equipmentFilter.equipType = equipmentType;
          }
        };

        $scope.toggleLocation = function (location) {
          var i;
          if (!$scope.equipmentFilter) {
            $scope.equipmentFilter = {isInDateRange: true};
          }
          if ($scope.locationList) {
            for (i in $scope.locationList) {
              if ($scope.locationList.hasOwnProperty(i) && $scope.locationList[i]) {
                $scope.locationList[i].toggled = false;
              }
            }
          }

          if ($scope.equipmentFilter.lastScanLocation === location) {
            $scope.equipmentFilter.lastScanLocation = '';
            $scope.strictLocationFilter = undefined;
          } else {
            $scope.equipmentFilter.lastScanLocation = location;
            $scope.strictLocationFilter = location;
            if ($scope.locationList) {
              $scope.locationList[location].toggled = true;
            }
          }
        };

        $scope.formatDate = function (timeSecs) {
          if (!timeSecs || timeSecs < july2017Cutoff) {
            return undefined;
          }

          var aDate = new Date(timeSecs * 1000),
            time = aDate.toLocaleTimeString();

          time = time.replace(/\u200E/g, '');//from https://stackoverflow.com/questions/17913681
          time = time.replace(/^([^\d]*\d{1,2}:\d{1,2}):\d{1,2}([^\d]*)$/, '$1$2');
          return aDate.toLocaleDateString() + " " + time;
        };

        $scope.formatDateRange = function () {
          if (!$scope.smallestDate || !$scope.largestDate || !($scope.smallestDate > july2017Cutoff) || !($scope.largestDate > july2017Cutoff)) {
            return undefined;
          }
          var date1 = new Date($scope.smallestDate * 1000),
            date2 = new Date($scope.largestDate * 1000);

          return date1.toLocaleDateString() + " - " + date2.toLocaleDateString();
        };

        $scope.setDateRange = function () {
          ionicDatePicker.openDoubleDatePicker({
            callback: function (newDate) {
              if (!newDate || !newDate.start || !newDate.end) {
                return;
              }
              var start = Math.round(newDate.start / 1000),
                end = Math.round(newDate.end / 1000) + 24 * 60 * 60 - 1,
                i;

              if (start < $scope.absoluteSmallestDate) {
                start = $scope.absoluteSmallestDate;
              }
              if (end > $scope.absoluteLargestDate) {
                end = $scope.absoluteLargestDate;
              }
              $scope.smallestDate = start;
              $scope.largestDate = end;
              if ($scope.inventory && $scope.inventory.length) {
                for (i = 0; i < $scope.inventory.length; i += 1) {
                  if ($scope.inventory[i]) {
                    if ($scope.inventory[i].lastScanSecs && $scope.inventory[i].lastScanSecs <= $scope.largestDate &&
                        $scope.inventory[i].lastScanSecs >= $scope.smallestDate) {
                      $scope.inventory[i].isInDateRange = true;
                    } else {
                      $scope.inventory[i].isInDateRange = false;
                    }
                  }
                }
              }
              $scope.timeFiltering = true;
            },
            from: new Date($scope.absoluteSmallestDate * 1000),
            to: new Date(),
            inputDate: new Date($scope.smallestDate * 1000),
            inputEnd: new Date($scope.largestDate * 1000)
          });
        };

        $scope.sort = function (field, isSortable) {
          var tempList = [],
            i;

          if (!$scope.inventory || !$scope.inventory.length || !isSortable) {
            return;
          }
          if ($scope.lastSortType === field) {
            for (i = $scope.inventory.length; i > 0; i -= 1) {
              tempList.push($scope.inventory[i - 1]);
            }
          } else {
            tempList = angular.copy($scope.inventory);
            tempList.sort(function (a, b) {
              var compareA, compareB;
              if (!a) {
                return -1;
              }
              if (!b) {
                return 1;
              }
              compareA = a[field];
              compareB = b[field];
              if (compareA === undefined || compareA === null) {
                compareA = "";
              }
              if (compareB === undefined || compareB === null) {
                compareB = "";
              }
              if (field === 'dryTAGbattery') {
                compareA = parseInt(compareA.replace(/%|</g, ""), 10) || 0;
                compareB = parseInt(compareB.replace(/%|</g, ""), 10) || 0;
              } else if (compareA && compareA.toLowerCase && compareB && compareB.toLowerCase) {
                compareA = compareA.toLowerCase();
                compareB = compareB.toLowerCase();
              }
              if (compareA < compareB) {
                return -1;
              }
              if (compareA > compareB) {
                return 1;
              }
              return 0;
            });
          }
          $scope.lastSortType = field;
          $scope.inventory = tempList;
        };

        if (!$rootScope.userName || !$rootScope.companyName) {
          User.get();
        }
        $ionicSideMenuDelegate.toggleLeft(false);//Close menu on start--otherwise it defaults to open sometimes.
        $localStorage.alreadyWelcomed = true;
        delete $localStorage.createUser;

        User.getLegalStuff().then(function (legalStuff) {
          if (legalStuff && legalStuff.length) {
            $rootScope.legalStuff = legalStuff;

            $rootScope.closePopup = $ionicPopup.confirm({
              title: "License Agreement",
              template: 'We have updated our license agreements. Please review and accept the policies to continue using DryLINK.',
              okText: "REVIEW",
              okType: "right-button",
              cancelText: "LOGOUT",
              cancelType: "left-button"
            });
            $rootScope.closePopup.then(function (res) {
              if (res) {
                $state.go('legalStuff');
              } else {
                $rootScope.signOut();
              }
            });
          }
        });

        $rootScope.hideLoading = function () {
          $ionicLoading.show(loadingMessage("Stopping..."));
          $timeout(function () {
            $ionicLoading.hide();
          }, 300);
        };

        $scope.refresh = function () {
          $ionicLoading.show(loadingMessage("Generating list...", "hideLoading"));
          Job.getRefreshedInventoryReport().then(refreshThePage, function (err) {
            $rootScope.handleInternetErrorMessage("Could not retrieve inventory list from server", err);
          });
        };

        $ionicLoading.show(loadingMessage("Generating list...", "hideLoading"));
        Job.getInventoryReport().then(refreshThePage, function (err) {
          $rootScope.handleInternetErrorMessage("Could not retrieve inventory list from server", err);
        }).then(function () {
          if ($scope.shouldRefreshFurther) {
            $scope.refresh();
          }
        });
      }];
  angular.module('User')
    .config(homeConfig)
    .controller('HomeController', homeCtrl);
}());

(function () {
  'use strict';
  var integrationConfig = ['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('app.integrations', {
        url: '/integrations',
        views: {
          main: {
            templateUrl: "views/Main/integrations.html",
            controller: 'IntegrationController'
          }
        }
      });
  }],
    integrationCtrl = ['$scope', '$rootScope', '$ionicPopup', 'User', '$ionicLoading', '$timeout', '$ionicPopover',
      function ($scope, $rootScope, $ionicPopup, User, $ionicLoading, $timeout, $ionicPopover) {
        $scope.integrations = [];

        $scope.addIntegration = function (id, realName, isEnabled) {
          $ionicLoading.show();
          User.addIntegration(id, isEnabled !== true && isEnabled !== false).then(function (newKey) {
            var i,
              url;

            for (i = 0; i < $scope.integrations.length; i += 1) {
              if ($scope.integrations[i].id === id) {
                $scope.integrations[i].isEnabled = true;
                url = $scope.integrations[i].url;
                break;
              }
            }
            $timeout(function () {
              $ionicLoading.hide();
            }, 100);
            if (url) {
              $rootScope.openInNewWindow(url + "?apiKey=" + (newKey && newKey.message));
            } else if (newKey && newKey.message) {
              $ionicPopup.alert({title: 'Add Integration', template: 'To finish integrating with ' + realName +
                ', enter this key into your account with them: <br><br>' + (newKey && newKey.message)});
            } else {
              $ionicPopup.alert({title: 'Integration Enabled', template: realName + ' integration ready for use'});
            }
          }, function (err) {
            $rootScope.handleInternetErrorMessage("Could not add integration.", err);
          });
        };

        $scope.deleteIntegration = function (id) {
          if ($rootScope.popover) {
            $rootScope.popover.hide();
            $rootScope.popover = undefined;
          }
          $ionicLoading.show();

          User.deleteIntegration(id).then(function (status) {
            var i,
              newEnabledStatus;
            if (status && status.integrationReady) {
              newEnabledStatus = false;
            }

            for (i = 0; i < $scope.integrations.length; i += 1) {
              if ($scope.integrations[i].id === id) {
                $scope.integrations[i].isEnabled = newEnabledStatus;
                break;
              }
            }
            $timeout(function () {
              $ionicLoading.hide();
            }, 100);
          }, function (err) {
            $rootScope.handleInternetErrorMessage("Could not delete integration.", err);
          });
        };

        $scope.openMoreOptions = function ($event, anIntegration) {
          var popoverPromise;
          $scope.anIntegration = anIntegration;
          popoverPromise = $ionicPopover.fromTemplateUrl('views/Main/integrationOptions.html', {scope: $scope});
          popoverPromise.then(function (popover) {
            $rootScope.popover = popover;
            $rootScope.popover.show($event);
          });
        };

        $ionicLoading.show();
        User.getPossibleIntegrations().then(function (integrations) {
          User.getExistingIntegrations().then(function (existences) {
            var i, j;
            if (integrations && integrations.length) {
              for (j = 0; j < integrations.length; j += 1) {
                if (integrations[j]) {
                  if (integrations[j].clientName === 'RestorationMgr') {
                    integrations[j].realName = "Restoration Manager";
                    integrations[j].blurb = "Cloud-based, mobile friendly, restoration project management software " +
                      "to help your restoration business decrease cycle times and take on more jobs.";
                    integrations[j].image = "img/restoration-manager-logo.png";
                  } else {
                    integrations[j].realName = integrations[j].clientName;
                  }
                  if (existences && existences.length) {
                    for (i = 0; i < existences.length; i += 1) {
                      if (existences[i] && existences[i].clientId === integrations[j].id) {
                        integrations[j].isEnabled = existences[i].isEnabled;
                        break;
                      }
                    }
                  }
                }
              }
            }
            $scope.integrations = integrations;

            $timeout(function () {
              $ionicLoading.hide();
            }, 100);
          }, function (err) {
            $rootScope.handleInternetErrorMessage("Could not retrieve integration information from server.", err);
          });
        }, function (err) {
          $rootScope.handleInternetErrorMessage("Could not retrieve integration information from server.", err);
        });
      }];
  angular.module('User')
    .config(integrationConfig)
    .controller('IntegrationController', integrationCtrl);
}());

(function () {
  'use strict';
  var locationListConfig = ['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('app.locationList', {
        url: '/locationList',
        views: {
          main: {
            templateUrl: "views/Main/locationList.html",
            controller: 'LocationListController'
          }
        }
      });
  }],
    locationListCtrl = ['$scope', '$rootScope', '$ionicPopup', 'Job', '$ionicLoading', '$timeout', '$ionicPopover', '$q', 'ionicDatePicker',
      function ($scope, $rootScope, $ionicPopup, Job, $ionicLoading, $timeout, $ionicPopover, $q, ionicDatePicker) {
        var july2017Cutoff = 1500000000,
          oct2096Cutoff = 4000000000,
          enableButtonTimeout,
          innerDelete = function () {
            if ($scope.aLocation.locationType === 'Gateway') {
              if (!$scope.aLocation || !$scope.aLocation.gatewayUuid) {
                return $q.reject({message: "Gateway UUID not found", status: 400});
              }
              return Job.deleteGateway($scope.aLocation.gatewayUuid);
            }
            if (!$scope.aLocation || !$scope.aLocation.invLocationId) {
              return $q.reject({message: "Location ID not found", status: 400});
            }
            return Job.deleteLocation($scope.aLocation.invLocationId);
          },
          initializeList = function () {
            Job.getInventoryLocations().then(function (locations) {
              var i,
                smallestDate,
                largestDate;

              $ionicLoading.hide();
              if (locations && locations.fixed && locations.fixed.length) {
                for (i = 0; i < locations.fixed.length; i += 1) {
                  if (locations.fixed[i]) {
                    if (!locations.fixed[i].name) {
                      locations.fixed[i].name = '';
                    }
                    if (!locations.fixed[i].address) {
                      locations.fixed[i].address = '';
                    }
                  }
                }
              }
              if (locations && locations.job && locations.job.length) {
                for (i = 0; i < locations.job.length; i += 1) {
                  if (locations.job[i]) {
                    if (!locations.job[i].name) {
                      locations.job[i].name = '';
                    }
                    if (!locations.job[i].address) {
                      locations.job[i].address = '';
                    }
                  }
                }
              }
              if (locations && locations.mobile && locations.mobile.length) {
                for (i = 0; i < locations.mobile.length; i += 1) {
                  if (locations.mobile[i]) {
                    locations.mobile[i].isInDateRange = true;
                    if (locations.mobile[i].lastReadSecs > oct2096Cutoff && Date.now() < oct2096Cutoff * 1000) {
                      locations.mobile[i].lastReadSecs = Math.round(locations.mobile[i].lastReadSecs / 1000);
                    }
                    if (locations.mobile[i].lastReadSecs > july2017Cutoff) {
                      if (!smallestDate || locations.mobile[i].lastReadSecs < smallestDate) {
                        smallestDate = locations.mobile[i].lastReadSecs;
                      }
                      if (!largestDate || locations.mobile[i].lastReadSecs > largestDate) {
                        largestDate = locations.mobile[i].lastReadSecs;
                      }
                    }
                  }
                }
              }
              $scope.smallestDate[0] = smallestDate;
              $scope.largestDate[0] = largestDate;
              $scope.absoluteSmallestDate[0] = smallestDate;
              $scope.absoluteLargestDate[0] = largestDate;

              smallestDate = undefined;
              largestDate = undefined;
              if (locations && locations.gateway && locations.gateway.length) {
                for (i = 0; i < locations.gateway.length; i += 1) {
                  if (locations.gateway[i]) {
                    locations.gateway[i].isInDateRange = true;
                    if (locations.gateway[i].lastReadSecs > oct2096Cutoff && Date.now() < oct2096Cutoff * 1000) {
                      locations.gateway[i].lastReadSecs = Math.round(locations.gateway[i].lastReadSecs / 1000);
                    }
                    if (locations.gateway[i].lastReadSecs > july2017Cutoff) {
                      if (!smallestDate || locations.gateway[i].lastReadSecs < smallestDate) {
                        smallestDate = locations.gateway[i].lastReadSecs;
                      }
                      if (!largestDate || locations.gateway[i].lastReadSecs > largestDate) {
                        largestDate = locations.gateway[i].lastReadSecs;
                      }
                    }
                  }
                }
              }
              $scope.smallestDate[1] = smallestDate;
              $scope.largestDate[1] = largestDate;
              $scope.absoluteSmallestDate[1] = smallestDate;
              $scope.absoluteLargestDate[1] = largestDate;


              console.log(locations);
              $scope.locationList = locations;
            }, function (err) {
              $rootScope.handleInternetErrorMessage("Could not retrieve inventory list from server", err);
            });
          },
          loadingMessage = function (message, cancelFunction) {
            if (!cancelFunction) {
              return {template: '<ion-spinner class="spinner-assertive huge"></ion-spinner><h3>' + message + '</h3>', delay: 100};
            }
            return {template: '<ion-spinner class="spinner-assertive huge"></ion-spinner><h3>' + message +
              '</h3><button class="button button-block button-transparent icon icon-right ion-close-circled" ng-click="' + cancelFunction + '()">Cancel</button>', delay: 100};
          };

        $scope.locationFilter = {};
        $scope.smallestDate = [];
        $scope.largestDate = [];
        $scope.absoluteSmallestDate = [];
        $scope.absoluteLargestDate = [];

        $scope.formatDate = function (timeSecs) {
          if (!timeSecs || timeSecs < july2017Cutoff) {
            return undefined;
          }

          var aDate = new Date(timeSecs * 1000),
            time = aDate.toLocaleTimeString();

          time = time.replace(/\u200E/g, '');//from https://stackoverflow.com/questions/17913681
          time = time.replace(/^([^\d]*\d{1,2}:\d{1,2}):\d{1,2}([^\d]*)$/, '$1$2');
          return aDate.toLocaleDateString() + " " + time;
        };

        $scope.formatDateRange = function (useGateways) {
          if (!useGateways) {
            useGateways = 0;
          }
          if (!$scope.smallestDate[useGateways] || !$scope.largestDate[useGateways] ||
              !($scope.smallestDate[useGateways] > july2017Cutoff) || !($scope.largestDate[useGateways] > july2017Cutoff)) {
            return undefined;
          }
          var date1 = new Date($scope.smallestDate[useGateways] * 1000),
            date2 = new Date($scope.largestDate[useGateways] * 1000);

          return date1.toLocaleDateString() + " - " + date2.toLocaleDateString();
        };

        $scope.setDateRange = function (useGateways) {
          if (!useGateways) {
            useGateways = 0;
          }
          ionicDatePicker.openDoubleDatePicker({
            callback: function (newDate) {
              if (!newDate || !newDate.start || !newDate.end) {
                return;
              }
              var start = Math.round(newDate.start / 1000),
                end = Math.round(newDate.end / 1000) + 24 * 60 * 60 - 1,
                i;

              if (start < $scope.absoluteSmallestDate[useGateways]) {
                start = $scope.absoluteSmallestDate[useGateways];
              }
              if (end > $scope.absoluteLargestDate[useGateways]) {
                end = $scope.absoluteLargestDate[useGateways];
              }
              $scope.smallestDate[useGateways] = start;
              $scope.largestDate[useGateways] = end;
              if (useGateways) {
                if ($scope.locationList && $scope.locationList.gateway && $scope.locationList.gateway.length) {
                  for (i = 0; i < $scope.locationList.gateway.length; i += 1) {
                    if ($scope.locationList.gateway[i]) {
                      if ($scope.locationList.gateway[i].lastReadSecs && $scope.locationList.gateway[i].lastReadSecs <= $scope.largestDate[useGateways] &&
                          $scope.locationList.gateway[i].lastReadSecs >= $scope.smallestDate[useGateways]) {
                        $scope.locationList.gateway[i].isInDateRange = true;
                      } else {
                        $scope.locationList.gateway[i].isInDateRange = false;
                      }
                    }
                  }
                }
              } else {
                if ($scope.locationList && $scope.locationList.mobile && $scope.locationList.mobile.length) {
                  for (i = 0; i < $scope.locationList.mobile.length; i += 1) {
                    if ($scope.locationList.mobile[i]) {
                      if ($scope.locationList.mobile[i].lastReadSecs && $scope.locationList.mobile[i].lastReadSecs <= $scope.largestDate[useGateways] &&
                          $scope.locationList.mobile[i].lastReadSecs >= $scope.smallestDate[useGateways]) {
                        $scope.locationList.mobile[i].isInDateRange = true;
                      } else {
                        $scope.locationList.mobile[i].isInDateRange = false;
                      }
                    }
                  }
                }
              }
            },
            from: new Date($scope.absoluteSmallestDate[useGateways] * 1000),
            to: new Date(),
            inputDate: new Date($scope.smallestDate[useGateways] * 1000),
            inputEnd: new Date($scope.largestDate[useGateways] * 1000)
          });
        };

        $scope.sort = function (field, sortType) {
          var tempList = [],
            i;

          if (!$scope.locationList || (!sortType && (!$scope.locationList.fixed || !$scope.locationList.fixed.length)) ||
              (sortType === 1 && (!$scope.locationList.job || !$scope.locationList.job.length)) ||
              (sortType === 2 && (!$scope.locationList.mobile || !$scope.locationList.mobile.length)) ||
              (sortType === 3 && (!$scope.locationList.gateway || !$scope.locationList.gateway.length))) {
            return;
          }
          if ((!sortType && $scope.lastSortType === field) || (sortType === 1 && $scope.lastJobSortType === field) ||
              (sortType === 2 && $scope.lastMobileSortType === field) || (sortType === 3 && $scope.lastGatewaySortType === field)) {
            if (sortType === 3) {
              for (i = $scope.locationList.gateway.length; i > 0; i -= 1) {
                tempList.push($scope.locationList.gateway[i - 1]);
              }
            } else if (sortType === 2) {
              for (i = $scope.locationList.mobile.length; i > 0; i -= 1) {
                tempList.push($scope.locationList.mobile[i - 1]);
              }
            } else if (sortType === 1) {
              for (i = $scope.locationList.job.length; i > 0; i -= 1) {
                tempList.push($scope.locationList.job[i - 1]);
              }
            } else {
              for (i = $scope.locationList.fixed.length; i > 0; i -= 1) {
                tempList.push($scope.locationList.fixed[i - 1]);
              }
            }
          } else {
            if (sortType === 3) {
              tempList = angular.copy($scope.locationList.gateway);
            } else if (sortType === 2) {
              tempList = angular.copy($scope.locationList.mobile);
            } else if (sortType === 1) {
              tempList = angular.copy($scope.locationList.job);
            } else {
              tempList = angular.copy($scope.locationList.fixed);
            }
            tempList.sort(function (a, b) {
              var compareA, compareB;
              if (!a) {
                return -1;
              }
              if (!b) {
                return 1;
              }
              compareA = a[field];
              compareB = b[field];
              if (compareA === undefined || compareA === null) {
                compareA = "";
              }
              if (compareB === undefined || compareB === null) {
                compareB = "";
              }
              if (compareA && compareA.toLowerCase && compareB && compareB.toLowerCase) {
                compareA = compareA.toLowerCase();
                compareB = compareB.toLowerCase();
              }
              if (compareA < compareB) {
                return -1;
              }
              if (compareA > compareB) {
                return 1;
              }
              return 0;
            });
          }
          if (sortType === 3) {
            $scope.lastGatewaySortType = field;
            $scope.locationList.gateway = tempList;
          } else if (sortType === 2) {
            $scope.lastMobileSortType = field;
            $scope.locationList.mobile = tempList;
          } else if (sortType === 1) {
            $scope.lastJobSortType = field;
            $scope.locationList.job = tempList;
          } else {
            $scope.lastSortType = field;
            $scope.locationList.fixed = tempList;
          }
        };

        $scope.openMoreOptions = function ($event, aLocation) {
          var popoverPromise;
          $scope.aLocation = aLocation;
          popoverPromise = $ionicPopover.fromTemplateUrl('views/Main/fixedOptions.html', {scope: $scope});
          popoverPromise.then(function (popover) {
            $rootScope.popover = popover;
            $rootScope.popover.show($event);
          });
        };

        $scope.deleteLocation = function () {
          if ($rootScope.popover) {
            $rootScope.popover.hide();
            $rootScope.popover = undefined;
          }
          $timeout(function () {
            $rootScope.closePopup = $ionicPopup.confirm({
              title: "Are you sure?",
              scope: $scope,
              template: 'Do you really want to delete this location?' + ($scope.aLocation && $scope.aLocation.locationType === 'Mobile' ?
                         ' Any phones that have the name "' + ($scope.aLocation && $scope.aLocation.name) +
                         '" would no longer be tracked on this website unless that name was manually chosen again using the DryLINK app.' : ''),
              okText: "OK",
              okType: "right-button",
              cancelText: "CANCEL",
              cancelType: "left-button"
            });
            $rootScope.closePopup.then(function (res) {
              if (res) {
                $ionicLoading.show();
                innerDelete().then(function () {
                  initializeList();
                }, function (err) {
                  $rootScope.handleInternetErrorMessage("Could not delete location", err);
                });
              }
            });
          }, 100);
        };

        $scope.addLocationOrEdit = function (isExisting) {
          if ($rootScope.popover) {
            $rootScope.popover.hide();
            $rootScope.popover = undefined;
          }
          if (isExisting) {
            $scope.newLocation = angular.copy($scope.aLocation) || {};
          } else {
            $scope.newLocation = {};
          }
          $scope.formattedAddress = undefined;
          $timeout(function () {
            $rootScope.closePopup = $ionicPopup.show({
              title: isExisting ? "Edit Location" : "New Fixed Location",
              templateUrl: 'views/Main/createLocation.html',
              scope: $scope,
              buttons: [{
                text: "CANCEL",
                type: 'left-button',
                onTap: function (e) {
                  e.preventDefault();
                  $timeout(function () {
                    if (!$rootScope.disablePopupButtons) {
                      $rootScope.closePopup.close();
                    }
                  }, 200);
                }
              }, {
                text: "SAVE",
                type: 'right-button',
                onTap: function (e) {
                  e.preventDefault();
                  $timeout(function () {
                    if (!$rootScope.disablePopupButtons) {
                      $rootScope.closePopup.close();
                      $ionicLoading.show();
                      if (isExisting && $scope.aLocation && $scope.aLocation.invLocationId) {
                        innerDelete().then(function () {
                          if ($scope.aLocation) {
                            $scope.aLocation.invLocationId = undefined;
                          }
                          Job.createLocation($scope.newLocation).then(function () {
                            initializeList();
                          }, function (err) {
                            $rootScope.handleInternetErrorMessage("Could not edit location", err);
                          });
                        }, function (err) {
                          $rootScope.handleInternetErrorMessage("Could not edit location", err);
                        });
                      } else {
                        Job.createLocation($scope.newLocation).then(function () {
                          initializeList();
                        }, function (err) {
                          $rootScope.handleInternetErrorMessage("Could not create location", err);
                        });
                      }
                    }
                  }, 200);
                }
              }]
            });
          }, 100);

          $timeout(function () {
            var addressInput,
              autocomplete;

            try {
              addressInput = document.getElementById('nj-address-field');
              if (!addressInput) {
                $rootScope.useBackUpAddresses = true;
                return;
              }
              $rootScope.useBackUpAddresses = false;
              autocomplete = new google.maps.places.Autocomplete(addressInput, {types: ['address']});
              autocomplete.setFields(['address_components', 'formatted_address']);
              autocomplete.addListener('place_changed', function () {
                if (enableButtonTimeout) {
                  $timeout.cancel(enableButtonTimeout);
                }
                $rootScope.disablePopupButtons = true;
                enableButtonTimeout = $timeout(function () {
                  $rootScope.disablePopupButtons = false;
                }, 500);
                var place = autocomplete.getPlace(),
                  i,
                  j,
                  translationTable = [{type: 'address', name: 'street_number', name2: 'route'},
                                      {type: 'city', name: 'locality'},
                                      {type: 'state', name: 'administrative_area_level_1'},
                                      {type: 'country', name: 'country'},
                                      {type: 'zip', name: 'postal_code'}];

                if (place && place.address_components && place.address_components.length) {
                  for (i = 0; i < place.address_components.length; i += 1) {
                    if (place.address_components[i] && place.address_components[i].types && place.address_components[i].short_name) {
                      for (j = 0; j < translationTable.length; j += 1) {
                        if ((translationTable[j].name && place.address_components[i].types.indexOf(translationTable[j].name) > -1) ||
                            (translationTable[j].name2 && place.address_components[i].types.indexOf(translationTable[j].name2) > -1)) {
                          if (translationTable[j].value) {
                            translationTable[j].value += " " + place.address_components[i].short_name;
                          } else {
                            translationTable[j].value = place.address_components[i].short_name;
                          }
                          break;
                        }
                      }
                    }
                  }
                  $timeout(function () {
                    for (j = 0; j < translationTable.length; j += 1) {
                      $scope.newLocation[translationTable[j].type] = translationTable[j].value;
                    }
                  }, 0);
                }
                if (place && place.formatted_address) {
                  $scope.formattedAddress = place.formatted_address;
                }
              });
            } catch (e) {
              $rootScope.useBackUpAddresses = true;
              $rootScope.reloadAutoComplete();
            }
          }, 500);
        };

        $scope.addGatewayOrEdit = function (isExisting) {
          if ($rootScope.popover) {
            $rootScope.popover.hide();
            $rootScope.popover = undefined;
          }
          if (isExisting) {
            $scope.newLocation = angular.copy($scope.aLocation) || {};
          } else {
            $scope.newLocation = {};
          }
          $scope.hideGatewayUUID = isExisting;
          $timeout(function () {
            $rootScope.closePopup = $ionicPopup.confirm({
              title: isExisting ? "Edit Gateway" : "New Gateway",
              templateUrl: 'views/Main/createGateway.html',
              scope: $scope,
              okText: "OK",
              okType: "right-button",
              cancelText: "CANCEL",
              cancelType: "left-button"
            });
            $rootScope.closePopup.then(function (res) {
              if (res) {
                $ionicLoading.show();
                Job.createOrUpdateGateway($scope.newLocation.gatewayUuid, $scope.newLocation.name, isExisting).then(function () {
                  initializeList();
                }, function (err) {
                  $rootScope.handleInternetErrorMessage(isExisting ? "Could not edit gateway" : "Could not create gateway", err);
                });
              }
            });
          }, 100);
        };

        $rootScope.hideLoading = function () {
          $ionicLoading.show(loadingMessage("Stopping..."));
          $timeout(function () {
            $ionicLoading.hide();
          }, 300);
        };

        $ionicLoading.show(loadingMessage("Generating list...", "hideLoading"));
        initializeList();
      }];
  angular.module('User')
    .config(locationListConfig)
    .controller('LocationListController', locationListCtrl);
}());

(function () {
  'use strict';
  var syncConfig = ['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('app.syncEquipment', {
        url: '/sync/:clientId/:clientName',
        views: {
          main: {
            templateUrl: "views/Main/syncEquipment.html",
            controller: 'SyncController'
          }
        }
      });
  }],
    syncCtrl = ['$scope', '$rootScope', '$state', 'User', '$ionicLoading', '$timeout', '$stateParams', '$q',
      function ($scope, $rootScope, $state, User, $ionicLoading, $timeout, $stateParams, $q) {
        $scope.syncList = {};
        $scope.headers = {};
        $scope.clientName = $stateParams.clientName;

        $scope.equipmentFilter = {};
        $scope.columnPreferences = {equipName: true, serialNumber: true, drytagUuid: true, typeName: true, trueStatus: true, manufacturer: true, model: true};
        $scope.columnList = [
          { displayName: 'Equipment Name',
            serverName: 'equipName',
            allowFilter: true,
            allowSort: true,
            increaseWidth: true },
          { displayName: 'Serial Number',
            serverName: 'serialNumber',
            allowFilter: true,
            allowSort: true,
            increaseWidth: true },
          { displayName: 'DryTAG UUID',
            serverName: 'drytagUuid',
            allowFilter: true,
            allowSort: true,
            increaseWidth: true },
          { displayName: 'Type',
            serverName: 'equipType',
            allowFilter: true,
            allowSort: true,
            increaseWidth: false },
          { displayName: 'Type',
            serverName: 'typeName',
            allowFilter: true,
            allowSort: true,
            increaseWidth: false },
          { displayName: 'Manufacturer',
            serverName: 'manufacturer',
            allowFilter: true,
            allowSort: true,
            increaseWidth: false },
          { displayName: 'Model',
            serverName: 'model',
            allowFilter: true,
            allowSort: true,
            increaseWidth: false },
          { displayName: 'Status',
            serverName: 'equipStatus',
            allowFilter: true,
            allowSort: true,
            increaseWidth: false },
          { displayName: 'Error Status',
            serverName: 'errorStatus',
            allowFilter: false,
            allowSort: false,
            increaseWidth: false },
          { displayName: 'Status',
            serverName: 'trueStatus',
            allowFilter: false,
            allowSort: false,
            isStatus: true,
            increaseWidth: false },
          { displayName: 'Sync Status',
            serverName: 'syncStatus',
            allowFilter: true,
            allowSort: true,
            increaseWidth: false }
        ];

        $scope.sortAList = function (field, list, lastSortType) {
          var tempList = [],
            i;

          if (!list || !list.length) {
            return [];
          }
          if (lastSortType === field) {
            for (i = list.length; i > 0; i -= 1) {
              tempList.push(list[i - 1]);
            }
          } else {
            tempList = angular.copy(list);
            tempList.sort(function (a, b) {
              var compareA, compareB;
              if (!a) {
                return -1;
              }
              if (!b) {
                return 1;
              }
              compareA = a[field];
              compareB = b[field];
              if (compareA === undefined || compareA === null) {
                compareA = "";
              }
              if (compareB === undefined || compareB === null) {
                compareB = "";
              }
              if (compareA && compareA.toLowerCase && compareB && compareB.toLowerCase) {
                compareA = compareA.toLowerCase();
                compareB = compareB.toLowerCase();
              }
              if (compareA < compareB) {
                return -1;
              }
              if (compareA > compareB) {
                return 1;
              }
              return 0;
            });
          }
          return tempList;
        };

        $scope.sort = function (field, isSortable, notErr, notSync) {
          if (!$scope.syncList || !isSortable) {
            return;
          }
          var errList = notErr ? $scope.syncList.errors : $scope.sortAList(field, $scope.syncList.errors, $scope.lastSortTypeErr),
            syncList = notSync ? $scope.syncList.synced : $scope.sortAList(field, $scope.syncList.synced, $scope.lastSortTypeSynced);
          if (!notErr) {
            $scope.lastSortTypeErr = field;
          }
          if (!notSync) {
            $scope.lastSortTypeSynced = field;
          }
          $scope.syncList = {errors: errList, synced: syncList};
        };

        $scope.cancel = function () {
          //$ionicHistory.goBack();
          $state.go('app.integrations');
        };

        $scope.selectAll = function (isErrList) {
          var aList = isErrList ? $scope.syncList.errors : $scope.syncList.synced,
            hadABlank = false,
            i;

          for (i = 0; i < aList.length; i += 1) {
            if (aList[i] && !aList[i].shouldSyncIt) {
              hadABlank = true;
              aList[i].shouldSyncIt = true;
            }
          }

          if (!hadABlank) {
            for (i = 0; i < aList.length; i += 1) {
              if (aList[i]) {
                aList[i].shouldSyncIt = false;
              }
            }
            if (isErrList) {
              $scope.headers.err = false;
            } else {
              $scope.headers.sync = false;
            }
          } else {
            if (isErrList) {
              $scope.headers.err = true;
            } else {
              $scope.headers.sync = true;
            }
          }
        };

        $scope.finish = function () {
          var j,
            temp,
            promises = [];
          $ionicLoading.show();
          $scope.lastSortTypeErr = undefined;
          $scope.lastSortTypeSynced = undefined;
          if ($scope.syncList) {
            if ($scope.syncList.errors && $scope.syncList.errors.length) {
              for (j = 0; j < $scope.syncList.errors.length; j += 1) {
                if ($scope.syncList.errors[j] && $scope.syncList.errors[j].shouldSyncIt) {
                  temp = $scope.syncList.errors.splice(j, 1);
                  j -= 1;
                  promises.push(User.updateSyncStatus($stateParams.clientId, temp[0]));
                }
              }
            }
            if ($scope.syncList.synced && $scope.syncList.synced.length) {
              for (j = 0; j < $scope.syncList.synced.length; j += 1) {
                if ($scope.syncList.synced[j] && $scope.syncList.synced[j].shouldSyncIt) {
                  temp = $scope.syncList.synced.splice(j, 1);
                  j -= 1;
                  promises.push(User.updateSyncStatus($stateParams.clientId, temp[0]));
                }
              }
            }
          }
          $q.all(promises).then(function (newData) {
            $timeout(function () {
              $ionicLoading.hide();
            }, 100);
            for (j = 0; j < newData.length; j += 1) {
              if (newData[j]) {
                newData[j].shouldSyncIt = true;
                if (newData[j].trueStatus) {
                  $scope.syncList.errors.push(newData[j]);
                } else {
                  $scope.syncList.synced.push(newData[j]);
                }
              }
            }
          }, function (err) {
            $rootScope.handleInternetErrorMessage("Could not sync equipment.", err);
          });
        };

        $ionicLoading.show();
        User.getSyncStatus($stateParams.clientId).then(function (syncList) {
          $scope.syncList = syncList;

          $timeout(function () {
            $ionicLoading.hide();
          }, 100);
        }, function (err) {
          $rootScope.handleInternetErrorMessage("Could not retrieve integration information from server.", err);
        });
      }];
  angular.module('User')
    .config(syncConfig)
    .controller('SyncController', syncCtrl);
}());

(function () {
  'use strict';
  var forgotPassConfig = ['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('forgotPassword', {
        url: '/forgotPassword',
        templateUrl: 'views/SignIn/ForgotPassword.html',
        controller: 'ForgotPasswordController as vm'
      });
  }],
    forgotPassCtrl = ['$state', 'Authentication', '$ionicPopup', '$ionicLoading', 'User', '$rootScope',
      function ($state, Authentication, $ionicPopup, $ionicLoading, User, $rootScope) {
        var vm = this;

        vm.forgotPassword = function () {
          $ionicLoading.show();
          Authentication.forgotPassword(vm.emailAddress).then(function () {
            $ionicLoading.hide();
            $ionicPopup.alert({
              title: 'Password Reset',
              template: 'Please check your email for further instructions'
            }).then(function () {
              $state.go('login');
            });
          }, function (err) {
            if (err && err.status === 412) {
              User.retryEmail(vm.emailAddress).then(function () {
                $ionicLoading.hide();
                $ionicPopup.alert({
                  title: 'Verify email address',
                  template: 'An email has been sent to you--use it to verify your address before resetting your password.'
                });
              }, function (error) {
                $rootScope.preLoginErrorMessage('Error Verifying Email', error);
              });
            } else {
              $rootScope.preLoginErrorMessage('Error Resetting Password', err);
            }
          });
        };
      }];
  angular.module('User')
    .config(forgotPassConfig)
    .controller('ForgotPasswordController', forgotPassCtrl);
}());

// SignIn.user.js
(function () {
  'use strict';
  var LoginCtrl = [
    '$state',
    'Authentication',
    'User',
    '$ionicLoading',
    '$ionicPopup',
    '$timeout',
    '$scope',
    '$localStorage',
    '$rootScope',
    function ($state, Authentication, User, $ionicLoading, $ionicPopup, $timeout, $scope, $localStorage, $rootScope) {
      $scope.user = {};
      $scope.signIn = function () {
        $ionicLoading.show();
        User.authenticate($scope.user)
          .then(function () {// (user) {
            //console.log(user);
            $localStorage.userEmail = $scope.user.email;
            // if (!$rootScope.welcomed && !$localStorage.alreadyWelcomed) {
            //   $ionicLoading.hide();
            //   $state.go('dehuExplanation');
            // } else {
            User.getLegalStuff().then(function (legalStuff) {
              $ionicLoading.hide();
              if (legalStuff && legalStuff.length) {
                $rootScope.legalStuff = legalStuff;
                $state.go('legalStuff');
              } else {
                $state.go('app.home');
              }
            }, function () {
              $ionicLoading.hide();
              $state.go('app.home');
            });
            // }
          }, function (err) {
            if (err && err.status === 412) {
              User.retryEmail($scope.user.email).then(function () {
                $ionicLoading.hide();
                $ionicPopup.alert({
                  title: 'Verify email address',
                  template: 'An email has been sent to you--use it to verify your address before logging in.'
                });
              }, function (error) {
                $rootScope.preLoginErrorMessage('Error Verifying Email', error);
              });
            } else {
              $rootScope.preLoginErrorMessage('Error Logging In', err);
            }
          });
      };
      $ionicLoading.show();
      $scope.showPassword = false;
      // $scope.allowSkip = $localStorage.alreadyWelcomed;

      if ($localStorage.token) {
        // if ($localStorage.createUser && ($localStorage.createUser.userType === 'admin' || $localStorage.createUser.userType === 'owner')) {
        //   $state.go('initialAddUsers');
        // } else if ($localStorage.createUser || (!$rootScope.welcomed && !$localStorage.alreadyWelcomed)) {
        //   $state.go('dehuExplanation');
        // } else {
        //   $state.go('app.home');
        // }
        $state.go('app.home');
      } else if ($localStorage.createUser) {
        $state.go('signupWait');
      // } else if (!$rootScope.welcomed && !$localStorage.alreadyWelcomed) {
      //   $state.go('welcome');
      } else {
        Authentication.login().then(function () {
          $state.go('app.home');
        }, function (err) {
          $timeout(function () {
            $ionicLoading.hide();
          }, 0);
          if ($localStorage.token) {
            if (!err.status || err.status < 1) {
              $ionicPopup.alert({title: 'No internet connection', template: 'You will be able to download data over Bluetooth but not upload that data to the cloud.'});
            }
            $state.go('app.home');
          } else {
            $scope.initializationFinished = true;
          }
        });
      }
      $timeout(function () {
        $scope.tenSecondsPassed = true;
      }, 10000);
    }],
    LoginConfig = ['$stateProvider', function ($stateProvider) {
      $stateProvider
        .state('login', {
          url: '/login',
          templateUrl: 'views/SignIn/SignIn.user.html',
          controller: 'LoginController as vm'
        });
    }];

  angular.module('User')
    .config(LoginConfig)
    .controller('LoginController', LoginCtrl);
}());

(function () {
  'use strict';
  var legalStuffConfig = ['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('legalStuff', {
        url: '/legalstuff',
        templateUrl: 'views/SignIn/legalStuff.html',
        controller: 'legalStuffController'
      });
  }],
    legalStuffCtrl = ['$ionicLoading', 'User', '$timeout', '$scope', '$rootScope', '$state', '$q', '$ionicScrollDelegate',
      function ($ionicLoading, User, $timeout, $scope, $rootScope, $state, $q, $ionicScrollDelegate) {
        var displayLicenseAgreement = function () {
          if (!$rootScope.legalStuff || !$rootScope.legalStuff[0] || !$rootScope.legalStuff[0].docText || !$rootScope.legalStuff[0].docText.replace) {
            $scope.legalBlurb = "";
            return;
          }
          $scope.legalBlurb = $rootScope.legalStuff[0].docText.replace(/\r\n/g, '<br>').replace(/\r/g, '<br>').replace(/\n/g, '<br>');
        }, scrollingTextField;

        $scope.finish = function () {
          var displayNext = $q.defer();

          if ($rootScope.legalStuff && $rootScope.legalStuff.length) {
            if ($rootScope.legalStuff[0] && $rootScope.legalStuff[0].docId) {
              $ionicLoading.show();
              User.legalAgreement($rootScope.legalStuff[0].docId).then(function () {
                $timeout(function () {
                  $ionicLoading.hide();
                }, 100);
                $rootScope.legalStuff.splice(0, 1);
                displayNext.resolve();
              }, function (err) {
                $rootScope.handleInternetErrorMessage("Could not accept license agreement.", err);
                displayNext.reject();
              });
            } else {
              $rootScope.legalStuff.splice(0, 1);
              displayNext.resolve();
            }
          } else {
            displayNext.resolve();
          }

          displayNext.promise.then(function () {
            if ($rootScope.legalStuff && $rootScope.legalStuff.length) {
              displayLicenseAgreement();
              $ionicScrollDelegate.scrollTop();
              $scope.finishedReading = false;
            } else {
              $state.go('app.home');
            }
          });
        };

        if ($rootScope.legalStuff && $rootScope.legalStuff.length) {
          displayLicenseAgreement();
        } else {
          $ionicLoading.show();
          User.getLegalStuff().then(function (legalStuff) {
            $timeout(function () {
              $ionicLoading.hide();
            }, 100);
            if (legalStuff && legalStuff.length) {
              $rootScope.legalStuff = legalStuff;
              displayLicenseAgreement();
            } else {
              $state.go('app.home');
            }
          }, function (err) {
            $rootScope.handleInternetErrorMessage("Could not download license agreements.", err);
          });
        }

        scrollingTextField = angular.element(document.getElementById('scrollingTextField'));
        scrollingTextField.on('scroll', function (newPosition) {
          var scrollField = (scrollingTextField && scrollingTextField[0]) || {},
            scrollTop = (newPosition && newPosition.detail && newPosition.detail.scrollTop) || scrollField.scrollTop;

          if (scrollTop + scrollField.offsetHeight + 10 >= scrollField.scrollHeight) {
            $timeout(function () {
              $scope.finishedReading = true;
            }, 0);
          }
        });
      }];
  angular.module('User')
    .config(legalStuffConfig)
    .controller('legalStuffController', legalStuffCtrl);
}());

(function () {
  'use strict';
  var SignUpCtrl = [
    '$state',
    '$ionicLoading',
    '$ionicPopup',
    '$timeout',
    'User',
    '$scope',
    '$rootScope',
    '$localStorage',
    function ($state, $ionicLoading, $ionicPopup, $timeout, User, $scope, $rootScope, $localStorage) {
      var myStringify = function (x) {
        return JSON.stringify(x).replace(/"/g, "");
      };

      if ($rootScope.createUser) {
        $rootScope.createUser.hasEmail = true;
        $rootScope.createUser.hasSMS = false;
      } else {
        $state.go('enterEmail');
      }

      $scope.signUp = function () {
        if ($rootScope.createUser.pwd !== $rootScope.createUser.confirmPwd) {
          $ionicPopup.alert({title: 'Invalid Password Entry', template: '"Confirm Password" does not match "Password"'});
        } else if (!$rootScope.createUser.phone) {
          $ionicPopup.alert({title: 'Phone Number Required', template: 'Enter a phone number to create an account'});
        } else {
          $ionicLoading.show();
          User.create($rootScope.createUser)
            .then(function (user) {
              $timeout(function () {
                $ionicLoading.hide();
              }, 100);
              //$ionicPopup.alert({title: 'Success', template: 'Your email address has been sent a verification email that will let you activate your account.'});
              //$state.go('login');
              $localStorage.createUser = $rootScope.createUser;
              $localStorage.createUser.accountId = user && user.id;
              $state.go('signupWait');
            }, function (err) {
              if (err && err.status === 409) {
                $timeout(function () {
                  $ionicLoading.hide();
                }, 100);
                $ionicPopup.alert({
                  title: 'Error Signing Up',
                  template: myStringify(err.message || (err.data && err.data.message) || err.data || err)
                }).then(function () {
                  $state.go('enterEmail');
                });
              } else {
                $rootScope.preLoginErrorMessage('Error Signing Up', err);
              }
            });
        }
      };
      $scope.showPassword = false;
    }],
    SignUpConfig = ['$stateProvider', function ($stateProvider) {
      $stateProvider
        .state('signUp', {
          url: '/signUp',
          templateUrl: 'views/SignUp/SignUp.user.html',
          controller: 'SignUpController as vm'
        });
    }];

  angular.module('User')
    .config(SignUpConfig)
    .controller('SignUpController', SignUpCtrl);
}());

(function () {
  'use strict';
  var dehuExplanationConfig = ['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('dehuExplanation', {
        url: '/dehuExplanation',
        templateUrl: 'views/SignUp/dehuExplanation.html',
        controller: 'dehuExplanationController'
      });
  }],
    dehuExplanationCtrl = ['$ionicLoading', 'User', '$timeout', '$scope', '$rootScope', '$state',
      function ($ionicLoading, User, $timeout, $scope, $rootScope, $state) {
        $scope.checkForLegalStuff = function () {
          User.getLegalStuff().then(function (legalStuff) {
            $timeout(function () {
              $ionicLoading.hide();
            }, 100);
            if (legalStuff && legalStuff.length) {
              $rootScope.legalStuff = legalStuff;
              $state.go('legalStuff');
            } else {
              $state.go('app.home');
            }
          }, function () {
            $timeout(function () {
              $ionicLoading.hide();
            }, 100);
            $state.go('app.home');
          });
        };

        $ionicLoading.hide();
      }];
  angular.module('User')
    .config(dehuExplanationConfig)
    .controller('dehuExplanationController', dehuExplanationCtrl);
}());

(function () {
  'use strict';
  var enterEmailCtrl = ['$state', '$scope', '$rootScope', '$ionicLoading', '$timeout', '$ionicPopup', '$q', 'User',
    function ($state, $scope, $rootScope, $ionicLoading, $timeout, $ionicPopup, $q, User) {
      //Authentication.logOut();

      $scope.user = {};
      $scope.enterEmail = function () {
        $ionicLoading.show();
        User.checkForInvitations($scope.user.email).then(function (invitation) {
          var invitationPromise = $q.defer();
          $rootScope.createUser = {email: $scope.user.email};

          $timeout(function () {
            $ionicLoading.hide();
          }, 100);

          if (invitation && invitation.companyId) {
            $rootScope.closePopup = $ionicPopup.confirm({
              title: "Invitation found",
              template: $scope.user.email + ' has been invited to join the company "' + invitation.companyName + '"<br><br> Do you want to accept this invitation?',
              okText: "ACCEPT",
              okType: "right-button",
              cancelText: "REJECT",
              cancelType: "left-button"
            });

            $rootScope.closePopup.then(function (res) {
              if (res) {
                $rootScope.createUser.companyId = invitation.companyId;
                $rootScope.createUser.userType = invitation.inviteeType;
              }
              invitationPromise.resolve();
            });
          } else {
            invitationPromise.resolve();
          }

          invitationPromise.promise.then(function () {
            $state.go('signUp');
          });
        }, function (err) {
          $rootScope.preLoginErrorMessage("Email address lookup failed.", err);
        });
      };
    }],
    enterEmailConfig = ['$stateProvider', function ($stateProvider) {
      $stateProvider
        .state('enterEmail', {
          url: '/enterEmail',
          templateUrl: 'views/SignUp/enterEmail.html',
          controller: 'enterEmailController as vm'
        });
    }];

  angular.module('User')
    .config(enterEmailConfig)
    .controller('enterEmailController', enterEmailCtrl);
}());

(function () {
  'use strict';
  var initialAddUsersConfig = ['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('initialAddUsers', {
        url: '/initialAddUsers',
        templateUrl: 'views/SignUp/initialAddUsers.html',
        controller: 'initialAddUsersController'
      });
  }],
    initialAddUsersCtrl = ['$ionicLoading', '$scope', '$rootScope', '$ionicPopup', 'User', '$timeout',
      function ($ionicLoading, $scope, $rootScope, $ionicPopup, User, $timeout) {
        $ionicLoading.hide();

        $scope.invitations = [];
        $scope.roles = [{name: "Administrator", userType: "admin"}, {name: "Technician", userType: "fulltech"}];

        $scope.inviteEmployee = function () {
          $scope.chosen = {role: 'fulltech'};
          $rootScope.closePopup = $ionicPopup.confirm({
            title: "Invite New Users to Join your Company",
            templateUrl: 'views/Bookkeeping/sendInvite.html',
            scope: $scope,
            okText: "SEND",
            okType: "right-button",
            cancelText: "CANCEL",
            cancelType: "left-button"
          });
          $rootScope.closePopup.then(function (res) {
            if (res) {
              $ionicLoading.show();
              User.sendInvitation($scope.chosen.email, $scope.chosen.role).then(function () {
                $timeout(function () {
                  $ionicLoading.hide();
                }, 100);

                $scope.invitations.push($scope.chosen.email);
              }, function (err) {
                $rootScope.handleInternetErrorMessage("Could not send invitation.", err);
              });
            }
          });
        };
      }];
  angular.module('User')
    .config(initialAddUsersConfig)
    .controller('initialAddUsersController', initialAddUsersCtrl);
}());

(function () {
  'use strict';
  var signupWaitCtrl = [
    '$state',
    '$ionicLoading',
    '$timeout',
    'User',
    '$scope',
    '$rootScope',
    '$localStorage',
    '$ionicPlatform',
    function ($state, $ionicLoading, $timeout, User, $scope, $rootScope, $localStorage, $ionicPlatform) {
      var cancelSignup = function () {
        delete $localStorage.createUser;
        $state.go('login');
      }, tryLogin = function () {
        var finished = false;

        if ($state.current.url === '/signupWait' && $localStorage.createUser) {
          if ($rootScope.currentSignupAttempt) {
            $timeout.cancel($rootScope.currentSignupAttempt);
          }
          $localStorage.createUser.password = $localStorage.createUser.pwd;//Diane uses different names for the same field in different endpoints, for some reason.
          User.authenticate($localStorage.createUser).then(function () {
            finished = true;
            if ($state.current.url === '/signupWait' && $localStorage.createUser) {
              $localStorage.userEmail = $localStorage.createUser.email;

              // if (!$localStorage.createUser.companyId || $localStorage.createUser.userType === 'admin' || $localStorage.createUser.userType === 'owner') {
              //   $state.go('initialAddUsers');
              // } else {
              //   $state.go('dehuExplanation');
              // }
              $state.go('app.home');
            }
          }, function () {
            finished = true;
            $rootScope.currentSignupAttempt = $timeout(function () {
              tryLogin();
            }, 2000);
          });

          if ($rootScope.currentSignupAttempt) {
            $timeout.cancel($rootScope.currentSignupAttempt);
          }
          $rootScope.currentSignupAttempt = $timeout(function () {
            if (!finished) {
              tryLogin();
            }
          }, 30000);
        }
      }, retrySignup = function () {
        User.create($localStorage.createUser).then(function (user) {
          $timeout(function () {
            $ionicLoading.hide();
          }, 100);
          $localStorage.createUser.accountId = user && user.id;
        }, function (err) {
          $rootScope.preLoginErrorMessage('Could not resend email', err);
        });
      };

      if ($localStorage.token) {
        //$state.go('dehuExplanation');
        $state.go('app.home');
      } else if (!$localStorage.createUser) {
        $state.go('login');
      }
      $scope.myEmail = $localStorage.createUser && $localStorage.createUser.email;
      $ionicLoading.hide();

      $scope.retrySignup = function () {
        $ionicLoading.show();
        if ($localStorage.createUser && $localStorage.createUser.accountId) {
          User.cancelAccount($localStorage.createUser.accountId).then(function () {
            retrySignup();
          }, function (err) {
            $rootScope.preLoginErrorMessage('Could not resend email', err);
          });
        } else {
          retrySignup();
        }
      };

      $scope.cancelSignup = function () {
        if ($localStorage.createUser && $localStorage.createUser.accountId) {
          $ionicLoading.show();
          User.cancelAccount($localStorage.createUser.accountId).then(function () {
            $timeout(function () {
              $ionicLoading.hide();
            }, 100);
            cancelSignup();
          }, function (err) {
            $rootScope.preLoginErrorMessage('Could not cancel sign-up attempt', err);
          });
        } else {
          cancelSignup();
        }
      };

      if ($rootScope.removeResumeListener) {
        $rootScope.removeResumeListener();
      }
      $rootScope.removeResumeListener = $ionicPlatform.on("resume", function () {
        tryLogin();
      });
      tryLogin();
    }],
    signupWaitConfig = ['$stateProvider', function ($stateProvider) {
      $stateProvider
        .state('signupWait', {
          url: '/signupWait',
          templateUrl: 'views/SignUp/signupWait.html',
          controller: 'signupWaitController'
        });
    }];

  angular.module('User')
    .config(signupWaitConfig)
    .controller('signupWaitController', signupWaitCtrl);
}());

(function () {
  'use strict';
  var welcomeConfig = ['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('welcome', {
        url: '/welcome',
        templateUrl: 'views/SignUp/welcome.html',
        controller: 'welcomeController'
      });
  }],
    welcomeCtrl = ['$ionicLoading', '$rootScope',
      function ($ionicLoading, $rootScope) {
        $ionicLoading.hide();
        $rootScope.welcomed = true;
      }];
  angular.module('User')
    .config(welcomeConfig)
    .controller('welcomeController', welcomeCtrl);
}());
