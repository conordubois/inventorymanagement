(function () {
  'use strict';
  var forgotPassConfig = ['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('forgotPassword', {
        url: '/forgotPassword',
        templateUrl: 'views/SignIn/ForgotPassword.html',
        controller: 'ForgotPasswordController as vm'
      });
  }],
    forgotPassCtrl = ['$state', 'Authentication', '$ionicPopup', '$ionicLoading', 'User', '$rootScope',
      function ($state, Authentication, $ionicPopup, $ionicLoading, User, $rootScope) {
        var vm = this;

        vm.forgotPassword = function () {
          $ionicLoading.show();
          Authentication.forgotPassword(vm.emailAddress).then(function () {
            $ionicLoading.hide();
            $ionicPopup.alert({
              title: 'Password Reset',
              template: 'Please check your email for further instructions'
            }).then(function () {
              $state.go('login');
            });
          }, function (err) {
            if (err && err.status === 412) {
              User.retryEmail(vm.emailAddress).then(function () {
                $ionicLoading.hide();
                $ionicPopup.alert({
                  title: 'Verify email address',
                  template: 'An email has been sent to you--use it to verify your address before resetting your password.'
                });
              }, function (error) {
                $rootScope.preLoginErrorMessage('Error Verifying Email', error);
              });
            } else {
              $rootScope.preLoginErrorMessage('Error Resetting Password', err);
            }
          });
        };
      }];
  angular.module('User')
    .config(forgotPassConfig)
    .controller('ForgotPasswordController', forgotPassCtrl);
}());
