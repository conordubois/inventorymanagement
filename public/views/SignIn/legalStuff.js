(function () {
  'use strict';
  var legalStuffConfig = ['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('legalStuff', {
        url: '/legalstuff',
        templateUrl: 'views/SignIn/legalStuff.html',
        controller: 'legalStuffController'
      });
  }],
    legalStuffCtrl = ['$ionicLoading', 'User', '$timeout', '$scope', '$rootScope', '$state', '$q', '$ionicScrollDelegate',
      function ($ionicLoading, User, $timeout, $scope, $rootScope, $state, $q, $ionicScrollDelegate) {
        var displayLicenseAgreement = function () {
          if (!$rootScope.legalStuff || !$rootScope.legalStuff[0] || !$rootScope.legalStuff[0].docText || !$rootScope.legalStuff[0].docText.replace) {
            $scope.legalBlurb = "";
            return;
          }
          $scope.legalBlurb = $rootScope.legalStuff[0].docText.replace(/\r\n/g, '<br>').replace(/\r/g, '<br>').replace(/\n/g, '<br>');
        }, scrollingTextField;

        $scope.finish = function () {
          var displayNext = $q.defer();

          if ($rootScope.legalStuff && $rootScope.legalStuff.length) {
            if ($rootScope.legalStuff[0] && $rootScope.legalStuff[0].docId) {
              $ionicLoading.show();
              User.legalAgreement($rootScope.legalStuff[0].docId).then(function () {
                $timeout(function () {
                  $ionicLoading.hide();
                }, 100);
                $rootScope.legalStuff.splice(0, 1);
                displayNext.resolve();
              }, function (err) {
                $rootScope.handleInternetErrorMessage("Could not accept license agreement.", err);
                displayNext.reject();
              });
            } else {
              $rootScope.legalStuff.splice(0, 1);
              displayNext.resolve();
            }
          } else {
            displayNext.resolve();
          }

          displayNext.promise.then(function () {
            if ($rootScope.legalStuff && $rootScope.legalStuff.length) {
              displayLicenseAgreement();
              $ionicScrollDelegate.scrollTop();
              $scope.finishedReading = false;
            } else {
              $state.go('app.home');
            }
          });
        };

        if ($rootScope.legalStuff && $rootScope.legalStuff.length) {
          displayLicenseAgreement();
        } else {
          $ionicLoading.show();
          User.getLegalStuff().then(function (legalStuff) {
            $timeout(function () {
              $ionicLoading.hide();
            }, 100);
            if (legalStuff && legalStuff.length) {
              $rootScope.legalStuff = legalStuff;
              displayLicenseAgreement();
            } else {
              $state.go('app.home');
            }
          }, function (err) {
            $rootScope.handleInternetErrorMessage("Could not download license agreements.", err);
          });
        }

        scrollingTextField = angular.element(document.getElementById('scrollingTextField'));
        scrollingTextField.on('scroll', function (newPosition) {
          var scrollField = (scrollingTextField && scrollingTextField[0]) || {},
            scrollTop = (newPosition && newPosition.detail && newPosition.detail.scrollTop) || scrollField.scrollTop;

          if (scrollTop + scrollField.offsetHeight + 10 >= scrollField.scrollHeight) {
            $timeout(function () {
              $scope.finishedReading = true;
            }, 0);
          }
        });
      }];
  angular.module('User')
    .config(legalStuffConfig)
    .controller('legalStuffController', legalStuffCtrl);
}());
