// SignIn.user.js
(function () {
  'use strict';
  var LoginCtrl = [
    '$state',
    'Authentication',
    'User',
    '$ionicLoading',
    '$ionicPopup',
    '$timeout',
    '$scope',
    '$localStorage',
    '$rootScope',
    function ($state, Authentication, User, $ionicLoading, $ionicPopup, $timeout, $scope, $localStorage, $rootScope) {
      $scope.user = {};
      $scope.signIn = function () {
        $ionicLoading.show();
        User.authenticate($scope.user)
          .then(function () {// (user) {
            //console.log(user);
            $localStorage.userEmail = $scope.user.email;
            // if (!$rootScope.welcomed && !$localStorage.alreadyWelcomed) {
            //   $ionicLoading.hide();
            //   $state.go('dehuExplanation');
            // } else {
            User.getLegalStuff().then(function (legalStuff) {
              $ionicLoading.hide();
              if (legalStuff && legalStuff.length) {
                $rootScope.legalStuff = legalStuff;
                $state.go('legalStuff');
              } else {
                $state.go('app.home');
              }
            }, function () {
              $ionicLoading.hide();
              $state.go('app.home');
            });
            // }
          }, function (err) {
            if (err && err.status === 412) {
              User.retryEmail($scope.user.email).then(function () {
                $ionicLoading.hide();
                $ionicPopup.alert({
                  title: 'Verify email address',
                  template: 'An email has been sent to you--use it to verify your address before logging in.'
                });
              }, function (error) {
                $rootScope.preLoginErrorMessage('Error Verifying Email', error);
              });
            } else {
              $rootScope.preLoginErrorMessage('Error Logging In', err);
            }
          });
      };
      $ionicLoading.show();
      $scope.showPassword = false;
      // $scope.allowSkip = $localStorage.alreadyWelcomed;

      if ($localStorage.token) {
        // if ($localStorage.createUser && ($localStorage.createUser.userType === 'admin' || $localStorage.createUser.userType === 'owner')) {
        //   $state.go('initialAddUsers');
        // } else if ($localStorage.createUser || (!$rootScope.welcomed && !$localStorage.alreadyWelcomed)) {
        //   $state.go('dehuExplanation');
        // } else {
        //   $state.go('app.home');
        // }
        $state.go('app.home');
      } else if ($localStorage.createUser) {
        $state.go('signupWait');
      // } else if (!$rootScope.welcomed && !$localStorage.alreadyWelcomed) {
      //   $state.go('welcome');
      } else {
        Authentication.login().then(function () {
          $state.go('app.home');
        }, function (err) {
          $timeout(function () {
            $ionicLoading.hide();
          }, 0);
          if ($localStorage.token) {
            if (!err.status || err.status < 1) {
              $ionicPopup.alert({title: 'No internet connection', template: 'You will be able to download data over Bluetooth but not upload that data to the cloud.'});
            }
            $state.go('app.home');
          } else {
            $scope.initializationFinished = true;
          }
        });
      }
      $timeout(function () {
        $scope.tenSecondsPassed = true;
      }, 10000);
    }],
    LoginConfig = ['$stateProvider', function ($stateProvider) {
      $stateProvider
        .state('login', {
          url: '/login',
          templateUrl: 'views/SignIn/SignIn.user.html',
          controller: 'LoginController as vm'
        });
    }];

  angular.module('User')
    .config(LoginConfig)
    .controller('LoginController', LoginCtrl);
}());
