(function () {
  'use strict';
  var signupWaitCtrl = [
    '$state',
    '$ionicLoading',
    '$timeout',
    'User',
    '$scope',
    '$rootScope',
    '$localStorage',
    '$ionicPlatform',
    function ($state, $ionicLoading, $timeout, User, $scope, $rootScope, $localStorage, $ionicPlatform) {
      var cancelSignup = function () {
        delete $localStorage.createUser;
        $state.go('login');
      }, tryLogin = function () {
        var finished = false;

        if ($state.current.url === '/signupWait' && $localStorage.createUser) {
          if ($rootScope.currentSignupAttempt) {
            $timeout.cancel($rootScope.currentSignupAttempt);
          }
          $localStorage.createUser.password = $localStorage.createUser.pwd;//Diane uses different names for the same field in different endpoints, for some reason.
          User.authenticate($localStorage.createUser).then(function () {
            finished = true;
            if ($state.current.url === '/signupWait' && $localStorage.createUser) {
              $localStorage.userEmail = $localStorage.createUser.email;

              // if (!$localStorage.createUser.companyId || $localStorage.createUser.userType === 'admin' || $localStorage.createUser.userType === 'owner') {
              //   $state.go('initialAddUsers');
              // } else {
              //   $state.go('dehuExplanation');
              // }
              $state.go('app.home');
            }
          }, function () {
            finished = true;
            $rootScope.currentSignupAttempt = $timeout(function () {
              tryLogin();
            }, 2000);
          });

          if ($rootScope.currentSignupAttempt) {
            $timeout.cancel($rootScope.currentSignupAttempt);
          }
          $rootScope.currentSignupAttempt = $timeout(function () {
            if (!finished) {
              tryLogin();
            }
          }, 30000);
        }
      }, retrySignup = function () {
        User.create($localStorage.createUser).then(function (user) {
          $timeout(function () {
            $ionicLoading.hide();
          }, 100);
          $localStorage.createUser.accountId = user && user.id;
        }, function (err) {
          $rootScope.preLoginErrorMessage('Could not resend email', err);
        });
      };

      if ($localStorage.token) {
        //$state.go('dehuExplanation');
        $state.go('app.home');
      } else if (!$localStorage.createUser) {
        $state.go('login');
      }
      $scope.myEmail = $localStorage.createUser && $localStorage.createUser.email;
      $ionicLoading.hide();

      $scope.retrySignup = function () {
        $ionicLoading.show();
        if ($localStorage.createUser && $localStorage.createUser.accountId) {
          User.cancelAccount($localStorage.createUser.accountId).then(function () {
            retrySignup();
          }, function (err) {
            $rootScope.preLoginErrorMessage('Could not resend email', err);
          });
        } else {
          retrySignup();
        }
      };

      $scope.cancelSignup = function () {
        if ($localStorage.createUser && $localStorage.createUser.accountId) {
          $ionicLoading.show();
          User.cancelAccount($localStorage.createUser.accountId).then(function () {
            $timeout(function () {
              $ionicLoading.hide();
            }, 100);
            cancelSignup();
          }, function (err) {
            $rootScope.preLoginErrorMessage('Could not cancel sign-up attempt', err);
          });
        } else {
          cancelSignup();
        }
      };

      if ($rootScope.removeResumeListener) {
        $rootScope.removeResumeListener();
      }
      $rootScope.removeResumeListener = $ionicPlatform.on("resume", function () {
        tryLogin();
      });
      tryLogin();
    }],
    signupWaitConfig = ['$stateProvider', function ($stateProvider) {
      $stateProvider
        .state('signupWait', {
          url: '/signupWait',
          templateUrl: 'views/SignUp/signupWait.html',
          controller: 'signupWaitController'
        });
    }];

  angular.module('User')
    .config(signupWaitConfig)
    .controller('signupWaitController', signupWaitCtrl);
}());
