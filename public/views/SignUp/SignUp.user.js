(function () {
  'use strict';
  var SignUpCtrl = [
    '$state',
    '$ionicLoading',
    '$ionicPopup',
    '$timeout',
    'User',
    '$scope',
    '$rootScope',
    '$localStorage',
    function ($state, $ionicLoading, $ionicPopup, $timeout, User, $scope, $rootScope, $localStorage) {
      var myStringify = function (x) {
        return JSON.stringify(x).replace(/"/g, "");
      };

      if ($rootScope.createUser) {
        $rootScope.createUser.hasEmail = true;
        $rootScope.createUser.hasSMS = false;
      } else {
        $state.go('enterEmail');
      }

      $scope.signUp = function () {
        if ($rootScope.createUser.pwd !== $rootScope.createUser.confirmPwd) {
          $ionicPopup.alert({title: 'Invalid Password Entry', template: '"Confirm Password" does not match "Password"'});
        } else if (!$rootScope.createUser.phone) {
          $ionicPopup.alert({title: 'Phone Number Required', template: 'Enter a phone number to create an account'});
        } else {
          $ionicLoading.show();
          User.create($rootScope.createUser)
            .then(function (user) {
              $timeout(function () {
                $ionicLoading.hide();
              }, 100);
              //$ionicPopup.alert({title: 'Success', template: 'Your email address has been sent a verification email that will let you activate your account.'});
              //$state.go('login');
              $localStorage.createUser = $rootScope.createUser;
              $localStorage.createUser.accountId = user && user.id;
              $state.go('signupWait');
            }, function (err) {
              if (err && err.status === 409) {
                $timeout(function () {
                  $ionicLoading.hide();
                }, 100);
                $ionicPopup.alert({
                  title: 'Error Signing Up',
                  template: myStringify(err.message || (err.data && err.data.message) || err.data || err)
                }).then(function () {
                  $state.go('enterEmail');
                });
              } else {
                $rootScope.preLoginErrorMessage('Error Signing Up', err);
              }
            });
        }
      };
      $scope.showPassword = false;
    }],
    SignUpConfig = ['$stateProvider', function ($stateProvider) {
      $stateProvider
        .state('signUp', {
          url: '/signUp',
          templateUrl: 'views/SignUp/SignUp.user.html',
          controller: 'SignUpController as vm'
        });
    }];

  angular.module('User')
    .config(SignUpConfig)
    .controller('SignUpController', SignUpCtrl);
}());
