(function () {
  'use strict';
  var welcomeConfig = ['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('welcome', {
        url: '/welcome',
        templateUrl: 'views/SignUp/welcome.html',
        controller: 'welcomeController'
      });
  }],
    welcomeCtrl = ['$ionicLoading', '$rootScope',
      function ($ionicLoading, $rootScope) {
        $ionicLoading.hide();
        $rootScope.welcomed = true;
      }];
  angular.module('User')
    .config(welcomeConfig)
    .controller('welcomeController', welcomeCtrl);
}());
