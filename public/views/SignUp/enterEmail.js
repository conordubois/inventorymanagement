(function () {
  'use strict';
  var enterEmailCtrl = ['$state', '$scope', '$rootScope', '$ionicLoading', '$timeout', '$ionicPopup', '$q', 'User',
    function ($state, $scope, $rootScope, $ionicLoading, $timeout, $ionicPopup, $q, User) {
      //Authentication.logOut();

      $scope.user = {};
      $scope.enterEmail = function () {
        $ionicLoading.show();
        User.checkForInvitations($scope.user.email).then(function (invitation) {
          var invitationPromise = $q.defer();
          $rootScope.createUser = {email: $scope.user.email};

          $timeout(function () {
            $ionicLoading.hide();
          }, 100);

          if (invitation && invitation.companyId) {
            $rootScope.closePopup = $ionicPopup.confirm({
              title: "Invitation found",
              template: $scope.user.email + ' has been invited to join the company "' + invitation.companyName + '"<br><br> Do you want to accept this invitation?',
              okText: "ACCEPT",
              okType: "right-button",
              cancelText: "REJECT",
              cancelType: "left-button"
            });

            $rootScope.closePopup.then(function (res) {
              if (res) {
                $rootScope.createUser.companyId = invitation.companyId;
                $rootScope.createUser.userType = invitation.inviteeType;
              }
              invitationPromise.resolve();
            });
          } else {
            invitationPromise.resolve();
          }

          invitationPromise.promise.then(function () {
            $state.go('signUp');
          });
        }, function (err) {
          $rootScope.preLoginErrorMessage("Email address lookup failed.", err);
        });
      };
    }],
    enterEmailConfig = ['$stateProvider', function ($stateProvider) {
      $stateProvider
        .state('enterEmail', {
          url: '/enterEmail',
          templateUrl: 'views/SignUp/enterEmail.html',
          controller: 'enterEmailController as vm'
        });
    }];

  angular.module('User')
    .config(enterEmailConfig)
    .controller('enterEmailController', enterEmailCtrl);
}());
