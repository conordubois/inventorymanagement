(function () {
  'use strict';
  var dehuExplanationConfig = ['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('dehuExplanation', {
        url: '/dehuExplanation',
        templateUrl: 'views/SignUp/dehuExplanation.html',
        controller: 'dehuExplanationController'
      });
  }],
    dehuExplanationCtrl = ['$ionicLoading', 'User', '$timeout', '$scope', '$rootScope', '$state',
      function ($ionicLoading, User, $timeout, $scope, $rootScope, $state) {
        $scope.checkForLegalStuff = function () {
          User.getLegalStuff().then(function (legalStuff) {
            $timeout(function () {
              $ionicLoading.hide();
            }, 100);
            if (legalStuff && legalStuff.length) {
              $rootScope.legalStuff = legalStuff;
              $state.go('legalStuff');
            } else {
              $state.go('app.home');
            }
          }, function () {
            $timeout(function () {
              $ionicLoading.hide();
            }, 100);
            $state.go('app.home');
          });
        };

        $ionicLoading.hide();
      }];
  angular.module('User')
    .config(dehuExplanationConfig)
    .controller('dehuExplanationController', dehuExplanationCtrl);
}());
