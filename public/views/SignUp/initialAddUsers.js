(function () {
  'use strict';
  var initialAddUsersConfig = ['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('initialAddUsers', {
        url: '/initialAddUsers',
        templateUrl: 'views/SignUp/initialAddUsers.html',
        controller: 'initialAddUsersController'
      });
  }],
    initialAddUsersCtrl = ['$ionicLoading', '$scope', '$rootScope', '$ionicPopup', 'User', '$timeout',
      function ($ionicLoading, $scope, $rootScope, $ionicPopup, User, $timeout) {
        $ionicLoading.hide();

        $scope.invitations = [];
        $scope.roles = [{name: "Administrator", userType: "admin"}, {name: "Technician", userType: "fulltech"}];

        $scope.inviteEmployee = function () {
          $scope.chosen = {role: 'fulltech'};
          $rootScope.closePopup = $ionicPopup.confirm({
            title: "Invite New Users to Join your Company",
            templateUrl: 'views/Bookkeeping/sendInvite.html',
            scope: $scope,
            okText: "SEND",
            okType: "right-button",
            cancelText: "CANCEL",
            cancelType: "left-button"
          });
          $rootScope.closePopup.then(function (res) {
            if (res) {
              $ionicLoading.show();
              User.sendInvitation($scope.chosen.email, $scope.chosen.role).then(function () {
                $timeout(function () {
                  $ionicLoading.hide();
                }, 100);

                $scope.invitations.push($scope.chosen.email);
              }, function (err) {
                $rootScope.handleInternetErrorMessage("Could not send invitation.", err);
              });
            }
          });
        };
      }];
  angular.module('User')
    .config(initialAddUsersConfig)
    .controller('initialAddUsersController', initialAddUsersCtrl);
}());
