(function () {
  'use strict';
  var homeConfig = ['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('app.home', {
        url: '/dashboard',
        views: {
          main: {
            templateUrl: "views/Main/home.html",
            controller: 'HomeController'
          }
        }
      });
  }],
    homeCtrl = ['$state', '$scope', '$ionicHistory', '$ionicPopup', '$ionicPopover', 'User', 'Job', 'Dehumidifier', '$localStorage', '$ionicSideMenuDelegate', '$rootScope', '$ionicLoading', '$timeout', 'ionicDatePicker', '$filter', '$q',
      function ($state, $scope, $ionicHistory, $ionicPopup, $ionicPopover, User, Job, Dehumidifier, $localStorage, $ionicSideMenuDelegate, $rootScope, $ionicLoading, $timeout, ionicDatePicker, $filter, $q) {
        var july2017Cutoff = 1500000000,
          oct2096Cutoff = 4000000000,
          k,
          loadingMessage = function (message, cancelFunction) {
            if (!cancelFunction) {
              return {template: '<ion-spinner class="spinner-assertive huge"></ion-spinner><h3>' + message + '</h3>', delay: 100};
            }
            return {template: '<ion-spinner class="spinner-assertive huge"></ion-spinner><h3>' + message +
              '</h3><button class="button button-block button-transparent icon icon-right ion-close-circled" ng-click="' + cancelFunction + '()">Cancel</button>', delay: 100};
          },
          refreshThePage = function (inventory) {
            var i,
              milliampHoursUsed,
              countsSinceBatteryChanged,
              vibrationsSinceBatteryChanged,
              smallestDate,
              largestDate,
              aRefreshTimestamp,
              equipmentTypes = [],
              locations = {};
            $ionicLoading.hide();
            if (!inventory || !inventory.length) {
              inventory = [];
            }
            for (i = 0; i < inventory.length; i += 1) {
              if (inventory[i] && inventory[i].equipName) {
                if (!aRefreshTimestamp && inventory[i].tstampSecs) {
                  aRefreshTimestamp = new Date(inventory[i].tstampSecs * 1000);
                }
                if (inventory[i].drytagUuid) {
                  if (inventory[i].batteryStatus < 2.3) {
                    inventory[i].dryTAGbattery = "< 10%";
                  } else {
                    countsSinceBatteryChanged = inventory[i].lastSeqCount - inventory[i].newBatterySeqCount;
                    vibrationsSinceBatteryChanged = inventory[i].lastVibSeqCount - inventory[i].newBatteryVibSeqCount;
                    if (countsSinceBatteryChanged < 0) {
                      countsSinceBatteryChanged = 0;
                    }
                    if (vibrationsSinceBatteryChanged < 0) {
                      vibrationsSinceBatteryChanged = 0;
                    }
                    milliampHoursUsed = vibrationsSinceBatteryChanged * 0.000078125 + countsSinceBatteryChanged * 0.000057292;
                    if (milliampHoursUsed >= 418.5) {
                      inventory[i].dryTAGbattery = "< 10%";
                    } else if (milliampHoursUsed < 0) {
                      inventory[i].dryTAGbattery = "100%";
                    } else {
                      inventory[i].dryTAGbattery = Math.round(100 - milliampHoursUsed / 4.65) + "%";
                    }
                  }
                }
                if (inventory[i].lastScanSecs && inventory[i].lastScanSecs > july2017Cutoff) {
                  if (inventory[i].lastScanSecs > oct2096Cutoff && Date.now() < oct2096Cutoff * 1000) {
                    inventory[i].lastScanSecs = Math.round(inventory[i].lastScanSecs / 1000);
                  }
                  if (inventory[i].lastScanSecs > july2017Cutoff) {
                    if (!smallestDate || inventory[i].lastScanSecs < smallestDate) {
                      smallestDate = inventory[i].lastScanSecs;
                    }
                    if (!largestDate || inventory[i].lastScanSecs > largestDate) {
                      largestDate = inventory[i].lastScanSecs;
                    }
                  }
                }
                if (inventory[i].equipType && equipmentTypes.indexOf(inventory[i].equipType) < 0) {
                  equipmentTypes.push(inventory[i].equipType);
                }
                if (inventory[i].lastScanLocation) {
                  if (!locations[inventory[i].lastScanLocation]) {
                    locations[inventory[i].lastScanLocation] = {toggled: false};
                  }
                }
                inventory[i].isInDateRange = true;
              }
            }
            if (!aRefreshTimestamp || ((new Date()).getTime() - aRefreshTimestamp.getTime() > (1000 * 3600 * 24 * 7))) {
              //If last refresh date is more than a week old, trigger a refresh automatically
              //(since server only does the daily auto-updates for a month after user clicks the button).
              $scope.shouldRefreshFurther = true;
            }
            $scope.updatedTime = "Updated " + aRefreshTimestamp.toLocaleDateString() + " at " + aRefreshTimestamp.toLocaleTimeString();
            $scope.smallestDate = smallestDate;
            $scope.largestDate = largestDate;
            $scope.absoluteSmallestDate = $scope.smallestDate;
            $scope.absoluteLargestDate = $scope.largestDate;
            $scope.locationList = locations;
            $scope.equipmentTypeList = equipmentTypes;
            $scope.inventory = inventory;
          };

        $scope.equipmentFilter = {isInDateRange: true};
        if (!$localStorage.columnPreferences) {
          $localStorage.columnPreferences = {equipName: true, status: true, lastScanSecs: true, lastScanLocation: true, equipType: true};
        }
        $scope.columnPreferences = $localStorage.columnPreferences;
        if (!$localStorage.columnList) {
          $localStorage.columnList = [
            { displayName: 'Equipment Name',
              serverName: 'equipName',
              allowFilter: true,
              allowSort: true,
              increaseWidth: true },
            { displayName: 'Status',
              serverName: 'status',
              allowFilter: true,
              allowSort: true,
              isStatus: true,
              increaseWidth: false },
            { displayName: 'Last Read Time',
              serverName: 'lastScanSecs',
              allowFilter: false,
              allowSort: true,
              isDateRange: true,
              increaseWidth: true },
            { displayName: 'Location',
              serverName: 'lastScanLocation',
              allowFilter: true,
              allowSort: true,
              increaseWidth: false },
            { displayName: 'Location Coordinates',
              serverName: 'coordinates',
              allowFilter: false,
              allowSort: false,
              isMapLocation: true,
              increaseWidth: false },
            { displayName: 'Job',
              serverName: 'lastJobLocation',
              allowFilter: true,
              allowSort: true,
              increaseWidth: false },
            { displayName: 'Type',
              serverName: 'equipType',
              allowFilter: true,
              allowSort: true,
              increaseWidth: false },
            { displayName: 'DryTAG UUID',
              serverName: 'drytagUuid',
              allowFilter: true,
              allowSort: true,
              increaseWidth: true },
            { displayName: 'Serial Number',
              serverName: 'serialNumber',
              allowFilter: true,
              allowSort: true,
              increaseWidth: true },
            { displayName: 'Last User',
              serverName: 'lastUser',
              allowFilter: true,
              allowSort: true,
              increaseWidth: false },
            { displayName: 'Warning',
              serverName: 'warning',
              allowFilter: true,
              allowSort: true,
              increaseWidth: true }
          ];
        }
        if (!$localStorage.columnMigration || $localStorage.columnMigration < 1) {
          //The dryTAGbattery field wasn't present originally when the website was first published
          //so we need to do a migration of the data structure to make sure it updates for browsers
          //that have the old format stored.
          $localStorage.columnMigration = 1;
          $localStorage.columnList.push({ displayName: 'DryTAG Battery',
              serverName: 'dryTAGbattery',
              allowFilter: true,
              allowSort: true,
              increaseWidth: true });
        }
        if ($localStorage.columnList && $localStorage.columnList.length && (!$localStorage.columnMigration || $localStorage.columnMigration < 2)) {
          $localStorage.columnMigration = 2;
          for (k = 0; k < $localStorage.columnList.length; k += 1) {
            if ($localStorage.columnList[k].serverName === 'lastScanLocation') {
              $localStorage.columnList[k].increaseWidthModerate = true;
            }
          }
        }
        if ($localStorage.columnList && $localStorage.columnList.length && (!$localStorage.columnMigration || $localStorage.columnMigration < 3)) {
          $localStorage.columnMigration = 3;
          for (k = 0; k < $localStorage.columnList.length; k += 1) {
            if ($localStorage.columnList[k].serverName === 'lastScanLocation') {
              $localStorage.columnList[k].isLocation = true;
            }
          }
        }
        $scope.columnList = $localStorage.columnList;

        $scope.toggleCheckboxes = function () {
          var boxCheckCount = 0,
            dryTagChecked = false,
            onlyInactive = true,
            i;

          if ($scope.inventory && $scope.inventory.length) {
            for (i = 0; i < $scope.inventory.length; i += 1) {
              if ($scope.inventory[i] && $scope.inventory[i].isChecked) {
                boxCheckCount += 1;
                if ($scope.inventory[i].drytagUuid) {
                  dryTagChecked = true;
                }
                if ($scope.inventory[i].status !== 'Inactive') {
                  onlyInactive = false;
                }
              }
            }
          }
          $scope.checkingCount = boxCheckCount;
          $scope.showFloatingBottom = boxCheckCount > 0;
          $scope.canChangeBatteries = dryTagChecked;
          $scope.onlyInactiveChecked = onlyInactive;
        };

        $scope.clearCheckboxes = function () {
          var i;
          if ($scope.inventory && $scope.inventory.length) {
            for (i = 0; i < $scope.inventory.length; i += 1) {
              if ($scope.inventory[i]) {
                $scope.inventory[i].isChecked = false;
              }
            }
          }
          $scope.showFloatingBottom = false;
          $scope.canChangeBatteries = false;
          $scope.checkingCount = 0;
        };

        $scope.highlightCheckboxes = function () {
          var boxCheckCount = 0,
            dryTagChecked = false,
            onlyInactive = true,
            i;
          if ($scope.inventory && $scope.inventory.length) {
            for (i = 0; i < $scope.inventory.length; i += 1) {
              if ($scope.inventory[i]) {
                boxCheckCount += 1;
                $scope.inventory[i].isChecked = true;
                if ($scope.inventory[i].drytagUuid) {
                  dryTagChecked = true;
                }
                if ($scope.inventory[i].status !== 'Inactive') {
                  onlyInactive = false;
                }
              }
            }
          }
          $scope.checkingCount = boxCheckCount;
          $scope.showFloatingBottom = true;
          $scope.canChangeBatteries = dryTagChecked;
          $scope.onlyInactiveChecked = onlyInactive;
        };

        $scope.bulkBattery = function () {
          var i,
            initialPromise = $q.defer(),
            promises = [initialPromise.promise],
            promiseClosure = function (previousPromise, currentDevice) {
              promises.push(previousPromise.then(function () {
                return Dehumidifier.changeDryTAGBattery(currentDevice.drytagUuid);
              }, $q.reject));
            };
          $ionicLoading.show();
          if ($scope.inventory && $scope.inventory.length) {
            for (i = 0; i < $scope.inventory.length; i += 1) {
              if ($scope.inventory[i] && $scope.inventory[i].drytagUuid && $scope.inventory[i].isChecked) {
                promiseClosure(promises[promises.length - 1], $scope.inventory[i]);
              }
            }
          }
          initialPromise.resolve();
          promises[promises.length - 1].then(function () {
            if ($scope.inventory && $scope.inventory.length) {
              for (i = 0; i < $scope.inventory.length; i += 1) {
                if ($scope.inventory[i] && $scope.inventory[i].isChecked) {
                  if ($scope.inventory[i].drytagUuid) {
                    $scope.inventory[i].dryTAGbattery = "100%";
                  }
                  $scope.inventory[i].isChecked = false;
                }
              }
            }
            $scope.showFloatingBottom = false;
            $ionicPopup.alert({title: 'Battery Changed',
              template: 'The battery status of these DryTAGs have been reset to 100%. You will be warned to change their battery again once enough time passes.'});
            $timeout(function () {
              $ionicLoading.hide();
            }, 100);
          }, function (err) {
            $rootScope.handleInternetErrorMessage("Could not edit battery status", err);
          });
        };

        $scope.bulkInactive = function () {
          var i,
            initialPromise = $q.defer(),
            newStatus = $scope.onlyInactiveChecked ? 'Active' : 'Inactive',
            newDeviceStatus = newStatus === 'Active' ? 'Untagged' : newStatus,//Use untagged as a placeholder since it will be made invisible.
            promises = [initialPromise.promise],
            promiseClosure = function (previousPromise, currentDevice) {
              promises.push(previousPromise.then(function () {
                return Job.updateInventoryStatus(newStatus, currentDevice).then(function () {
                  currentDevice.status = newDeviceStatus;
                });
              }, $q.reject));
            };
          $ionicLoading.show();
          if ($scope.inventory && $scope.inventory.length) {
            for (i = 0; i < $scope.inventory.length; i += 1) {
              if ($scope.inventory[i] && $scope.inventory[i].isChecked && $scope.inventory[i].status !== newStatus) {
                promiseClosure(promises[promises.length - 1], $scope.inventory[i]);
              }
            }
          }
          initialPromise.resolve();
          promises[promises.length - 1].then(function () {
            if ($scope.inventory && $scope.inventory.length) {
              for (i = 0; i < $scope.inventory.length; i += 1) {
                if ($scope.inventory[i]) {
                  $scope.inventory[i].isChecked = false;
                }
              }
            }
            $scope.showFloatingBottom = false;
            $ionicPopup.alert({title: 'Status changed',
              template: 'These pieces of equipment have now been marked as ' + newStatus});
            $timeout(function () {
              $ionicLoading.hide();
            }, 100);
          }, function (err) {
            $rootScope.handleInternetErrorMessage("Could not update equipment status", err);
          });
        };

        $scope.resetBattery = function (aDevice) {
          if ($rootScope.popover) {
            $rootScope.popover.hide();
            $rootScope.popover = undefined;
          }

          Dehumidifier.changeDryTAGBattery(aDevice.drytagUuid).then(function () {
            aDevice.dryTAGbattery = "100%";
            $ionicPopup.alert({title: 'Battery Changed',
              template: 'The battery status of this DryTAG has been reset to 100%. You will be warned to change the battery again once enough time passes.'});
          }, function (err) {
            $rootScope.handleInternetErrorMessage("Could not edit battery status", err);
          });
        };

        $scope.setInactiveStatus = function (shouldBeInactive, aDevice) {
          if ($rootScope.popover) {
            $rootScope.popover.hide();
            $rootScope.popover = undefined;
          }

          Job.updateInventoryStatus(shouldBeInactive ? 'Inactive' : 'Active', aDevice).then(function () {
            aDevice.status = shouldBeInactive ? 'Inactive' : 'Untagged';//Use untagged as a placeholder since it will be made invisible.
          }, function (err) {
            $rootScope.handleInternetErrorMessage("Could not update equipment status", err);
          });
        };

        $scope.checkColumnPreferences = function (column) {
          if (column && column.serverName && $scope.columnPreferences[column.serverName]) {
            return true;
          }
          return false;
        };

        $scope.download = function (content, fileName) {
          //From https://stackoverflow.com/questions/14964035
          var hiddenElement = document.createElement('a');

          if (navigator.msSaveBlob) { // IE10
            navigator.msSaveBlob(new Blob([content], {
              type: 'text/csv;charset=utf-8;'
            }), fileName);
          } else if ('download' in hiddenElement) { //html5 A[download]
            hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(content);
            hiddenElement.target = '_blank';

            hiddenElement.download = fileName;
            hiddenElement.click();
          } else {
            location.href = 'data:application/octet-stream,' + encodeURIComponent(content);
          }
        };

        $scope.export = function () {
          var columns = $filter('filter')($scope.columnList, $scope.checkColumnPreferences),
            devices = $filter('filter')($scope.inventory, $scope.fullFilter),
            i,
            j,
            csvData = [[]],
            info,
            csv = '';

          if (!columns || !columns.length) {
            $ionicPopup.alert({title: 'Could not generate CSV', template: 'No data to export.'});
            return;
          }
          for (i = 0; i < columns.length; i += 1) {
            info = columns[i].displayName || '';
            info = info.replace(/"/g, '');
            csvData[0].push(('"' + info + '"') || '');
          }
          for (j = 0; j < devices.length; j += 1) {
            csvData.push([]);
            if (devices[j]) {
              for (i = 0; i < columns.length; i += 1) {
                if (columns[i].isMapLocation) {
                  csvData[csvData.length - 1].push('"' + (devices[j].latitude || 0) + ',' +  (devices[j].longitude || 0) + '"');
                } else if (columns[i].isDateRange) {
                  info = $scope.formatDate(devices[j][columns[i].serverName]);
                  if (info) {
                    csvData[csvData.length - 1].push('"' + info + '"');
                  } else {
                    csvData[csvData.length - 1].push('');
                  }
                } else {
                  info = devices[j][columns[i].serverName];
                  if (info === undefined || info === null) {
                    csvData[csvData.length - 1].push('');
                  } else {
                    info = String(info);
                    info = info.replace(/"/g, '');
                    csvData[csvData.length - 1].push('"' + info + '"');
                  }
                }
              }
            }
          }
          for (i = 0; i < csvData.length; i += 1) {
            csv += csvData[i].join(',') + '\n';
          }
          $scope.download(csv, 'inventory.csv');
        };

        $scope.chooseColumns = function ($event) {
          var popoverPromise;
          popoverPromise = $ionicPopover.fromTemplateUrl('views/Main/columnChoice.html', {scope: $scope});
          popoverPromise.then(function (popover) {
            $rootScope.popover = popover;
            $rootScope.popover.show($event);
          });
        };

        $scope.countColumns = function () {
          var i,
            count = 0;
          for (i in $scope.columnPreferences) {
            if ($scope.columnPreferences.hasOwnProperty(i) && $scope.columnPreferences) {
              count += 1;
            }
          }
          return count;
        };

        $scope.columnMoveUp = function (index) {
          var temp;
          if (index <= 0) {
            return;
          }
          temp = $scope.columnList[index - 1];
          $scope.columnList[index - 1] = $scope.columnList[index];
          $scope.columnList[index] = temp;
        };

        $scope.columnMoveDown = function (index) {
          var temp;
          if (index >= $scope.columnList.length - 1) {
            return;
          }
          temp = $scope.columnList[index + 1];
          $scope.columnList[index + 1] = $scope.columnList[index];
          $scope.columnList[index] = temp;
        };

        $scope.go = function (destination) {
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $state.go(destination);
        };

        $scope.canClearFilters = function () {
          var i;
          if ($scope.timeFiltering || $scope.tagFilter !== undefined) {
            return true;
          }
          if (!$scope.equipmentFilter) {
            return false;
          }
          for (i in $scope.equipmentFilter) {
            if ($scope.equipmentFilter.hasOwnProperty(i) && i !== 'isInDateRange' && $scope.equipmentFilter[i]) {
              return true;
            }
          }
          return false;
        };

        $scope.clearFilters = function () {
          var i;
          $scope.equipmentFilter = {isInDateRange: true};
          if ($scope.locationList) {
            for (i in $scope.locationList) {
              if ($scope.locationList.hasOwnProperty(i) && $scope.locationList[i]) {
                $scope.locationList[i].toggled = false;
              }
            }
          }
          if ($scope.inventory && $scope.inventory.length) {
            for (i = 0; i < $scope.inventory.length; i += 1) {
              if ($scope.inventory[i]) {
                $scope.inventory[i].isInDateRange = true;
              }
            }
          }
          $scope.tagFilter = undefined;
          $scope.timeFiltering = false;
          $scope.smallestDate = $scope.absoluteSmallestDate;
          $scope.largestDate = $scope.absoluteLargestDate;
          $scope.strictLocationFilter = undefined;
        };

        $scope.toggleOptions = function (value) {
          $scope.hideSideOptions = value;
        };

        $scope.toggleStatus = function (newStatus) {
          if (!$scope.equipmentFilter) {
            $scope.equipmentFilter = {isInDateRange: true};
          }
          if ($scope.equipmentFilter.status === newStatus) {
            $scope.equipmentFilter.status = '';
          } else {
            $scope.equipmentFilter.status = newStatus;
          }
        };

        $scope.toggleTagged = function (newToggling) {
          if (newToggling === $scope.tagFilter) {
            $scope.tagFilter = undefined;
          } else {
            $scope.tagFilter = newToggling;
          }
        };

        $scope.fullFilter = function (device) {
          var possibleDevice;
          if ($scope.tagFilter !== undefined) {
            if (($scope.tagFilter && !device.drytagUuid) || (!$scope.tagFilter && device.drytagUuid)) {
              return false;
            }
          }
          if ($scope.strictLocationFilter && $scope.strictLocationFilter !== device.lastScanLocation) {
            return false;
          }
          possibleDevice = $filter('filter')([device], $scope.equipmentFilter);
          if (possibleDevice && possibleDevice.length) {
            return true;
          }
          return false;
        };

        $scope.fullFilterSelect = function (property, value, reverse) {
          return function (device) {
            if (!device) {
              return false;
            }
            if (property) {
              if (reverse) {
                if (device[property]) {
                  return false;
                }
                if (value && device[property] === value) {
                  return false;
                }
              } else {
                if (!device[property]) {
                  return false;
                }
                if (value && device[property] !== value) {
                  return false;
                }
              }
            }
            return $scope.fullFilter(device);
          };
        };

        $scope.setLocationFilter = function () {
          if (!$scope.equipmentFilter) {
            $scope.equipmentFilter = {isInDateRange: true};
          }
        };

        $scope.unsetLocationFilter = function (isLocation) {
          var i;
          if (isLocation) {
            if ($scope.locationList) {
              for (i in $scope.locationList) {
                if ($scope.locationList.hasOwnProperty(i) && $scope.locationList[i]) {
                  $scope.locationList[i].toggled = false;
                }
              }
            }
            $scope.strictLocationFilter = undefined;
          }
        };

        $scope.toggleEquipmentType = function (equipmentType) {
          if (!$scope.equipmentFilter) {
            $scope.equipmentFilter = {isInDateRange: true};
          }
          if ($scope.equipmentFilter.equipType === equipmentType) {
            $scope.equipmentFilter.equipType = '';
          } else {
            $scope.equipmentFilter.equipType = equipmentType;
          }
        };

        $scope.toggleLocation = function (location) {
          var i;
          if (!$scope.equipmentFilter) {
            $scope.equipmentFilter = {isInDateRange: true};
          }
          if ($scope.locationList) {
            for (i in $scope.locationList) {
              if ($scope.locationList.hasOwnProperty(i) && $scope.locationList[i]) {
                $scope.locationList[i].toggled = false;
              }
            }
          }

          if ($scope.equipmentFilter.lastScanLocation === location) {
            $scope.equipmentFilter.lastScanLocation = '';
            $scope.strictLocationFilter = undefined;
          } else {
            $scope.equipmentFilter.lastScanLocation = location;
            $scope.strictLocationFilter = location;
            if ($scope.locationList) {
              $scope.locationList[location].toggled = true;
            }
          }
        };

        $scope.formatDate = function (timeSecs) {
          if (!timeSecs || timeSecs < july2017Cutoff) {
            return undefined;
          }

          var aDate = new Date(timeSecs * 1000),
            time = aDate.toLocaleTimeString();

          time = time.replace(/\u200E/g, '');//from https://stackoverflow.com/questions/17913681
          time = time.replace(/^([^\d]*\d{1,2}:\d{1,2}):\d{1,2}([^\d]*)$/, '$1$2');
          return aDate.toLocaleDateString() + " " + time;
        };

        $scope.formatDateRange = function () {
          if (!$scope.smallestDate || !$scope.largestDate || !($scope.smallestDate > july2017Cutoff) || !($scope.largestDate > july2017Cutoff)) {
            return undefined;
          }
          var date1 = new Date($scope.smallestDate * 1000),
            date2 = new Date($scope.largestDate * 1000);

          return date1.toLocaleDateString() + " - " + date2.toLocaleDateString();
        };

        $scope.setDateRange = function () {
          ionicDatePicker.openDoubleDatePicker({
            callback: function (newDate) {
              if (!newDate || !newDate.start || !newDate.end) {
                return;
              }
              var start = Math.round(newDate.start / 1000),
                end = Math.round(newDate.end / 1000) + 24 * 60 * 60 - 1,
                i;

              if (start < $scope.absoluteSmallestDate) {
                start = $scope.absoluteSmallestDate;
              }
              if (end > $scope.absoluteLargestDate) {
                end = $scope.absoluteLargestDate;
              }
              $scope.smallestDate = start;
              $scope.largestDate = end;
              if ($scope.inventory && $scope.inventory.length) {
                for (i = 0; i < $scope.inventory.length; i += 1) {
                  if ($scope.inventory[i]) {
                    if ($scope.inventory[i].lastScanSecs && $scope.inventory[i].lastScanSecs <= $scope.largestDate &&
                        $scope.inventory[i].lastScanSecs >= $scope.smallestDate) {
                      $scope.inventory[i].isInDateRange = true;
                    } else {
                      $scope.inventory[i].isInDateRange = false;
                    }
                  }
                }
              }
              $scope.timeFiltering = true;
            },
            from: new Date($scope.absoluteSmallestDate * 1000),
            to: new Date(),
            inputDate: new Date($scope.smallestDate * 1000),
            inputEnd: new Date($scope.largestDate * 1000)
          });
        };

        $scope.sort = function (field, isSortable) {
          var tempList = [],
            i;

          if (!$scope.inventory || !$scope.inventory.length || !isSortable) {
            return;
          }
          if ($scope.lastSortType === field) {
            for (i = $scope.inventory.length; i > 0; i -= 1) {
              tempList.push($scope.inventory[i - 1]);
            }
          } else {
            tempList = angular.copy($scope.inventory);
            tempList.sort(function (a, b) {
              var compareA, compareB;
              if (!a) {
                return -1;
              }
              if (!b) {
                return 1;
              }
              compareA = a[field];
              compareB = b[field];
              if (compareA === undefined || compareA === null) {
                compareA = "";
              }
              if (compareB === undefined || compareB === null) {
                compareB = "";
              }
              if (field === 'dryTAGbattery') {
                compareA = parseInt(compareA.replace(/%|</g, ""), 10) || 0;
                compareB = parseInt(compareB.replace(/%|</g, ""), 10) || 0;
              } else if (compareA && compareA.toLowerCase && compareB && compareB.toLowerCase) {
                compareA = compareA.toLowerCase();
                compareB = compareB.toLowerCase();
              }
              if (compareA < compareB) {
                return -1;
              }
              if (compareA > compareB) {
                return 1;
              }
              return 0;
            });
          }
          $scope.lastSortType = field;
          $scope.inventory = tempList;
        };

        if (!$rootScope.userName || !$rootScope.companyName) {
          User.get();
        }
        $ionicSideMenuDelegate.toggleLeft(false);//Close menu on start--otherwise it defaults to open sometimes.
        $localStorage.alreadyWelcomed = true;
        delete $localStorage.createUser;

        User.getLegalStuff().then(function (legalStuff) {
          if (legalStuff && legalStuff.length) {
            $rootScope.legalStuff = legalStuff;

            $rootScope.closePopup = $ionicPopup.confirm({
              title: "License Agreement",
              template: 'We have updated our license agreements. Please review and accept the policies to continue using DryLINK.',
              okText: "REVIEW",
              okType: "right-button",
              cancelText: "LOGOUT",
              cancelType: "left-button"
            });
            $rootScope.closePopup.then(function (res) {
              if (res) {
                $state.go('legalStuff');
              } else {
                $rootScope.signOut();
              }
            });
          }
        });

        $rootScope.hideLoading = function () {
          $ionicLoading.show(loadingMessage("Stopping..."));
          $timeout(function () {
            $ionicLoading.hide();
          }, 300);
        };

        $scope.refresh = function () {
          $ionicLoading.show(loadingMessage("Generating list...", "hideLoading"));
          Job.getRefreshedInventoryReport().then(refreshThePage, function (err) {
            $rootScope.handleInternetErrorMessage("Could not retrieve inventory list from server", err);
          });
        };

        $ionicLoading.show(loadingMessage("Generating list...", "hideLoading"));
        Job.getInventoryReport().then(refreshThePage, function (err) {
          $rootScope.handleInternetErrorMessage("Could not retrieve inventory list from server", err);
        }).then(function () {
          if ($scope.shouldRefreshFurther) {
            $scope.refresh();
          }
        });
      }];
  angular.module('User')
    .config(homeConfig)
    .controller('HomeController', homeCtrl);
}());
