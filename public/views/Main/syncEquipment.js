(function () {
  'use strict';
  var syncConfig = ['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('app.syncEquipment', {
        url: '/sync/:clientId/:clientName',
        views: {
          main: {
            templateUrl: "views/Main/syncEquipment.html",
            controller: 'SyncController'
          }
        }
      });
  }],
    syncCtrl = ['$scope', '$rootScope', '$state', 'User', '$ionicLoading', '$timeout', '$stateParams', '$q',
      function ($scope, $rootScope, $state, User, $ionicLoading, $timeout, $stateParams, $q) {
        $scope.syncList = {};
        $scope.headers = {};
        $scope.clientName = $stateParams.clientName;

        $scope.equipmentFilter = {};
        $scope.columnPreferences = {equipName: true, serialNumber: true, drytagUuid: true, typeName: true, trueStatus: true, manufacturer: true, model: true};
        $scope.columnList = [
          { displayName: 'Equipment Name',
            serverName: 'equipName',
            allowFilter: true,
            allowSort: true,
            increaseWidth: true },
          { displayName: 'Serial Number',
            serverName: 'serialNumber',
            allowFilter: true,
            allowSort: true,
            increaseWidth: true },
          { displayName: 'DryTAG UUID',
            serverName: 'drytagUuid',
            allowFilter: true,
            allowSort: true,
            increaseWidth: true },
          { displayName: 'Type',
            serverName: 'equipType',
            allowFilter: true,
            allowSort: true,
            increaseWidth: false },
          { displayName: 'Type',
            serverName: 'typeName',
            allowFilter: true,
            allowSort: true,
            increaseWidth: false },
          { displayName: 'Manufacturer',
            serverName: 'manufacturer',
            allowFilter: true,
            allowSort: true,
            increaseWidth: false },
          { displayName: 'Model',
            serverName: 'model',
            allowFilter: true,
            allowSort: true,
            increaseWidth: false },
          { displayName: 'Status',
            serverName: 'equipStatus',
            allowFilter: true,
            allowSort: true,
            increaseWidth: false },
          { displayName: 'Error Status',
            serverName: 'errorStatus',
            allowFilter: false,
            allowSort: false,
            increaseWidth: false },
          { displayName: 'Status',
            serverName: 'trueStatus',
            allowFilter: false,
            allowSort: false,
            isStatus: true,
            increaseWidth: false },
          { displayName: 'Sync Status',
            serverName: 'syncStatus',
            allowFilter: true,
            allowSort: true,
            increaseWidth: false }
        ];

        $scope.sortAList = function (field, list, lastSortType) {
          var tempList = [],
            i;

          if (!list || !list.length) {
            return [];
          }
          if (lastSortType === field) {
            for (i = list.length; i > 0; i -= 1) {
              tempList.push(list[i - 1]);
            }
          } else {
            tempList = angular.copy(list);
            tempList.sort(function (a, b) {
              var compareA, compareB;
              if (!a) {
                return -1;
              }
              if (!b) {
                return 1;
              }
              compareA = a[field];
              compareB = b[field];
              if (compareA === undefined || compareA === null) {
                compareA = "";
              }
              if (compareB === undefined || compareB === null) {
                compareB = "";
              }
              if (compareA && compareA.toLowerCase && compareB && compareB.toLowerCase) {
                compareA = compareA.toLowerCase();
                compareB = compareB.toLowerCase();
              }
              if (compareA < compareB) {
                return -1;
              }
              if (compareA > compareB) {
                return 1;
              }
              return 0;
            });
          }
          return tempList;
        };

        $scope.sort = function (field, isSortable, notErr, notSync) {
          if (!$scope.syncList || !isSortable) {
            return;
          }
          var errList = notErr ? $scope.syncList.errors : $scope.sortAList(field, $scope.syncList.errors, $scope.lastSortTypeErr),
            syncList = notSync ? $scope.syncList.synced : $scope.sortAList(field, $scope.syncList.synced, $scope.lastSortTypeSynced);
          if (!notErr) {
            $scope.lastSortTypeErr = field;
          }
          if (!notSync) {
            $scope.lastSortTypeSynced = field;
          }
          $scope.syncList = {errors: errList, synced: syncList};
        };

        $scope.cancel = function () {
          //$ionicHistory.goBack();
          $state.go('app.integrations');
        };

        $scope.selectAll = function (isErrList) {
          var aList = isErrList ? $scope.syncList.errors : $scope.syncList.synced,
            hadABlank = false,
            i;

          for (i = 0; i < aList.length; i += 1) {
            if (aList[i] && !aList[i].shouldSyncIt) {
              hadABlank = true;
              aList[i].shouldSyncIt = true;
            }
          }

          if (!hadABlank) {
            for (i = 0; i < aList.length; i += 1) {
              if (aList[i]) {
                aList[i].shouldSyncIt = false;
              }
            }
            if (isErrList) {
              $scope.headers.err = false;
            } else {
              $scope.headers.sync = false;
            }
          } else {
            if (isErrList) {
              $scope.headers.err = true;
            } else {
              $scope.headers.sync = true;
            }
          }
        };

        $scope.finish = function () {
          var j,
            temp,
            promises = [];
          $ionicLoading.show();
          $scope.lastSortTypeErr = undefined;
          $scope.lastSortTypeSynced = undefined;
          if ($scope.syncList) {
            if ($scope.syncList.errors && $scope.syncList.errors.length) {
              for (j = 0; j < $scope.syncList.errors.length; j += 1) {
                if ($scope.syncList.errors[j] && $scope.syncList.errors[j].shouldSyncIt) {
                  temp = $scope.syncList.errors.splice(j, 1);
                  j -= 1;
                  promises.push(User.updateSyncStatus($stateParams.clientId, temp[0]));
                }
              }
            }
            if ($scope.syncList.synced && $scope.syncList.synced.length) {
              for (j = 0; j < $scope.syncList.synced.length; j += 1) {
                if ($scope.syncList.synced[j] && $scope.syncList.synced[j].shouldSyncIt) {
                  temp = $scope.syncList.synced.splice(j, 1);
                  j -= 1;
                  promises.push(User.updateSyncStatus($stateParams.clientId, temp[0]));
                }
              }
            }
          }
          $q.all(promises).then(function (newData) {
            $timeout(function () {
              $ionicLoading.hide();
            }, 100);
            for (j = 0; j < newData.length; j += 1) {
              if (newData[j]) {
                newData[j].shouldSyncIt = true;
                if (newData[j].trueStatus) {
                  $scope.syncList.errors.push(newData[j]);
                } else {
                  $scope.syncList.synced.push(newData[j]);
                }
              }
            }
          }, function (err) {
            $rootScope.handleInternetErrorMessage("Could not sync equipment.", err);
          });
        };

        $ionicLoading.show();
        User.getSyncStatus($stateParams.clientId).then(function (syncList) {
          $scope.syncList = syncList;

          $timeout(function () {
            $ionicLoading.hide();
          }, 100);
        }, function (err) {
          $rootScope.handleInternetErrorMessage("Could not retrieve integration information from server.", err);
        });
      }];
  angular.module('User')
    .config(syncConfig)
    .controller('SyncController', syncCtrl);
}());
