(function () {
  'use strict';
  var integrationConfig = ['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('app.integrations', {
        url: '/integrations',
        views: {
          main: {
            templateUrl: "views/Main/integrations.html",
            controller: 'IntegrationController'
          }
        }
      });
  }],
    integrationCtrl = ['$scope', '$rootScope', '$ionicPopup', 'User', '$ionicLoading', '$timeout', '$ionicPopover',
      function ($scope, $rootScope, $ionicPopup, User, $ionicLoading, $timeout, $ionicPopover) {
        $scope.integrations = [];

        $scope.addIntegration = function (id, realName, isEnabled) {
          $ionicLoading.show();
          User.addIntegration(id, isEnabled !== true && isEnabled !== false).then(function (newKey) {
            var i,
              url;

            for (i = 0; i < $scope.integrations.length; i += 1) {
              if ($scope.integrations[i].id === id) {
                $scope.integrations[i].isEnabled = true;
                url = $scope.integrations[i].url;
                break;
              }
            }
            $timeout(function () {
              $ionicLoading.hide();
            }, 100);
            if (url) {
              $rootScope.openInNewWindow(url + "?apiKey=" + (newKey && newKey.message));
            } else if (newKey && newKey.message) {
              $ionicPopup.alert({title: 'Add Integration', template: 'To finish integrating with ' + realName +
                ', enter this key into your account with them: <br><br>' + (newKey && newKey.message)});
            } else {
              $ionicPopup.alert({title: 'Integration Enabled', template: realName + ' integration ready for use'});
            }
          }, function (err) {
            $rootScope.handleInternetErrorMessage("Could not add integration.", err);
          });
        };

        $scope.deleteIntegration = function (id) {
          if ($rootScope.popover) {
            $rootScope.popover.hide();
            $rootScope.popover = undefined;
          }
          $ionicLoading.show();

          User.deleteIntegration(id).then(function (status) {
            var i,
              newEnabledStatus;
            if (status && status.integrationReady) {
              newEnabledStatus = false;
            }

            for (i = 0; i < $scope.integrations.length; i += 1) {
              if ($scope.integrations[i].id === id) {
                $scope.integrations[i].isEnabled = newEnabledStatus;
                break;
              }
            }
            $timeout(function () {
              $ionicLoading.hide();
            }, 100);
          }, function (err) {
            $rootScope.handleInternetErrorMessage("Could not delete integration.", err);
          });
        };

        $scope.openMoreOptions = function ($event, anIntegration) {
          var popoverPromise;
          $scope.anIntegration = anIntegration;
          popoverPromise = $ionicPopover.fromTemplateUrl('views/Main/integrationOptions.html', {scope: $scope});
          popoverPromise.then(function (popover) {
            $rootScope.popover = popover;
            $rootScope.popover.show($event);
          });
        };

        $ionicLoading.show();
        User.getPossibleIntegrations().then(function (integrations) {
          User.getExistingIntegrations().then(function (existences) {
            var i, j;
            if (integrations && integrations.length) {
              for (j = 0; j < integrations.length; j += 1) {
                if (integrations[j]) {
                  if (integrations[j].clientName === 'RestorationMgr') {
                    integrations[j].realName = "Restoration Manager";
                    integrations[j].blurb = "Cloud-based, mobile friendly, restoration project management software " +
                      "to help your restoration business decrease cycle times and take on more jobs.";
                    integrations[j].image = "img/restoration-manager-logo.png";
                  } else {
                    integrations[j].realName = integrations[j].clientName;
                  }
                  if (existences && existences.length) {
                    for (i = 0; i < existences.length; i += 1) {
                      if (existences[i] && existences[i].clientId === integrations[j].id) {
                        integrations[j].isEnabled = existences[i].isEnabled;
                        break;
                      }
                    }
                  }
                }
              }
            }
            $scope.integrations = integrations;

            $timeout(function () {
              $ionicLoading.hide();
            }, 100);
          }, function (err) {
            $rootScope.handleInternetErrorMessage("Could not retrieve integration information from server.", err);
          });
        }, function (err) {
          $rootScope.handleInternetErrorMessage("Could not retrieve integration information from server.", err);
        });
      }];
  angular.module('User')
    .config(integrationConfig)
    .controller('IntegrationController', integrationCtrl);
}());
