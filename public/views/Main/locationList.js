(function () {
  'use strict';
  var locationListConfig = ['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('app.locationList', {
        url: '/locationList',
        views: {
          main: {
            templateUrl: "views/Main/locationList.html",
            controller: 'LocationListController'
          }
        }
      });
  }],
    locationListCtrl = ['$scope', '$rootScope', '$ionicPopup', 'Job', '$ionicLoading', '$timeout', '$ionicPopover', '$q', 'ionicDatePicker',
      function ($scope, $rootScope, $ionicPopup, Job, $ionicLoading, $timeout, $ionicPopover, $q, ionicDatePicker) {
        var july2017Cutoff = 1500000000,
          oct2096Cutoff = 4000000000,
          enableButtonTimeout,
          innerDelete = function () {
            if ($scope.aLocation.locationType === 'Gateway') {
              if (!$scope.aLocation || !$scope.aLocation.gatewayUuid) {
                return $q.reject({message: "Gateway UUID not found", status: 400});
              }
              return Job.deleteGateway($scope.aLocation.gatewayUuid);
            }
            if (!$scope.aLocation || !$scope.aLocation.invLocationId) {
              return $q.reject({message: "Location ID not found", status: 400});
            }
            return Job.deleteLocation($scope.aLocation.invLocationId);
          },
          initializeList = function () {
            Job.getInventoryLocations().then(function (locations) {
              var i,
                smallestDate,
                largestDate;

              $ionicLoading.hide();
              if (locations && locations.fixed && locations.fixed.length) {
                for (i = 0; i < locations.fixed.length; i += 1) {
                  if (locations.fixed[i]) {
                    if (!locations.fixed[i].name) {
                      locations.fixed[i].name = '';
                    }
                    if (!locations.fixed[i].address) {
                      locations.fixed[i].address = '';
                    }
                  }
                }
              }
              if (locations && locations.job && locations.job.length) {
                for (i = 0; i < locations.job.length; i += 1) {
                  if (locations.job[i]) {
                    if (!locations.job[i].name) {
                      locations.job[i].name = '';
                    }
                    if (!locations.job[i].address) {
                      locations.job[i].address = '';
                    }
                  }
                }
              }
              if (locations && locations.mobile && locations.mobile.length) {
                for (i = 0; i < locations.mobile.length; i += 1) {
                  if (locations.mobile[i]) {
                    locations.mobile[i].isInDateRange = true;
                    if (locations.mobile[i].lastReadSecs > oct2096Cutoff && Date.now() < oct2096Cutoff * 1000) {
                      locations.mobile[i].lastReadSecs = Math.round(locations.mobile[i].lastReadSecs / 1000);
                    }
                    if (locations.mobile[i].lastReadSecs > july2017Cutoff) {
                      if (!smallestDate || locations.mobile[i].lastReadSecs < smallestDate) {
                        smallestDate = locations.mobile[i].lastReadSecs;
                      }
                      if (!largestDate || locations.mobile[i].lastReadSecs > largestDate) {
                        largestDate = locations.mobile[i].lastReadSecs;
                      }
                    }
                  }
                }
              }
              $scope.smallestDate[0] = smallestDate;
              $scope.largestDate[0] = largestDate;
              $scope.absoluteSmallestDate[0] = smallestDate;
              $scope.absoluteLargestDate[0] = largestDate;

              smallestDate = undefined;
              largestDate = undefined;
              if (locations && locations.gateway && locations.gateway.length) {
                for (i = 0; i < locations.gateway.length; i += 1) {
                  if (locations.gateway[i]) {
                    locations.gateway[i].isInDateRange = true;
                    if (locations.gateway[i].lastReadSecs > oct2096Cutoff && Date.now() < oct2096Cutoff * 1000) {
                      locations.gateway[i].lastReadSecs = Math.round(locations.gateway[i].lastReadSecs / 1000);
                    }
                    if (locations.gateway[i].lastReadSecs > july2017Cutoff) {
                      if (!smallestDate || locations.gateway[i].lastReadSecs < smallestDate) {
                        smallestDate = locations.gateway[i].lastReadSecs;
                      }
                      if (!largestDate || locations.gateway[i].lastReadSecs > largestDate) {
                        largestDate = locations.gateway[i].lastReadSecs;
                      }
                    }
                  }
                }
              }
              $scope.smallestDate[1] = smallestDate;
              $scope.largestDate[1] = largestDate;
              $scope.absoluteSmallestDate[1] = smallestDate;
              $scope.absoluteLargestDate[1] = largestDate;


              console.log(locations);
              $scope.locationList = locations;
            }, function (err) {
              $rootScope.handleInternetErrorMessage("Could not retrieve inventory list from server", err);
            });
          },
          loadingMessage = function (message, cancelFunction) {
            if (!cancelFunction) {
              return {template: '<ion-spinner class="spinner-assertive huge"></ion-spinner><h3>' + message + '</h3>', delay: 100};
            }
            return {template: '<ion-spinner class="spinner-assertive huge"></ion-spinner><h3>' + message +
              '</h3><button class="button button-block button-transparent icon icon-right ion-close-circled" ng-click="' + cancelFunction + '()">Cancel</button>', delay: 100};
          };

        $scope.locationFilter = {};
        $scope.smallestDate = [];
        $scope.largestDate = [];
        $scope.absoluteSmallestDate = [];
        $scope.absoluteLargestDate = [];

        $scope.formatDate = function (timeSecs) {
          if (!timeSecs || timeSecs < july2017Cutoff) {
            return undefined;
          }

          var aDate = new Date(timeSecs * 1000),
            time = aDate.toLocaleTimeString();

          time = time.replace(/\u200E/g, '');//from https://stackoverflow.com/questions/17913681
          time = time.replace(/^([^\d]*\d{1,2}:\d{1,2}):\d{1,2}([^\d]*)$/, '$1$2');
          return aDate.toLocaleDateString() + " " + time;
        };

        $scope.formatDateRange = function (useGateways) {
          if (!useGateways) {
            useGateways = 0;
          }
          if (!$scope.smallestDate[useGateways] || !$scope.largestDate[useGateways] ||
              !($scope.smallestDate[useGateways] > july2017Cutoff) || !($scope.largestDate[useGateways] > july2017Cutoff)) {
            return undefined;
          }
          var date1 = new Date($scope.smallestDate[useGateways] * 1000),
            date2 = new Date($scope.largestDate[useGateways] * 1000);

          return date1.toLocaleDateString() + " - " + date2.toLocaleDateString();
        };

        $scope.setDateRange = function (useGateways) {
          if (!useGateways) {
            useGateways = 0;
          }
          ionicDatePicker.openDoubleDatePicker({
            callback: function (newDate) {
              if (!newDate || !newDate.start || !newDate.end) {
                return;
              }
              var start = Math.round(newDate.start / 1000),
                end = Math.round(newDate.end / 1000) + 24 * 60 * 60 - 1,
                i;

              if (start < $scope.absoluteSmallestDate[useGateways]) {
                start = $scope.absoluteSmallestDate[useGateways];
              }
              if (end > $scope.absoluteLargestDate[useGateways]) {
                end = $scope.absoluteLargestDate[useGateways];
              }
              $scope.smallestDate[useGateways] = start;
              $scope.largestDate[useGateways] = end;
              if (useGateways) {
                if ($scope.locationList && $scope.locationList.gateway && $scope.locationList.gateway.length) {
                  for (i = 0; i < $scope.locationList.gateway.length; i += 1) {
                    if ($scope.locationList.gateway[i]) {
                      if ($scope.locationList.gateway[i].lastReadSecs && $scope.locationList.gateway[i].lastReadSecs <= $scope.largestDate[useGateways] &&
                          $scope.locationList.gateway[i].lastReadSecs >= $scope.smallestDate[useGateways]) {
                        $scope.locationList.gateway[i].isInDateRange = true;
                      } else {
                        $scope.locationList.gateway[i].isInDateRange = false;
                      }
                    }
                  }
                }
              } else {
                if ($scope.locationList && $scope.locationList.mobile && $scope.locationList.mobile.length) {
                  for (i = 0; i < $scope.locationList.mobile.length; i += 1) {
                    if ($scope.locationList.mobile[i]) {
                      if ($scope.locationList.mobile[i].lastReadSecs && $scope.locationList.mobile[i].lastReadSecs <= $scope.largestDate[useGateways] &&
                          $scope.locationList.mobile[i].lastReadSecs >= $scope.smallestDate[useGateways]) {
                        $scope.locationList.mobile[i].isInDateRange = true;
                      } else {
                        $scope.locationList.mobile[i].isInDateRange = false;
                      }
                    }
                  }
                }
              }
            },
            from: new Date($scope.absoluteSmallestDate[useGateways] * 1000),
            to: new Date(),
            inputDate: new Date($scope.smallestDate[useGateways] * 1000),
            inputEnd: new Date($scope.largestDate[useGateways] * 1000)
          });
        };

        $scope.sort = function (field, sortType) {
          var tempList = [],
            i;

          if (!$scope.locationList || (!sortType && (!$scope.locationList.fixed || !$scope.locationList.fixed.length)) ||
              (sortType === 1 && (!$scope.locationList.job || !$scope.locationList.job.length)) ||
              (sortType === 2 && (!$scope.locationList.mobile || !$scope.locationList.mobile.length)) ||
              (sortType === 3 && (!$scope.locationList.gateway || !$scope.locationList.gateway.length))) {
            return;
          }
          if ((!sortType && $scope.lastSortType === field) || (sortType === 1 && $scope.lastJobSortType === field) ||
              (sortType === 2 && $scope.lastMobileSortType === field) || (sortType === 3 && $scope.lastGatewaySortType === field)) {
            if (sortType === 3) {
              for (i = $scope.locationList.gateway.length; i > 0; i -= 1) {
                tempList.push($scope.locationList.gateway[i - 1]);
              }
            } else if (sortType === 2) {
              for (i = $scope.locationList.mobile.length; i > 0; i -= 1) {
                tempList.push($scope.locationList.mobile[i - 1]);
              }
            } else if (sortType === 1) {
              for (i = $scope.locationList.job.length; i > 0; i -= 1) {
                tempList.push($scope.locationList.job[i - 1]);
              }
            } else {
              for (i = $scope.locationList.fixed.length; i > 0; i -= 1) {
                tempList.push($scope.locationList.fixed[i - 1]);
              }
            }
          } else {
            if (sortType === 3) {
              tempList = angular.copy($scope.locationList.gateway);
            } else if (sortType === 2) {
              tempList = angular.copy($scope.locationList.mobile);
            } else if (sortType === 1) {
              tempList = angular.copy($scope.locationList.job);
            } else {
              tempList = angular.copy($scope.locationList.fixed);
            }
            tempList.sort(function (a, b) {
              var compareA, compareB;
              if (!a) {
                return -1;
              }
              if (!b) {
                return 1;
              }
              compareA = a[field];
              compareB = b[field];
              if (compareA === undefined || compareA === null) {
                compareA = "";
              }
              if (compareB === undefined || compareB === null) {
                compareB = "";
              }
              if (compareA && compareA.toLowerCase && compareB && compareB.toLowerCase) {
                compareA = compareA.toLowerCase();
                compareB = compareB.toLowerCase();
              }
              if (compareA < compareB) {
                return -1;
              }
              if (compareA > compareB) {
                return 1;
              }
              return 0;
            });
          }
          if (sortType === 3) {
            $scope.lastGatewaySortType = field;
            $scope.locationList.gateway = tempList;
          } else if (sortType === 2) {
            $scope.lastMobileSortType = field;
            $scope.locationList.mobile = tempList;
          } else if (sortType === 1) {
            $scope.lastJobSortType = field;
            $scope.locationList.job = tempList;
          } else {
            $scope.lastSortType = field;
            $scope.locationList.fixed = tempList;
          }
        };

        $scope.openMoreOptions = function ($event, aLocation) {
          var popoverPromise;
          $scope.aLocation = aLocation;
          popoverPromise = $ionicPopover.fromTemplateUrl('views/Main/fixedOptions.html', {scope: $scope});
          popoverPromise.then(function (popover) {
            $rootScope.popover = popover;
            $rootScope.popover.show($event);
          });
        };

        $scope.deleteLocation = function () {
          if ($rootScope.popover) {
            $rootScope.popover.hide();
            $rootScope.popover = undefined;
          }
          $timeout(function () {
            $rootScope.closePopup = $ionicPopup.confirm({
              title: "Are you sure?",
              scope: $scope,
              template: 'Do you really want to delete this location?' + ($scope.aLocation && $scope.aLocation.locationType === 'Mobile' ?
                         ' Any phones that have the name "' + ($scope.aLocation && $scope.aLocation.name) +
                         '" would no longer be tracked on this website unless that name was manually chosen again using the DryLINK app.' : ''),
              okText: "OK",
              okType: "right-button",
              cancelText: "CANCEL",
              cancelType: "left-button"
            });
            $rootScope.closePopup.then(function (res) {
              if (res) {
                $ionicLoading.show();
                innerDelete().then(function () {
                  initializeList();
                }, function (err) {
                  $rootScope.handleInternetErrorMessage("Could not delete location", err);
                });
              }
            });
          }, 100);
        };

        $scope.addLocationOrEdit = function (isExisting) {
          if ($rootScope.popover) {
            $rootScope.popover.hide();
            $rootScope.popover = undefined;
          }
          if (isExisting) {
            $scope.newLocation = angular.copy($scope.aLocation) || {};
          } else {
            $scope.newLocation = {};
          }
          $scope.formattedAddress = undefined;
          $timeout(function () {
            $rootScope.closePopup = $ionicPopup.show({
              title: isExisting ? "Edit Location" : "New Fixed Location",
              templateUrl: 'views/Main/createLocation.html',
              scope: $scope,
              buttons: [{
                text: "CANCEL",
                type: 'left-button',
                onTap: function (e) {
                  e.preventDefault();
                  $timeout(function () {
                    if (!$rootScope.disablePopupButtons) {
                      $rootScope.closePopup.close();
                    }
                  }, 200);
                }
              }, {
                text: "SAVE",
                type: 'right-button',
                onTap: function (e) {
                  e.preventDefault();
                  $timeout(function () {
                    if (!$rootScope.disablePopupButtons) {
                      $rootScope.closePopup.close();
                      $ionicLoading.show();
                      if (isExisting && $scope.aLocation && $scope.aLocation.invLocationId) {
                        innerDelete().then(function () {
                          if ($scope.aLocation) {
                            $scope.aLocation.invLocationId = undefined;
                          }
                          Job.createLocation($scope.newLocation).then(function () {
                            initializeList();
                          }, function (err) {
                            $rootScope.handleInternetErrorMessage("Could not edit location", err);
                          });
                        }, function (err) {
                          $rootScope.handleInternetErrorMessage("Could not edit location", err);
                        });
                      } else {
                        Job.createLocation($scope.newLocation).then(function () {
                          initializeList();
                        }, function (err) {
                          $rootScope.handleInternetErrorMessage("Could not create location", err);
                        });
                      }
                    }
                  }, 200);
                }
              }]
            });
          }, 100);

          $timeout(function () {
            var addressInput,
              autocomplete;

            try {
              addressInput = document.getElementById('nj-address-field');
              if (!addressInput) {
                $rootScope.useBackUpAddresses = true;
                return;
              }
              $rootScope.useBackUpAddresses = false;
              autocomplete = new google.maps.places.Autocomplete(addressInput, {types: ['address']});
              autocomplete.setFields(['address_components', 'formatted_address']);
              autocomplete.addListener('place_changed', function () {
                if (enableButtonTimeout) {
                  $timeout.cancel(enableButtonTimeout);
                }
                $rootScope.disablePopupButtons = true;
                enableButtonTimeout = $timeout(function () {
                  $rootScope.disablePopupButtons = false;
                }, 500);
                var place = autocomplete.getPlace(),
                  i,
                  j,
                  translationTable = [{type: 'address', name: 'street_number', name2: 'route'},
                                      {type: 'city', name: 'locality'},
                                      {type: 'state', name: 'administrative_area_level_1'},
                                      {type: 'country', name: 'country'},
                                      {type: 'zip', name: 'postal_code'}];

                if (place && place.address_components && place.address_components.length) {
                  for (i = 0; i < place.address_components.length; i += 1) {
                    if (place.address_components[i] && place.address_components[i].types && place.address_components[i].short_name) {
                      for (j = 0; j < translationTable.length; j += 1) {
                        if ((translationTable[j].name && place.address_components[i].types.indexOf(translationTable[j].name) > -1) ||
                            (translationTable[j].name2 && place.address_components[i].types.indexOf(translationTable[j].name2) > -1)) {
                          if (translationTable[j].value) {
                            translationTable[j].value += " " + place.address_components[i].short_name;
                          } else {
                            translationTable[j].value = place.address_components[i].short_name;
                          }
                          break;
                        }
                      }
                    }
                  }
                  $timeout(function () {
                    for (j = 0; j < translationTable.length; j += 1) {
                      $scope.newLocation[translationTable[j].type] = translationTable[j].value;
                    }
                  }, 0);
                }
                if (place && place.formatted_address) {
                  $scope.formattedAddress = place.formatted_address;
                }
              });
            } catch (e) {
              $rootScope.useBackUpAddresses = true;
              $rootScope.reloadAutoComplete();
            }
          }, 500);
        };

        $scope.addGatewayOrEdit = function (isExisting) {
          if ($rootScope.popover) {
            $rootScope.popover.hide();
            $rootScope.popover = undefined;
          }
          if (isExisting) {
            $scope.newLocation = angular.copy($scope.aLocation) || {};
          } else {
            $scope.newLocation = {};
          }
          $scope.hideGatewayUUID = isExisting;
          $timeout(function () {
            $rootScope.closePopup = $ionicPopup.confirm({
              title: isExisting ? "Edit Gateway" : "New Gateway",
              templateUrl: 'views/Main/createGateway.html',
              scope: $scope,
              okText: "OK",
              okType: "right-button",
              cancelText: "CANCEL",
              cancelType: "left-button"
            });
            $rootScope.closePopup.then(function (res) {
              if (res) {
                $ionicLoading.show();
                Job.createOrUpdateGateway($scope.newLocation.gatewayUuid, $scope.newLocation.name, isExisting).then(function () {
                  initializeList();
                }, function (err) {
                  $rootScope.handleInternetErrorMessage(isExisting ? "Could not edit gateway" : "Could not create gateway", err);
                });
              }
            });
          }, 100);
        };

        $rootScope.hideLoading = function () {
          $ionicLoading.show(loadingMessage("Stopping..."));
          $timeout(function () {
            $ionicLoading.hide();
          }, 300);
        };

        $ionicLoading.show(loadingMessage("Generating list...", "hideLoading"));
        initializeList();
      }];
  angular.module('User')
    .config(locationListConfig)
    .controller('LocationListController', locationListCtrl);
}());
